<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use Phalcon\Di;

/**
 * RouteLoader loads a set of Phalcon Mvc\Micro\Collections from the Collections directory.
 * PHP files in the Collections directory must return Collection Objects only!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return array $collections Returns all found Routes as Collections.
 */
return call_user_func(function () {

	// Get default Dependency Injector in order to get access to Application Services.
	$di = Di::getDefault();

	// Try to retrieve Routes from APCu Cache.
	$collections = $di['apcCache']->get('routes');

	// Check if Routes were successfully loaded (if they were previously cached).
	if (empty($collections)) {
		// Generate a list of all Routes.
		$collections = [];
		// Scan for all existing Collections.
		$collectionFiles = scandir(__DIR__ . '/collections');

		// Traverse through all found Collections and generate a list of all Routes.
		foreach ($collectionFiles as $collectionFile) {
			$pathinfo = pathinfo($collectionFile);

			// Only include PHP files.
			if ($pathinfo['extension'] === 'php') {
				// Collection files return their Collection Objects, so mount them directly into the Router.
				/** @noinspection PhpIncludeInspection */
				$collections[] = require_once __DIR__ . '/collections/' . $collectionFile;
			}
		}

		// Cache generated list of Routes into APCu Cache.
		$di['apcCache']->save('routes', $collections, $di['config']->apc->routesCacheLifetime);
	}

	// Return all Routes.
	return $collections;
});
