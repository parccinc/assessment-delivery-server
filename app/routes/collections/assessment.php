<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use Phalcon\Di;
use Phalcon\Mvc\Micro\Collection;

/**
 * Collections let us define groups of Routes that will all use the same Controller. We can also set the Handler to be
 * lazy loaded. Collections can share a common Prefix.
 * This is an immediately invoked function in PHP. The return value of the anonymous function will be returned to any
 * file that "includes" it. e.g. $collection = include('example.php');
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return array $assessmentCollection Collection of routes.
 */
return call_user_func(function () {

	$assessmentCollection = new Collection();

	/**
	 * This is the Main Application Route!
	 * This is the only Route that doesn't use "api" prefix!
	 */
	$assessmentCollection->setPrefix('')
	                     // Must be a string in order to support lazy loading!
	                     ->setHandler('PARCC\ADP\Controllers\AssessmentController')
	                     ->setLazy(true);

	/**
	 * First parameter is the Route; Second parameter is the name of the Handling Method of the Controller;
	 * Third parameter is the name of the Route.
	 * Third parameter is only supported by Phalcon v2.x.x and will cause Phalcon v1.x.x to crash!!!
	 */
	/** @noinspection PhpUndefinedCallbackInspection */
	$assessmentCollection->get('/', 'loadTestDriverLaunch', 'Assessment');
	/** @noinspection PhpUndefinedCallbackInspection */
	$assessmentCollection->post('/', 'loadTestDriver', 'Assessment');

	// Get default Dependency Injector in order to get access to Application Services.
	$di = Di::getDefault();
	// Check if Debugging is enabled. These Routes should NOT be used in Production!
	if ($di['config']->app->debug === true) {
		// Add Route to Release version of Test Driver when deployed to the Server.
		if (!empty($di['config']->Assessment->releaseAssetPath)) {
			/** @noinspection PhpUndefinedCallbackInspection */
			$assessmentCollection->get('/assessment/release', 'loadTestDriverLaunch', 'Assessment');
			/** @noinspection PhpUndefinedCallbackInspection */
			$assessmentCollection->post('/assessment/release', 'loadTestDriver', 'Assessment');
		}
		// Add Route to Debug version of Test Driver when deployed on the Server.
		if (!empty($di['config']->Assessment->debugAssetPath)) {
			/** @noinspection PhpUndefinedCallbackInspection */
			$assessmentCollection->get('/assessment/debug', 'loadTestDriverLaunch', 'Assessment');
			/** @noinspection PhpUndefinedCallbackInspection */
			$assessmentCollection->post('/assessment/debug', 'loadTestDriver', 'Assessment');
		}
	}

	return $assessmentCollection;
});
