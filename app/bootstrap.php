<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use Phalcon\Di\FactoryDefault as DefaultDI;
use Phalcon\Loader;
use Phalcon\Version;

/**
 * Bootstrap initializes the Application and provides various Application Services which are all lazy loaded.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */


/**
 * Set Application Execution Start Timestamp.
 */
$startTimestamp = microtime(true);


/**
 * Make sure to validate first if the most critical environment configuration is in place!
 * The rest of the environment configuration can be checked by calling the Status Health Check web service.
 */
if (!class_exists('Phalcon\Version')) {
	// Exit in case Phalcon extension is not installed.
	exit('Phalcon extension is not installed!');
} elseif (Version::getId() < 2001040) {
	// Exit in case installed Phalcon extension is older version.
	exit('Phalcon ' . Version::get() . ' is installed, but at least 2.0.10 is required!');
} elseif (!file_exists(__DIR__ . '/config/config.php')) {
	// Exit in case Configuration file is missing.
	exit('Configuration file is missing!');
}


/**
 * By default, Namespaces are assumed to be the same as the path. This function allows us to assign Namespaces to
 * alternative folders. It also puts the classes into the PSR-0 AutoLoader.
 *
 * @var Loader $loader Automatically loads Application Classes based on their Namespaces.
 */
$loader = new Loader();
$loader->registerNamespaces([
	'PARCC\ADP\Controllers' => __DIR__ . '/controllers',
	'PARCC\ADP\Exceptions' => __DIR__ . '/exceptions',
	'PARCC\ADP\Models' => __DIR__ . '/models',
	'PARCC\ADP\Responses' => __DIR__ . '/responses',
	'PARCC\ADP\Services' => __DIR__ . '/services',
	'PARCC\ADP\Services\AWS' => __DIR__ . '/services/aws',
	'PARCC\ADP\Services\JSV' => __DIR__ . '/services/jsv',
	'PARCC\ADP\Services\Logger' => __DIR__ . '/services/logger'
])->register();


/**
 * The DI is the Direct Injector. It will store pointers to all web services and will be inserted into all Controllers.
 *
 * @var DefaultDI $di Default Dependency Injector.
 */
$di = new DefaultDI();


/**
 * Application Start Timestamp Service.
 *
 * Set Application Execution Start Timestamp as DI's Service, so it can be used by executionTime Bootstrap Service
 * during Application Shutdown.
 *
 * @return float Returns Unix Timestamp as a float in seconds.
 */
$di->setShared('startTimestamp', function () use ($startTimestamp) {

	// Return Application Start Timestamp.
	return $startTimestamp;
})->resolve();


/**
 * Application APCu Caching Service.
 *
 * This is Application Caching inside APCu (Alternative PHP Cache User). It stores all data frequently used by
 * Application in Memory which improves overall Application performance, so it doesn't have to be accessed/generated
 * each time it's needed. This includes Application Configuration, API Routes, Users, Test Driver Configuration etc.
 * All data is by default permanently cached, so in order to re-cache it, either Web Server has to be restarted or data
 * has to be flushed manually!
 *
 * @return Phalcon\Cache\Backend\Apc Application Data frequently being used.
 */
$di->setShared('apcCache', function () {

	return require_once 'core/bootstrap/apcCache.php';
});


/**
 * Application Configuration Service.
 *
 * Returns Application Configuration. It also caches it inside APCu Cache. If not already cached, it reads Configuration
 * from the Config file.
 *
 * @return Phalcon\Config Application Configuration.
 */
$di->setShared('config', function () use ($di) {

	return require_once 'core/bootstrap/config.php';
});


/**
 * Router Collections Service.
 *
 * Returns array of all Collections, which define a group of Routes (from "routes/collections"). These will be mounted
 * into the Application itself later.
 *
 * @return array $collections Returns all found Routes as Collections.
 */
$di->setShared('collections', function () {

	return require_once __DIR__ . '/routes/routeLoader.php';
});


/**
 * Application View Service.
 *
 * Returns View (PHTML Template) of the Application. Micro Applications in general don't use views, so this is an
 * exception and is only used for launching of Test Driver and protection from bots.
 * DI's setShared method provides a singleton instance. If the second parameter is a function, then the Service is
 * lazy-loaded on its first instantiation.
 *
 * @return Phalcon\Mvc\View\Simple $view View (PHTML Template).
 */
$di->setShared('view', function () {

	return require_once 'core/bootstrap/view.php';
});


/**
 * Database Profiler Service.
 *
 * Configures Database Profiler.
 *
 * @return Phalcon\Db\Profiler Database Tables Metadata (only the ones used by Models).
 */
$di->setShared('dbProfiler', function () {

	return require_once 'core/bootstrap/dbProfiler.php';
});


/**
 * Database Profiler Logger Service.
 *
 * Generates Log Entries for each executed SQL Query being profiled. It includes the SQL Query itself, Start Time, End
 * Time and Total Execution Time for each executed SQL Query during processing of a Request.
 * This should only be used for Debugging and should NOT be used in Production!
 *
 * @return string Database Profiler Log Entries for all SQL Queries that were executed.
 */
$di->setShared('dbProfilerLog', function () use ($di) {

	return require_once 'core/bootstrap/dbProfilerLog.php';
});


/**
 * Database Models Metadata Caching Service.
 *
 * Configure Database Metadata Caching inside APCu Cache. This ensures that Phalcon queries Metadata of all tables
 * linked to its Models only once and caches it inside PHP Op-Cache instead of querying it each time a Model is required
 * to be accessed in Database.
 *
 * @return Phalcon\Mvc\Model\MetaData\Apc Database Tables Metadata (only the ones used by Models).
 */
$di->setShared('modelsMetadata', function () use ($di) {

	return require_once 'core/bootstrap/modelsMetadata.php';
});


/**
 * Default Database Service.
 *
 * Detects Default Database Connection Service. If Test Publishing is enabled, regardless if Test Delivery is enabled or
 * not, Application will always use Secondary Database Connection for User Authentication and Logging, and for
 * processing Request Queue. In any other case, only the Primary Database Connection will be used!
 *
 * @return string {'db'|'db2'} Returns Database Connection Service which should be used by default for the Application
 *                             Core functionality.
 */
$di->setShared('defaultDb', function () use ($di) {

	return require_once 'core/bootstrap/defaultDb.php';
});


/**
 * Primary Database Connection Service.
 *
 * This is Test Delivery Database and is exclusively used for Test Delivery. If Application is exclusively used for Test
 * Delivery this is the only Database Connection Service it will use - it will use Users from this Database and will
 * save Logs into this Database.
 *
 * @return Phalcon\Db\Adapter\Pdo\Mysql $mysqlConnection Database Connection with enabled Profiling and Logging.
 */
$di->setShared('db', function () use ($di) {

	return require_once 'core/bootstrap/db.php';
});


/**
 * Secondary Database Connection Service.
 *
 * This is Test Delivery Database and is exclusively used for Test Publishing. If Application is exclusively used for
 * Test Publishing, or is used for both Test Delivery and Test Publishing this will be the primary Database Connection
 * Service it will use whereas "db" Database Connection Service will be used as the secondary Database Connection
 * Service - it will use Users from this Database, will save Logs into this Database, will use this Database for saving
 * and processing Request Queue, but will use "db" Database Connection Service for everything else (Student, System,
 * Test, Test Content, Test Form, Test Form Revision, Test Results and Test Session), even if Application is used both
 * for Test Delivery and Test Publishing!
 *
 * @return Phalcon\Db\Adapter\Pdo\Mysql $mysqlConnection Database Connection with enabled Profiling and Logging.
 */
$di->setShared('db2', function () use ($di) {

	return require_once 'core/bootstrap/db2.php';
});


/**
 * AWS S3 API Service.
 *
 * It communicates with AWS S3 API in order to download and upload files from/to AWS S3 Bucket.
 *
 * @return PARCC\ADP\Services\AWS\S3 AWS S3 Service with Authentication.
 */
$di->setShared('s3', function () use ($di) {

	return require_once 'core/bootstrap/s3.php';
});


/**
 * JSV (JSON Schema Validator) Service.
 *
 * It is used to validate JSON against given JSON Schema.
 *
 * @return PARCC\ADP\Services\JSV\JSV JSON Schema Validator Service.
 */
$di->setShared('jsv', function () {

	return require_once 'core/bootstrap/jsv.php';
});


/**
 * Application Logger Service.
 *
 * Configure Application Multi-Logger and its formatting. It can log to multiple Loggers concurrently and supports the
 * following Logger Adapters: File Logger, Database Logger and Socket Logger.
 * The following Log Types (Severity Levels) are supported:
 * DEBUG (default), INFO, NOTICE, WARNING, ERROR, ALERT, CRITICAL and EMERGENCY.
 * CUSTOM (LogLevel = 8) and SPECIAL (LogLevel = 9) are reserved, but are not implemented!
 *
 * @return PARCC\ADP\Services\Logger\Multiple $multipleLogger Application Multi-Logger.
 */
$di->setShared('logger', function () use ($di) {

	return require_once 'core/bootstrap/logger.php';
});


/**
 * HTTP Client Service.
 *
 * This is simple HTTP Client based on CURL used to make REST API Requests.
 *
 * @return PARCC\ADP\Services\HttpClient HTTP Client.
 */
$di->setShared('httpClient', function () {

	return require_once 'core/bootstrap/httpClient.php';
});


/**
 * CORS Policies Setter Service.
 *
 * This is the default setter for CORS (Cross-Origin Resource Sharing) Policies. Additional Policies that are unique to
 * specific Web Service may be set by its Controller.
 * It is important to set CORS Policies as soon as possible, and always have them in place, even when no Route was
 * matched the Request in order to be able to return proper 404 Responses or reports about Errors and Exceptions!
 */
$di->setShared('setCors', function () use ($di) {

	return require_once 'core/bootstrap/setCors.php';
});


/**
 * Tenant Detection Service.
 *
 * Identifies Current Tenant based on the requested URL and returns Tenant details.
 * If it cannot identify Current Tenant, then it returns the Default Tenant details.
 *
 * @return PARCC\ADP\Models\Tenant Tenant details.
 */
$di->setShared('setTenant', function () use ($di) {

	return require_once 'core/bootstrap/tenant.php';
});


/**
 * Client IP Address Detection Service.
 *
 * Retrieves Client's IP Address. It also supports forwarded Client's IP Address from AWS ELB (Elastic Loaded Balancer)
 * in which case $_SERVER['REMOTE_ADDR'] contains the IP Address of ELB instead of actual Client's IP Address!
 *
 * @return string|null Client's IP Address, OR NULL if failed to be determined.
 */
$di->setShared('clientIpAddress', function () use ($di) {

	return require_once 'core/bootstrap/clientIpAddress.php';
});


/**
 * HTTPS Request Detection Service.
 *
 * Checks if Request was made over HTTPS Protocol. It also supports forwarded originating Protocol from AWS ELB (Elastic
 * Loaded Balancer) in which case $_SERVER['HTTPS'] returns FALSE as Requests are always forwarded over HTTP from ELB,
 * regardless if originating Request came over HTTP or HTTPS Protocol (due to ELB design/limitation).
 *
 * @return boolean Returns TRUE if Originating Request was made over HTTPS Protocol. Otherwise, it returns FALSE.
 */
$di->setShared('useHttps', function () use ($di) {

	return require_once 'core/bootstrap/useHttps.php';
});


/**
 * Application Execution Time Service.
 *
 * Calculates Application Execution Time. Only used for debugging purposes.
 *
 * @return string|null Application Execution Time in milliseconds, if Debugging is enabled.
 */
$di->setShared('executionTime', function () use ($di) {

	return require_once 'core/bootstrap/executionTime.php';
});


/**
 * Application Memory Usage Service.
 *
 * Calculates Memory Usage and Peak Memory Usage, and converts them into a human readable format. Only used for
 * debugging purposes.
 *
 * @return string|null Amount of Memory and Peak Memory allocated to PHP, if Debugging is enabled.
 */
$di->setShared('memoryUsage', function () use ($di) {

	return require_once 'core/bootstrap/memoryUsage.php';
});
