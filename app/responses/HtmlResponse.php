<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Responses;

use Phalcon\Di;

/**
 * Class HtmlResponse
 *
 * This is the HTML Response. It is used only when a Request comes directly to the (sub)domain name (document root) or
 * as an Invalid Request to the Test Driver which is not supposed to happen for regular Users. In most cases, these
 * Requests would come from search engine spider robots, from random visitors, or from potentially malicious attackers.
 * In any of these cases we should return an usual HTML HTTP Error Pages.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class HtmlResponse extends BaseResponse
{

	/**
	 * Sends HTML Response.
	 *
	 * @param array   $records Array of records to be returned, OR an empty array if no records are found.
	 *                         It can also hold be an Error if an Exception was thrown while trying to retrieve records.
	 * @param boolean $isError Indicates if given array contains an Error details instead of actual Data.
	 *                         Errors come from HttpExceptions.
	 */
	public function respond($records, $isError = false)
	{
		// Get default Dependency Injector in order to get access to Application Services.
		$di = Di::getDefault();

		// Set the Status and Status Description in the Header.
		if ($isError && $this->useHeaderStatus) {
			// Set the Error Status and Error Status Description if necessary, based on the Application Configuration.
			$this->setStatusCode($records['statusCode'], $records['statusDescription']);
			// Set the Error Code, Error Description and Time if necessary, based on the Application Configuration.
			// Besides Redirect Response, this is the only exception when Error Code and Error Description are directly
			// returned in the Response Header as Response Body doesn't contain Error Code.
			$this->setHeader('X-Error-Code', $records['errorCode']);
			$this->setHeader('X-Error-Description', $records['errorDescription']);
			$this->setHeader('X-Time', $records['time']);
		} else {
			// Set the default Status and Status Description.
			$this->setStatusCode('200', 'OK');
		}

		// Set default output Content Type.
		$this->setContentType('text/html');

		// Calculate ETag, if necessary.
		if ($this->useETag) {
			$this->setEtag(md5(filemtime(dirname(__DIR__) . '/views/error.phtml')));
		}

		// Send all Response Headers first.
		$this->sendHeaders();

		// Check if Response should be logged.
		if ($di['config']->logger->logResponse === true) {
			// Check if Response is an Error (Exception Handler is logging all Errors).
			if (!$isError) {
				// Log the Response.
				$di['logger']->debug("HTML Response:\n" . json_encode($records));
			}
		}

		// HEAD Requests are detected in the parent Constructor.
		// HEAD Response does everything exactly the same as GET Response, but contains no Body.
		if ($this->isHeadRequest) {
			// Send the Response.
			$this->send();

		} else {
			// Try to retrieve Current Tenant.
			$tenantId = null;
			if ($di->has('tenant')) {
				// Check if Current Tenant exists.
				if (!empty($di['tenant']->tenantId)) {
					// Pass Current Tenant ID to reference which favicon.ico should be used by View template.
					$tenantId = $di['tenant']->tenantId;
				}
			}

			// Set the Response View.
			$this->setContent(
				$di['view']->render(
					'error',
					[
						'message' => ($records['statusCode'] !== 200 ?
						             ($records['statusCode'] . ' ' . $records['statusDescription']) : ''),
						'description' => $records['errorDescription'],
						'tenantId' => $tenantId
					]
				)
			);

			// Send the Response.
			$this->send();
		}
	}
}
