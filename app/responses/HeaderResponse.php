<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Responses;

/**
 * Class HeaderResponse
 *
 * This is the Header Response. It only sends Response Header, but completely omits the Response Body. It is mostly used
 * when a 404 Response is being sent as a result of an API Request (i.e. an invalid content resource was requested).
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class HeaderResponse extends BaseResponse
{

	/**
	 * Sends HTML Response.
	 *
	 * @param array   $records Array of records to be returned, OR an empty array if no records are found.
	 *                         It can also hold be an Error if an Exception was thrown while trying to retrieve records.
	 * @param boolean $isError Indicates if given array contains an Error details instead of actual Data.
	 *                         Errors come from HttpExceptions.
	 */
	public function respond($records, $isError = false)
	{
		// Set the Status and Status Description in the Header.
		if ($isError && $this->useHeaderStatus) {
			// Set the Error Status and Error Status Description if necessary, based on the Application Configuration.
			$this->setStatusCode($records['statusCode'], $records['statusDescription']);
			// Set the Error Code, Error Description and Time if necessary, based on the Application Configuration.
			// Besides Redirect Response, this is the only exception when Error Code and Error Description are directly
			// returned in the Response Header as Response Body is omitted.
			$this->setHeader('X-Error-Code', $records['errorCode']);
			$this->setHeader('X-Error-Description', $records['errorDescription']);
			$this->setHeader('X-Time', $records['time']);
		} else {
			// Set the default Status and Status Description.
			$this->setStatusCode('200', 'OK');
		}

		// Set default output Content Type.
		$this->setContentType('text/html');

		// Calculate ETag, if necessary.
		if ($this->useETag) {
			$this->setEtag(md5(mt_rand()));
		}

		// Check if Response should be logged.
		if ($this->getDI()->get('config')->logger->logResponse === true) {
			// Check if Response is an Error (Exception Handler is logging all Errors).
			if (!$isError) {
				// Log the Response.
				$this->getDI()->get('logger')->debug("Header Response:\n" . json_encode($records));
			}
		}

		// Send the Response.
		$this->send();
	}
}
