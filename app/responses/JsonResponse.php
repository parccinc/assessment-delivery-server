<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Responses;

use stdClass;

/**
 * Class JsonResponse
 *
 * This is the JSON Response. It is the default Response Type and it supports optional Envelope which is configurable as
 * one of Application parameters within the Configuration.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class JsonResponse extends BaseResponse
{

	/**
	 * Sends JSON Response.
	 *
	 * @param array   $records Array of records to be returned, OR an empty array if no records are found.
	 *                         It can also hold be an Error if an Exception was thrown while trying to retrieve records.
	 * @param boolean $isError Indicates if given array contains an Error details instead of actual Data.
	 *                         Errors come from HttpExceptions.
	 */
	public function respond($records, $isError = false)
	{
		// Set the Status and Status Description in the Header.
		if ($isError && $this->useHeaderStatus) {
			// Set the Error Status and Error Status Description if necessary, based on the Application Configuration.
			$this->setStatusCode($records['statusCode'], $records['statusDescription']);
		} else {
			// Set the default Status and Status Description.
			$this->setStatusCode('200', 'OK');
		}

		// Determine correct Response Status.
		$success = ($isError) ? 'ERROR' : 'SUCCESS';

		// Check if Envelope needs to be generated based on the Application Configuration.
		if ($this->getDI()->get('config')->app->useEnvelope === true) {
			// Provide an Envelope for JSON Responses. Put Envelope details into 'meta' and Data records into 'result'.
			$result = [];
			$result['meta'] = [
				'status' => $success,
				'count' => count($records)
			];

			// Handle 0 record Responses, OR assign found records.
			if ($result['meta']['count'] === 0) {
				// This is required to make the JSON Response return an empty JavaScript Object.
				// Without this, JSON returns an empty array: [] instead of {}, but this should never happen !!!
				$result['result'] = new stdClass();
			} else {
				$result['result'] = $records;
			}

		} else {
			// In case Envelope is disabled, return only the results.
			$result = $records;
		}

		// Calculate ETag, if necessary.
		if ($this->useETag) {
			$this->setEtag(md5(serialize($result)));
		}

		// HEAD Requests are detected in the parent Constructor.
		// HEAD Response does everything exactly the same as GET Response, but contains no Body.
		if (!$this->isHeadRequest) {
			$this->setContentType('application/json');
			$this->setJsonContent($result);
		}

		// Check if Response should be logged.
		if ($this->getDI()->get('config')->logger->logResponse === true) {
			// Check if Response is an Error (Exception Handler is logging all Errors).
			if (!$isError) {
				// Include Request Body only if smaller than configured maximum size!
				// Test Results for example can be enormous and would consume a lot of memory and cause Logs to be huge.
				if (!isset($this->getContent()[$this->getDI()->get('config')->logger->logResponseMaxSize])) {
					$this->getDI()->get('logger')->debug("JSON Response:\n" . $this->getContent());
				} else {
					$this->getDI()->get('logger')->debug("JSON Response:\n*** Size Limit Exceeded ***");
				}
			}
		}

		// Send the Response.
		$this->send();
	}
}
