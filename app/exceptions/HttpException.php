<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Exceptions;

use Phalcon\Di;
use Phalcon\Exception;

/**
 * Class HttpException
 *
 * This is the Base HTTP Exception Handler. It handles all Exceptions triggered by the Application.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class HttpException extends Exception
{

	/**
	 * @var array $statusCodes List of all HTTP Status Codes.
	 */
	protected $statusCodes = [
		// Informational 1xx
		100 => 'Continue',
		101 => 'Switching Protocols',

		// Success 2xx
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		203 => 'Non-Authoritative Information',
		204 => 'No Content',
		205 => 'Reset Content',
		206 => 'Partial Content',

		// Redirection 3xx
		300 => 'Multiple Choices',
		301 => 'Moved Permanently',
		302 => 'Found', // 1.1
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		// 306 is deprecated but reserved
		307 => 'Temporary Redirect',

		// Client Error 4xx
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Request Entity Too Large',
		414 => 'Request-URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Requested Range Not Satisfiable',
		417 => 'Expectation Failed',

		// Server Error 5xx
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported',
		509 => 'Bandwidth Limit Exceeded'
	];

	/**
	 * @var array $error Error with all Exception details: Status Code, Status Description, Error Code, Error
	 *                   Description and current Server Time (UTC Timestamp).
	 */
	public $error;

	/**
	 * @var string $internalError Internal Error Message used only for Application Logging.
	 */
	public $internalError;


	/**
	 * Constructor.
	 *
	 * It generates Response in predefined format (Header, HTML, Redirect or JSON by default), includes given
	 * Application Error Code and Error Description accompanied by well-known HTTP Status and Status Code. By default,
	 * it also returns HTTP Status, Status Code and current Server Time as a Unix Timestamp in the Response Header
	 * unless suppressed.
	 *
	 * @param string  $errorCode        Application Error Code.
	 * @param integer $statusCode       Status Code of the Response.
	 * @param string  $errorDescription Application Error Description.
	 * @param string  $internalError    Application Internal Error Description.
	 * @param string  $responseType     The type of the Exception Response
	 *                                  {HeaderResponse|HtmlResponse|JsonResponse|RedirectResponse}
	 *                                  (@default='PARCC\ADP\Responses\JsonResponse').
	 * @param boolean $throwException   Indicator if Exception should immediately be sent, or can be overridden later
	 *                                  (@default=true).
	 */
	public function __construct(
		$errorCode,
		$statusCode,
		$errorDescription,
		$internalError = '',
		$responseType = 'PARCC\ADP\Responses\JsonResponse',
		$throwException = true
	) {

		// Get default Dependency Injector in order to get access to Application Services.
		$di = Di::getDefault();

		// Check if Tenant Exception has already been triggered.
		if ($di->get('setTenant') !== 1 && $throwException === true) {

			// Retrieve Tenant Exception details.
			/** @var HttpException $tenantError */
			$tenantError = $di->get('setTenant');

			// Override Exception details with Tenant Exception details.
			$this->code = $tenantError->code;
			$this->file = $tenantError->file;
			$this->line = $tenantError->line;
			$this->message = $tenantError->message;

			// Override Exception Error details with Tenant Exception Error details.
			$errorCode = $tenantError->error['errorCode'];
			$statusCode = $tenantError->error['statusCode'];
			$errorDescription = $tenantError->error['errorDescription'];
			$internalError = $tenantError->internalError;
		}


		// Set the Exception Error details.
		$this->error = [
			'statusCode' => $statusCode,
			'statusDescription' => (isset($this->statusCodes[$statusCode]) ?
			                        $this->statusCodes[$statusCode] : 'Unknown Status Code'),
			'errorCode' => $errorCode,
			'errorDescription' => $errorDescription,
			'time' => time()
		];
		$this->internalError = $internalError;

		// Retrieve existing Response Headers.
		$responseHeaders = $di->get('response')->getHeaders();

		// Set the Exception Response Type and restore existing Response Headers.
		$di->setShared('response', $responseType);
		$di->get('response')->setHeaders($responseHeaders);

		// Make sure to clear any previously generated Content.
		$di->get('response')->setContent(null);


		// Check if Exception Response should be sent immediately.
		if ($throwException === true) {

			// Send Exception Response.
			$di->get('response')->respond($this->error, true);
		}
	}
}
