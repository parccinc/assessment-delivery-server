<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use Phalcon\Config;

/**
 * Application Settings for particular environment.
 *
 * Supported Log Levels:
 * 0: EMERGENCY
 * 1: CRITICAL
 * 2: ALERT
 * 3: ERROR
 * 4: WARNING
 * 5: NOTICE
 * 6: INFO
 * 7: DEBUG
 * 8: CUSTOM
 * 9: SPECIAL (Default)
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
return new Config([
	// General Application Configuration.
	'app' => [
		// Path to the Controllers folder, relative to the Application root. Do NOT change!
		'controllersDir'          => '/app/controllers/',
		// Path to the Models folder, relative to the Application root. Do NOT change!
		'modelsDir'               => '/app/models/',
		// Path to the Views folder, relative to the Application root. Do NOT change!
		'viewsDir'                => '/app/views/',
		// Application Instance Identifier (@default=1).
		'applicationId'           => 1,
		// Flag if Application is allowed to operate in Test Delivery mode (@default=false).
		'allowTestDelivery'       => false,
		// Flag if Application is allowed to operate in Test Publishing mode (@default=false).
		'allowTestPublishing'     => false,
		// Flag if Envelope should be used in JSON Responses (@default=false).
		'useEnvelope'             => false,
		// Flag if Status Code and Status Description should be included in the Response Header for all Response Types
		// (@default=true).
		'useHeaderStatus'         => true,
		// Flag if ETags should be used as hash for the Responses (@default=false).
		'useETag'                 => false,
		// Duration in seconds for how long HMAC generated Authentication Hash will be considered as valid. However, the
		// Timestamp when it was generated is on the Client side and its Clock may be out of sync with Application
		// Clock! Network Lag and Processing Time should also be considered, so this value should not be too low
		// (@default=300).
		'hmacTimeTolerance'       => 300,
		// Reserved Prefix for Test Keys used by Practice Test Battery Form Revisions. These Tests are being handled
		// completely differently both by Test Publishing and Test Delivery. It can contain only uppercase alphanumeric
		// characters! Minimum length is 1 character, it must start with "P" and maximum length is 10 characters, but it
		// is not recommended to go above 5.
		// If changed, related configuration must be updated on LDR side which excludes all Test Keys with this Prefix!
		// (@default='PRACT').
		'practiceTestKeyPrefix'   => 'PRACT',
		// Flag if Maintenance Mode should be enabled. Once enabled, only access to Ping web service is permitted to
		// allow Health Check required for Load Balancer (@default=false).
		'enableMaintenanceMode'   => false,
		// Flag to turn On/Off Application Debugging/Profiling (@default=false).
		'debug'                   => false
	],
	// Primary Database Configuration.
	'database' => [
		// Database Adapter/Driver {MySQL|PostgreSQL|SQLite|Oracle} (@default='MySQL').
		'adapter'                 => 'MySQL',
		// Database Hostname.
		'host'                    => '',
		// Database Username.
		'username'                => '',
		// Database Password.
		'password'                => '',
		// Database Name.
		'dbname'                  => '',
		// Default Character Set used for every Database Connection. It also enforces use of default Collation for
		// selected Character Set. Default Collation for 'utf8' Character Set is 'utf8_general_ci'! (@default='utf8').
		'charset'                 => 'utf8',
		// Switch for using Persistent Connections to Database (@default=true).
		'persistent'              => true,
		// Flag to turn On/Off Database Logging, so all SQL Queries being executed are saved into a Log File on the
		// configured "logPath", and is only available if Debugging is enabled! (@default=false).
		'useSqlLogging'           => false,
		// Path to the Database Log, relative to the Application root. This is used only when Debugging is turned On and
		// contains all SQL Queries being executed by the Application. "{date}" will be replaced with the Current Date.
		// (@default='/logs/db_{date}.log')
		'sqlLogPath'              => '/logs/db_{date}.log'
	],
	// Secondary Database Configuration.
	'database2' => [
		// Database Adapter/Driver {MySQL|PostgreSQL|SQLite|Oracle} (@default='MySQL').
		'adapter'                 => 'MySQL',
		// Database Hostname.
		'host'                    => '',
		// Database Username.
		'username'                => '',
		// Database Password.
		'password'                => '',
		// Database Name.
		'dbname'                  => '',
		// Default Character Set used for every Database Connection. It also enforces use of default Collation for
		// selected Character Set. Default Collation for 'utf8' Character Set is 'utf8_general_ci'! (@default='utf8').
		'charset'                 => 'utf8',
		// Switch for using Persistent Connections to Database (@default=true).
		'persistent'              => true,
		// Flag to turn On/Off Database Logging, so all SQL Queries being executed are saved into a Log File on the
		// configured "logPath", and is only available if Debugging is enabled! (@default=false).
		'useSqlLogging'           => false,
		// Path to the Database Log, relative to the Application root. This is used only when Debugging is turned On and
		// contains all SQL Queries being executed by the Application. "{date}" will be replaced with the Current Date.
		// (@default='/logs/db2_{date}.log')
		'sqlLogPath'              => '/logs/db2_{date}.log'
	],
	// APCu Configuration.
	'apc' => [
		// Duration in seconds for how long Application Configuration and System Configuration should be cached before
		// Application reloads the Config file and System Table from the Database, and updates the Cache (0 means
		// forever in which case either a Server Restart is needed, OR APCu Cache has to be flushed manually!)
		// (@default=0).
		'configCacheLifetime'     => 0,
		// Duration in seconds for how long API Routes should be cached before Application re-scans all available Routes
		// and updates the Routes Cache (0 means forever in which case either a Server Restart is needed, OR APCu Cache
		// has to be flushed manually!) (@default=0).
		'routesCacheLifetime'     => 0,
		// Duration in seconds for how long Database Schema should be cached before Application re-scans the Database
		// and updates the Model Cache (0 means forever in which case either a Server Restart is needed, OR APCu Cache
		// has to be flushed manually!) (@default=0).
		'dbSchemaCacheLifetime'   => 0,
		// Duration in seconds for how long AWS IAM Role should be cached before Application retries to retrieve it from
		// AWS Instance Metadata and User Data Service (0 means forever in which case either a Server Restart is needed,
		// or APCu Cache has to be flushed manually!) (@default=0).
		'iamRoleCacheLifetime'    => 0
	],
	// AWS S3 and CloudFront Configuration.
	's3' => [
		// AWS S3 Hostname (without "http://" or "https://" scheme and trailing slash!) (@default='s3.amazonaws.com').
		'host'                    => 's3.amazonaws.com',
		// AWS STS (Security Token Service) Hostname (without trailing slash!). Currently, AWS STS uses only one IP
		// Address, doesn't provide Hostname and is only available over HTTP (@default='http://169.254.169.254').
		'stsHost'                 => 'http://169.254.169.254',
		// Flag if AWS S3 should be accessed using HTTPS Protocol (@default=true).
		'useHttps'                => true,
		// AWS S3 Access Key ID (should be empty if AWS IAM Role is assigned to the server!) (@default='').
		'accessKeyId'             => '',
		// AWS S3 Secret Access Key (should be empty if AWS IAM Role is assigned to the server!) (@default='').
		'secretAccessKey'         => '',
		// AWS S3 Bucket Name holding Test Content files.
		'contentBucket'           => '',
		// AWS S3 Bucket Name holding Test Content Archived (Package) files.
		'contentArchiveBucket'    => '',
		// AWS S3 Bucket Name holding Test Results files.
		'resultsBucket'           => '',
		// AWS S3 Bucket Name holding Test Results Archived files.
		'resultsArchiveBucket'    => '',
		// AWS S3 Bucket Key (subfolder) holding Test Content files (without leading or trailing slash!) (@default='').
		'contentPrefix'           => '',
		// AWS S3 Bucket Key (subfolder) holding Test Content Archived (Package) files (without leading or trailing
		// slash!) (@default='').
		'contentArchivePrefix'    => '',
		// AWS S3 Bucket Key (subfolder) holding Test Results files (without leading or trailing slash!) (@default='').
		'resultsPrefix'           => '',
		// AWS S3 Bucket Key (subfolder) holding Test Results Archived files (without leading or trailing slash!)
		// (@default='').
		'resultsArchivePrefix'    => '',
		// List of File Extensions for which both Original and GZipped (compressed) version of the file should be
		// uploaded to AWS S3, so Browser can download compressed version if it supports GZip. Do NOT change!
		'gzipFileExtensions'      => ['htm', 'html', 'xhtml', 'txt', 'js', 'json', 'css', 'xml', 'ttf', 'otf', 'eot',
		                              'woff', 'svg', 'ico']
	],
	// Application Logging Configuration.
	'logger' => [
		// Logging Level [0-9] (@default=6).
		'logLevel'                => 6,
		// Flag to turn On/Off Logger Transactions (all Log Entries are saved together at the end to improve
		// performance) (@default=true).
		'useLogTransactions'      => true,
		// Flag to turn On/Off Database Profiler Logs being included into Application Log. This is similar to Database
		// Log with additional performance details, and is only available if both Debugging and Database SQL Logging are
		// enabled! (@default=false).
		'includeDbProfilerLog'    => false,
		// Flag to turn On/Off logging of Request URLs as INFO level (@default=true).
		'logRequestUrl'           => true,
		// Flag to turn On/Off logging of Request Bodies. It depends on Request URLs logging (@default=true).
		'logRequestBody'          => true,
		// Amount of Bytes to be logged if Request Bodies logging is enabled. This should be reasonably small!
		// (@default=1024).
		'logRequestBodyMaxSize'   => 1024,
		// Flag to turn On/Off logging of Responses as DEBUG level (@default=true).
		'logResponse'             => true,
		// Amount of Bytes to be logged if Responses logging is enabled. This is only used for JSON Responses and should
		// be reasonably small! (@default=1024).
		'logResponseMaxSize'      => 1024,
		// Flag to turn On/Off logging of Ping Status Request (used for Health Check by Load Balancer). This will only
		// work when Logger Transactions are enabled! (@default=false).
		'logPingStatusRequest'    => false,
		// Flag to turn On/Off File Logger, so all Log Entries are saved into a Log File on the configured "logPath".
		// This is the default Logger Adapter (@default=true).
		'useFileLogger'           => true,
		// Path to the Application Log, relative to the Application root. "{date}" will be replaced with Current Date
		// (@default='/logs/app_{date}.log').
		'fileLoggerPath'          => '/logs/app_{date}.log',
		// Flag to turn On/Off Database Logger, so all Log Entries are also saved into centralized Application Database
		// (in case there are multiple Application instances behind the Load Balancer) (@default=false).
		'useDatabaseLogger'       => false,
		// Flag to turn On/Off Socket Logger, so all Log Entries are also sent to Socket Connection over Data Stream.
		// This should only be used for debugging, and should NOT be used in Production! It requires Debugging to be
		// enabled (@default=false).
		'useSocketLogger'         => false,
		// Socket Logger Hostname (@default='127.0.0.1').
		'socketLoggerHost'        => '127.0.0.1',
		// Socket Logger Port (@default=9999).
		'socketLoggerPort'        => 9999
	],
	// Tenant specific Configuration. For new Tenants simply add new tenantHost and tenantTheme.
	'tenant' => [
		// List of Hostnames unique to each Tenant. This includes Tenant specific Hostname as Key and Tenant specific ID
		// as Value! Hostname MUST be in lowercase!!!
		'tenantHost'              => [
			// PARCC specific Hostname (@default=['<parccHost>'=>1]).
			'<parccHost>'         => 1,
			// ISBE specific Hostname (@default=['<isbeHost>'=>2]).
			'<isbeHost>'          => 2
		],
		// List of Themes used by each Tenant. This includes one of available Themes to be used by specific Tenant as
		// Key and Tenant specific ID as Value!
		'tenantTheme'             => [
			// PARCC specific Theme (@default='purple').
			'1'                   => 'purple',
			// ISBE specific Theme (@default='green').
			'2'                   => 'green'
		],
		// Flag if Application is allowed to operate in Test Delivery mode for specific Tenant (i.e. if Test Window is
		// open). This will only be used when Test Delivery is enabled at the Application level!
		'allowTestDelivery'       => [
			// PARCC specific Test Delivery flag (@default=true).
			'1'                   => true,
			// ISBE specific Test Delivery flag (@default=true).
			'2'                   => true
		]
	],
	//@TODO: Rename 'acr' to 'publishingSource'??!
	// Publishing Origin Configuration. These credentials are used for calling back Application that requested Test
	// Publishing. For new Users simply add new section.
	'acr' => [
		// User ID=1 (PARCC) Configuration.
		'1' => [
			// Publishing Origin Hostname (without trailing slash!) (@default='').
			'host'                => '',
			// Publishing Origin Username (@default='').
			'username'            => '',
			// Publishing Origin Password (@default='').
			'password'            => '',
			// Publishing Origin Secret (@default='').
			'secret'              => ''
		],
		// User ID=2 (ISBE) Configuration.
		'2' => [
			// Publishing Origin Hostname (without trailing slash!) (@default='').
			'host'                => '',
			// Publishing Origin Username (@default='').
			'username'            => '',
			// Publishing Origin Password (@default='').
			'password'            => '',
			// Publishing Origin Secret (@default='').
			'secret'              => ''
		]
	],
	//@TODO: Rename 'ldr' to 'publishingDestination'??!
	// Publishing Target Configuration. These Tenant specific credentials are used for notifying Application about
	// successful Test Publishing that was requested by Publishing Origin. For new Tenants simply add new section.
	'ldr' => [
		// Tenant ID=1 (PARCC) Configuration.
		'1' =>[
			// Publishing Target Hostname (without trailing slash!) (@default='').
			'host'                => '',
			// Publishing Target Username (@default='').
			'username'            => '',
			// Publishing Target Password (@default='').
			'password'            => '',
			// Publishing Target Secret (@default='').
			'secret'              => '',
			// Publishing Target Token Lifetime in seconds (@default=1200).
			'tokenLifetime'       => 1200
		],
		// Tenant ID=2 (ISBE) Configuration.
		'2' => [
			// Publishing Target Hostname (without trailing slash!) (@default='').
			'host'                => '',
			// Publishing Target Username (@default='').
			'username'            => '',
			// Publishing Target Password (@default='').
			'password'            => '',
			// Publishing Target Secret (@default='').
			'secret'              => '',
			// Publishing Target Token Lifetime in seconds (@default=1200).
			'tokenLifetime'       => 1200
		]
	],
	// Assessment Web Service specific Configuration. It depends on Tenants! For new Tenants simply add new section.
	'Assessment' => [
		// API Hostname (without "http://" or "https://" scheme and trailing slash!) (@default='').
		'apiHost'                 => '',
		// AWS CloudFront Hostname for Production version of Test Driver Assets (Images, JavaScript, CSS, Fonts etc.)
		// (without "http://" or "https://" scheme and trailing slash!) (@default='*.cloudfront.net').
		'cloudFrontHost'          => '*.cloudfront.net',
		// Relative Path to Release version of Test Driver Assets (Images, JavaScript, CSS, Fonts etc.). This should NOT
		// be used in Production and should be empty! (@default='').
		'releaseAssetPath'        => '',
		// Relative Path to Debug version of Test Driver Assets (Images, JavaScript, CSS, Fonts etc.). This should NOT
		// be used in Production and should be empty! (@default='').
		'debugAssetPath'          => '',
		// Tenant ID=1 (PARCC) Configuration.
		'1' => [
			// Tenant specific Username used by Test Driver for Basic and HMAC Authentication.
			'testDriverUsername'  => '',
			// Tenant specific Password used by Test Driver for Basic and HMAC Authentication.
			'testDriverPassword'  => '',
			// Tenant specific Secret used by Test Driver for Basic and HMAC Authentication.
			'testDriverSecret'    => ''
		],
		// Tenant ID=2 (ISBE) Configuration.
		'2' => [
			// Tenant specific Username used by Test Driver for Basic and HMAC Authentication.
			'testDriverUsername'  => '',
			// Tenant specific Password used by Test Driver for Basic and HMAC Authentication.
			'testDriverPassword'  => '',
			// Tenant specific Secret used by Test Driver for Basic and HMAC Authentication.
			'testDriverSecret'    => ''
		]
	],
	// Login Web Service specific Configuration.
	'Login' => [
		// Number of consecutive retries to generate a unique Test Session Token in case of previous failure before
		// quitting and triggering an Error (@default=3).
		'tokenRetryCount'         => 3,
		// Duration in seconds for how long Test Session Token will be valid once generated/refreshed. It gets reset
		// each time Content or Results Web Services are called after a successful Authentication and validation
		// (@default=3600).
		'tokenLifetime'           => 3600,
		// Flag to turn On/Off validation if Test is already in progress, and is only available if Debugging is enabled!
		// (@default=true).
		'useInProgressCheck'      => true,
		// Pearson CAT/DCM Adaptive Engine URL (without trailing slash!) (@default='').
		'catUrl'                  => ''
	],
	// Content Web Service specific Configuration.
	'Content' => [
		// AWS CloudFront Hostname (without "http://" or "https://" scheme and trailing slash!)
		// (@default='*.cloudfront.net').
		'cloudFrontHost'          => '*.cloudfront.net',
		// AWS CloudFront Key Pair ID (indicates AWS which RSA Key Pair to use to validate URL Signature).
		'sshKeyPairId'            => '',
		// AWS S3 Private RSA (SSH-2) Key for signing CloudFront URLs by Trusted Signer (relative from the Application
		// root folder) (@default='/app/config/private_key.pem').
		'sshPrivateKey'           => '/app/config/private_key.pem',
		// Flag if AWS CloudFront Signed ULR should be locked to a single IP Address (@default=true).
		'signedUrlLockToIp'       => true,
		// Duration in seconds for how long AWS CloudFront Signed URLs will be valid once generated (@default=300).
		'signedUrlLifetime'       => 60
	],
	// Results Web Service specific Configuration.
	'Results' => [
		// Path to Test Results JSON Schema used for Results validation (relative from the Application root folder). Do
		// NOT change! (@default='/app/include/resultsSchema.json').
		'resultsSchema'           => '/app/include/resultsSchema.json',
		// Flag to turn On/Off Test Results JSON validation against Test Results JSON Schema (@default=true).
		'useResultsValidation'    => true
	],
	// Test Web Service specific Configuration.
	'Test' => [
		// Path to Test JSON Schema used for Test validation (relative from the Application root folder). Do NOT change!
		// (@default='/app/include/testSchema.json').
		'testSchema'              => '/app/include/testSchema.json',
		// Flag to turn On/Off Test JSON validation against Test JSON Schema (@default=true).
		'useTestValidation'       => true,
		// Gzip compression level (@default=9).
		'gzipCompressionLevel'    => 9,
		// Chunk size that we use to gz-encode files. default value is 0.5MB (@default=524288).
		'gzipChunkSize'           => 524288,
		// Flag to validate pub_resourceManifest node in Test JSON and check if all referenced File Resources are found.
		// If enabled and referenced File Resource is missing, it will stop further test publishing. If disabled and
		// referenced File Resource is missing, it will NOT stop test publishing. In any case an Error will be logged.
		// (@default=true).
		'strictFileManifestCheck' => true,
		// Resource Prefix
		'resourcePrefix'          => '[[ADP]]'
	]
]);
