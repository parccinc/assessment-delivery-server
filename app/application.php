<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

// Report on ALL Errors!
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


use Phalcon\Mvc\Micro;

/**
 * This is the Application core. It loads Bootstrap, defines all Routes, Request Controllers, Error and Exception
 * Handlers.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */

require_once 'bootstrap.php';


/**
 * This Application is a Micro Application, so all Routes must be explicitly defined. For APIs, this is ideal. This is
 * as opposed to the more robust MVC Application. It sets $di as a default Dependency Injector.
 *
 * @var Micro $app Micro Application instance.
 */
$app = new Micro($di);


// Initialize Application Logger.
/** @var PARCC\ADP\Services\Logger\Multiple $app->logger */
$app->logger->begin();


/**
 * Mount all Collections and activate all Routes defined by found Collections.
 */
foreach ($di->get('collections') as $collection) {

	/** @var Micro $app */
	$app->mount($collection);
}


/**
 * Identify Current Tenant.
 */
/** @var object $app->setTenant */
$app->setTenant;


// Check if there was any issue with connecting to the Primary Database.
// This will only be logged if reconnecting to the Primary Database was successful!
/** @var Micro $app */
if (isset($app['dbError'])) {
	// Log Primary Database Connection Exception.
	/** @var PARCC\ADP\Services\Logger\Multiple $app->dbError */
	$app->logger->alert($app['dbError']);
}
// Check if there was any issue with connecting to the Secondary Database.
// This will only be logged if reconnecting to the Secondary Database was successful!
/** @var Micro $app */
if (isset($app['dbError2'])) {
	// Log Secondary Database Connection Exception.
	/** @var PARCC\ADP\Services\Logger\Multiple $app->dbError */
	$app->logger->alert($app['dbError2']);
}


/**
 * Before Route Handler.
 *
 * Before any Request gets processed, make sure to Log the Request details, assign appropriate Response Type, Set CORS
 * policies and Authenticate User first!
 * This supports Basic Authentication and HMAC Authorization. It also allows all OPTIONS Requests without Authentication
 * as those tend to not come with Cookies or any Authentication details (credentials in general), and are used for
 * Pre-flight Requests which are not implemented the same way in all Browsers.
 *
 * @throws HttpException In case User is not authenticated for any reason.
*/
/** @var Micro $app */
$app->before(function () use ($app) {

	return require_once 'core/application/authentication.php';
});


/**
 * After Route Handler.
 *
 * After a Route is executed (usually when its Controller returns the final value), the Application runs the following
 * function which actually sends the Response to the Client. The only exception from this workflow is if an Exception is
 * being thrown in the meantime.
 */
/*
$app->after(function () use ($app) {

	// Post-processing steps...
});
*/


/**
 * Finish Route Handler.
 *
 * After a Request has been served (usually when $app->after is being executed), the Application runs the following
 * function which is used for saving Log Entries and collecting statistics about handled Request. The only exception
 * from this workflow is if an Exception is being thrown in the meantime.
 */
$app->finish(function () use ($app) {

	return require_once 'core/application/shutdown.php';
});


/**
 * Not Found Application Handler.
 *
 * NotFound Service is the default Handler function that runs when no Route was matched. It returns an appropriate
 * Request Type with related details.
 *
 * @throws HttpException
 */
$app->notFound(function () use ($app) {

	return require_once 'core/application/notFound.php';
});


/** @noinspection PhpUnusedParameterInspection
 * Default Application Exception Handler.
 *
 * This is only supported by Phalcon v2.x.x and will cause Phalcon v1.x.x to crash!!!
 *
 * @var Exception|HttpException $exception Exception being caught.
 */
$app->error(function ($exception) use ($app) {

	return require_once 'core/application/errorHandler.php';
});


/** @noinspection PhpUnusedParameterInspection
 * Default PHP Exception Handler.
 *
 * If the Application throws a HttpException, it will be sent to the Client as appropriate Response Type (i.e. as JSON).
 * Otherwise, this is an Unhandled PHP Exception, so just log it.
 *
 * @var Exception|HttpException $exception Exception being caught.
 */
set_exception_handler(function ($exception) use ($app) {

	return require_once 'core/application/exceptionHandler.php';
});


// Main Application Handler.
$app->handle();
