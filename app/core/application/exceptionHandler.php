<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use PARCC\ADP\Exceptions\HttpException;

/**
 * Application Exception Handler.
 *
 * This is Default PHP Exception Handler. If the Application throws a HttpException, it will be sent to the Client as
 * appropriate Response Type (i.e. as JSON). Otherwise, this is an Unhandled PHP Exception, so just log it.
 *
 * @var Exception|HttpException $exception Exception being caught.
 */
// Check if Exception is an instance of HttpException.
if ($exception instanceof HttpException) {
	// Log HttpException details.
	/** @var PARCC\ADP\Services\Logger\Multiple $app->logger */
	$app->logger->warning("HttpException: " . $exception->getFile() . "#" . $exception->getLine() . "\n" .
	                      $exception->internalError . "\n" . json_encode($exception->error));
	// Check if Debugging is turned On.
	/** @var object $app->config */
	if ($app->config->app->debug === true) {
		// Log MySQL Profiler details (only available when Debugging is turned On).
		/** @var object $app ->dbProfilerLog */
		$app->dbProfilerLog;
		// Log Application Execution Time (only available when Debugging is turned On).
		/** @var PARCC\ADP\Services\Logger\Multiple $app ->logger */
		$app->logger->debug($app['executionTime']);
		// Log Memory Usage and Peak Memory Usage details (only available when Debugging is turned On).
		$app->logger->debug("{$app['memoryUsage']}\n======================  HTTP EXCEPTION  ======================\n");
	}

} else {
	// Log HttpException details.
	/** @var PARCC\ADP\Services\Logger\Multiple $app->logger */
	$app->logger->emergency("Unhandled PHP Exception:\n" . json_encode($exception));

	// Check if Debugging is turned On.
	/** @var object $app->config */
	if ($app->config->app->debug === true) {
		// Log MySQL Profiler details (only available when Debugging is turned On).
		/** @var object $app ->dbProfilerLog */
		$app->dbProfilerLog;
		// Log Application Execution Time (only available when Debugging is turned On).
		/** @var PARCC\ADP\Services\Logger\Multiple $app ->logger */
		$app->logger->debug($app['executionTime']);
		// Log Memory Usage and Peak Memory Usage details (only available when Debugging is turned On).
		$app->logger->debug("{$app['memoryUsage']}\n==================  UNHANDLED PHP EXCEPTION  =================\n");
	}

	// Add Unhandled Exception to the PHP Log. This should never happen!
	error_log("{$exception}\n==============================================================\n");
}

// Make sure to save all Log Transaction details!
$app->logger->commit();
// Make sure to close all open Logs!
$app->logger->close();
