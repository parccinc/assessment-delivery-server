<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use PARCC\ADP\Exceptions\HttpException;

/**
 * Application Error Handler.
 *
 * This is Default Application Exception Handler.
 * This is only supported by Phalcon v2.x.x and will cause Phalcon v1.x.x to crash!!!
 *
 * @var Exception|HttpException $exception Exception being caught.
 */
// Check if Exception is an instance of PDOException.
if ($exception instanceof PDOException) {
	// Log HttpException details.
	/** @var PARCC\ADP\Services\Logger\Multiple $app->logger */
	$app->logger->critical("PDOException: " . $exception->getFile() . "#" . $exception->getLine() . "\n" .
	                       json_encode($exception->getMessage()) .
	                       "\n======================  PDO EXCEPTION  =======================");

	// Check if PDOException is triggered from the Client side (i.e. failed to establish Database Connection), OR it was
	// caused by an invalid SQL Query.
	if ($exception->getCode() >= 2000 && $exception->getCode() <= 2061) {
		// Throw Service Unavailable Exception using JSON Response Type for all Requests.
		throw new HttpException('ADP-1080', 503, 'Internal Error.', 'Database Connection Error!');
	} else {
		// Throw Internal Server Exception using JSON Response Type for all Requests.
		throw new HttpException('ADP-1081', 500, 'Internal Error.', 'Database SQL Error!');
	}
}
