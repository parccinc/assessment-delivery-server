<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Application Shutdown (After Route Execution) Handler.
 *
 * After a Request has been served (usually when $app->after is being executed), the Application runs the following
 * function which is used for saving Log Entries and collecting statistics about handled Request. The only exception
 * from this workflow is if an Exception is being thrown in the meantime.
 */
// Check if Debugging is turned On.
/** @var object $app->config */
if ($app->config->app->debug === true) {
	// Log MySQL Profiler details (only available when Debugging is turned On).
	/** @var object $app->dbProfilerLog */
	$app->dbProfilerLog;
	// Log Application Execution Time (only available when Debugging is turned On).
	/** @var PARCC\ADP\Services\Logger\Multiple $app ->logger */
	$app->logger->debug($app['executionTime']);
	// Log Memory Usage and Peak Memory Usage details (only available when Debugging is turned On).
	$app->logger->debug("{$app['memoryUsage']}\n================================================================\n");
}

// Check if Ping Status Request should be logged.
if ($app->config->logger->logPingStatusRequest === true || $app->request->getURI() !== '/api/ping') {
	// Make sure to save all Log Transaction details!
	$app->logger->commit();
}
// Make sure to close all open Logs!
$app->logger->close();
