<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use PARCC\ADP\Exceptions\HttpException;

/**
 * Not Found Application Handler.
 *
 * NotFound Service is the default Handler function that runs when no Route was matched. It returns an appropriate
 * Request Type with related details.
 *
 * @throws HttpException
 */
// Retrieve Request details.
$requestURI = $app->request->getURI();


// Check if Request should be logged.
/** @var object $app->config */
if ($app->config->logger->logRequestUrl === true) {
	// Generate Log Entry with the Request details.
	/** @var Phalcon\Mvc\Micro $app */
	$request = 'Not Found: ' . $app->request->getMethod() . ' ' . $app->request->getScheme() . '://' .
	           $app->request->getHttpHost() . $requestURI .
	           ' [Tenant ID: ' . (isset($app['tenant']) ? $app['tenant']->tenantId : 'N/A') . ']';
	// Generate Log Entry with the Request details.
	if (!$app->request->isGet() && !$app->request->isHead() && !$app->request->isOptions()) {
		// Add Request Body for POST, PUT, PATCH and DELETE Requests, only if Debugging is enabled!
		/** @var object $app->config */
		if ($app->config->app->debug === true) {
			// Include Request Body only if smaller than configured maximum size!
			// Test Results for example can be enormous and would consume a lot of memory and cause Logs to be huge.
			if ($_SERVER['CONTENT_LENGTH'] < $app->config->logger->logRequestBodyMaxSize) {
				/** @var Phalcon\Mvc\Micro $app */
				$requestBody = $app->request->getJsonRawBody();
				// Check if Request Body is empty.
				if (empty($requestBody)) {
					// Check if Request contains Form POST Data.
					if (count($_POST) > 0) {
						// Retrieve Form POST Data.
						$requestBody = 'Form Data: ' . json_encode($_POST);
					}
				} else {
					$requestBody = json_encode($requestBody);
				}
				$request .= (empty($requestBody) ? '' : (":\n" . $requestBody));
			} else {
				$request .= ":\n*** Size Limit Exceeded ***";
			}
		}
	}
	// Log Request details.
	/** @var PARCC\ADP\Services\Logger\Multiple $app->logger */
	$app->logger->info($request);
}


// Make sure to set the CORS Policy as soon as possible. This is very important!!!
/** @var object $app->setCors */
$app->setCors;


/*
 * Maintenance Mode.
 */
// Check if Maintenance Mode is turned On for any Request.
/** @var object $app->config */
if ($app->config->app->enableMaintenanceMode === true) {
	// Throw Maintenance Exception using appropriate Response Type based on the Controller handling the Request.
	/** @var Phalcon\Mvc\Micro $app */
	if (!$app->request->isGet() && !$app->request->isHead() && !$app->request->isOptions()) {
		// Throw Maintenance Exception using JSON Response Type for all POST, PUT, PATCH and DELETE Requests.
		throw new HttpException('ADP-1060', 503, 'System is currently undergoing maintenance.', 'System Maintenance.');

	} elseif ($app->request->isOptions()) {
		// Throw Maintenance Exception using Header Response Type for all OPTIONS Requests.
		// Per CORS Policy, all Pre-flight (OPTIONS) Requests must always receive 200 "OK" as a Response, so this is an
		// exception from other Maintenance cases.
		throw new HttpException(
			'ADP-1061',
			200,
			'System is currently undergoing maintenance.',
			'System Maintenance.',
			'PARCC\ADP\Responses\HeaderResponse'
		);

	} elseif (mb_strpos($requestURI, '/api/content') === 0) {
		// Throw Maintenance Exception using Header Response Type for all Content Requests.
		throw new HttpException(
			'ADP-1062',
			503,
			'System is currently undergoing maintenance.',
			'System Maintenance.',
			'PARCC\ADP\Responses\HeaderResponse'
		);

	} elseif (mb_strpos($requestURI, '/api/results') === 0) {
		// Throw Maintenance Exception using JSON Response Type for all Results Requests.
		throw new HttpException('ADP-1063', 503, 'System is currently undergoing maintenance.', 'System Maintenance.');

	} elseif ($app->request->isGet()) {
		// Throw Maintenance Exception using HTML Response Type for all other GET Requests.
		throw new HttpException(
			'ADP-1064',
			503,
			'The system is currently undergoing maintenance. We are sorry for the inconvenience.<br/>Please try ' .
			'again later.',
			'System Maintenance.',
			'PARCC\ADP\Responses\HtmlResponse'
		);

	} else {
		// Throw Maintenance Exception using Header Response Type for all other Requests.
		throw new HttpException(
			'ADP-1065',
			503,
			'System is currently undergoing maintenance.',
			'System Maintenance.',
			'PARCC\ADP\Responses\HeaderResponse'
		);
	}
}


// Throw Not Found Exception using appropriate Response Type based on the Controller handling the Request.
// HtmlResponse is only used for special scenarios (i.e. 404 for Homepage, 403 for Test Driver Launch or 503 for
// Maintenance Mode).
/** @var Phalcon\Mvc\Micro $app */
if (!$app->request->isGet() && !$app->request->isHead() && !$app->request->isOptions()) {
	// Throw Not Found Exception using JSON Response Type for all POST, PUT, PATCH and DELETE Requests.
	throw new HttpException('ADP-1066', 404, 'Data Not Found.', 'Requested Resource Not Found.');

} elseif ($app->request->isOptions()) {
	// Throw Not Found Exception using Header Response Type for all OPTIONS Requests.
	// Per CORS Policy, all Pre-flight (OPTIONS) Requests must always receive 200 "OK" as a Response, so this is an
	// exception from other Not Found cases.
	throw new HttpException(
		'ADP-1067',
		200,
		'Data Not Found.',
		'Requested Resource Not Found.',
		'PARCC\ADP\Responses\HeaderResponse'
	);

} elseif (mb_strpos($requestURI, '/api/content') === 0) {
	// Throw Not Found Exception using Header Response Type for all Content Requests.
	throw new HttpException(
		'ADP-1068',
		404,
		'Data Not Found.',
		'Requested Resource Not Found.',
		'PARCC\ADP\Responses\HeaderResponse'
	);

} elseif (mb_strpos($requestURI, '/api/results') === 0) {
	// Throw Not Found Exception using JSON Response Type for all Results Requests.
	throw new HttpException('ADP-1069', 404, 'Data Not Found.', 'Requested Resource Not Found.');

} elseif ($app->request->isGet()) {
	// Throw Not Found Exception using HTML Response Type for all other GET Requests.
	throw new HttpException(
		'ADP-1070',
		404,
		'Sorry, requested page was not found.',
		'Page Not Found.',
		'PARCC\ADP\Responses\HtmlResponse'
	);

} else {
	// Throw Not Found Exception using Header Response Type for all other Requests.
	throw new HttpException(
		'ADP-1071',
		404,
		'Data Not Found.',
		'Requested Resource Not Found.',
		'PARCC\ADP\Responses\HeaderResponse'
	);
}
