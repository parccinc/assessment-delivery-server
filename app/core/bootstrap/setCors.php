<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Bootstrap Set CORS Policies Service.
 *
 * This is the default setter for CORS (Cross-Origin Resource Sharing) Policies. Additional Policies that are unique to
 * specific Web Service may be set by its Controller.
 * It is important to set CORS Policies as soon as possible, and always have them in place, even when no Route was
 * matched the Request in order to be able to return proper 404 Responses or reports about Errors and Exceptions!
 *
 * This Provides support for CORS Policy. Origin is allowed from all URLs. Setting it here using the Origin header from
 * the Request allows multiple Origins to be served. It is done this way instead of with a wildcard '*' because wildcard
 * Requests are not supported when a Request needs credentials. Wildcard '*' is only used as a fail-over option, but it
 * will lead to Authentication failure if CORS is required!
 *
 * A Pre-flight Request is the special type of the Request when due to missing CORS Policies a Browser automatically
 * initiates OPTIONS Request prior to proceeding with the original Request. If CORS Policies are correctly setup as
 * a result of Pre-flight Request, it will automatically be followed by the intended Request. Therefore, all
 * Controllers must support OPTIONS Requests! Further, once Pre-flight Request was successfully received, Browser will
 * cache it for a period of time and will not repeat them as long as cached Response doesn't expire.
 *
 * Browser mostly have very low tolerance for 'Access-Control-Max-Age'. For example, Chrome doesn't allow it to be
 * more than 10min!
 *
 * Base support for Routes like '/tests' that represent a Resource's base URL are:
 *   GET, POST, OPTIONS, HEAD.
 * Extended support for Routes like '/tests/123' that represent a specific resource are:
 *   GET, PUT, PATCH, DELETE, OPTIONS, HEAD.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
// Try to retrieve Origin of the Request.
if (!filter_var($di->getShared('request')->getHeader('ORIGIN'), FILTER_VALIDATE_URL) === false) {
	$requestOrigin = $di->getShared('request')->getHeader('ORIGIN');
} else {
	$requestOrigin = '*';
}

// Set Basic CORS Policies, prohibit Clickjacking Attacks by denying usage of frames and iframes, and enforce HTTPS
// Requests for 365 days!
/*
 * Clickjacking, also known as a "UI redress attack", is when an attacker uses multiple transparent or opaque layers to
 * trick a user into clicking on a button or link on another page when they were intending to click on the the top level
 * page. Thus, the attacker is "hijacking" clicks meant for their page and routing them to another page, most likely
 * owned by another application, domain, or both.
 * Using a similar technique, keystrokes can also be hijacked. With a carefully crafted combination of stylesheets,
 * iframes, and text boxes, a user can be led to believe they are typing in the password to their email or bank account,
 * but are instead typing into an invisible frame controlled by the attacker.
 *
 * @see OWASP Documentation: https://www.owasp.org/index.php/Clickjacking
 */
/*
 * HTTP Strict Transport Security (HSTS) Header is a mechanism that Web Sites have to communicate to the Browsers that
 * all traffic exchanged with a given domain must always be sent over HTTPS, this will help protect the information from
 * being passed over unencrypted Requests. Considering the importance of this security measure it is important to verify
 * that the Web Site is using this HTTP header, in order to ensure that all the data travels encrypted from the Web
 * Browser to the Server. The HTTP Strict Transport Security (HSTS) feature lets a Web Application to inform the
 * Browser, through the use of a special Response Header, that it should never establish a Connection to the the
 * specified domain servers using HTTP. Instead it should automatically establish all Connection Requests to access the
 * site through HTTPS.
 * The HTTP Strict Transport Security Header uses two directives:
 *   max-age: indicates the number of seconds that Browser should automatically convert all HTTP requests to HTTPS.
 *   includeSubDomains: indicates that all Web Application’s sub-domains must use HTTPS.
 *
 * @see OWASP Documentation: https://www.owasp.org/index.php/HTTP_Strict_Transport_Security
 */
$di ->getShared('response')
	->setHeader('Access-Control-Allow-Credentials', 'true')
	->setHeader('Access-Control-Allow-Headers', 'Authentication, Authorization, Content-Type, X-Requested-With')
	->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
	->setHeader('Access-Control-Allow-Origin', $requestOrigin)
	->setHeader('Access-Control-Max-Age', '3600')
	->setHeader('X-Frame-Options', 'DENY')
	->setHeader('Strict-Transport-Security', 'max-age=31536000; includeSubDomains');

// Set Additional CORS Policies, if Debugging is turned on. Browser only allows access to simple Response Headers if
// CORS Policy is enforced, unless Server explicitly allows access to additional Response Headers. In order to do so
// additional CORS Policy must be set to allow Client to access specific additional Headers. This is used only for
// debugging purposes and should NOT be used in Production!
if ($di['config']->app->debug === true) {
	$di->getShared('response')
	   ->setHeader(
	       'Access-Control-Expose-Headers',
	       'Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Access-Control-Allow-Methods, ' .
	       'Access-Control-Allow-Origin, Access-Control-Max-Age, Cache-Directive, Connection, Content-Length, ' .
	       'Content-Type, Date, Etag, Keep-Alive, Pragma, Pragma-Directive, Server, Status, Vary, X-Error-Code, ' .
	       'X-Error-Description, X-Powered-By, X-Time'
	   );
}
