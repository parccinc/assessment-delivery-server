<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use Phalcon\Mvc\View\Simple as SimpleView;

/**
 * Bootstrap View Service.
 *
 * Returns View (PHTML Template) of the Application. Micro Applications in general don't use views, so this is an
 * exception and is only used for launching of Test Driver and protection from bots.
 * DI's setShared method provides a singleton instance. If the second parameter is a function, then the Service is
 * lazy-loaded on its first instantiation.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return  SimpleView $view View (PHTML Template).
 */
$view = new SimpleView();
$view->setViewsDir(dirname(dirname(__DIR__)) . '/views/');

return $view;
