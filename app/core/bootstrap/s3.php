<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use PARCC\ADP\Services\AWS\S3;

/**
 * Bootstrap S3 Service.
 *
 * AWS S3 API Service. It communicates with AWS S3 API in order to download and upload files from/to AWS S3 Bucket.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return  S3 AWS S3 Service with Authentication.
 */
return new S3(
	$di['config']->s3->accessKeyId,
	$di['config']->s3->secretAccessKey,
	$di['config']->s3->host,
	$di['config']->s3->stsHost,
	$di['config']->s3->useHttps
);
