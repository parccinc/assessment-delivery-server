<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use Phalcon\Mvc\Model\MetaData\Apc as ApcMetaData;

/**
 * Bootstrap Database Models Metadata Caching Service.
 *
 * Configure Database Metadata Caching inside APCu Cache. This ensures that Phalcon queries Metadata of all tables
 * linked to its Models only once and caches it inside PHP Op-Cache instead of querying it each time a Model is required
 * to be accessed in Database.
 *
 * As PHP v5.6 comes with built-in Zend OpCache, we cannot use APC extension, but we can use APCu Cache which has
 * stripped opcode caching, but still supports user caching which is used for caching Application data.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return  ApcMetaData Database Tables Metadata (only the ones used by Models).
 */
return new ApcMetaData([
	'lifetime' => $di['config']->apc->dbSchemaCacheLifetime,
	'prefix' => 'db-'
]);
