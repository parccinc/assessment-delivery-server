<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Bootstrap Service.
 *
 * Generates Log Entries for each executed SQL Query being profiled. It includes the SQL Query itself, Start Time, End
 * Time and Total Execution Time for each executed SQL Query during processing of a Request.
 * This should only be used for Debugging and should NOT be used in Production!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return  string Database Profiler Log Entries for all SQL Queries that were executed.
 */
// Check if Database Profiler Logging is enabled.
if ($di['config']->logger->includeDbProfilerLog === true) {
	// Get all generated Primary Database Profiles from the Primary Database Profiler.
	$dbProfiles = $di['dbProfiler']->getProfiles();
	if (isset($dbProfiles)) {
		$di['logger']->debug("\n----------------------------------------------------------------");
		/** @var Phalcon\Db\Profiler\Item $dbProfile */
		foreach ($dbProfiles as $dbProfile) {
			$di['logger']->debug(
				"\nSQL Statement: " . $dbProfile->getSqlStatement() .
				"\nStart Time: " . $dbProfile->getInitialTime() .
				"\nFinal Time: " . $dbProfile->getFinalTime() .
				"\nTotal Elapsed Time: " . $dbProfile->getTotalElapsedSeconds() .
				"\n----------------------------------------------------------------"
			);
		}
	}
}
