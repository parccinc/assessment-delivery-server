<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Bootstrap Client IP Address Detection Service.
 *
 * Retrieves Client's IP Address. It also supports forwarded Client's IP Address from AWS ELB (Elastic Loaded Balancer)
 * in which case $_SERVER['REMOTE_ADDR'] contains the IP Address of ELB instead of actual Client's IP Address!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return  string|null Client's IP Address, OR NULL if failed to be determined.
 */
/*
 * If a Request goes through multiple Proxies, the Client IP Address in the "X-Forwarded-For" Request Header is followed
 * by the IP Addresses of each successive Proxy that the Request goes through before it reaches Load Balancer. The
 * right-most IP Address is the IP Address of the most recent Proxy and the left-most IP Address is the IP Address of
 * the Originating Client.
 * @see AWS Documentation:
 *      http://docs.aws.amazon.com/ElasticLoadBalancing/latest/DeveloperGuide/x-forwarded-headers.html
 */
// Try to retrieve Forwarded Client's IP Address(es) from AWS ELB in case Application runs behind an ELB.
$ipAddresses = $di->getShared('request')->getHeader('X-Forwarded-For');

// Check if Client's IP Address was successfully retrieved.
if (!empty($ipAddresses)) {
	// Parse the originating Client's IP Address (in case multiple IP Addresses are provided).
	$ipAddresses = explode(', ', $ipAddresses);

	// Check if parsed IP Address is a valid IPv4 Address.
	if (filter_var($ipAddresses[0], FILTER_VALIDATE_IP) !== false) {
		// Return forwarded Client's IP Address.
		return $ipAddresses[0];

	} else {
		// Return NULL In case of failure to successfully parse Client's IP Address.
		return null;
	}

} else {
	// In case Application is not running behind AWS ELB, return what Server sees as an Originating IP Address.
	return $di->getShared('request')->getClientAddress();
}
