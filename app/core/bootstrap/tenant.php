<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use PARCC\ADP\Exceptions\HttpException;
use PARCC\ADP\Models\Tenant;

/**
 * Bootstrap Service.
 *
 * Identifies Current Tenant based on the requested URL.
 * If it cannot identify Current Tenant, then it uses the Default Tenant.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return Tenant Tenant details.
 */
// Retrieve Tenant Hostname.
$tenantHostname = mb_strtolower($di->getShared('request')->getHttpHost());

// Check if retrieved Tenant Hostname exists in Application Configuration.
if (!isset($di['config']->tenant->tenantHost->{$tenantHostname})) {
	// Return Tenant Exception if Tenant Hostname doesn't exist in Application Configuration.
	// It will be used to override another Exception being thrown later.
	return new HttpException(
		'ADP-1090',
		404,
		'Sorry, requested data was not found.',
		'Tenant Hostname Not Found!',
		'PARCC\ADP\Responses\JsonResponse',
		false
	);
}

// Retrieve matching Tenant ID based on requested HTTP Hostname.
$tenantId = $di['config']->tenant->tenantHost->{$tenantHostname};

// Check if Current Tenant was successfully identified.
if ($tenantId === false) {
	$tenantId = $di['config']->tenant->defaultTenant;
}

// Try to retrieve referenced Tenant from APCu Cache.
$tenant = $di['apcCache']->get('tenant-' . $tenantId);

// Check if referenced Tenant was successfully loaded (if it was previously cached).
if (empty($tenant)) {
	// Try to retrieve referenced Tenant first.
	/** @var Tenant $tenant */
	$tenant = Tenant::findFirst([
		'columns' => 'tenantId, name, isActive',
		'conditions' => 'tenantId = ?1',
		'bind' => [
			1 => $tenantId
		]
	]);

	$test = get_class($di->getShared('response'));
	// Check if Tenant was found and is Active.
	if ($tenant === false) {
		// Return Tenant Exception if no matching Tenant was found in Database.
		// It will be used to override another Exception being thrown later.
		return new HttpException(
			'ADP-1091',
			404,
			'Sorry, requested data was not found.',
			'Tenant Not Found!',
			'PARCC\ADP\Responses\JsonResponse',
			false
		);
	} elseif ($tenant->isActive === false) {
		// Return Tenant Exception if no matching Tenant was found in Database.
		// It will be used to override another Exception being thrown later.
		return new HttpException(
			'ADP-1092',
			403,
			'You are not authorized for access. We are sorry for the inconvenience.',
			'Tenant is Deactivated.',
			'PARCC\ADP\Responses\JsonResponse',
			false
		);
	}

	// Encode Tenant details as JSON.
	$tenant = json_encode($tenant);

	// Cache Tenant details into APCu Cache.
	$di['apcCache']->save('tenant-' . $tenantId, $tenant, $di['config']->apc->configCacheLifetime);
}

// Parse retrieved Tenant details.
$tenant = json_decode($tenant);

// Check if Tenant is a valid JSON Object.
if (!is_object($tenant)) {
	// Return Tenant Exception if Tenant is not a valid JSON Object.
	// It will be used to override another Exception being thrown later.
	return new HttpException(
		'ADP-1093',
		500,
		'Server encountered Internal Error. We are sorry for the inconvenience.',
		'Invalid Tenant Details.',
		'PARCC\ADP\Responses\JsonResponse',
		false
	);
}

// Set the Tenant as DI's Service, so it can be used within Controllers.
$di->setShared('tenant', $tenant)->resolve();
// Log Tenant details.
$di['logger']->debug("Tenant: {$tenant->name}");
