<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Bootstrap Service.
 *
 * Detects Default Database Connection Service. If Test Publishing is enabled, regardless if Test Delivery is enabled or
 * not, Application will always use Secondary Database Connection for User Authentication and Logging, and for
 * processing Request Queue. In any other case, only the Primary Database Connection will be used!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return  string {'db'|'db2'} Returns Database Connection Service which should be used by default for the Application
 *                              Core functionality.
 */
// Check if Test Publishing is enabled.
if ($di['config']->app->allowTestPublishing === true) {
	return 'db2';
} else {
	return 'db';
}
