<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Logger\Formatter\Line as LoggerFormatter;
use PARCC\ADP\Services\Logger\Database as DatabaseLogger;
use PARCC\ADP\Services\Logger\Multiple as MultipleLogger;
use PARCC\ADP\Services\Logger\Socket as SocketLogger;

/**
 * Bootstrap Logger Service.
 *
 * Configure Application Multi-Logger and its formatting. It can log to multiple Loggers concurrently and supports the
 * following Logger Adapters: File Logger, Database Logger and Socket Logger.
 * The following Log Types (Severity Levels) are supported:
 * DEBUG (default), INFO, NOTICE, WARNING, ERROR, ALERT, CRITICAL and EMERGENCY.
 * CUSTOM (LogLevel = 8) and SPECIAL (LogLevel = 9) are reserved, but are not implemented!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return  MultipleLogger $multipleLogger Application Multi-Logger.
 */
// Initialize Application Multi-Logger.
$multipleLogger = new MultipleLogger($di['config']->logger->useLogTransactions);

// Check if at least one Logger Adapter is enabled.
if ($di['config']->logger->useFileLogger === true || $di['config']->logger->useDatabaseLogger === true ||
	($di['config']->logger->useSocketLogger === true && $di['config']->app->debug === true)
) {
	// Get the Process ID (PID) handling current Request and append a random number to differentiate Log Entries from
	// different Requests handled by the same Process. PID by default is limited to the maximum of 32,768 in Linux, but
	// is configurable and can be additionally increased up to the maximum of 2^22 (4,194,304).
	$requestId = getmypid() . '-' . mt_rand(1, 9999);

	// Initialize Application Database Logger, if enabled.
	if ($di['config']->logger->useDatabaseLogger === true) {
		// This is an optional Logger Adapter saving Logs either into a local or a remote Database. It is suitable when
		// multiple Application instances are used concurrently behind the Load Balancer as a centralized Logging
		// storage, but it adds an overhead to the Database and causes overall performance impact!
		// It uses Default Database Connection!
		$databaseLogger = new DatabaseLogger(
			$di[$di['defaultDb']],
			[
				'applicationId' => $di['config']->app->applicationId,
				'requestId' => $requestId
			]
		);
		$databaseLogger->setLogLevel($di['config']->logger->logLevel);
		$multipleLogger->push($databaseLogger);
	}

	// Check if either File Logger or Socket Logger are enabled.
	if ($di['config']->logger->useFileLogger === true ||
	    ($di['config']->logger->useSocketLogger === true && $di['config']->app->debug === true)
	) {
		// Configure Logger Formatter to generate Log Entries in the following format:
		// "[Timestamp][ErrorType][ProcessID-RandomInteger] ErrorMessage".
		$loggerFormatter = new LoggerFormatter('[%date%][%type%][' . $requestId . '] %message%');
		$loggerFormatter->setDateFormat('m/d/Y h:i:sa');

		// Initialize Application File Logger, if enabled.
		if ($di['config']->logger->useFileLogger === true) {
			// This is the default Logger Adapter saving Logs on the local file system.
			$fileLogger = new FileLogger(
				dirname(dirname(dirname(__DIR__))) .
				strtr($di['config']->logger->fileLoggerPath, ['{date}' => date('Ymd')])
			);
			$fileLogger->setLogLevel($di['config']->logger->logLevel)
			           ->setFormatter($loggerFormatter);
			$multipleLogger->push($fileLogger);
		}

		// Initialize Application Socket Logger, if enabled, but only if Debugging is enabled!
		if ($di['config']->logger->useSocketLogger === true && $di['config']->logger->useSocketLogger === true) {
			// This is the optional Logger Adapter sending Logs over the Socket Connection as a Data Stream. It is
			// suitable for debugging purposes and should NOT be used in Production!
			$socketLogger = new SocketLogger(
				$di['config']->logger->socketLoggerHost,
				[
					'port' => $di['config']->logger->socketLoggerPort
				]
			);
			$socketLogger->setLogLevel($di['config']->logger->logLevel)
			             ->setFormatter($loggerFormatter);
			$multipleLogger->push($socketLogger);
		}
	}
}

return $multipleLogger;
