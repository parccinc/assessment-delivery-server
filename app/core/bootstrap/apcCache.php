<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use Phalcon\Cache\Backend\Apc;
use Phalcon\Cache\Frontend\Data;

/**
 * Bootstrap APC Cache Service.
 *
 * Configure Application Caching inside APCu (Alternative PHP Cache User). This stores all data frequently used by
 * Application in Memory which improves overall Application performance, so it doesn't have to be accessed/generated
 * each time it's needed. This includes Application Configuration, API Routes, Users, Test Driver Configuration etc.
 * All data is by default permanently cached, so in order to re-cache it, either Web Server has to be restarted or data
 * has to be flushed manually!
 *
 * As PHP v5.6 comes with built-in Zend OpCache, we cannot use APC extension, but we can use APCu which has stripped
 * opcode caching, but still supports user caching which is used for caching Application data.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return  Apc Application Data frequently being used.
 */
return new Apc(
	new Data([
		'lifetime' => 0
	]),
	[
		'prefix' => 'api-'
	]
);
