<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Bootstrap HTTPS Usage Detection Service.
 *
 * Checks if Request was made over HTTPS Protocol. It also supports forwarded originating Protocol from AWS ELB (Elastic
 * Loaded Balancer) in which case $_SERVER['HTTPS'] returns FALSE as Requests are always forwarded over HTTP from ELB,
 * regardless if originating Request came over HTTP or HTTPS Protocol (due to ELB design/limitation).
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return  boolean Returns TRUE if Originating Request was made over HTTPS Protocol. Otherwise, it returns FALSE.
 */
/*
 * The "X-Forwarded-Proto" Request Header helps identify the Protocol (HTTP or HTTPS) that a Client used to connect to
 * the Server. Server Access Logs contain only the Protocol used between the Server and the Load Balancer, but contain
 * no information about the Protocol used between the Client and the Load Balancer.
 * @see AWS Documentation:
 *      http://docs.aws.amazon.com/ElasticLoadBalancing/latest/DeveloperGuide/x-forwarded-headers.html
 */
// Try to retrieve Forwarded Originating Protocol from AWS ELB in case Application runs behind an ELB.
$useHttps = $di->getShared('request')->getHeader('X-Forwarded-Proto');

// Check if Client's IP Address was successfully retrieved.
if (!empty($useHttps)) {
	// Check if Originating Request was made over HTTPS Protocol.
	if (strtolower($useHttps) === 'https') {
		// In case Originating Request was made over HTTPS Protocol.
		return true;

	} else {
		// In case Originating Request was made over HTTP Protocol.
		return false;
	}

} else {
	// In case Application is not running behind AWS ELB, return what Server sees as a Request Protocol.
	if (isset($_SERVER['HTTPS'])) {
		// In case Request was made over HTTPS Protocol.
		return true;

	} else {
		// In case Request was made over HTTP Protocol.
		return false;
	}
}
