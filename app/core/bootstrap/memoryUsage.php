<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Bootstrap Memory Usage Service.
 *
 * Calculates Memory Usage and Peak Memory Usage, and converts them into a human readable format. Only used for
 * debugging purposes.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @param  integer     $memoryAmount Memory Amount in Bytes.
 * @return null|string               Returns Amounts of Memory and Peak Memory allocated to PHP, both real and
 *                                   allocated, but only if Debugging is enabled. Otherwise, it returns NULL.
 */
/*
 * Memory Amount Formatter.
 *
 * @param integer $memoryAmount Memory Amount in Bytes.
 */
$memoryFormatter = function ($memoryAmount) {
	// Memory Amount Units.
	$units = ['B', 'KB', 'MB', 'GB', 'TB'];

	// Return formatted Memory Amount with appended appropriate Memory Unit at the end.
	return round($memoryAmount / pow(1024, ($i = floor(log($memoryAmount, 1024)))), 2) . ' ' . $units[(int) $i];
};

// Retrieve Memory Usage: Current Real Memory, Current Allocated Memory, Peak Real Memory and Peak Allocated Memory.
$memoryUsageReal = memory_get_usage();
$memoryUsageAllocated = memory_get_usage(true);
$peakMemoryUsageReal = memory_get_peak_usage();
$peakMemoryUsageAllocated = memory_get_peak_usage(true);

// Return Memory Usage details.
return '[Memory Usage: ' . $memoryFormatter($memoryUsageReal) . ' / ' . $memoryFormatter($memoryUsageAllocated) .
	   '][Peak Memory Usage: ' . $memoryFormatter($peakMemoryUsageReal) . ' / ' .
	   $memoryFormatter($peakMemoryUsageAllocated) . ']';
