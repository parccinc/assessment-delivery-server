<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Controllers;

use PARCC\ADP\Exceptions\HttpException;

/**
 * Class AssessmentController
 *
 * This is the RESTful Controller for handling launching of Test Driver for Diagnostic and Practice Tests.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class AssessmentController extends BaseController
{

	/**
	 * Constructor.
	 *
	 * Sets the allowed Methods for Assessment Web Service and validates CORS Request.
	 * It also Grants Permission to be used only for Test Delivery.
	 * In addition, it allows Access to all Users including "Anonymous User" (without any Authentication).
	 *
	 * @internal param string $allowedMethods Provides a list of supported Methods by each Controller.
	 */
	public function __construct()
	{
		parent::__construct('GET, POST', true);
	}


	/**
	 * Returns Theme for Assessment Views based on Configuration for Current Tenant.
	 *
	 * @param  boolean       $returnThemeView Indicates if Theme Name or Theme View Name should be returned
	 *                                        (@default=false).
	 * @param  string        $viewName        Name of the View Template (@default='assessment').
	 * @throws HttpException                  Throws a HttpException if configured Tenant Theme does not exist.
	 * @return string        $tenantTheme     Returns Theme Name, OR Theme View Name for Current Tenant.
	 */
	private function getTheme($returnThemeView = false, $viewName = 'assessment')
	{
		// Retrieve Theme Name and Theme View Name for Current Tenant.
		$tenantTheme = $this->config->tenant->tenantTheme->{$this->tenant->tenantId};
		$tenantThemeView = $viewName . ucfirst($tenantTheme);

		// Construct expected file path to the Tenant Theme View.
		$tenantThemeFilePath = dirname(__DIR__) . '/views/' . $tenantThemeView . '.phtml';
		// Check if Tenant Theme View exists.
		if (!file_exists($tenantThemeFilePath)) {
			// Throw S3Exception if File Path is not valid.
			// Throw Internal Error Exception if Tenant Theme View doesn't exist.
			throw new HttpException(
				'ADP-1500',
				500,
				'There is a problem accessing this page. We are sorry for the inconvenience.',
				'Tenant Theme Not Found!',
				'PARCC\ADP\Responses\HtmlResponse'
			);
		}
		// Clear File status cache.
		clearstatcache(false, $tenantThemeFilePath);

		return ($returnThemeView === true ? $tenantThemeView : $tenantTheme);
	}


	/**
	 * GET Request Handler. This is the Request for getting the Test Driver Launch Page.
	 */
	public function loadTestDriverLaunch()
	{
		// Check if Test Delivery is enabled for Current Tenant.
		if ($this->config->tenant->allowTestDelivery->{$this->tenant->tenantId} !== true) {
			// Return HTML Page with a message that Test Window is currently inactive in case Test Delivery is disabled
			// for Current Tenant.
			$this->response->setContent(
				$this->view->render(
					$this->getTheme(true, 'page'),
					[
						'tenantId' => $this->tenant->tenantId,
						'message' => 'The test window is not currently open.<br/>Please ask your teacher for help.'
					]
				)
			);

		} else {
			// Generate random Test Key to be used for Test Driver launch, render View and set it as a Response.
			// Test Key is a 10-character long string with all uppercase characters except "P" to avoid small
			// possibility to end up with "PRACTXXXXX" Test Key which is reserved for Practice Tests and Quizzes.
			$this->response->setContent(
				$this->view->render(
					$this->getTheme(true),
					[
						'tenantId' => $this->tenant->tenantId,
						'testKey' => substr(
							str_shuffle(
								str_repeat(
									'ABCDEFGHIJKLMNOQRSTUVWXYZ', // List of 23 characters to pick from.
									mt_rand(1, 10) // Repeat the Seed between 1 and 10 times.
								)
							),
							1,
							10
						)
					]
				)
			);
		}

		// Send rendered View as a Response.
		$this->response->send();
	}


	/**
	 * POST Request Handler. This Request actually Launches the Test Driver.
	 * It should come from the Assessment View (returned as part of Assessment GET Handler), but it can also come
	 * directly from PRC in case a Practice Test or Quiz is being launched.
	 *
	 * @throws HttpException Throws a HttpException if invalid Assessment Launch details are received.
	 */
	public function loadTestDriver()
	{
		// Check if Test Delivery is enabled for Current Tenant.
		if ($this->config->tenant->allowTestDelivery->{$this->tenant->tenantId} !== true) {
			// Log invalid try to launch Test Driver while Test Delivery is disabled for Current Tenant.
			$this->logger->notice('Invalid Test Driver Launch while Test Window is closed!');

			// Return HTML Page with a message that Test Window is currently inactive in case Test Delivery is disabled
			// for Current Tenant.
			$this->response->setContent(
				$this->view->render(
					$this->getTheme(true, 'page'),
					[
						'tenantId' => $this->tenant->tenantId,
						'message' => 'The test window is not currently open.<br/>Please ask your teacher for help.'
					]
				)
			);

		} else {

			// Validate if Test Driver Launch contains valid number of POST parameters.
			if (count($_POST) !== 2) {
				// Log invalid Test Driver launch.
				$this->logger->notice('Invalid Test Driver Launch!');

				// Throw Access Denied Exception if unauthorized try was made to launch Test Driver.
				throw new HttpException(
					'ADP-1501',
					403,
					'You are not authorized to access this page.',
					'Unauthorized Test Driver launch: Invalid Parameters provided!',
					'PARCC\ADP\Responses\HtmlResponse'
				);
			}

			// Validate Test Driver Launch Name. It must remain empty as human would never fill it in as it's hidden,
			// but bots may unless they are very smart.
			if ($this->request->getPost('name', 'alphanum') !== '') {
				// Log invalid Test Driver launch.
				$this->logger->notice('Invalid Test Driver Launch!');

				// Throw Access Denied Exception if unauthorized try was made to launch Test Driver.
				throw new HttpException(
					'ADP-1502',
					403,
					'You are not authorized to access this page.',
					'Unauthorized Test Driver launch: Invalid Name provided!',
					'PARCC\ADP\Responses\HtmlResponse'
				);
			}

			// Retrieve Test Driver Launch Test Key and sanitize it using AlphaNumeric POST filter.
			$testKey = $this->request->getPost('testKey', 'alphanum');

			// Validate Test Driver Launch Test Key.
			if (!preg_match('/^[A-Z]{10}$/', $testKey)) {
				// Log invalid Test Driver launch.
				$this->logger->notice('Invalid Test Driver Launch!');

				// Throw Access Denied Exception if unauthorized try was made to launch Test Driver.
				throw new HttpException(
					'ADP-1503',
					403,
					'You are not authorized to access this page.',
					'Unauthorized Test Driver launch: Invalid Test Key provided!',
					'PARCC\ADP\Responses\HtmlResponse'
				);
			}

			// Check if provided Test Key is for a Practice Test.
			if ($this->checkIfPracticeTest($testKey) === true) {
				// Check if Test Assignment for Practice Tests exists with referenced Test Key.
				// This is a simplified validation and does not check all Test Details related to the Test Assignment as
				// those will be verified during Login. It only checks for existence of Test Assignment with particular
				// Test Key in order to avoid unnecessary download of the whole Test Driver if Test will certainly fail
				// to launch.
				$this->validateBrowserCORS($testKey);

				// Retrieve Login Configuration for Practice Tests.
				$loginConfiguration = $this->getSystemConfiguration('loginConfigurationPractice');

			} else {
				// Reset Test Key (it will be provided by Student during Login).
				$testKey = '';

				// Retrieve Login Configuration for non-Practice Tests.
				$loginConfiguration = $this->getSystemConfiguration('loginConfiguration');
			}

			// Generate obfuscated Tenant specific Authentication Credentials for Test Driver.
			$userCredentials = $this->config->Assessment->{$this->tenant->tenantId}->testDriverUsername . '-' .
			                   $this->config->Assessment->{$this->tenant->tenantId}->testDriverPassword . '-' .
			                   $this->config->Assessment->{$this->tenant->tenantId}->testDriverSecret;
			// Base64 Encode recursively Test Driver Credentials 5 times.
			for ($i = 1; $i < 6; ++$i) {
				$userCredentials = base64_encode($userCredentials);
			}

			// Generate API URL.
			$apiHost = $this->config->Assessment->apiHost;
			if (!empty($apiHost)) {
				$apiHost = ($this->useHttps === true ? 'https://' : 'http://') . $apiHost;
			}

			// Generate View and Absolute Path for Production version of Test Driver Assets.
			$view = 'testDriver';
			$assetPath = ($this->useHttps === true ? 'https://' : 'http://') .
			             $this->config->Assessment->cloudFrontHost;

			// Check if Browser supports GZip Compression.
			if ($this->isGzipSupported() === true) {
				// Use GZip File Extension in case Browser supports GZip Compression.
				$gzipExtension = '.gz';

			} else {
				// Use uncompressed version of all files in case Browser does not supports GZip Compression.
				$gzipExtension = '';
			}

			// Check if Debugging is enabled. The additional Routes should NOT be used in Production!
			if ($this->config->app->debug === true) {
				// Check which version of Test Driver is being launched (Production, Release or Debug).
				switch ($this->router->getMatchedRoute()->getCompiledPattern()) {
					case '/assessment/release':
						// Generate Relative Path and View for Release version of Test Driver Assets.
						$assetPath = $this->config->Assessment->releaseAssetPath;
						$gzipExtension = '';
						break;

					case '/assessment/debug':
						// Generate Relative Path and View for Debug version of Test Driver Assets.
						$assetPath = $this->config->Assessment->debugAssetPath;
						$gzipExtension = '';
						$view = 'testDriverDebug';
						break;

					default:
						// Use previously defined Absolute Path and View for Production version of Test Driver Assets.
				}
			}

			// Render View with embedded Authentication Credentials, Login Configuration, Test Key, Tenant ID, Tenant
			// Theme and Test Driver Asset Path, and set it as a Response.
			$this->response->setContent(
				$this->view->render(
					$view,
					[
						'tenantId' => $this->tenant->tenantId,
						'userCredentials' => $userCredentials,
						'apiHost' => $apiHost,
						'loginConfiguration' => $loginConfiguration,
						'testKey' => $testKey,
						'theme' => $this->getTheme(),
						'assetPath' => $assetPath,
						'gzip' => $gzipExtension,
						'protocol' => ($this->useHttps === true ? 'https://' : 'http://')
					]
				)
			);
		}

		// Send rendered View as a Response.
		$this->response->send();
	}
}
