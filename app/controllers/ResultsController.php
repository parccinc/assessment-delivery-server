<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Controllers;

use Phalcon\Db\RawValue;
use Phalcon\Mvc\Model\Transaction\Failed as TransactionException;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use PARCC\ADP\Exceptions\HttpException;
use PARCC\ADP\Models\TestResults;
use PARCC\ADP\Services\AWS\S3Exception;
use PARCC\ADP\Services\JSV\JSVSchema;

/**
 * Class ResultsController
 *
 * This is the RESTful Controller for handling Client's Results functionality.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @property \PARCC\ADP\Models\TestSession $testSession
 */
final class ResultsController extends BaseController
{

	/**
	 * Constructor.
	 *
	 * Sets the allowed Methods for Results Web Service and validates CORS Request.
	 * It also Grants Permission to be used only for Test Delivery.
	 *
	 * @internal param string $allowedMethods Provides a list of supported Methods by each Controller.
	 */
	public function __construct()
	{
		parent::__construct('GET, POST, OPTIONS', true);

		// Grant Access only to Test Driver Users.
		$this->validateAccess(['TD']);

		// Validate Request and retrieve Test Session.
		if ($this->request->isGet() || $this->request->isPost()) {
			$this->validateBrowserCORS();
		}
	}


	/**
	 * GET Request Handler. This handles Test Results Download.
	 *
	 * @param  string $token              Test Session Token.
	 * @param  string $testFormRevisionId Test Battery Form Revision ID.
	 * @return array                      Response contains confirmation about requested action.
	 * @throws HttpException|S3Exception  In case invalid Results details are being received or Test Session Token has
	 *                                    expired, OR in case of an Error while generating CloudFront Signed URL for
	 *                                    Download of Test Results from S3.
	 */
	public function downloadResults($token, $testFormRevisionId)
	{
		// Check if provided Test Session Token is for a Practice Test.
		// Practice Tests should NOT download or upload Test Results which is controlled with Test Driver Configuration.
		if ($this->checkIfPracticeTest($this->testSession->testKey) === true) {
			// Throw Results Exception if Test is a Practice Test.
			throw new HttpException('ADP-1400', 403, 'Access Denied.', 'Test is Practice Test!');
		}

		// Check if referenced Test Session Token and Test Form Revision match, and if Test Session has a valid Test
		// Status (Test Session is already being retrieved and validated by Base Controller).
		// In case Test, Test Form and/or Test Form Revision were Deactivated during the Test Session, Student will NOT
		// be interrupted and will be allowed to continue with the Test. This is ONLY validated during Login and if any
		// is Deactivated Login will not be allowed!
		if ($token !== $this->testSession->token) {
			// Throw Authentication Exception if Test Session Token doesn't match.
			throw new HttpException('ADP-1401', 401, 'Authorization Failed.', 'Invalid Token.');

		} elseif ($testFormRevisionId !== $this->testSession->parentTestFormRevisionId) {
			// Throw Authentication Exception if Test Form Revision doesn't match.
			throw new HttpException('ADP-1402', 401, 'Authorization Failed.', 'Invalid Test Form Revision ID.');

		} elseif ($this->testSession->status === 'Submitted') {
			// Throw Results Exception if Test is already Submitted.
			throw new HttpException('ADP-1403', 403, 'Access Denied.', 'Test is already Submitted.');

		} elseif ($this->testSession->status === 'Completed') {
			// Throw Results Exception if Test is already Completed.
			throw new HttpException('ADP-1404', 403, 'Access Denied.', 'Test is already Completed.');

		} elseif ($this->testSession->status === 'Canceled') {
			// Throw Results Exception if Test is Canceled.
			throw new HttpException('ADP-1405', 403, 'Access Denied.', 'Test is Canceled.');

		} elseif ((float) $this->testSession->tokenExpirationTimestamp < time()) {
			// Throw Authorization Exception if Test Session Token has expired.
			throw new HttpException('ADP-1406', 401, 'Authorization Failed.', 'Token has Expired.');
		}

		// Refresh Test Session Token Timestamp first.
		$this->refreshTestSessionToken();

		// Retrieve Test Results details.
		/** @var TestResults $testResults */
		$testResults = TestResults::findFirst([
			'columns' => 'path',
			'conditions' => 'tenantId = ?1 AND parentTestSessionId = ?2 AND isActive = 1',
			'order' => 'createdDateTime DESC',
			'bind' => [
				1 => $this->tenant->tenantId,
				2 => $this->testSession->testSessionId
			]
		]);

		// Check if Test Results were successfully retrieved.
		if ($testResults === false) {
			// Throw Results Exception if Test Results were not found.
			throw new HttpException('ADP-1407', 404, 'Data Not Found.', 'Results Details Not Found.');
		}

		/*
		 * Download Test Results from AWS S3.
		 */
		try {
			// Try to download Test Results from AWS S3.
			$s3Response = $this->s3->downloadObject(
				$this->config->s3->resultsBucket,
				$this->config->s3->resultsPrefix . '/' . $testResults->path
			);

		} catch (S3Exception $exception) {
			// Log S3 Exception details about failed Test Results download.
			$this->logger->alert("S3Exception: " . $exception->getFile() . "#" . $exception->getLine() . "\n" .
			                     $exception->getMessage());
			// Throw Results Exception if Test Results failed to be downloaded from AWS S3.
			throw new HttpException(
				'ADP-' . $exception->getCode(),
				500,
				'Internal Error.',
				'Unable to provide Results Details.'
			);
		}

		// Send downloaded Test Results as a Response.
		$this->respond(
			json_decode($s3Response->body, true)
		);
	}


	/**
	 * POST Request Handler. This handles Test Results Upload.
	 *
	 * @param  string $token              Test Session Token.
	 * @param  string $testFormRevisionId Test Battery Form Revision ID.
	 * @param  string $testStatus         Test Status that should be set upon successful upload of Test Results. In case
	 *                                    of Save & Exit it should be 'paused' and in case of Test Submit it should be
	 *                                    'submitted'. For any other case (i.e. In Progress), it should be omitted!
	 *                                    (@default=null).
	 * @return array                      Response contains all requested records.
	 * @throws HttpException|S3Exception  In case invalid Results details are being received or Test Session Token has
	 *                                    expired, OR in case of an Error while uploading Test Results to AWS S3.
	 */
	public function uploadResults($token, $testFormRevisionId, $testStatus = null)
	{
		// Check if provided Test Session Token is for a Practice Test.
		// Practice Tests should NOT download or upload Test Results which is controlled with Test Driver Configuration.
		if ($this->checkIfPracticeTest($this->testSession->testKey) === true) {
			// Throw Results Exception if Test is a Practice Test.
			throw new HttpException('ADP-1450', 403, 'Access Denied.', 'Test is Practice Test!');
		}


		// Check if referenced Test Session Token and Test Form Revision match, if Test Session has a valid Test Status
		// (Test Session is already being retrieved and validated by Base Controller), and if Test Session Token has
		// expired.
		// In case Test, Test Form and/or Test Form Revision were Deactivated during the Test Session, Student will NOT
		// be interrupted and will be allowed to continue with the Test. This is ONLY validated during Login and if any
		// is Deactivated Login will not be allowed!
		if ($token !== $this->testSession->token) {
			// Throw Authentication Exception if Test Session Token doesn't match.
			throw new HttpException('ADP-1451', 401, 'Authorization Failed.', 'Invalid Token.');

		} elseif ($testFormRevisionId !== $this->testSession->parentTestFormRevisionId) {
			// Throw Authentication Exception if Test Form Revision doesn't match.
			throw new HttpException('ADP-1452', 401, 'Authorization Failed.', 'Invalid Test Form Revision ID.');

		} elseif ($this->testSession->status === 'Submitted') {
			// Throw Results Exception if Test is already Submitted.
			throw new HttpException('ADP-1453', 403, 'Access Denied.', 'Test is already Submitted.');

		} elseif ($this->testSession->status === 'Completed') {
			// Throw Results Exception if Test is already Completed.
			throw new HttpException('ADP-1454', 403, 'Access Denied.', 'Test is already Completed.');

		} elseif ($this->testSession->status === 'Canceled') {
			// Throw Results Exception if Test is Canceled.
			throw new HttpException('ADP-1455', 403, 'Access Denied.', 'Test is Canceled.');
		}

		// Check if Test Status needs to be updated.
		switch ($testStatus) {
			case 'pause':
				// Check if Test is currently In Progress.
				if ($this->testSession->status !== 'InProgress') {
					// Throw Results Exception if Test is Paused (NOT In Progress).
					throw new HttpException('ADP-1456', 403, 'Access Denied.', 'Test is not In Progress.');
				}
				// Set new Test Status.
				$testStatus = 'Paused';
				break;

			case 'submit':
				// Check if Test is currently In Progress.
				if ($this->testSession->status !== 'InProgress') {
					// Throw Results Exception if Test is Submitted (NOT In Progress).
					throw new HttpException('ADP-1457', 403, 'Access Denied.', 'Test is not In Progress.');
				}
				// Set new Test Status.
				$testStatus = 'Submitted';
				// Check is Test End Date and Time is missing (if Test has just been Submitted).
				if (empty($this->testSession->endDateTime)) {
					// Set Test End Date and Time.
					$this->testSession->endDateTime = new RawValue('NOW()');
				}
				break;

			case null:
				// Set new Test Status.
				$testStatus = 'InProgress';
				// Check is Test Start Date and Time is missing (if Test has just Started).
				if (empty($this->testSession->startDateTime)) {
					// Set Test Start Date and Time.
					$this->testSession->startDateTime = new RawValue('NOW()');
				}
				break;

			default:
				// Throw Results Exception if referenced Test Status is unknown. This should not happen!
				throw new HttpException('ADP-1458', 406, 'Invalid Request.', 'Invalid Test Status.');
		}

		// Refresh Test Session Token Timestamp first, but only if Test Session Token hasn't already expired!
		if ((float) $this->testSession->tokenExpirationTimestamp > time()) {
			// Refresh Test Session Token Timestamp first, but don't save it yet as Test Session might require
			// additional updates!
			$this->refreshTestSessionToken(false, false);
			// Set indicator that Test Session Token has not expired, so it doesn't get deleted.
			$isTokenExpired = false;

		} elseif ($this->testSession->status === 'Scheduled') {
			// Throw Results Exception if Test is Scheduled and hasn't started yet (if Test Session Token expired while
			// User was on Pretest Screens). Results Upload with expired Test Session Token is ONLY allowed when Test is
			// already In Progress!
			throw new HttpException('ADP-1459', 408, 'Access Denied.', 'Test is not In Progress.');

		} elseif ($this->testSession->status === 'Paused') {
			// Throw Results Exception if Test is Paused and hasn't started yet (if Test Session Token expired while
			// User was on Pretest Screens). Results Upload with expired Test Session Token is ONLY allowed when Test is
			// already In Progress!
			throw new HttpException('ADP-1460', 408, 'Access Denied.', 'Test is not In Progress.');

		} else {
			// If Test Session Token has expired, Test must be paused by force!
			// This will allow Test Results to be saved even if Test Session Token has expired, but only once as Test
			// Session Token will be deleted and any consecutive Request to Upload Test Results will fail!
			$testStatus = 'Paused';
			// Set indicator that Test Session Token has expired, so it gets deleted.
			$isTokenExpired = true;
		}

		// Parse Request Body (uploaded Test Results).
		$requestBody = $this->request->getJsonRawBody();


		/*
		 * Validate Test Results against JSON Schema.
		 */
		// Check if uploaded Test Results JSON should be validated against Test Results JSON Schema.
		if ($this->config->Results->useResultsValidation === true) {
			// Try to load Test Results JSON Schema File.
			$resultsSchema = file_get_contents(
				dirname(dirname(__DIR__)) . $this->config->Results->resultsSchema
			);

			// Check if Test Results JSON Schema File was successfully loaded.
			if ($resultsSchema === false) {
				// Log an Error if Test Results JSON Schema failed to be loaded.
				$this->logger->emergency('System cannot open the Results Schema File!');
				// Throw Results Exception if Test Results JSON Schema failed to be loaded.
				// Assuming Application is correctly configured this should never happen!
				throw new HttpException('ADP-1461', 500, 'Internal Error.', 'Unable to process Results.');
			}

			// Try to parse Results JSON Schema.
			$resultsSchema = json_decode($resultsSchema);
			// Check if Test Results JSON Schema was successfully parsed.
			if (!is_object($resultsSchema)) {
				// Log an Error if Test Results JSON Schema failed to be parsed.
				$this->logger->emergency("Results Schema is invalid.");
				// Throw Results Exception if Test Results JSON Schema failed to be parsed.
				// Assuming Application is correctly deployed and configured this should not happen!
				throw new HttpException('ADP-1462', 500, 'Internal Error.', 'Unable to process Results.');
			}

			// Initialize Test Results JSON Schema.
			$jsvSchema = new JSVSchema('resultsSchema', $resultsSchema);
			// Check if Test Results JSON Schema is a valid JSON Schema.
			if (count($jsvSchema->errors) > 0) {
				// Log an Error if Test Results JSON Schema is not a valid JSON Schema.
				$this->logger->emergency("Invalid Results JSON Schema:\n" . json_encode($jsvSchema->errors));
				// Throw Results Exception if Test Results JSON Schema is not a valid JSON Schema.
				// Assuming Application is correctly deployed and configured this should not happen!
				throw new HttpException('ADP-1463', 500, 'Internal Error.', 'Unable to process Results.');
			}

			// Validate Test Results JSON against Test Results JSON Schema.
			$jsv = $this->jsv->validate($requestBody, $jsvSchema->get('resultsSchema'));

			// Release Test Results JSON Schema to avoid memory leaks.
			unset($jsvSchema);

			// Check if Test Results JSON is valid.
			if ($jsv->isValid === false) {
				// Log an Error if Test Results JSON is NOT valid Test Results JSON Object.
				$this->logger->error("Invalid Results Details Received:\n" .  json_encode($jsv->errors));
				// Throw Results Exception if Test Results JSON is NOT valid Test Results JSON Object.
				// Assuming Test Results are being uploaded by Test Driver and Test Driver version is in sync with
				// Application which is correctly deployed and configured this should not happen!
				throw new HttpException('ADP-1464', 412, 'Invalid Request.', 'Invalid Results Details.');
			}

			// Release Test Results JSON Schema Validation Result to avoid memory leaks.
			unset($jsv);
		}


		// Initialize Model Transaction.
		$transactionManager = new TransactionManager();
		$transaction = $transactionManager->get();

		// Create new Results Model instance and set its Properties.
		$testResults = new TestResults();
		$testResults->setTransaction($transaction);
		$testResults->tenantId = $this->tenant->tenantId;
		$testResults->parentTestSessionId = $this->testSession->testSessionId;
		$testResults->usedToken = $token;
		$testResults->isActive = true;


		/*
		 * Create Test Results reference in Database, and upload Test Results to AWS S3.
		 */
		// Error Code reference in case an Exception is being thrown while saving Data into Database, OR during upload
		// of Test Results to AWS S3.
		$errorCode = null;
		try {
			// Try to create Test Results reference in Database, and check if it was successfully created.
			if ($testResults->create() === false) {
				// Set Results Exception Error Code if Test Results reference failed to be saved into Database.
				$errorCode = 'ADP-1465';
				// Initialize Model Transaction Rollback which will automatically trigger a Transaction Exception.
				$transaction->rollback();
			}

			/*
			 * Construct AWS S3 Key (Object URI) where Test Results should be saved.
			 * In order to maximize use of AWS S3 Partitioning, all Test Session IDs and Test Results IDs must be in a
			 * reverse order!
			 * @example: id=1234567890 must be reverted as key=0987654321.
			 *
			 * In order to minimize impact of AWS S3 limitations around maximum concurrent uploads/download of 300 per
			 * S3 Bucket, it is highly recommended to use unique prefixes/hashes for all Keys in order to benefit from
			 * AWS S3 Bucket partitioning/indexing behavior. This is a must in order to improve performance and minimize
			 * possibility to trigger limits when AWS S3 API Request returns "503 Slow Down" Exception and does NOT
			 * upload referenced Object!
			 * @see AWS S3 API Documentation:
			 *      http://docs.aws.amazon.com/AmazonS3/latest/dev/request-rate-perf-considerations.html
			 */
			$testResults->path =
				// Reverse Test Session ID.
				strrev($this->testSession->testSessionId) . '/' .
				// Reverse Test Results ID.
				strrev($testResults->testResultsId) .
				// Append '.started' suffix if Test just got Started.
				($this->testSession->status === 'Scheduled' && $testStatus === 'InProgress' ? '.started' : '') .
				// Append '.paused' suffix if Test just got Paused.
				($this->testSession->status === 'InProgress' && $testStatus === 'Paused' ? '.paused' : '') .
				// Append '.resumed' suffix if Test just got Resumed.
				($this->testSession->status === 'Paused' && $testStatus === 'InProgress' ? '.resumed' : '') .
				// Append '.submitted' suffix if Test just got Submitted.
				($this->testSession->status === 'InProgress' && $testStatus === 'Submitted' ? '.submitted' : '') .
				// Append Tenant Identified.
				'.' . $this->tenant->tenantId .
				// Append JSON File Extension.
				'.json';

			// Try to update Test Results reference in Database, and check if it was successfully updated.
			// Test Results reference has to be created first in order to retrieve it's Primary ID to use it as a name
			// for Test Results File, so once filename is known its path can be generated and saved into Database.
			if ($testResults->update() === false) {
				// Set Results Exception Error Code if Test Results reference failed to be saved into Database.
				$errorCode = 'ADP-1466';
				// Initialize Model Transaction Rollback which will automatically trigger a Transaction Exception.
				$transaction->rollback();
			}


			/*
			 * Upload Test Results to AWS S3.
			 */
			try {
				// Try to upload Test Results to AWS S3.
				$this->s3->uploadObject(
					json_encode($requestBody),
					$this->config->s3->resultsBucket,
					$this->config->s3->resultsPrefix . '/' . $testResults->path,
					[
						'Content-Type' => 'application/json',
						'Cache-Control' =>
							'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0',
						'Expires' => 0
					]
				);

				// Log successful upload of Test Results to AWS S3.
				$this->logger->info("Test Results '{$testResults->path}' successfully uploaded to S3.");

			} catch (S3Exception $exception) {
				// Log an Error if Test Results failed to be uploaded to AWS S3.
				$this->logger->alert("S3Exception: " . $exception->getFile() . "#" . $exception->getLine() . "\n" .
				                     "Failed to upload '{$testResults->path}' to S3!\n" . $exception->getMessage());

				// Retrieve Error Code generated by AWS S3 Service, and set it as Results Exception Error Code when Test
				// Results failed to be uploaded to AWS S3.
				$errorCode = 'ADP-' . $exception->getCode();
				// Initialize Model Transaction Rollback which will automatically trigger a Transaction Exception.
				$transaction->rollback();
			}

			// Try to commit Model Transaction changes to Database. If failed, a Transaction Exception will be
			// automatically triggered.
			$transaction->commit();

		} catch (TransactionException $exception) {
			// Throw Results Exception if Test Results failed to be saved, either as a reference in Database or actual
			// Test Results JSON Object failed to be saved to AWS S3.
			// This is the main Exception for failed Test Results upload and its Error Code is different based on the
			// root cause of the failure.
			throw new HttpException($errorCode, 500, 'Internal Error.', 'Failed to save Results Details.');
		}


		// Update Test Session Properties including Test Status.
		$this->testSession->status = $testStatus;
		$this->testSession->updatedByUserId = $this->user->userId;

		// Check if Test Session Token and Test Session Token Expiration Timestamp should be deleted in case Test is
		// being Paused, OR in case of regular upload of Test Results when Test Session Token has already expired which
		// caused Test to be paused by force. ONLY the first exceptional Test Results upload is allowed with valid, but
		// expired Test Session Token!
		// In case Test is being Submitted, Test Session Token will remain as a reference for the last uploaded Test
		// Results.
		if ($testStatus === 'Paused' || $isTokenExpired === true) {
			// Delete Test Session Token in case it already expired, and force Test to be Paused!
			$this->testSession->token = null;
		}
		// Update Test Session.
		$this->testSession->update();

		// Check if valid Test Session Token has expired.
		if ($isTokenExpired) {
			// Throw Results Exception if Test Session Token expired and only one exceptional Test Results upload was
			// accepted.
			throw new HttpException('ADP-1467', 408, 'Access Denied.', 'Test is Paused by the system.');
		}


		// Send Test Results upload confirmation as a Response.
		$this->respond([
			'api' => [
				'expires' => (float) $this->testSession->tokenExpirationTimestamp,
				'time' => time() /* UTC server time. */
			],
			'test' => [
				'status' => $this->testSession->status
			]
		]);
	}
}
