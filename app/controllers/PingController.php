<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Controllers;

use Phalcon\Di;
use PARCC\ADP\Models\System;

/**
 * Class PingController
 *
 * This is the RESTful Controller for handling Ping Requests from Load Balancer and Status Health Check in general.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class PingController extends BaseController
{

	/**
	 * Constructor.
	 *
	 * Sets the allowed Methods for Ping Web Service and validates CORS Request.
	 * It also Grants Permission to be used both for Test Delivery and Test Publishing.
	 * In addition, it allows Access to all Users including "Anonymous User" (without any Authentication).
	 *
	 * @internal param string $allowedMethods Provides a list of supported Methods by each Controller.
	 */
	public function __construct()
	{
		parent::__construct('GET', true, true);
	}


	/**
	 * Trims line endings from a given string.
	 *
	 * @param  string $string String that needs to be trimmed from line endings.
	 * @return string         Returns given string without line endings.
	 */
	private function trimLineEndings($string)
	{
		return strtr($string, ["\n" => '', "\r" => '', "\r\n" => '', ]);
	}


	/**
	 * GET Request Handler for Ping Requests coming from Load Balancer as check if Server is up and running.
	 */
	public function ping()
	{
		// Send simple "OK" in Response Body as a Response (Load Balancer only cares if HTTP Status is "200 OK").
		$this->response->setContent('OK');
		$this->response->send();
	}


	/**
	 * GET Request Handler for Health Check.
	 *
	 * @throws \PDOException If Status Health Check failed to connect to Database it will automatically trigger
	 *                       PDOException.
	 */
	public function healthCheck()
	{
		// Get default Dependency Injector in order to get access to Application Services.
		$di = Di::getDefault();

		// Try to open Database Connection. If failed, it will automatically trigger a PDOException.
		$this->db;


		// Retrieve Application Version.
		$apiVersionFile = dirname(dirname(__DIR__)) . '/.phalcon/version';
		$apiVersion = file_get_contents($apiVersionFile);
		if ($apiVersion === false) {
			$this->logger->alert('System cannot read the Version file!');
			$apiVersion = 'N/A';

		} else {
			// Clear File status cache.
			clearstatcache(false, $apiVersionFile);
			// Append Last Modified Date of the API Version File.
			// This does NOT automatically mean that Date is tied to the actual API Version, but only when API Version
			// File was modified last time!
			$apiVersion .= ' [' . date('m/d/Y', filemtime($apiVersionFile)) . ']';
		}


		// Retrieve Database Schema Version.
		$dbVersionFile = dirname(dirname(__DIR__)) . '/.phalcon/dbVersion';
		$dbVersion = file_get_contents($dbVersionFile);
		if ($dbVersion === false) {
			$this->logger->alert('System cannot read the Database Schema Version file!');
			$dbVersion = 'N/A';

		} else {
			// Clear File status cache.
			clearstatcache(false, $dbVersionFile);
			// Append Last Modified Date of the Database Schema Version File.
			// This does NOT automatically mean that Date is tied to the actual Database Schema Version, but only when
			// Database Schema Version File was modified last time!
			$dbVersion .= ' [' . date('m/d/Y', filemtime($dbVersionFile)) . ']';
		}


		// Try to retrieve Local Installed Application Version.
		$installedVersionFile = dirname(dirname(__DIR__)) . '/.phalcon/installedVersion';
		$apiVersionLocal = file_get_contents($installedVersionFile);
		if ($dbVersion === false) {
			$this->logger->notice('System cannot read the Local Installed Version file!');
			$apiVersionLocal = 'N/A';

		} else {
			// Clear File status cache.
			clearstatcache(false, $installedVersionFile);
			// Append Last Modified Date of the Local Installed Application Version File.
			// This does NOT automatically mean that Date is tied to the actual Local Installed Application Version, but
			// only when Local Installed Application Version File was modified last time!
			$apiVersionLocal .= ' [' . date('m/d/Y', filemtime($installedVersionFile)) . ']';
		}


		// Try to retrieve Installed System Application Version.
		/** @var System $versionApi */
		$versionApi = System::findFirst([
			'columns' => 'value, createdDateTime, updatedDateTime',
			'conditions' => 'tenantId = 0 AND name = ?1',
			'bind' => [
				1 => 'versionApi'
			]
		]);
		// Check if Installed System Application Version was successfully retrieved.
		if ($versionApi === false) {
			// Log failure to load Installed System Application Version.
			$this->logger->alert("System Configuration 'versionApi' Not Found!");
			$apiVersionSystem = 'N/A';

		} else {
			// Check if Installed Application Version is valid.
			if (empty($versionApi->value)) {
				$apiVersionSystem = 'N/A [';

			} else {
				$apiVersionSystem = $versionApi->value . ' [';
			}

			// Check if Installed System Application Version has updated or created timestamp and append
			// Installation/Update Date to it.
			if (!empty($versionApi->updatedDateTime)) {
				$apiVersionSystem .= date('m/d/Y', strtotime($versionApi->updatedDateTime)) . ']';

			} elseif (!empty($versionApi->createdDateTime)) {
				$apiVersionSystem .= date('m/d/Y', strtotime($versionApi->createdDateTime)) . ']';

			} else {
				$apiVersionSystem .= 'N/A]';
			}
		}


		// Try to retrieve Installed Database Schema Version.
		/** @var System $versionApi */
		$versionDb = System::findFirst([
			'columns' => 'value, createdDateTime, updatedDateTime',
			'conditions' => 'tenantId = 0 AND name = ?1',
			'bind' => [
				1 => 'versionDb'
			]
		]);
		// Check if Installed System Database Schema Version was successfully retrieved.
		if ($versionDb === false) {
			// Log failure to load Installed Database Schema Version.
			$this->logger->alert("System Configuration 'versionDb' Not Found!");
			$dbVersionSystem = 'N/A';

		} else {
			// Check if Installed System Database Schema Version is valid.
			if (empty($versionDb->value)) {
				$dbVersionSystem = 'N/A [';

			} else {
				$dbVersionSystem = $versionDb->value . ' [';
			}

			// Check if Installed System Database Schema Version has updated or created timestamp and append
			// Installation/Update Date to it.
			if (!empty($versionDb->updatedDateTime)) {
				$dbVersionSystem .= date('m/d/Y', strtotime($versionDb->updatedDateTime)) . ']';

			} elseif (!empty($versionDb->createdDateTime)) {
				$dbVersionSystem .= date('m/d/Y', strtotime($versionDb->createdDateTime)) . ']';

			} else {
				$dbVersionSystem .= 'N/A]';
			}
		}


		// Make sure to properly initialize Controller Response Type for JSON Response!
		$headers = $di->get('response')->getHeaders();
		$di->setShared('response', 'PARCC\ADP\Responses\JsonResponse');
		$this->response = $di->get('response');

		// Send JSON Response with Status Health Check Status details as a Response.
		$this->response
		     ->setheaders($headers)
		     ->respond([
		          'status' => 'OK',
		          'apiVersion' => $this->trimLineEndings($apiVersion),
		          'dbVersion' => $this->trimLineEndings($dbVersion),
		          'apiVersionInstalledLocal' => $this->trimLineEndings($apiVersionLocal),
		          'apiVersionInstalledSystem' => $apiVersionSystem,
		          'dbVersionInstalledSystem' => $dbVersionSystem
		     ]);
	}
}
