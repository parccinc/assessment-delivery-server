<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Controllers;

use PARCC\ADP\Exceptions\HttpException;

/**
 * Class AdminController
 *
 * This is the RESTful Controller for handling Administration Requests to Install Application, Update Application or
 * Clear APCu Cache. For security reasons these Requests are only available from localhost!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class AdminController extends BaseController
{

	/**
	 * Constructor.
	 *
	 * Sets the allowed Methods for Admin Web Service and validates CORS Request.
	 * It also Grants Permission to be used both for Test Delivery and Test Publishing.
	 * In addition, it allows Access to all Users including "Anonymous User" (without any Authentication).
	 *
	 * @internal param string $allowedMethods Provides a list of supported Methods by each Controller.
	 */
	public function __construct()
	{
		parent::__construct('GET', true, true);

		// Grant Access only to Requests coming from the localhost, or from all hosts if Debugging is enabled!!!
		$this->validateAccess();
	}


	/** @noinspection PhpMissingParentCallCommonInspection
	 * Validates if User has been Granted Access to Administer Application based on Host where the Admin Request was
	 * made. Only Requests made form the localhost are allowed to Administer Application, OR any Host if Debugging is
	 * enabled!!!
	 *
	 * @param  array         $allowedUserRoles A list of User Roles permitted to use particular Controller.
	 * @throws HttpException                   In case permission is denied to Administer Application.
	 */
	protected function validateAccess($allowedUserRoles = [])
	{
		// Check if Request is made from the localhost, or if Debugging is enabled.
		if ($this->request->getClientAddress() !== '127.0.0.1' && $this->config->app->debug !== true) {
			// Log invalid Admin Request.
			$this->logger->alert('Invalid Admin Request Origin: ' . $this->request->getClientAddress() . ' !!!');

			// Throw Admin Exception if Request to Clear Cache was not made from localhost.
			throw new HttpException(
				'ADP-0001',
				401,
				'Access Denied.',
				'Admin Request did not originate from localhost!'
			);
		}

	}


	/**
	 * Searches for Search String in the referenced Configuration File and replaces it with Replacement String using
	 * Regular Expression which ignores spaces used for indenting.
	 * It returns confirmation if Search/Replace was successful or not.
	 *
	 * @param  string        $propertyName     Optional Configuration Property Name to search and update its Value. If
	 *                                         not set, raw string replacement will take place!
	 * @param  string        $propertyValue    Configuration Property Value to set for referenced Configuration
	 *                                         Property.
	 * @param  string        $propertyValueOld Optional old Configuration Property Value that needs to be replaced with
	 *                                         new Configuration Property Value. If set, new Configuration Property will
	 *                                         ONLY be set if old Configuration Property was found!
	 * @param  string        $configFilePath   Optional Path to Configuration File relative to Application Root Path
	 *                                         (@default='/config/config.php').
	 * @return boolean                         Returns TRUE if Configuration was successfully updated, OR FALSE if it
	 *                                         failed.
	 * @throws HttpException                   Throws Exception if Configuration File cannot be read or written to.
	 *
	 * Update existing values examples:
	 * @example $this->updateConfigurationProperty('app->enableMaintenanceMode', true);
	 * @example $this->updateConfigurationProperty('database->sqlLogPath', '/logs/db_{date}.log');
	 * @example $this->updateConfigurationProperty('tenant->tenantHost->adp.parcc.dev', 2, 1);
	 * @example $this->updateConfigurationProperty('Test->resourcePrefix', "'[[ADP]]'");
	 * Replace raw string from particular config file example:
	 * @example $this->updateConfigurationProperty('', '@version v2.0.0', '@version v1.0.0', '/config/config.php');
	 * Add whole new line line as raw string example:
	 * @example $this->updateConfigurationProperty('', "\t// General Application Configuration.\n",
	 *                                             "\t// General Application\n\t// Configuration.\n");
	 * Remove whole line as raw string example:
	 * @example $this->updateConfigurationProperty('', '', "\t// General Application Configuration.\n");
	 */
	private function updateConfigurationProperty(
		$propertyName,
		$propertyValue,
		$propertyValueOld = null,
		$configFilePath = '/config/config.php'
	) {
		// Check if Property Value is valid Type. Only following Types are allowed: Boolean, Numeric, String and NULL!
		if (is_object($propertyValue) || is_resource($propertyValue) || is_array($propertyValue)) {
			// Throw Admin Exception if provided Configuration Property Type is invalid.
			throw new HttpException('ADP-0002', 500, 'Internal Error.', 'Invalid Configuration Property Type!');
		}

		// Transform Property Value if it is Boolean or String.
		if (is_bool($propertyValue)) {
			// Convert Boolean Property Value into String.
			if ($propertyValue === true) {
				// In case of TRUE.
				$propertyValue = 'true';
			} else {
				// In case of FALSE.
				$propertyValue = 'false';
			}
		} elseif (is_string($propertyValue)) {
			// Check if Property Name is provided and if String Property Value is actually Array, or if String uses
			// special single or double quotes. If Property Name is omitted, raw string replacement is taking place!
			if ($propertyName !== '' &&
			    ($propertyValue[0] !== '[' && $propertyValue[strlen($propertyValue) - 1] !== ']') &&
			    ($propertyValue[0] !== '"' && $propertyValue[strlen($propertyValue) - 1] !== '"') &&
			    ($propertyValue[0] !== "'" && $propertyValue[strlen($propertyValue) - 1] !== "'")
			) {
				// Wrap String Property Value with quotes.
				$propertyValue = "'{$propertyValue}'";
			}
		}

		// Check if old Configuration Property was provided.
		if (isset($propertyValueOld)) {
			// Quote/Escape Regular Expression Characters.
			$propertyValueOld = preg_quote($propertyValueOld);
		}

		// Path to Configuration File.
		$configFilePath = realpath(dirname(__DIR__) . $configFilePath);

		// Try to Read Configuration File Content.
		$fileContent = file_get_contents($configFilePath);

		// Check if Configuration File Content was successfully read.
		if ($fileContent === false) {
			// Log an Error if Configuration File Content failed to be loaded.
			$this->logger->alert("System cannot read file: {$configFilePath}");

			// Throw Admin Exception if Configuration File Content failed to be loaded.
			// Assuming Application is correctly configured this should never happen!
			throw new HttpException('ADP-0003', 500, 'Internal Error.', 'Unable to Read File.');
		}

		// Split hierarchical Configuration Property Nodes.
		$propertyNames = explode('->', $propertyName);
		// Loop through all Configuration Property nodes and construct Regular Expression Pattern to search for.
		$regExpPattern = '/(.*?';
		foreach ($propertyNames as $propertyNameIndex => $currentPropertyName) {
			// Quote/Escape Regular Expression Characters.
			$currentPropertyName = preg_quote($currentPropertyName);

			// Check if current Property Name should be wrapped with quotes.
			if (count($propertyNames) > 1) {
				$currentPropertyName = "'" . $currentPropertyName . "'";
			}

			// Check if current Configuration Property Node is the last one.
			if ($propertyNameIndex + 1 < count($propertyNames)) {
				// In case of parent Configuration Property.
				$regExpPattern .= "{$currentPropertyName}.*?";

			} else {
				// In case of child/leaf Configuration Property.
				$regExpPattern .= "{$currentPropertyName}" . (count($propertyNames) > 1 ? "\\s*?=>\\s*" : "") .
				                  ")({$propertyValueOld})(.+?(?=,|\\n))(.*)/s";
			}
		}

		// Try to Search referenced Configuration Property and Replace its Value in the Configuration File Content.
		$fileContentUpdated = preg_replace(
			$regExpPattern,
			'${1}' . $propertyValue . (isset($propertyValueOld) ? '${3}' : '') . '${4}',
			$fileContent
		);

		// Check if Configuration File Content was successfully updated.
		if ($fileContent === $fileContentUpdated) {

			// Return FALSE if Configuration Property was not updated within Configuration File Content.
			return false;
		}

		// Try to Write Configuration File Content and check if it was written successfully.
		if (!file_put_contents($configFilePath, $fileContentUpdated)) {
			// Log an Error if Configuration File Content failed to be saved.
			$this->logger->alert("System cannot write to file: {$configFilePath}");

			// Throw Admin Exception if Configuration File Content failed to be saved.
			// Assuming is correctly configured this should never happen!
			throw new HttpException('ADP-0004', 500, 'Internal Error.', 'Unable to Write to File.');
		}

		// Retrieve all matches in order to Log Configuration Property changes.
		preg_match(
			$regExpPattern,
			$fileContent,
			$regExpMatches
		);
		// Check if raw String replacement took place and prepare result message.
		if ($propertyName !== '') {
			$message = "Configuration Property '{$propertyName}' is updated from \"" .
			           (isset($propertyValueOld) ? $regExpMatches[2] : $regExpMatches[3]) .
			           "\" to \"{$propertyValue}\" in '{$configFilePath}' Configuration File!";

		} else {
			$message = "Configuration String is updated from \"" .
			           (isset($propertyValueOld) ? $regExpMatches[2] : $regExpMatches[3]) .
			           "\" to \"{$propertyValue}\" in '{$configFilePath}' Configuration File!";
		}
		// Log Configuration Property changes.
		$this->logger->alert($message);
		// Print Configuration Property changes.
		echo $message . "\n\n";

		// Return confirmation about successful update of Configuration Property.
		return true;
	}


	/**
	 * GET Request Handler for APCu Cache Reset.
	 *
	 * @param  boolean       $sendConfirmation Indicates if Response should be sent to confirm if Cache was successfully
	 *                                         cleared.
	 * @throws HttpException                   Throws Exception if Request is not made from localhost as a security
	 *                                         measure.
	 */
	public function clearCache($sendConfirmation = true)
	{
		// Try to Flush APCu Cache (clear ALL Cached Data!).
		$this->apcCache->flush();

		// Check if APCu Cache was successfully cleared.
		if (count($this->apcCache->queryKeys()) !== 0) {
			// Log invalid Clear Cache Request.
			$this->logger->error('Application Cache Clear failed!');

			// Throw Admin Exception if Request to Clear Cache was not made from localhost.
			throw new HttpException(
				'ADP-0005',
				500,
				'Internal Error.',
				'Clear Cache Request failed to clear all keys!'
			);

		}

		// Log successful Clear Cache Request.
		$this->logger->notice('Application Cache was cleared.');

		// Check if Confirmation Response should be sent.
		// In case Request to Clear Cache was made internally (i.e. for turning Maintenance Mode On/Off), no Response
		// should be sent as original Request handler will take care of Response.
		if ($sendConfirmation === true) {
			// Send Confirmation Response about successfully flushed Application Cache.
			$this->response->setContent("Application Cache was cleared successfully.\n");
			$this->response->send();
		}
	}


	/**
	 * GET Request Handler for Maintenance Mode Status.
	 * It returns current status if Maintenance Mode is Enabled or Disabled.
	 */
	public function maintenanceStatus()
	{
		// Send Current Status for Maintenance Mode.
		$this->response->setContent(
			"Maintenance Mode is " . ($this->config->app->enableMaintenanceMode === true ? "Enabled" : "Disabled") .
			".\n"
		);
		$this->response->send();
	}


	/**
	 * Maintenance Mode Updater.
	 * It switches Maintenance Mode from Enabled to Disabled and vice versa, and returns confirmation about the outcome.
	 *
	 * @param  boolean       $enable Indicates if Maintenance Mode should be Enabled, OR Disabled.
	 * @throws HttpException         Throws Exception if switching Maintenance Mode fails.
	 */
	private function maintenanceUpdate($enable)
	{
		// Check if Maintenance Mode is already in requested state.
		if ($this->config->app->enableMaintenanceMode === $enable) {

			// Set Current Status for Maintenance Mode as Response.
			$this->response->setContent(
				'Maintenance Mode is already ' . ($enable === true ? 'Enabled' : 'Disabled') . ".\n"
			);

		} else {
			// Turn On output buffering in case Maintenance Mode needs to be updated.
			ob_start();
			// Try to update Maintenance Mode State.
			$isConfigUpdated = $this->updateConfigurationProperty('app->enableMaintenanceMode', $enable);
			// Clean (erase) the output buffer and turn Off output buffering to omit any printed messages.
			ob_end_clean();

			// Check if Maintenance Mode was successfully updated.
			if ($isConfigUpdated === false) {
				// Log failure to update Maintenance Mode.
				$this->logger->alert('Updating Maintenance Mode failed!');

				// Throw Admin Exception in case of failure to update Maintenance Mode.
				throw new HttpException(
					'ADP-0006',
					500,
					'Internal Error.',
					($enable === true ? 'Enabling' : 'Disabling') . ' Maintenance Mode failed!'
				);
			}

			// Reload and parse Application Configuration.
			$config = require dirname(__DIR__) . '/config/config.php';

			// Check if Maintenance Mode was correctly updated.
			/** @var object $config */
			if ($config->app->enableMaintenanceMode !== $enable) {
				// Log failure to update Maintenance Mode.
				$this->logger->alert('Switching Maintenance Mode failed!');

				// Throw Admin Exception in case of failure to update Maintenance Mode.
				throw new HttpException(
					'ADP-0007',
					500,
					'Internal Error.',
					($enable === true ? 'Enabling' : 'Disabling') . ' Maintenance Mode failed!'
				);
			}

			// Clear Application Cache.
			$this->clearCache(false);

			// Log switch of Maintenance Mode.
			$this->logger->alert('Maintenance Mode is ' . ($enable === true ? 'Enabled' : 'Disabled') . '.');

			// Set Updated Status for Maintenance Mode as Response.
			$this->response->setContent('Maintenance Mode is ' . ($enable === true ? 'Enabled' : 'Disabled') . ".\n");
		}

		// Send Response.
		$this->response->send();
	}


	/**
	 * GET Request Handler for Enabling Maintenance Mode.
	 * It tries to Enable Maintenance Mode and returns confirmation about the outcome.
	 */
	public function maintenanceEnable()
	{
		// Try to Enable Maintenance Mode.
		$this->maintenanceUpdate(true);
	}


	/**
	 * GET Request Handler for Disabling Maintenance Mode.
	 * It tries to Disable Maintenance Mode and returns confirmation about the outcome.
	 */
	public function maintenanceDisable()
	{
		// Try to Disable Maintenance Mode.
		$this->maintenanceUpdate(false);
	}


	/**
	 * GET Request Handler for Application Installation.
	 */
	public function install()
	{
		// Send simple "TBD in Response Body as a Response.
		$this->response->setContent("Application Installation: TBD.\n");
		$this->response->send();
	}


	/**
	 * GET Request Handler for Application Update.
	 */
	public function update()
	{
		// Send simple "TBD in Response Body as a Response.
		$this->response->setContent("Application Update: TBD.\n");
		$this->response->send();
	}
}
