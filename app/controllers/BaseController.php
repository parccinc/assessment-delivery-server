<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Controllers;

use Phalcon\Di;
use Phalcon\Di\Injectable;
use PARCC\ADP\Exceptions\HttpException;
use PARCC\ADP\Models\System;
use PARCC\ADP\Models\TestSession;

/**
 * Class BaseController
 *
 * As Phalcon\Mvc\Controller class has a final __construct() Method, the Constructor cannot be extended.
 * Therefore, we have to use Di\Injectable class instead, but we also must manually initialize the Parent Constructor!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @property      \PARCC\ADP\Services\Logger\Multiple logger          Bootstrap Service
 * @property      \PARCC\ADP\Responses\BaseResponse   response        Bootstrap Service
 * @property      TestSession                         testSession     Bootstrap Object
 * @property      \PARCC\ADP\Services\AWS\S3          s3              Bootstrap Service
 * @property      \PARCC\ADP\Services\JSV\JSV         jsv             Bootstrap Service
 * @property-read \Phalcon\Cache\Backend\Apc          apcCache        Bootstrap Service
 * @property-read object                              config          Bootstrap Service
 * @property-read string                              clientIpAddress Bootstrap Service
 * @property-read boolean                             useHttps        Bootstrap Service
 */
class BaseController extends Injectable
{

	/**
	 * @var \PARCC\ADP\Models\User $user User details.
	 */
	protected $user;
	/**
	 * @var \PARCC\ADP\Models\Tenant $tenant Tenant details.
	 */
	protected $tenant;
	/**
	 * @var TestSession $testSession Test Session details.
	 */
	protected $testSession;


	/**
	 * Constructor.
	 *
	 * It validates each Request and makes sure it complies with CORS Policies.
	 * We must manually initialize Parent Constructor as we have to use Di\Injectable class instead of MVC Controller!
	 *
	 * @param  string        $allowedMethods       Provides a list of supported Methods for particular Controller.
	 * @param  boolean       $useForTestDelivery   Indicates if particular Controller can be used for Test Delivery
	 *                                             (@default=false).
	 * @param  boolean       $useForTestPublishing Indicates if particular Controller can be used for Test Publishing
	 *                                             (@default=false).
	 * @throws HttpException                       In case Application is not configured to use particular Controller
	 *                                             for Test Delivery and/or Test Publishing.
	 */
	public function __construct(
		$allowedMethods = 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD',
		$useForTestDelivery = false,
		$useForTestPublishing = false
	) {
		// Get default Dependency Injector in order to get access to Application Services.
		$di = Di::getDefault();
		$this->setDI($di);

		// Make sure to properly initialize Controller Response Type!
		// For some reason, after Server Restart, the very first Request fails to get assigned proper Response Type by
		// Dependency Injector, but any consecutive Request works fine. This should be further investigated!
		$this->response = $di->get('response');

		// Set reference to the Tenant current Request refers to, if available.
		if ($di->has('tenant')) {
			$this->tenant = $di['tenant'];
		}

		// Check if Application is configured to be used for Test Delivery and/or Test Publishing, and if Permission for
		// use of a particular Controller has been Granted.
		if (($useForTestDelivery === false || $this->config->app->allowTestDelivery === false) &&
		    ($useForTestPublishing === false || $this->config->app->allowTestPublishing === false)
		) {
			// Throw Access Denied Exception if Permission is NOT Granted to use particular Controller and set
			// appropriate Error Description based on the Response Type.
			if (get_class($this->response) === 'PARCC\ADP\Responses\HtmlResponse') {
				// In case of HTML Response Type.
				throw new HttpException(
					'ADP-1100',
					403,
					'You are not authorized to access this page.',
					'Missing User Role!',
					'PARCC\ADP\Responses\HtmlResponse'
				);

			} else {
				// In case of any other Response Type.
				throw new HttpException(
					'ADP-1101',
					403,
					'Access Denied.',
					'Missing User Role!',
					get_class($this->response)
				);
			}
		}

		// Skip further steps in case Test Driver is being launched.
		if ($this->router->getMatchedRoute()->getName() === 'Assessment') {
			return;
		}

		// Set reference to the User currently calling API, if available.
		if ($di->has('user')) {
			$this->user = $di['user'];
		}

		// Make sure to always setup CORS Policies in case of Pre-flight Request!
		$this->setCors($allowedMethods);

		// Any POST, PUT, PATCH or DELETE request must contain a valid JSON in the Request Body!
		if (!$this->request->isGet() && !$this->request->isHead() && !$this->request->isOptions()) {
			// Validate if Request Body is a valid JSON.
			$this->validateRequestJson();
		}
	}


	/**
	 * Validates if User has been Granted Access to a particular Controller based on User Role (User Type).
	 * All Pre-flight (OPTIONS) Requests are Granted Access as they are not validated against Authentication!
	 *
	 * @param  array         $allowedUserRoles A list of User Roles permitted to use particular Controller.
	 * @throws HttpException                   In case if invalid JSON is sent or is completely missing.
	 */
	protected function validateAccess($allowedUserRoles = [])
	{
		// Check if Request Method is OPTIONS.
		if ($this->request->isOptions()) {
			// Skip validation for all OPTIONS Requests.
			return;
		}

		// Check if User Role is in the list of Permitted User Roles.
		if (in_array($this->user->type, $allowedUserRoles) === false) {
			// Throw Access Denied Exception if User Role is NOT Granted Access to the Controller.
			if (get_class($this->response) === 'PARCC\ADP\Responses\HtmlResponse') {
				// In case of HTML Response Type.
				throw new HttpException(
					'ADP-1104',
					403,
					'You are not authorized to access this page.',
					'Missing User Permission!',
					'PARCC\ADP\Responses\HtmlResponse'
				);

			} else {
				// In case of any other Response Type.
				throw new HttpException(
					'ADP-1105',
					403,
					'Access Denied.',
					'Missing User Permission!',
					get_class($this->response)
				);
			}
		}
	}


	/**
	 * Provides System Configuration. In addition, it caches retrieved Configuration in APCu Cache based on configured
	 * Lifetime.
	 *
	 * @param  string        $configurationName Name of the System Configuration to be retrieved.
	 * @return object                           Returns System Configuration as a JSON Object.
	 * @throws HttpException                    In case requested Configuration cannot be retrieved.
	 */
	protected function getSystemConfiguration($configurationName)
	{
		// Try to retrieve System Configuration from APCu Cache.
		$systemConfiguration = $this->apcCache->get($configurationName . '-' . $this->tenant->tenantId);

		// Check if System Configuration was successfully loaded (if it was previously cached).
		if (empty($systemConfiguration)) {
			// Try to retrieve requested System Configuration.
			/** @var System $systemConfiguration */
			$systemConfiguration = System::findFirst([
				'columns' => 'value',
				'conditions' => 'tenantId = ?1 AND name = ?2',
				'bind' => [
					1 => $this->tenant->tenantId,
					2 => $configurationName
				]
			]);

			// Check if System Configuration was successfully retrieved.
			if ($systemConfiguration === false) {
				// Log failure to load System Configuration.
				$this->logger->critical("System Configuration '{$configurationName}' Not Found!");

				// Throw Internal Error Exception if System Configuration was not found.
				throw new HttpException(
					'ADP-1102',
					500,
					'Internal Error.',
					'Test Assignment is Deactivated.',
					get_class($this->response)
				);
			}

			// Parse System Configuration.
			$systemConfiguration = $systemConfiguration->value;

			// Check if System Configuration is a valid JSON Array, if System Configuration belongs to Current Tenant
			// (for example Login Configuration and Test Driver Configuration must be valid JSON, but version is just a
			// simple string, so no validation is needed).
			if (!is_array(json_decode($systemConfiguration, true))) {
				// Log invalid System Configuration value.
				$this->logger->critical("System Configuration '{$configurationName}' is Empty or Invalid!");

				// Throw Internal Error Exception if System Configuration was not found.
				throw new HttpException(
					'ADP-1103',
					500,
					'Internal Error.',
					'Invalid Configuration!',
					get_class($this->response)
				);
			}

			// Cache System Configuration into APCu Cache with optional Tenant ID it belongs to.
			$this->apcCache->save(
				$configurationName . '-' . $this->tenant->tenantId,
				$systemConfiguration,
				$this->config->apc->configCacheLifetime
			);
		}

		// Return requested System Configuration.
		return $systemConfiguration;
	}


	/**
	 * If the Request contains a Body, it has to be a valid JSON. This parses the Request Body into a Standard Object,
	 * and in case it's not a valid JSON or is empty, it throws Data Exception.
	 *
	 * @throws HttpException In case if invalid JSON is sent or is completely missing.
	 */
	protected function validateRequestJson()
	{
		// If JSON Body could not be parsed, throw Data Exception. At this point it's safe to use posted JSON as is as
		// we're not further processing it yet, and the way json_decode() function is implemented it will always fail if
		// given string is not a valid JSON string. However, once we start parsing it, we need to further sanitize the
		// extracted parts separately!
		$jsonRequestBody = $this->request->getJsonRawBody();
		if (!is_object($jsonRequestBody) && !is_array($jsonRequestBody)) {
			// Throw Bad Request Exception if Request Body doesn't contain a valid JSON Object.
			throw new HttpException('ADP-1106', 400, 'Invalid Request.', 'Invalid JSON.', get_class($this->response));
		}
	}


	/**
	 * This is the additional setter for CORS Policies that are unique to specific Web Service may be set by its
	 * Controller. All the Basic CORS Policies should already be set by the Application!
	 * It is important to set CORS Policies as soon as possible, and always have them in place, even when no Route was
	 * matched the Request in order to be able to return proper 404 Responses or reports about Errors and Exceptions!
	 *
	 * Provides support for CORS (Cross-Origin Resource Sharing) Policy. Origin is allowed from all URLs. Setting it
	 * here using the Origin Header from the Request allows multiple Origins to be served. It is done this way instead
	 * of with a wildcard '*' because wildcard Requests are not supported when a Request needs Credentials.
	 * Wildcard '*' is only used as a fail-over option, but it will lead to Authentication failure if CORS is required!
	 *
	 * A Pre-flight Request is the special type of the Request when due to missing CORS Policies a Browser automatically
	 * initiates OPTIONS Request prior to proceeding with the original Request. If CORS Policies are correctly setup as
	 * a result of Pre-flight Request, it will automatically be followed by the intended Request. Therefore, all
	 * Controllers must support OPTIONS Requests! Further, once Pre-flight Request was successfully received, Browser
	 * will cache it for a period of time and will not repeat them as long as cached Response doesn't expire.
	 *
	 * Browser mostly have very low tolerance for 'Access-Control-Max-Age'. For example, Chrome doesn't allow it to be
	 * more than 10min!
	 *
	 * Base support for Routes like '/tests' that represent a Resource's base URL are:
	 *   GET, POST, OPTIONS, HEAD.
	 * Extended support for Routes like '/tests/123' that represent a specific resource are:
	 *   GET, PUT, PATCH, DELETE, OPTIONS, HEAD.
	 *
	 * @param string $allowedMethods Provides a list of supported Methods by each Web Service Controller.
	 */
	final protected function setCors($allowedMethods)
	{
		// Set additional CORS Policies.
		$this->response->setHeader('Access-Control-Allow-Methods', $allowedMethods);
	}


	/**
	 * This validates if a valid Test Key is being used with CORS as additional Authentication measure for when the
	 * Request is initiated by a Browser as a Client.
	 * Any Browser initiated Request must have Test Key in this custom Header! If CORS Policies are not put in place,
	 * Browser will not allow JavaScript to use X_REQUESTED_WITH custom Header!
	 *
	 * @param  string        $testKey Optional Test Key to validate against. If omitted, it should be present in the
	 *                                Request Header.
	 * @throws HttpException          In case of invalid Browser CORS Request or missing Test Key.
	 */
	final protected function validateBrowserCORS($testKey = null)
	{
		// Check if Test Key is provided.
		if (empty($testKey)) {
			// Retrieve Test Key from the Request Header.
			$testKey = base64_decode($this->request->getHeader('X_REQUESTED_WITH') ?
			                         $this->request->getHeader('X_REQUESTED_WITH') : '');

			// Check if Test Key is in expected format.
			// Test Key is 10-character uppercase string.
			if (!preg_match('/^[A-Z]{10}$/', $testKey)) {
				// Throw Authorization Exception if Test Key is in invalid format, is empty or is missing.
				throw new HttpException('ADP-1107', 401, 'Authorization Failed.', 'Invalid Test Key format.');
			}
		}

		// Try to retrieve referenced Test Session details.
		$this->testSession = TestSession::findFirst([
			'conditions' => 'tenantId = ?1 AND testKey = ?2',
			'bind' => [
				1 => $this->tenant->tenantId,
				2 => $testKey
			]
		]);

		// Check if Test Session was found, is Active and belongs to Current Tenant.
		if ($this->testSession === false) {
			// Throw Authentication Exception if Test Key was not found.
			if (get_class($this->response) === 'PARCC\ADP\Responses\HtmlResponse') {
				// In case of HTML Response Type.
				throw new HttpException(
					'ADP-1108',
					401,
					'You are not authorized to access this page.',
					'Test Key for Practice Test Not Found.',
					'PARCC\ADP\Responses\HtmlResponse'
				);

			} else {
				// In case of any other Response Type.
				throw new HttpException(
					'ADP-1109',
					401,
					'Authorization Failed.',
					'Test Key Not Found.',
					get_class($this->response)
				);
			}

		} elseif ($this->testSession->isActive === false) {
			// Throw Authentication Exception if Test Session is Deactivated.
			if (get_class($this->response) === 'PARCC\ADP\Responses\HtmlResponse') {
				// In case of HTML Response Type.
				throw new HttpException(
					'ADP-1110',
					403,
					'You are not authorized to access this page.',
					'Test Assignment for Practice Test is Deactivated.',
					'PARCC\ADP\Responses\HtmlResponse'
				);

			} else {
				// In case of any other Response Type.
				throw new HttpException(
					'ADP-1111',
					403,
					'Access Denied.',
					'Test Assignment is Deactivated.',
					get_class($this->response)
				);
			}
		}
	}


	/**
	 * Checks if Browser supports GZip Encoding, so compressed content can be returned whenever possible to improve
	 * download times and overall performance.
	 *
	 * @return boolean Returns TRUE if Browser supports GZip Encoding. Otherwise, it returns FALSE.
	 */
	protected function isGzipSupported()
	{
		// Parse the "Accepted-Encoding" provided by the Browser within Request Header.
		$acceptedEncoding = mb_strtolower($this->request->getHeader('Accept-Encoding'));

		// Check if Browser supports GZip Compression.
		if (mb_strpos($acceptedEncoding, 'gzip') !== false || mb_strpos($acceptedEncoding, 'deflate') !== false) {
			// In case Browser does support GZip Encoding.
			return true;

		} else {
			// In case Browser does not support GZip Encoding.
			return false;
		}
	}


	/**
	 * Updates Test Session Token Timestamp and Resets its Lifetime (Expiration Time).
	 * This is called by Content and Results Controllers after successful Authentication and validation are performed.
	 *
	 * @param boolean $isPracticeTest    Indicates if Test Session is for a Practice Test (@default=false).
	 * @param boolean $updateTestSession Indicates if Test Session should be automatically updated (@default=true).
	 */
	protected function refreshTestSessionToken($isPracticeTest = false, $updateTestSession = true)
	{
		// Update Test Session Token Expiration Timestamp based on the Test Type.
		if ($isPracticeTest === true) {
			// Update Test Session Token Expiration Timestamp for Practice Tests.
			// Assume that Token Expiration Timestamp for Practice Test will "never expire and set it far in the future.
			// As the maximum value for unsigned integer in MySQL is 4294967295, for convenience reasons we will round
			// it up, so the all Token Expiration Timestamps for Practice Tests will be set to the same Date and Time:
			// Tue, 02 Oct 2096 07:06:40 GMT.
			$this->testSession->tokenExpirationTimestamp = 4000000000;

			// Make sure to remove assigned Test Form Revision!
			// Test Form Revision should remain unassigned, so it can always be re-assigned during Login in order to be
			// able to always pull the most recent Test Battery Form Revision of the Practice Test.
			$this->testSession->parentTestFormRevisionId = null;

		} else {
			// Update Test Session Token Expiration Timestamp for non-Practice Tests.
			$this->testSession->tokenExpirationTimestamp = time() + (int) $this->config->Login->tokenLifetime;
		}

		// Check if Test Session should be updated.
		if ($updateTestSession === true) {
			// Update Updated by User property.
			$this->testSession->updatedByUserId = $this->user->userId;

			// Save Test Session.
			$this->testSession->update();
		}
	}


	/**
	 * Validates if Test Session appears to belong to a Practice Test.
	 * This check is done strictly based on Test Key Prefix and is compared against Configured Test Key Prefix if they
	 * match.
	 *
	 * @param  string  $testKey Test Key for referenced Test Session.
	 * @return boolean          Returns TRUE if Test is a Practice Test. Otherwise, it returns FALSE.
	 */
	protected function checkIfPracticeTest($testKey)
	{
		// Check if provided Test Key is for a Practice Test.
		if (mb_strpos($testKey, $this->config->app->practiceTestKeyPrefix) === 0) {
			// Return confirmation that Test is a Practice Test.
			return true;

		} else {
			// Return confirmation that Test is not a Practice Test.
			return false;
		}
	}


	/**
	 * OPTIONS Request Handler. This is the default OPTIONS Handler for all Controllers and is used for Pre-flight
	 * Requests only!
	 */
	public function preflightHandler()
	{
		// Make sure we at least return an empty array.
		$this->respond([]);
	}


	/**
	 * This is default Controller's Responder called by Methods in the Controllers that need to output results to the
	 * HTTP Response. It ensures that arrays conform to the patterns required by the Response Objects.
	 *
	 * @param  array         $response Array of records to format as returned output.
	 * @throws HttpException           In case of Invalid Response.
	 */
	protected function respond($response)
	{
		// Verify if Response is a valid array.
		if (is_array($response) === false) {
			// Throw Internal Error Exception in case generated Response is invalid. This should never happen!
			throw new HttpException('ADP-1112', 500, 'Internal Error.', 'Invalid Response!');
		}

		// Check if received Request was made using HEAD Method.
		if ($this->request->isHead()) {
			// Set indicator if received Request was made using HEAD Method.
			$this->response->setHead();
		}

		// Generate and send Response.
		/** @var mixed $this->response */
		$this->response->respond($response);
	}
}
