<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Controllers;

use PARCC\ADP\Exceptions\HttpException;
use PARCC\ADP\Models\TestContent;
use PARCC\ADP\Models\TestSession;
use PARCC\ADP\Models\User;
use PARCC\ADP\Services\AWS\S3Exception;

/**
 * Class ContentController
 *
 * This is the RESTful Controller for handling Client's Content Request functionality.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @property TestSession $testSession
 */
class ContentController extends BaseController
{

	/**
	 * Constructor.
	 *
	 * Sets the allowed Methods for Content Web Service and validates CORS Request.
	 * It also Grants Permission to be used only for Test Delivery.
	 *
	 * @internal param string $allowedMethods Provides a list of supported Methods by each Controller.
	 */
	public function __construct()
	{
		parent::__construct('GET, OPTIONS', true);

		// Construct the Anonymous Test Driver User. Because Content Web Service uses simple, Test Session Token based
		// Authorization and doesn't have any Authentication, it cannot determine who the User making a Request actually
		// is. Therefore, we have to rely on a dummy Anonymous User instead to Grant Access to the Web Service.
		$this->user = new User();
		$this->user->userId = 0;
		$this->user->username = 'Anonymous';
		$this->user->type = 'TD';

		// Grant Access only to Test Driver Users.
		$this->validateAccess(['TD']);
	}


	/**
	 * GET Request Handler. This handles Test Content Download.
	 *
	 * @param  string $token              Test Session Token.
	 * @param  string $testFormRevisionId Test Battery Form Revision ID.
	 * @param  string $contentFile        Optional File Resource (relative to Test Package root). If omitted, the Test
	 *                                    Definition File ('test.json') will be used! (@default='').
	 * @return array                      Response contains all requested records.
	 * @throws HttpException              In case invalid Content details are being received or Test Session Token has
	 *                                    expired.
	 */
	public function downloadContent($token, $testFormRevisionId, $contentFile = '')
	{
		// Try to retrieve Test Session details.
		$this->testSession = TestSession::findFirst([
			'conditions' => 'tenantId = ?1 AND token = ?2',
			'bind' => [
				1 => $this->tenant->tenantId,
				2 => $token
			]
		]);

		// Check if Test Session was successfully retrieved, is Active and belongs to Current Tenant.
		if ($this->testSession === false) {
			// Throw Authentication Exception if Test Session was not found.
			throw new HttpException(
				'ADP-1300',
				401,
				'Authorization Failed.',
				'Invalid Token.',
				'PARCC\ADP\Responses\HeaderResponse'
			);

		} elseif ($this->testSession->isActive === false) {
			// Throw Authentication Exception if Test Session is Deactivated.
			throw new HttpException(
				'ADP-1301',
				403,
				'Access Denied',
				'Test Assignment is Deactivated.',
				'PARCC\ADP\Responses\HeaderResponse'
			);

		}


		// Check if provided Test Session Token is for a Practice Test.
		$isPracticeTest = $this->checkIfPracticeTest($this->testSession->testKey);


		// Check if referenced Test Session Token and Test Form Revision match, but only check Test Form Revision if
		// Test Session is not for a Practice Test! Also check if Test Session has a valid Test Status.
		// In case Test, Test Form and/or Test Form Revision were Deactivated during the Test Session, Student will NOT
		// be interrupted and will be allowed to continue with the Test. This is ONLY validated during Login and if any
		// is Deactivated Login will not be allowed!
		if ($testFormRevisionId !== $this->testSession->parentTestFormRevisionId && $isPracticeTest === false) {
			// Throw Authentication Exception if Test Form Revision doesn't match.
			throw new HttpException(
				'ADP-1302',
				403,
				'Access Denied.',
				'Test Form Revision Not Found.',
				'PARCC\ADP\Responses\HeaderResponse'
			);

		} elseif ($this->testSession->status === 'Submitted') {
			// Throw Content Exception if Test is already Submitted.
			throw new HttpException(
				'ADP-1303',
				403,
				'Access Denied.',
				'Test is already Submitted.',
				'PARCC\ADP\Responses\HeaderResponse'
			);

		} elseif ($this->testSession->status === 'Completed') {
			// Throw Content Exception if Test is already Completed.
			throw new HttpException(
				'ADP-1304',
				403,
				'Access Denied.',
				'Test is already Completed.',
				'PARCC\ADP\Responses\HeaderResponse'
			);

		} elseif ($this->testSession->status === 'Canceled') {
			// Throw Content Exception if Test is Canceled.
			throw new HttpException(
				'ADP-1305',
				403,
				'Access Denied.',
				'Test is Canceled.',
				'PARCC\ADP\Responses\HeaderResponse'
			);

		} elseif ((float) $this->testSession->tokenExpirationTimestamp < time()) {
			// Throw Content Exception if Test Session Token has expired.
			throw new HttpException(
				'ADP-1306',
				401,
				'Authorization Failed.',
				'Token has Expired.',
				'PARCC\ADP\Responses\HeaderResponse'
			);
		}

		// Refresh Test Session Token Timestamp first if Test is not a Practice Test.
		if ($isPracticeTest === false) {
			// Update Test Session Token Expiration Timestamp for non-Practice Test.
			$this->refreshTestSessionToken();
		}

		// Check if specific Test Content Resource is requested.
		/** @var TestContent $testContent */
		if (empty($contentFile)) {
			// Retrieve Test Content details for the default Test Content Resource if no specific Test Content Resource
			// was referenced.
			$testContent = TestContent::findFirst([
				'columns' => 'path, isActive',
				'conditions' => 'tenantId = ?1 AND parentTestFormRevisionId = ?2 AND isDefault = 1',
				'bind' => [
					1 => $this->tenant->tenantId,
					2 => $testFormRevisionId
				]
			]);

		} else {
			// Retrieve Test Content details for the referenced Test Content Resource.
			$testContent = TestContent::findFirst([
				'columns' => 'path, isActive',
				'conditions' => 'tenantId = ?1 AND parentTestFormRevisionId = ?2 AND path = ?3',
				'bind' => [
					1 => $this->tenant->tenantId,
					2 => $testFormRevisionId,
					3 => $contentFile
				]
			]);
		}

		// Check if Test Content details were successfully retrieved, if Test Content Resource is Active and belongs to
		// Current Tenant.
		if ($testContent === false) {
			// Throw Content Exception if Test Content details were not retrieved successfully.
			throw new HttpException(
				'ADP-1307',
				404,
				'Data Not Found.',
				'Content Not Found.',
				'PARCC\ADP\Responses\HeaderResponse'
			);

		} elseif ($testContent->isActive === false) {
			// Throw Content Exception if Test Session is Deactivated.
			throw new HttpException(
				'ADP-1308',
				403,
				'Access Denied.',
				'Test Content is Deactivated.',
				'PARCC\ADP\Responses\HeaderResponse'
			);
		}


		// Generate AWS CloudFront URL.
		$cloudFrontUrl = ($this->useHttps === true ? 'https://' : 'http://') .
		                 $this->config->Content->cloudFrontHost . '/' .
		                 $testFormRevisionId . '/' .
		                 $testContent->path;

		// Check if Browser supports GZip Compression.
		if ($this->isGzipSupported() === true) {
			// Check if a GZipped version should be delivered for the requested Test Content File Resource (if its File
			// Extension is in the configured list of MIME Types that can be delivered as compressed).
			if (in_array(
				mb_strtolower(pathinfo($testContent->path, PATHINFO_EXTENSION)),
				(array) $this->config->s3->gzipFileExtensions
			)) {
				// Append GZip File Extension for the compressed version of the same Test Content File Resource.
				$cloudFrontUrl .= '.gz';
			}
		}

		// Check if generated AWS CloudFront URL should be locked to a single IP Address.
		if ($this->config->Content->signedUrlLockToIp === true) {
			// Try to parse Client's IP Address.
			$lockedIpAddress = $this->clientIpAddress;

			// Check if Client's IP Address was successfully parsed.
			if (empty($lockedIpAddress)) {
				// Log an Error if Client's IP Address cannot be parsed and proceed without locking AWS CloudFront URL
				// to a single IP Address.
				$this->logger->warning("S3Exception: Failed to parse Originating IP Address. Generated " .
				                       "CloudFront Signed URL will NOT be locked to a single IP Address!");
			}

		} else {
			// In case AWS CloudFront Signed URL should not be locked to a single IP Address.
			$lockedIpAddress = null;
		}


		/*
		 * Generate AWS CloudFront Signed URL.
		 * It should NOT include Content Prefix for AWS S3 Bucket as that should be automatically handled by AWS
		 * CloudFront Distribution Configuration!!!
		 * AWS CloudFront Distribution's Origin should be configured to point directly to AWS S3 Bucket Prefix, so it is
		 * omitted from the AWS CloudFront URL for security reasons to limit access only to certain files within the AWS
		 * S3 Bucket instead of all of them!
		 */
		try {
			// Try to generate AWS CloudFront Signed URL as the Response.
			$response = $this->s3->getCloudFrontSignedUrl(
				$cloudFrontUrl,
				dirname(dirname(__DIR__)) . $this->config->Content->sshPrivateKey,
				$this->config->Content->sshKeyPairId,
				$this->config->Content->signedUrlLifetime,
				$lockedIpAddress
			);

		} catch (S3Exception $exception) {
			// Log AWS S3 Service Exception if AWS CloudFront URL failed to be signed.
			$this->logger->emergency("S3Exception: " . $exception->getFile() . "#" . $exception->getLine() . "\n" .
			                         "Failed to generate CloudFront Signed URL!\n" . $exception->getMessage());

			// Throw AWS S3 Exception if AWS CloudFront URL failed to be signed.
			throw new HttpException(
				'ADP-' . $exception->getCode(),
				500,
				'Internal Error.',
				'Unable to provide S3 Content.',
				'PARCC\ADP\Responses\HeaderResponse'
			);
		}


		// Send 302 Temporary Redirect to AWS CloudFront Signed URL as a Response.
		$this->respond([$response]);
	}
}
