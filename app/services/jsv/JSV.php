<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Services\JSV;

use stdClass;

/**
 * Class JSV
 *
 * This is JSON Schema Validator Service. It is used to validate if given JSON file complies against referenced JSON
 * Schema. It uses version 4 of JSON Schemas.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
final class JSV
{

	/**
	 * JSON Errors.
	 */
	// General Errors.
	const ERROR_INVALID_TYPE = 11;
	const ERROR_ENUM_MISMATCH = 12;
	const ERROR_ANY_OF_MISSING = 13;
	const ERROR_ONE_OF_MISSING = 14;
	const ERROR_ONE_OF_MULTIPLE = 15;
	const ERROR_NOT_PASSED = 16;
	// Numeric Errors.
	const ERROR_NUMBER_MULTIPLE_OF = 21;
	const ERROR_NUMBER_MINIMUM = 22;
	const ERROR_NUMBER_MINIMUM_EXCLUSIVE = 23;
	const ERROR_NUMBER_MAXIMUM = 24;
	const ERROR_NUMBER_MAXIMUM_EXCLUSIVE = 25;
	// String Errors.
	const ERROR_STRING_LENGTH_SHORT = 31;
	const ERROR_STRING_LENGTH_LONG = 32;
	const ERROR_STRING_PATTERN = 33;
	// Object Errors.
	const ERROR_OBJECT_PROPERTIES_MINIMUM = 41;
	const ERROR_OBJECT_PROPERTIES_MAXIMUM = 42;
	const ERROR_OBJECT_REQUIRED = 43;
	const ERROR_OBJECT_ADDITIONAL_PROPERTIES = 44;
	const ERROR_OBJECT_DEPENDENCY_KEY = 45;
	// Array Errors.
	const ERROR_ARRAY_LENGTH_SHORT = 51;
	const ERROR_ARRAY_LENGTH_LONG = 52;
	const ERROR_ARRAY_UNIQUE = 53;
	const ERROR_ARRAY_ADDITIONAL_ITEMS = 54;

	/**
	 * @var mixed $data.
	 */
	private $data;
	/**
	 * @var mixed $schema.
	 */
	private $schema;
	/**
	 * @var boolean $stopOnFirstError.
	 */
	private $stopOnFirstError;
	/**
	 * @var boolean $coerce.
	 */
	private $coerce;
	/**
	 * @var boolean $stopValidation.
	 */
	private $stopValidation;

	/**
	 * @var boolean $isValid.
	 */
	public $isValid;
	/**
	 * @var array $errors.
	 */
	public $errors;


	/**
	 * Constructor.
	 *
	 * It automatically validates recursively all child JSON Parts and collects any found Errors.
	 *
	 * @param mixed   &$data            JSON object that needs to be validated against JSON Schema.
	 * @param mixed   $schema           JSON Schema that given JSON needs to be validated against.
	 * @param boolean $stopOnFirstError Indicates if JSON validation should stop as soon as first Error is found
	 *                                  (@default=true).
	 * @param boolean $coerce           Indicates if potentially invalid JSON should be made valid first, if possible
	 *                                  (@default=false).
	 */
	public function __construct(&$data = null, $schema = null, $stopOnFirstError = true, $coerce = false)
	{
		// Initialize JSV Properties.
		$this->data =& $data;
		$this->schema =& $schema;
		$this->stopOnFirstError = $stopOnFirstError;
		$this->coerce = $coerce;

		$this->isValid = true;
		$this->errors = [];

		// Validate Data Types.
		if ($this->checkTypes() === false) {
			// Stop further validation as soon as first Error is found, if necessary.
			return;
		}
		// Validate Enumerated Types.
		if ($this->checkEnum() === false) {
			// Stop further validation as soon as first Error is found, if necessary.
			return;
		}
		// Validate Objects.
		if ($this->checkObject() === false) {
			// Stop further validation as soon as first Error is found, if necessary.
			return;
		}
		// Validate Arrays.
		if ($this->checkArray() === false) {
			// Stop further validation as soon as first Error is found, if necessary.
			return;
		}
		// Validate Strings.
		if ($this->checkString() === false) {
			// Stop further validation as soon as first Error is found, if necessary.
			return;
		}
		// Validate Numbers.
		if ($this->checkNumber() === false) {
			// Stop further validation as soon as first Error is found, if necessary.
			return;
		}
		// Validate Composite Data Types.
		if ($this->checkComposite() === false) {
			// Stop further validation as soon as first Error is found, if necessary.
			return;
		}
	}


	/**
	 * Validates given JSON Part and all of its children Parts recursively, and returns the whole JSV class which
	 * contains all found Errors.
	 *
	 * @param  mixed   &$data            JSON object that needs to be validated against JSON Schema.
	 * @param  mixed   $schema           JSON Schema that given JSON needs to be validated against.
	 * @param  boolean $stopOnFirstError Indicates if JSON validation should stop as soon as first Error is found
	 *                                   (@default=true).
	 * @param  boolean $coerce           Indicates if potentially invalid JSON should be made valid first, if possible
	 *                                   (@default=false).
	 * @return JSV                       Returns the whole new JSV instance.
	 */
	public function validate(&$data, $schema, $stopOnFirstError = true, $coerce = false)
	{
		// Return new JSV instance for given JSON Part.
		return new JSV($data, $schema, $stopOnFirstError, $coerce);
	}


	/**
	 * Validates given JSON Part and all of its children Parts recursively, and returns simple boolean if given JSON
	 * Part is valid or not.
	 *
	 * @param  mixed   &$data            JSON object that needs to be validated against JSON Schema.
	 * @param  mixed   $schema           JSON Schema that given JSON needs to be validated against.
	 * @param  boolean $stopOnFirstError Indicates if JSON validation should stop as soon as first Error is found
	 *                                   (@default=true).
	 * @param  boolean $coerce           Indicates if potentially invalid JSON should be made valid first, if possible
	 *                                   (@default=false).
	 * @return boolean                   Returns TRUE if given JSON is valid. Otherwise, it returns FALSE.
	 */
	/*
	public function isValid(&$data, $schema, $stopOnFirstError = true, $coerce = false)
	{
		$jsv = new JSV($data, $schema, $stopOnFirstError, $coerce);

		// Return boolean if given JSON Part is valid.
		return $jsv->isValid;
	}
	*/


	/**
	 * Validates given JSON Part and all of its children Parts recursively, and returns the whole JSV class which
	 * contains all found Errors. It also tries to coerce given JSON to become valid in case its data is not 100%
	 * correct.
	 *
	 * @param  mixed   &$data            JSON object that needs to be validated against JSON Schema.
	 * @param  mixed   $schema           JSON Schema that given JSON needs to be validated against.
	 * @param  boolean $stopOnFirstError Indicates if JSON validation should stop as soon as first Error is found
	 *                                   (@default=true).
	 * @return JSV                       Returns the whole new JSV instance.
	 */
	/*
	public function coerce(&$data, $schema, $stopOnFirstError = true)
	{
		// Check if given Data is a valid JSON (either Object or Array).
		if (is_object($data) || is_array($data)) {
			// Try to coerce invalid data by serializing it first and then de-serializing it.
			$data = unserialize(serialize($data));
		}

		// Return new JSV instance with forced coercion for given JSON Part.
		return new JSV($data, $schema, $stopOnFirstError, true);
	}
	*/


	/**
	 * Parses found Errors and appends them to the existing list of previously found Errors.
	 *
	 * @param integer $errorCode    JSV Error Code.
	 * @param string  $dataPath     JSON Object path to the JSON Part being invalid.
	 * @param string  $schemaPath   JSON Schema path to the JSON Part being invalid.
	 * @param string  $errorMessage Error Message.
	 */
	private function error($errorCode, $dataPath, $schemaPath, $errorMessage)
	{
		// Set current JSV instance as being invalid.
		$this->isValid = false;

		// Append new Error.
		$this->errors[] = (object) [
			'errorCode' => $errorCode,
			'dataPath' => $dataPath,
			'schemaPath' => $schemaPath,
			'errorMessage' => $errorMessage
		];

		// Check if validation should stop as soon as the first Error is being found.
		if ($this->stopOnFirstError) {
			// Set indicator to stop immediately any further validation.
			$this->stopValidation = true;
		}
	}


	/**
	 * Traverses recursively through given JSON Parts and validates if they are equal.
	 *
	 * @param  mixed   $a 1st JSON Part (can be any primitive type).
	 * @param  mixed   $b 2nd JSON Part (can be any primitive type).
	 * @return boolean    Returns TRUE in case given JSON Parts are equal. Otherwise, it returns FALSE.
	 */
	private function recursiveEqual($a, $b)
	{
		// Check if given JSON Parts are Objects.
		if (is_object($a)) {
			if (!is_object($b)) {
				return false;
			}
			// Traverse through child Parts of 1st given JSON Part.
			foreach ($a as $key => $value) {
				if (!isset($b->$key)) {
					return false;
				}
				if (!$this->recursiveEqual($value, $b->$key)) {
					return false;
				}
			}
			// Traverse through child Parts of 2nd given JSON Part.
			foreach ($b as $key => $value) {
				if (!isset($a->$key)) {
					return false;
				}
			}
			return true;
		} elseif (is_array($a)) {
			// In case given JSON Parts are Arrays.
			if (!is_array($b)) {
				return false;
			}
			// Traverse through child Parts of 1st given JSON Part.
			foreach ($a as $key => $value) {
				if (!isset($b[$key])) {
					return false;
				}
				if (!$this->recursiveEqual($value, $b[$key])) {
					return false;
				}
			}
			// Traverse through child Parts of 2nd given JSON Part.
			foreach ($b as $key => $value) {
				if (!isset($a[$key])) {
					return false;
				}
			}
			return true;
		}

		// In case of any other primitive type, simply check if given JSON Parts are identical.
		return ($a === $b);
	}


	/**
	 * Sets Data Type for given Property (Key). It's used to check required JSON Properties.
	 *
	 * @param  mixed   $key Key for Property which Data Type needs to be set based on current JSON Schema.
	 * @return boolean      Returns TRUE if referenced Property was successfully set. Otherwise, it returns FALSE if
	 *                      referenced Property was not found in JSON Schema.
	 */
	private function createValueForProperty($key)
	{
		$schema = null;

		// Check if JSON Schema contains referenced Key.
		if (isset($this->schema->properties->$key)) {
			$schema = $this->schema->properties->$key;

		} elseif (isset($this->schema->patternProperties)) {
			// Loop through all Pattern Properties and check if any contains referenced Key.
			foreach ($this->schema->patternProperties as $pattern => $subSchema) {
				if (preg_match('/' . str_replace('/', '\\/', $pattern) . '/', $key)) {
					$schema = $subSchema;

					break;
				}
			}
		}

		// In case Key was not found, use Additional Properties, if they exist.
		if (!$schema && isset($this->schema->additionalProperties)) {
			$schema = $this->schema->additionalProperties;
		}

		// Check if any JSON Sub-Schema was found.
		if ($schema) {
			// Check if JSON Schema contains Default Property.
			if (isset($schema->default)) {
				// Use Default Property.
				$this->data->$key = unserialize(serialize($schema->default));
				return true;
			}

			// Check if JSON Schema contains Data Types.
			if (isset($schema->type)) {
				$types = (is_array($schema->type) ? $schema->type : [$schema->type]);

				// Loop through basic Data Types and check if any matches given Key.
				if (in_array('null', $types)) {
					$this->data->$key = null;
				} elseif (in_array('boolean', $types)) {
					$this->data->$key = true;
				} elseif (in_array('integer', $types) || in_array('number', $types)) {
					$this->data->$key = 0;
				} elseif (in_array('string', $types)) {
					$this->data->$key = '';
				} elseif (in_array('object', $types)) {
					$this->data->$key = new stdClass();
				} elseif (in_array('array', $types)) {
					$this->data->$key = [];
				} else {
					return false;
				}
			}

			return true;
		}

		return false;
	}


	/**
	 * Validates given JSON Sub-Part and all of its children Parts recursively, and returns the whole JSV class which
	 * contains all found Errors.
	 *
	 * @param  mixed   &$data            JSON object that needs to be validated against JSON Schema.
	 * @param  mixed   $schema           JSON Schema that given JSON needs to be validated against.
	 * @param  boolean $coerce           Indicates if potentially invalid JSON should be made valid first, if possible
	 *                                   (@default=false).
	 * @return JSV                       Returns the whole new JSV instance.
	 */
	private function getSubResult(&$data, $schema, $coerce = false)
	{
		// Return new JSV instance for given JSON Sub-Part.
		return new JSV($data, $schema, $this->stopOnFirstError, ($coerce && $this->coerce));
	}


	/**
	 * Appends all Errors found in JSON Sub-Part to the parent JSV instance.
	 *
	 * @param JSV    &$subResult   JSV instance of JSON Sub-Part.
	 * @param string $dataPrefix   JSON Sub-Part Prefix.
	 * @param string $schemaPrefix JSON Schema Prefix for given JSON Sub-Part.
	 */
	private function appendSubResultErrors(&$subResult, $dataPrefix, $schemaPrefix)
	{
		// Check if reference JSON Sub-Part is invalid.
		if ($subResult->isValid === false) {
			// Set parent JSON Part as invalid.
			$this->isValid = false;

			// Loop through all found Errors for referenced JSON Sub-Part and append them to parent's JSON Part Errors.
			foreach ($subResult->errors as $error) {
				$error->dataPath = $dataPrefix . $error->dataPath;
				$error->schemaPath = $schemaPrefix . $error->schemaPath;

				$this->errors[] = $error;
			}

			// Check if validation should stop as soon as the first Error is being found.
			if ($this->stopOnFirstError) {
				// Set indicator to stop immediately any further validation.
				$this->stopValidation = true;
			}
		}
	}


	/**
	 * Concatenates given list of Parts and returns the result. It's used to combine Keys of JSON Sub-Part Prefixes with
	 * their parent's Part Prefix.
	 *
	 * @param  array  $parts List of Parts (Keys) that need to be combined.
	 * @return string        Returns concatenated Key.
	 */
	private function combineParts($parts)
	{
		$result = '';
		// Loop through all given Parts and concatenate them.
		foreach ($parts as $part) {
			$part = strtr($part, ['~' => '~0']);
			$part = strtr($part, ['/' => '~1']);
			$result .= '/' . $part;
		}

		return $result;
	}


	/**
	 * Validates Data Types.
	 *
	 * @return bool Returns TRUE if current JSV instance contains all valid Data Types. Otherwise, it returns FALSE.
	 */
	private function checkTypes()
	{
		// Check if JSON Schema contains any Data Types.
		if (isset($this->schema->type)) {
			// Parse all Data Types.
			$types = $this->schema->type;

			if (!is_array($types)) {
				$types = [$types];
			}

			// Loop through all Data Types and try to match current JSON Part Data Type.
			foreach ($types as $type) {
				if ($type === 'object' && is_object($this->data)) {
					return true;
				} elseif ($type === 'array' && is_array($this->data)) {
					return true;
				} elseif ($type === 'string' && is_string($this->data)) {
					return true;
				} elseif ($type === 'number' && !is_string($this->data) && is_numeric($this->data)) {
					return true;
				} elseif ($type === 'integer' && !is_string($this->data) && preg_match('/^-?\d+$/', $this->data)) {
					return true;
				} elseif ($type === 'boolean' && is_bool($this->data)) {
					return true;
				} elseif ($type === 'null' && is_null($this->data)) {
					return true;
				}
			}

			// Check if JSON Part requires coercion.
			if ($this->coerce) {
				// Loop through all Data Types and try to coerce current JSON Part to match current JSON Part Data Type.
				foreach ($types as $type) {
					if ($type === 'number') {
						if (is_numeric($this->data)) {
							$this->data = (float) $this->data;
							return true;
						} elseif (is_bool($this->data)) {
							$this->data = ($this->data === true ? 1 : 0);
							return true;
						}
					} elseif ($type === 'integer') {
						if ((int) $this->data === $this->data) {
							$this->data = (int) $this->data;
							return true;
						}
					} elseif ($type === 'string') {
						if (is_numeric($this->data)) {
							$this->data = (string) $this->data;
							return true;
						} elseif (is_bool($this->data)) {
							$this->data = (($this->data) === true ? 'true' : 'false');
							return true;
						} elseif (is_null($this->data)) {
							$this->data = '';
							return true;
						}
					} elseif ($type === 'boolean') {
						if (is_numeric($this->data)) {
							$this->data = ($this->data !== '0');
							return true;
						} elseif ($this->data === 'yes' || $this->data === 'true') {
							$this->data = true;
							return true;
						} elseif ($this->data === 'no' || $this->data === 'false') {
							$this->data = false;
							return true;
						} elseif (is_null($this->data)) {
							$this->data = false;
							return true;
						}
					}
				}
			}

			// In case Data Type was not matches, try to determine which exact Data Type it is.
			$type = gettype($this->data);
			if ($type === 'double') {
				$type = (preg_match('/^-?\d+$/', $this->data) ? 'integer' : 'number');
			} elseif ($type === 'NULL') {
				$type = 'null';
			}

			// Trigger an Error if no valid Data Type was found.
			$this->error(self::ERROR_INVALID_TYPE, '', '/type', "Invalid Type: {$type}.");

			// Immediately stop further validation, if necessary.
			if ($this->stopValidation) {
				return false;
			}
		}

		return true;
	}


	/**
	 * Validates Enumerated Types.
	 *
	 * @return bool Returns TRUE if current JSV instance contains all valid Enumerated Types. Otherwise, it returns
	 *              FALSE.
	 */
	private function checkEnum()
	{
		// Check if JSON Schema contains any Enumerated Types.
		if (isset($this->schema->enum)) {
			// Loop through all Enumerated Types and try to match current JSON Part.
			foreach ($this->schema->enum as $option) {
				// Check if current JSON Part is equal to current Enumerated Type, including all of their JSON
				// Sub-Parts.
				if ($this->recursiveEqual($this->data, $option)) {
					return true;
				}
			}

			// Trigger an Error if no valid Enumerated Type was found.
			$this->error(self::ERROR_ENUM_MISMATCH, '', '/enum', 'Value must be one of the Enum Options.');

			// Immediately stop further validation, if necessary.
			if ($this->stopValidation) {
				return false;
			}
		}

		return true;
	}


	/**
	 * Validates Objects.
	 *
	 * @return bool Returns TRUE if current JSV instance contains all valid Objects. Otherwise, it returns FALSE.
	 */
	private function checkObject()
	{
		// Check if current JSON Part is a valid Object.
		if (is_object($this->data)) {
			// Check if JSON Schema contains any Required Properties.
			if (isset($this->schema->required)) {
				// Loop through all Required Properties and check if current JSON Part contains all of them.
				foreach ($this->schema->required as $index => $key) {
					if (!array_key_exists($key, (array) $this->data)) {
						// In case Required Property was not found, check again with coerced version of the current JSON
						// Part.
						if ($this->coerce && $this->createValueForProperty($key)) {
							continue;
						}

						// Trigger an Error if any Required Property is missing.
						$this->error(
							self::ERROR_OBJECT_REQUIRED,
							'',
							"/required/{$index}",
							"Missing Required Property: {$key}."
						);

						// Immediately stop further validation, if necessary.
						if ($this->stopValidation) {
							return false;
						}
					}
				}
			}

			// Keep track of checked Properties for current JSON Part.
			$checkedProperties = [];

			// Check if JSON Schema for current JSON Part contains any Properties.
			if (isset($this->schema->properties)) {
				// Loop through all Properties and check if they are valid for current JSON Part.
				foreach ($this->schema->properties as $key => $subSchema) {
					$checkedProperties[$key] = true;

					// Check if current JSON Part contains current Property.
					if (array_key_exists($key, (array) $this->data)) {
						// Validate all JSON Sub-Parts.
						$subResult = $this->getSubResult($this->data->$key, $subSchema);
						// Append all Errors found in JSON Sub-Parts.
						$this->appendSubResultErrors(
							$subResult,
							$this->combineParts([$key]),
							$this->combineParts(['properties', $key])
						);
					}

					// Immediately stop further validation, if necessary.
					if ($this->stopValidation) {
						return false;
					}
				}
			}

			// Check if JSON Schema for current JSON Part contains any Pattern Properties.
			if (isset($this->schema->patternProperties)) {
				// Loop through all Pattern Properties and check if they are valid for current JSON Part.
				foreach ($this->schema->patternProperties as $pattern => $subSchema) {
					/** @noinspection PhpUnusedLocalVariableInspection */
					foreach ($this->data as $key => &$subValue) {
						// Check if current JSON Part contains the current Pattern Property.
						if (preg_match('/' . str_replace('/', '\\/', $pattern) . '/', $key)) {
							$checkedProperties[$key] = true;

							// Validate all JSON Sub-Parts.
							$subResult = $this->getSubResult($this->data->$key, $subSchema);
							// Append all Errors found in JSON Sub-Parts.
							$this->appendSubResultErrors(
								$subResult,
								$this->combineParts([$key]),
								$this->combineParts(['patternProperties', $pattern])
							);
						}

						// Immediately stop further validation, if necessary.
						if ($this->stopValidation) {
							return false;
						}
					}
				}
			}

			// Check if JSON Schema for current JSON Part contains any Additional Properties.
			if (isset($this->schema->additionalProperties)) {
				// Retrieve all Additional Properties for current JSON Part.
				$additionalProperties = $this->schema->additionalProperties;
				// Loop through all Properties and check if they are valid for current JSON Part.
				foreach ($this->data as $key => &$subValue) {
					// Check if current JSON Part contains the current Additional Property.
					if (isset($checkedProperties[$key])) {
						continue;
					}

					// Check if Additional Properties are not defined in JSON Schema.
					if (!$additionalProperties) {
						// Trigger an Error if no Additional Properties are found, but are not allowed.
						$this->error(
							self::ERROR_OBJECT_ADDITIONAL_PROPERTIES,
							$this->combineParts([$key]),
							'/additionalProperties',
							'Additional Properties Not Allowed.'
						);

					} elseif (is_object($additionalProperties)) {
						// In case of Object, validate all JSON Sub-Parts.
						$subResult = $this->getSubResult($subValue, $additionalProperties);
						// Append all Errors found in JSON Sub-Parts.
						$this->appendSubResultErrors($subResult, $this->combineParts([$key]), '/additionalProperties');
					}

					// Immediately stop further validation, if necessary.
					if ($this->stopValidation) {
						return false;
					}
				}
			}

			// Check if JSON Schema for current JSON Part contains any Dependencies.
			if (isset($this->schema->dependencies)) {
				// Loop through all Dependencies.
				foreach ($this->schema->dependencies as $key => $dependency) {
					// Check if Key was found for current Dependency.
					if (!isset($this->data->$key)) {
						continue;
					}

					// Check if current Dependency is an Object.
					if (is_object($dependency)) {
						// Validate all JSON Sub-Parts.
						$subResult = $this->getSubResult($this->data, $dependency);
						// Append all Errors found in JSON Sub-Parts.
						$this->appendSubResultErrors($subResult, '', $this->combineParts(['dependencies', $key]));

					} elseif (is_array($dependency)) {
						// Loop through all Elements in case current Dependency is an Array.
						foreach ($dependency as $index => $dependencyKey) {
							// Check if Key was found for current Dependency.
							if (!isset($this->data->$dependencyKey)) {
								// Trigger an Error if no valid Dependency was found.
								$this->error(
									self::ERROR_OBJECT_DEPENDENCY_KEY,
									'',
									$this->combineParts(['dependencies', $key, $index]),
									"Property {$key} Depends on {$dependencyKey}."
								);

								// Immediately stop further validation, if necessary.
								if ($this->stopValidation) {
									return false;
								}
							}
						}

					} else {
						// Check if current Dependency is a different Data Type.
						if (!isset($this->data->$dependency)) {
							// Trigger an Error if no valid Dependency was found.
							$this->error(
								self::ERROR_OBJECT_DEPENDENCY_KEY,
								'',
								$this->combineParts(['dependencies', $key]),
								"Property {$key} Depends on {$dependency}."
							);
						}
					}

					// Immediately stop further validation, if necessary.
					if ($this->stopValidation) {
						return false;
					}
				}
			}

			// Check if JSON Schema for current JSON Part contains any Minimum Properties.
			if (isset($this->schema->minProperties)) {
				// Check if current JSON Part has less Properties than defined Minimum Property.
				if (count(get_object_vars($this->data)) < $this->schema->minProperties) {
					// Trigger an Error if no Minimum Properties were found.
					$this->error(
						self::ERROR_OBJECT_PROPERTIES_MINIMUM,
						'',
						'/minProperties',
						($this->schema->minProperties === 1 ? 'Object cannot be empty.' :
							"Object must have at least {$this->schema->minProperties} Defined Properties.")
					);

					// Immediately stop further validation, if necessary.
					if ($this->stopValidation) {
						return false;
					}
				}
			}

			// Check if JSON Schema for current JSON Part contains any Maximum Properties.
			if (isset($this->schema->maxProperties)) {
				// Check if current JSON Part has more Properties than defined Maximum Property.
				if (count(get_object_vars($this->data)) > $this->schema->maxProperties) {
					// Trigger an Error if no Maximum Properties were found.
					$this->error(
						self::ERROR_OBJECT_PROPERTIES_MAXIMUM,
						'',
						'/minProperties',
						($this->schema->maxProperties === 1 ? 'Object cannot have more than one Defined Property.' :
							"Object cannot have more than {$this->schema->maxProperties} Defined Properties.")
					);

					// Immediately stop further validation, if necessary.
					if ($this->stopValidation) {
						return false;
					}
				}
			}
		}

		return true;
	}


	/**
	 * Validates Arrays.
	 *
	 * @return bool Returns TRUE if current JSV instance contains all valid Arrays. Otherwise, it returns FALSE.
	 */
	private function checkArray()
	{
		// Check if JSON Part is a valid Array.
		if (is_array($this->data)) {
			// Check if JSON Schema contains any Items.
			if (isset($this->schema->items)) {
				// Retrieve Items from JSON Schema for current JSON Part.
				$items = $this->schema->items;

				// Check if Items are an Array.
				if (is_array($items)) {
					// Loop through all Items.
					foreach ($this->data as $index => &$subData) {
						// Check if Item Index is a valid number.
						if (!is_numeric($index)) {
							// Trigger an Error if an invalid Item Index was found.
							$this->error(
								self::ERROR_ARRAY_ADDITIONAL_ITEMS,
								"/{$index}",
								'/additionalItems',
								'Arrays must only be Numerically-Indexed.'
							);
						}

						// Check if Item for given Index exists.
						if (isset($items[$index])) {
							// Validate all JSON Sub-Parts.
							$subResult = $this->getSubResult($subData, $items[$index]);
							// Append all Errors found in JSON Sub-Parts.
							$this->appendSubResultErrors($subResult, "/{$index}", "/items/{$index}");

						} elseif (isset($this->schema->additionalItems)) {
							// Retrieve Additional Items, if found.
							$additionalItems = $this->schema->additionalItems;

							// Check if Additional Items are successfully retrieved.
							if (!$additionalItems) {
								// Trigger an Error if no valid Additional Items were found.
								$this->error(
									self::ERROR_ARRAY_ADDITIONAL_ITEMS,
									"/{$index}",
									'/additionalItems',
									"Additional Items (Index " . count($items) . " or more) are not allowed."
								);

							} elseif ($additionalItems !== true) {
								// Validate all JSON Sub-Parts.
								$subResult = $this->getSubResult($subData, $additionalItems);
								// Append all Errors found in JSON Sub-Parts.
								$this->appendSubResultErrors($subResult, "/{$index}", '/additionalItems');
							}
						}

						// Immediately stop further validation, if necessary.
						if ($this->stopValidation) {
							return false;
						}
					}

				} else {
					// Loop through all Object Items.
					foreach ($this->data as $index => &$subData) {
						// Check if Item Index is a valid number.
						if (!is_numeric($index)) {
							// Trigger an Error if an invalid Item Index was found.
							$this->error(
								self::ERROR_ARRAY_ADDITIONAL_ITEMS,
								"/{$index}",
								'/additionalItems',
								'Arrays must only be Numerically-Indexed.'
							);
						}

						// Validate all JSON Sub-Parts.
						$subResult = $this->getSubResult($subData, $items);
						// Append all Errors found in JSON Sub-Parts.
						$this->appendSubResultErrors($subResult, "/{$index}", '/items');

						// Immediately stop further validation, if necessary.
						if ($this->stopValidation) {
							return false;
						}
					}
				}
			}

			// Check if JSON Schema for current JSON Part contains any Minimum Items.
			if (isset($this->schema->minItems)) {
				// Check if current JSON Part has less Items than defined Minimum Items.
				if (count($this->data) < $this->schema->minItems) {
					// Trigger an Error if no Minimum Items were found.
					$this->error(
						self::ERROR_ARRAY_LENGTH_SHORT,
						'',
						'/minItems',
						"Array is too short (must have at least {$this->schema->minItems} Items)."
					);

					// Immediately stop further validation, if necessary.
					if ($this->stopValidation) {
						return false;
					}
				}
			}

			// Check if JSON Schema for current JSON Part contains any Maximum Items.
			if (isset($this->schema->maxItems)) {
				// Check if current JSON Part has more Items than defined Maximum Items.
				if (count($this->data) > $this->schema->maxItems) {
					// Trigger an Error if no Maximum Items were found.
					$this->error(
						self::ERROR_ARRAY_LENGTH_LONG,
						'',
						'/maxItems',
						"Array is too long (cannot have more than {$this->schema->maxItems} Items)."
					);

					// Immediately stop further validation, if necessary.
					if ($this->stopValidation) {
						return false;
					}
				}
			}

			// Check if JSON Schema for current JSON Part contains any Unique Items.
			if (isset($this->schema->uniqueItems)) {
				// Loop through all Items and check if all Unique Items are indeed unique in the current JSON Part.
				foreach ($this->data as $indexA => $itemA) {
					foreach ($this->data as $indexB => $itemB) {
						// Compare Item Indexes.
						if ($indexA < $indexB) {
							// Compare Items.
							if ($this->recursiveEqual($itemA, $itemB)) {
								// Trigger an Error if Unique Items are not unique.
								$this->error(
									self::ERROR_ARRAY_UNIQUE,
									'',
									'/uniqueItems',
									"Array Items must be Unique (Items $indexA and $indexB are equal)."
								);

								// Immediately stop further validation, if necessary.
								if ($this->stopValidation) {
									return false;
								}
							}
						}
					}
				}
			}
		}

		return true;
	}


	/**
	 * Validates Strings.
	 *
	 * @return bool Returns TRUE if current JSV instance contains all valid Strings. Otherwise, it returns FALSE.
	 */
	private function checkString()
	{
		// Check if JSON Part is a valid String.
		if (!is_string($this->data)) {
			// Check if JSON Schema for current JSON Part contains Defined Minimum Length.
			if (isset($this->schema->minLength)) {
				// Check if current JSON Part has shorter length than Defined Minimum Length.
				if (strlen($this->data) < $this->schema->minLength) {
					// Trigger an Error if String is shorter than Defined Minimum Length.
					$this->error(
						self::ERROR_STRING_LENGTH_SHORT,
						'',
						'/minLength',
						"String must be at least {$this->schema->minLength} characters long."
					);

					// Immediately stop further validation, if necessary.
					if ($this->stopValidation) {
						return false;
					}
				}
			}

			// Check if JSON Schema for current JSON Part contains Defined Maximum Length.
			if (isset($this->schema->maxLength)) {
				// Check if current JSON Part has longer length than Defined Maximum Length.
				if (strlen($this->data) > $this->schema->maxLength) {
					// Trigger an Error if String is longer than Defined Maximum Length.
					$this->error(
						self::ERROR_STRING_LENGTH_LONG,
						'',
						'/maxLength',
						"String cannot be longer than {$this->schema->maxLength} characters."
					);

					// Immediately stop further validation, if necessary.
					if ($this->stopValidation) {
						return false;
					}
				}
			}

			// Check if JSON Schema for current JSON Part contains Defined Pattern.
			if (isset($this->schema->pattern)) {
				// Retrieve Defined Pattern.
				$pattern = $this->schema->pattern;
				$patternFlags = (isset($this->schema->patternFlags) ? $this->schema->patternFlags : '');

				// Check if current JSON Part matches Defined Pattern.
				if (!preg_match('/' . str_replace('/', '\\/', $pattern) . '/' . $patternFlags, $this->data)) {
					// Trigger an Error if String doesn't match Defined Pattern.
					$this->error(
						self::ERROR_STRING_PATTERN,
						'',
						'/pattern',
						"String does not match Pattern: {$pattern}."
					);

					// Immediately stop further validation, if necessary.
					if ($this->stopValidation) {
						return false;
					}
				}
			}
		}

		return true;
	}


	/**
	 * Validates Numbers.
	 *
	 * @return bool Returns TRUE if current JSV instance contains all valid Numbers. Otherwise, it returns FALSE.
	 */
	private function checkNumber()
	{
		// Check if JSON Part is a valid Number.
		if (!is_string($this->data) && is_numeric($this->data)) {
			// Check if JSON Schema for current JSON Part contains Defined Multiple Of a Number
			// (i.e. Multiples of 10 would be: 0, 10, 20...).
			if (isset($this->schema->multipleOf)) {
				// Check if JSON Part is a valid Multiple Of a Defined Number.
				if (fmod($this->data / $this->schema->multipleOf, 1) !== 0) {
					// Trigger an Error if no valid Multiple Of a Number was found.
					$this->error(
						self::ERROR_NUMBER_MULTIPLE_OF,
						'',
						'/multipleOf',
						"Number must be a Multiple of {$this->schema->multipleOf}."
					);

					// Immediately stop further validation, if necessary.
					if ($this->stopValidation) {
						return false;
					}
				}
			}

			// Check if JSON Schema for current JSON Part contains Defined Minimum.
			if (isset($this->schema->minimum)) {
				// Retrieve Defined Minimum.
				$minimum = $this->schema->minimum;

				// Check if JSON Schema for current JSON Part contains Defined Exclusive Minimum and Exclusive Maximum.
				if (isset($this->schema->exclusiveMinimum) && $this->schema->exclusiveMinimum) {
					// Check if JSON Part is a not greater than Defined Minimum.
					if ($this->data <= $minimum) {
						// Trigger an Error if JSON Part is not greater than Defined Minimum.
						$this->error(
							self::ERROR_NUMBER_MINIMUM_EXCLUSIVE,
							'',
							'',
							"Number must be greater than {$minimum}."
						);
					}

				} else {
					// Check if JSON Part is a not greater than or equal to Defined Minimum.
					if ($this->data < $minimum) {
						// Trigger an Error if JSON Part is not greater than or equal to Defined Minimum.
						$this->error(
							self::ERROR_NUMBER_MINIMUM,
							'',
							'/minimum',
							"Number must be greater than or equal to {$minimum}."
						);
					}
				}

				// Immediately stop further validation, if necessary.
				if ($this->stopValidation) {
					return false;
				}
			}

			// Check if JSON Schema for current JSON Part contains Defined Maximum.
			if (isset($this->schema->maximum)) {
				// Retrieve Defined Maximum.
				$maximum = $this->schema->maximum;

				// Check if JSON Schema for current JSON Part contains Defined Exclusive Minimum and Exclusive Maximum.
				if (isset($this->schema->exclusiveMaximum) && $this->schema->exclusiveMaximum) {
					// Check if JSON Part is a not smaller than Defined Maximum.
					if ($this->data >= $maximum) {
						// Trigger an Error if JSON Part is not smaller than Defined Maximum.
						$this->error(
							self::ERROR_NUMBER_MAXIMUM_EXCLUSIVE,
							'',
							'',
							"Number must be smaller than {$maximum}."
						);
					}

				} else {
					// Check if JSON Part is a not smaller than or equal to Defined Maximum.
					if ($this->data > $maximum) {
						// Trigger an Error if JSON Part is not smaller than or equal to Defined Maximum.
						$this->error(
							self::ERROR_NUMBER_MAXIMUM,
							'',
							'/maximum',
							"Number must be smaller than or equal to {$maximum}."
						);
					}
				}

				// Immediately stop further validation, if necessary.
				if ($this->stopValidation) {
					return false;
				}
			}
		}

		return true;
	}


	/**
	 * Validates Composite Data Types.
	 *
	 * @return bool Returns TRUE if current JSV instance contains all valid Composite Data Types. Otherwise, it returns
	 *              FALSE.
	 */
	private function checkComposite()
	{
		// Check if JSON Schema for current JSON Part contains Defined All Of Properties (must be valid against all
		// Defined Properties).
		if (isset($this->schema->allOf)) {
			// Loop through all Defined All Of Properties.
			foreach ($this->schema->allOf as $index => $subSchema) {
				// Validate all JSON Sub-Parts.
				$subResult = $this->getSubResult($this->data, $subSchema, false);
				// Append all Errors found in JSON Sub-Parts.
				$this->appendSubResultErrors($subResult, '', '/allOf/' . (int) $index);

				// Immediately stop further validation, if necessary.
				if ($this->stopValidation) {
					return false;
				}
			}
		}

		// Check if JSON Schema for current JSON Part contains Defined Any Of Properties (must be valid against at least
		// one of the Defined Properties).
		if (isset($this->schema->anyOf)) {
			// Loop through all Defined Any Of Properties.
			foreach ($this->schema->anyOf as $index => $subSchema) {
				// Validate all JSON Sub-Parts.
				$subResult = $this->getSubResult($this->data, $subSchema, false);

				if ($subResult->isValid) {
					return true;
				}
			}

			// Trigger an Error if no valid Property was found.
			$this->error(self::ERROR_ANY_OF_MISSING, '', '/anyOf', 'Value must satisfy at least one of the Options.');

			// Immediately stop further validation, if necessary.
			if ($this->stopValidation) {
				return false;
			}
		}

		// Check if JSON Schema for current JSON Part contains Defined One Of Properties (must be valid against exactly
		// one of the Defined Properties).
		if (isset($this->schema->oneOf)) {
			$successIndex = null;

			// Loop through all Defined One Of Properties.
			foreach ($this->schema->oneOf as $index => $subSchema) {
				// Validate all JSON Sub-Parts.
				$subResult = $this->getSubResult($this->data, $subSchema, false);

				// Check if all JSON Sub-Parts are valid.
				if ($subResult->isValid) {
					// Check if valid Sub-Part Index is not determined yet.
					if (is_null($successIndex)) {
						$successIndex = $index;

					} else {
						// Trigger an Error if more than one JSON Sub-Part matches Defined Properties.
						$this->error(
							self::ERROR_ONE_OF_MULTIPLE,
							'',
							'/oneOf',
							"Value satisfies more than one of the Options ({$successIndex} and {$index})."
						);

						// Immediately stop further validation, if necessary.
						if ($subResult->stopValidation) {
							return false;
						}
					}

					continue;
				}
			}

			// Check if JSON Sub-Part matching Defined Properties was found.
			if (is_null($successIndex)) {
				// Trigger an Error if no JSON Sub-Part matching Defined Properties was found.
				$this->error(
					self::ERROR_ONE_OF_MISSING,
					'',
					'/oneOf',
					'Value must satisfy exactly one of the Options.'
				);

				// Immediately stop further validation, if necessary.
				if ($this->stopValidation) {
					return false;
				}
			}
		}

		// Check if JSON Schema for current JSON Part contains Defined Not Properties (must not contain any of the
		// Defined Properties).
		if (isset($this->schema->not)) {
			// Validate all JSON Sub-Parts.
			$subResult = $this->getSubResult($this->data, $this->schema->not, false);

			// Check if JSON Sub-Parts are valid.
			if ($subResult->isValid) {
				// Trigger an Error if an invalid JSON Sub-Part was found.
				$this->error(self::ERROR_NOT_PASSED, '', '/not', 'Value satisfies Prohibited Schema.');

				// Immediately stop further validation, if necessary.
				if ($this->stopValidation) {
					return false;
				}
			}
		}

		return true;
	}
}
