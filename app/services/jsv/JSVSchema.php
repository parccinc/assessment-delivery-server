<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Services\JSV;

/**
 * Class JSVSchema
 *
 * This is JSON Schema Store for JSV (JSON Schema Validator) Service. It stores one or many JSON Schemas to be used for
 * JSON Validation and also validates them.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
final class JSVSchema
{

	/**
	 * @var array $schemas.
	 */
	private $schemas = [];
	/**
	 * @var array $refs.
	 */
	private $refs = [];
	/**
	 * @var array $errors.
	 */
	public $errors;

	/**
	 * JSON Schema Definitions.
	 */
	public $type;
	public $enum;
	public $declared;
	public $required;
	public $properties;
	public $patternProperties;
	public $additionalProperties;
	public $dependencies;
	public $minProperties;
	public $maxProperties;
	public $items;
	public $additionalItems;
	public $minItems;
	public $maxItems;
	public $uniqueItems;
	public $minLength;
	public $maxLength;
	public $pattern;
	public $patternFlags;
	public $multipleOf;
	public $minimum;
	public $exclusiveMinimum;
	public $maximum;
	public $exclusiveMaximum;
	public $allOf;
	public $anyOf;
	public $oneOf;
	public $not;


	/**
	 * Constructor.
	 *
	 * It validates and adds provided JSON Schema to the JSON Schema Store.
	 *
	 * @param string       $url     JSON Schema URL (@default='').
	 * @param object|array &$schema  JSON Schema (@default=null).
	 * @param boolean      $trusted Indicator if JSON Schema should be trusted, OR should be validated first
	 *                              (@default=false).
	 */
	public function __construct($url = '', &$schema = null, $trusted = false)
	{
		// Initialize JSON Schema Properties.
		$this->errors = [];

		// Add JSON Schema to the JSON Schema Store, if necessary.
		if (!empty($url) && !empty($schema)) {
			$this->add($url, $schema, $trusted);
		}
	}


	/**
	 * Checks if all Keys for given Array are Numeric.
	 *
	 * @param  array   &$array Array that needs to be checked if all of its Keys are numeric.
	 * @return boolean         Returns TRUE if all Element Keys are numeric. Otherwise, it returns FALSE.
	 */
	private function isNumericArray(&$array)
	{
		$count = count($array);
		// Loop through all elements of the array.
		for ($i = 0; $i < $count; ++$i) {
			if (!isset($array[$i])) {
				return false;
			}
		}

		return true;
	}


	/**
	 * Resolves given URL.
	 *
	 * @param  string $baseUrl     Base URL.
	 * @param  string $relativeUrl Relative URL.
	 * @return string              Returns resolved URL.
	 */
	private function resolveUrl($baseUrl, $relativeUrl)
	{
		// Check if relative URL is actually already an absolute URL.
		if (parse_url($relativeUrl, PHP_URL_SCHEME) !== '') {
			return $relativeUrl;
		}

		// Parse parts from base URL.
		$baseParts = parse_url($baseUrl);

		// Check parts of relative URL.
		if ($relativeUrl[0] === '?') {
			// In case relative URL contains Query.
			$baseParts['query'] = mb_substr($relativeUrl, 1);

			unset($baseParts['fragment']);

		} elseif ($relativeUrl[0] === '#') {
			// In case relative URL contains Fragment.
			$baseParts['fragment'] = mb_substr($relativeUrl, 1);

		} elseif ($relativeUrl[0] === '/') {
			// In case relative URL contains trailing slash.
			if ($relativeUrl[1] === '/') {
				return $baseParts['scheme'].$relativeUrl;
			}

			$baseParts['path'] = $relativeUrl;

			unset($baseParts['query']);
			unset($baseParts['fragment']);

		} else {
			// In case relative URL has no trailing slash, Query of Fragment.
			$basePathParts = explode('/', $baseParts['path']);
			$relativePathParts = explode('/', $relativeUrl);

			array_pop($basePathParts);

			// Loop through all segments of relative URL.
			while (count($relativePathParts)) {
				if ($relativePathParts[0] === '..') {
					// In case relative URL contains relative path to parent folder.
					array_shift($relativePathParts);

					if (count($basePathParts)) {
						array_pop($basePathParts);
					}

				} elseif ($relativePathParts[0] === '.') {
					// In case relative URL contains relative path to current folder.
					array_shift($relativePathParts);

				} else {
					array_push($basePathParts, array_shift($relativePathParts));
				}
			}

			$baseParts['path'] = implode('/', $basePathParts);

			if ($baseParts['path'][0] !== '/') {
				$baseParts['path'] = '/' . $baseParts['path'];
			}
		}

		// Construct resulting URL.
		$resultUrl = '';
		// Append URL Scheme.
		if (isset($baseParts['scheme'])) {
			$resultUrl .= $baseParts['scheme'] . '://';

			// Append authentication details, if necessary.
			if (isset($baseParts['user'])) {
				$resultUrl .= ':' . $baseParts['user'];

				if (isset($baseParts['pass'])) {
					$resultUrl .= ':' . $baseParts['pass'];
				}

				$resultUrl .= '@';
			}

			$resultUrl .= $baseParts['host'];

			// Append custom port, if necessary.
			if (isset($baseParts['port'])) {
				$resultUrl .= ':' . $baseParts['port'];
			}
		}

		// Append Hostname and Path.
		$resultUrl .= $baseParts['path'];

		// Append Query, if necessary.
		if (isset($baseParts['query'])) {
			$resultUrl .= '?' . $baseParts['query'];
		}

		// Append Fragment, if necessary.
		if (isset($baseParts['fragment'])) {
			$resultUrl .= '#' . $baseParts['fragment'];
		}

		return $resultUrl;
	}


	/**
	 * Returns a SubSchema (Child Element) based on provided Path relative to its Parent Object.
	 *
	 * @param  mixed     &$value Parent Object.
	 * @param  string    $path   Path to the Child Object.
	 * @param  boolean   $strict Indicator if Strict Path parsing should be used (@default=false).
	 * @return mixed             Returns Child Object based on given Path if found. Otherwise, it returns NULL.
	 */
	private function getSubSchema(&$value, $path = '', $strict = false)
	{
		// Check Path.
		if ($path === '') {
			// Inc case Path is empty, return the Parent Object.
			return $value;
		} elseif ($path[0] !== '/') {
			// Append new Error if Path doesn't have a leading slash.
			$this->errors[] = "Invalid path: {$path}.";
		}

		// Parse provided Path.
		$parts = explode('/', $path);
		array_shift($parts);

		// Loop through all Path parts.
		foreach ($parts as $part) {
			// Concatenate all parts.
			$part = strtr($part, ['~1' => '/']);
			$part = strtr($part, ['~0' => '~']);

			if (is_array($value) && is_numeric($part)) {
				// In case parent is an Array with numeric Keys.
				$value =& $value[$part];

			} elseif (is_object($value)) {
				// In case parent is an Object.
				if (isset($value->$part)) {
					$value =& $value->$part;

				} elseif ($strict) {
					// Append new Error if Path doesn't exist and Strict parsing is enforced.
					$this->errors[] = "Path does not exist: {$path}.";

				} else {
					// In case requested Path doesn't exist.
					return null;
				}

			} elseif ($strict) {
				// Append new Error if Path doesn't exist and Parent Object is neither valid Array nor valid Object, and
				// Strict parsing is enforced.
				$this->errors[] = "Path does not exist: {$path}.";

			} else {
				// In case requested Path doesn't exist.
				return null;
			}
		}

		// Return Child Object referenced by given Path.
		return $value;
	}


	/**
	 * Returns requested JSON Schema from the JSON Schema Store.
	 *
	 * @param  string $url JSON Schema URL.
	 * @return mixed       Returns requested JSON Schema if found in the JSON Schema Store. Otherwise, it returns NULL.
	 */
	public function get($url)
	{
		// Return requested JSON Schema if it exists in the JSON Schema Store.
		if (isset($this->schemas[$url])) {
			return $this->schemas[$url];
		}

		// Parse requested JSON Schema URL parts.
		$urlParts = explode('#', $url);
		$baseUrl = array_shift($urlParts);
		$fragment = urldecode(implode('#', $urlParts));

		// Check if requested JSON Schema's parent exists in the JSON Schema Store.
		if (isset($this->schemas[$baseUrl])) {
			$schema = $this->schemas[$baseUrl];

			// Add new JSON Schema to the JSON Schema Store based on its Query/Fragment as a subset of its parent's JSON
			// Schema.
			if ($schema && ($fragment === '' || $fragment[0] === '/')) {
				$schema = $this->getSubSchema($schema, $fragment);
				$this->add($url, $schema);

				return $schema;
			}
		}

		// Return NULL in case requested JSON Schema does not exist in the JSON Schema Store.
		return null;
	}


	/**
	 * Normalizes JSON Schema.
	 *
	 * @param string         $url           JSON Schema URL.
	 * @param object|array   &$schema       JSON Schema (either JSON Object or JSON Array).
	 * @param string|boolean $trustedPrefix JSON Schema Path Prefix that should be trusted (no need to validate it), OR
	 *                                      if set as TRUE trust the whole JSON Schema.
	 */
	private function normalizeSchema($url, &$schema, $trustedPrefix = '')
	{
		// Check if given JSON Schema is not an Array with Numeric Keys.
		if (is_array($schema) && !$this->isNumericArray($schema)) {
			// Cast JSON Schema to an Object.
			$schema = (object) $schema;
		}

		// Check if JSON Schema is an Object.
		if (is_object($schema)) {
			// Check if JSON Schema contains References.
			if (isset($schema->{'$ref'})) {
				// Resolve JSON Schema References.
				$refUrl = $schema->{'$ref'} = $this->resolveUrl($url, $schema->{'$ref'});
				// Get JSON Schema References.
				$refSchema = $this->get($refUrl);

				// Check if JSON Schema References were successfully retrieved.
				if (!empty($refSchema)) {
					$schema = $refSchema;

					return;

				} else {
					// Store missing JSON Schema References.
					$urlParts = explode('#', $refUrl);
					$baseUrl = array_shift($urlParts);
					$this->refs[$baseUrl][$refUrl][] =& $schema;
				}

			} elseif (isset($schema->id) && is_string($schema->id)) {
				// Resolve JSON Schema Identifiers.
				$schema->id = $url = $this->resolveUrl($url, $schema->id);

				// Add JSON Schema based on Identifier.
				if (!isset($this->schemas[$schema->id]) && ($trustedPrefix === true ||
				    preg_match(('/^' . preg_quote($trustedPrefix, '/') . '(?:[#\/?].*)?$/'), $schema->id))
				) {
					$this->add($schema->id, $schema);
				}
			}

			// Loop through JSON Schema and normalize Enum Keys.
			foreach ($schema as $key => &$value) {
				if ($key !== 'enum') {
					$this->normalizeSchema($url, $value, $trustedPrefix);
				}
			}

		} elseif (is_array($schema)) {
			// Normalize JSON Schema Array.
			foreach ($schema as &$value) {
				$this->normalizeSchema($url, $value, $trustedPrefix);
			}
		}
	}


	/**
	 * Adds JSON Schema to the JSON Schema Storage.
	 *
	 * @param string       $url     JSON Schema URL.
	 * @param object|array &$schema JSON Schema.
	 * @param boolean      $trusted Indicator if JSON Schema should be trusted, OR should be validated first
	 *                              (@default=false).
	 */
	public function add($url, &$schema, $trusted = false)
	{
		// Parse JSON Schema URL.
		$urlParts = explode('#', $url);
		$baseUrl = array_shift($urlParts);
		$fragment = urldecode(implode('#', $urlParts));

		// Parse base URL for trusted JSON Schema Part.
		$trustedBase = explode('?', $baseUrl);
		$trustedBase = $trustedBase[0];

		$this->schemas[$url] =& $schema;

		// Normalize JSON Schema.
		$this->normalizeSchema($url, $schema, ($trusted === true ? true : $trustedBase));

		// Check if JSON Schema contains Fragments.
		if ($fragment === '') {
			$this->schemas[$baseUrl] = $schema;
		}

		// Loop through JSON Schema References, if necessary.
		if (isset($this->refs[$baseUrl])) {
			foreach ($this->refs[$baseUrl] as $fullUrl => $refSchemas) {
				foreach ($refSchemas as &$refSchema) {
					// Get all JSON Schema References.
					$refSchema = $this->get($fullUrl);
				}

				unset($this->refs[$baseUrl][$fullUrl]);
			}

			if (count($this->refs[$baseUrl]) === 0) {
				unset($this->refs[$baseUrl]);
			}
		}
	}


	/**
	 * Returns missing references from the JSON Schema.
	 *
	 * @return array Returns missing references.
	 */
	/*
	public function missing()
	{
		// Return missing JSON Schema References.
		return array_keys($this->refs);
	}
	*/
}
