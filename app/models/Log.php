<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Models;

/**
 * Class Log
 *
 * This is the Log Model. It contains all Log Entry related details that are mapped to the Database.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class Log extends BaseModel
{

	/**
	 * Log Properties.
	 *
	 * @var integer $logId
	 */
	public $logId;
	/**
	 * @var integer $tenantId
	 */
	public $tenantId;
	/**
	 * @var integer $applicationId
	 */
	public $applicationId;
	/**
	 * @var integer $requestId
	 */
	public $requestId;
	/**
	 * @var string $type
	 */
	public $type;
	/**
	 * @var string $message
	 */
	public $message;
	/**
	 * @var string $context
	 */
	public $context;
	/**
	 * @var string $createdDateTime
	 */
	public $createdDateTime;
	/**
	 * @var integer $createdByUserId
	 */
	public $createdByUserId;


	/**
	 * Sets Connection to the Database and Relationship to other Models.
	 * It also joins with other Models based on defined dependencies.
	 */
	public function initialize()
	{
		// Set Default Database Connection as Log always uses Default Database!
		// If Test Publishing is enabled it's always Secondary Database. Otherwise, it's always Primary Database!
		self::changeConnectionService($this->getDI()['defaultDb']);

		parent::initialize();

		// Set Relationship to other Models.
		$this->belongsTo('createdByUserId', 'PARCC\ADP\Models\User', 'userId', ['alias' => 'User']);
		$this->belongsTo('tenantId', 'PARCC\ADP\Models\Tenant', 'tenantId', ['alias' => 'Tenant']);
	}


	/**
	 * Returns the Database Table linked to the Model.
	 *
	 * @return string Database Table Name.
	 */
	public function getSource()
	{
		return 'log';
	}


	/**
	 * Column Mapping returns Keys as Field Names in the Database Table and Values as Application Variables.
	 *
	 * @return array Database Table Mapping to Table Columns.
	 */
	public function columnMap()
	{
		return [
			'lid' => 'logId',
			'fk_tenant_id' => 'tenantId',
			'application_id' => 'applicationId',
			'request_id' => 'requestId',
			'type' => 'type',
			'message' => 'message',
			'context' => 'context',
			'created' => 'createdDateTime',
			'fk_created_by' => 'createdByUserId'
		];
	}
}
