<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Models;

/**
 * Class System
 *
 * This is the System Model. It contains all System Configuration Properties that are mapped to the Database.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @method static findFirstByName($name) Returns System Value with a given Name.
 */
class System extends BaseModel
{

	/**
	 * System Properties.
	 *
	 * @var string $name
	 */
	public $name;
	/**
	 * @var integer $tenantId
	 */
	public $tenantId;
	/**
	 * @var string $value
	 */
	public $value;
	/**
	 * @var string $createdDateTime
	 */
	public $createdDateTime;
	/**
	 * @var string $updatedDateTime
	 */
	public $updatedDateTime;


	/**
	 * Sets Connection to the Database.
	 */
	public function initialize()
	{
		// Set Default Database Connection as System always uses Default Database!
		// If Test Publishing is enabled it's always Secondary Database. Otherwise, it's always Primary Database!
		self::changeConnectionService($this->getDI()['defaultDb']);

		parent::initialize();
	}


	/**
	 * Returns the Database Table linked to the Model.
	 *
	 * @return string Database Table Name.
	 */
	public function getSource()
	{
		return 'system';
	}


	/**
	 * Column Mapping returns Keys as Field Names in the Database Table and Values as Application Variables.
	 *
	 * @return array Database Table Mapping to Table Columns.
	 */
	public function columnMap()
	{
		return [
			'name' => 'name',
			'fk_tenant_id' => 'tenantId',
			'value' => 'value',
			'created' => 'createdDateTime',
			'updated' => 'updatedDateTime'
		];
	}
}
