<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Models;

use Phalcon\Db\RawValue;
use Phalcon\Mvc\Model;

/**
 * Class BaseModel
 *
 * This is the Base Model. It contains Helper Methods that other Models are using.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @property-read Log|Student|Test|TestContent|TestForm|TestFormRevision|TestResults|TestSession|User $createdDateTime
 * @property-read Log|Student|Test|TestContent|TestForm|TestFormRevision|TestResults|TestSession      $createdByUserId
 * @property-read Log|Student|Test|TestContent|TestForm|TestFormRevision|TestResults|TestSession|User $updatedDateTime
 * @property-read Log|Student|Test|TestContent|TestForm|TestFormRevision|TestResults|TestSession      $updatedByUserId
 */
class BaseModel extends Model
{

	/**
	 * @var string $connectionService Indicates which Database Connection Service should be used in case Data is stored
	 *                                across different Databases. (@default='db').
	 */
	public static $connectionService = 'db';


	/**
	 * Sets Connection to the Database.
	 * This also enables usage of Dynamic SQL Updates. SQL UPDATE Statements are by default created with every Column
	 * defined in the Model (full all-field SQL Update). Dynamic Update uses only the Fields that have changed when
	 * creating the final SQL Statement.
	 * In some cases this can improve the performance by reducing the traffic between the Application and the Database
	 * Server, and this especially helps when the Table has Blob/Text Fields.
	 */
	public function initialize()
	{
		$this->setConnectionService(self::$connectionService);
		$this->useDynamicUpdate(true);
	}


	/**
	 * Changes Default Database Connection Service.
	 *
	 * @param string $connectionService Name of the Database Connection Service (@default={'db'|'db2'}).
	 */
	public static function changeConnectionService($connectionService)
	{
		// Set Database Connection Service.
		self::$connectionService = $connectionService;
	}


	/**
	 * Adds Created Timestamp and/or Created by User (if missing) before creating a Model instance.
	 */
	public function beforeValidationOnCreate()
	{
		// Check if current Model has Created Timestamp property.
		if (property_exists($this, 'createdDateTime')) {
			// Check if Created Timestamp is already set.
			if (empty($this->createdDateTime)) {
				// Set Created Timestamp as UTC Timestamp on the Database Server (instead of using local Server
				// Timestamp as Database Server Timestamps is more reliable to rely on in case of multiple Application
				// Servers being used concurrently).
				$this->createdDateTime = new RawValue('UTC_TIMESTAMP()');
			}
		}

		// Check if current Model has Created by User property.
		if (property_exists($this, 'createdByUserId')) {
			// Check if Created by User is already set.
			if (empty($this->createdByUserId)) {
				// Check if User was successfully identified.
				if ($this->getDI()->has('user')) {
					// Set Created by User.
					$this->createdByUserId = $this->getDI()['user']->userId;
				}
			}
		}
	}


	/**
	 * Adds Updated Timestamp and/or Updated by User (if missing) before updating the edited Model instance.
	 */
	public function beforeValidationOnUpdate()
	{
		// Check if current Model has Created Timestamp property.
		if (property_exists($this, 'updatedDateTime')) {
			// Always update Updated Timestamp to the current time!
			// Set Updated Timestamp as UTC Timestamp on the Database Server (instead of using local Server Timestamp as
			// Database Server Timestamps is more reliable to rely on in case of multiple Application Servers being used
			// concurrently).
			$this->updatedDateTime = new RawValue('UTC_TIMESTAMP()');
		}

		// Check if current Model has Updated by User property.
		if (property_exists($this, 'updatedByUserId')) {
			// Check if Updated by User is already set.
			if (empty($this->updatedByUserId)) {
				// Check if User was successfully identified.
				if ($this->getDI()->has('user')) {
					// Set Updated by User.
					$this->updatedByUserId = $this->getDI()['user']->userId;
				}
			}
		}
	}


	/**
	 * Sets a list of Database Fields that should be skipped (should not change) when Model is either saved or updated.
	 *
	 * @param array $fieldNames List of Database Fields that should be skipped
	 *                          (i.e. 'createdDateTime' or 'updatedDateTime').
	 */
	/*
	public function skipFields($fieldNames)
	{

		$this->skipAttributes($fieldNames);
	}
	*/
}
