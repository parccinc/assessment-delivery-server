<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Models;

/**
 * Class User
 *
 * This is the User Model. It contains all User related details that are mapped to the Database.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class User extends BaseModel
{

	/**
	 * User Properties.
	 *
	 * @var integer $userId
	 */
	public $userId;
	/**
	 * @var integer $tenantId
	 */
	public $tenantId;
	/**
	 * @var string $username
	 */
	public $username;
	/**
	 * @var string $password
	 */
	public $password;
	/**
	 * @var string $secret
	 */
	public $secret;
	/**
	 * @var string $type
	 */
	public $type;
	/**
	 * @var boolean $isActive
	 */
	public $isActive;
	/**
	 * @var string $createdDateTime
	 */
	public $createdDateTime;
	/**
	 * @var string $updatedDateTime
	 */
	public $updatedDateTime;


	/**
	 * Sets Connection to the Database and Relationship to other Models.
	 * It also joins with other Models based on defined dependencies.
	 */
	public function initialize()
	{
		// Set Default Database Connection as User always uses Default Database!
		// If Test Publishing is enabled it's always Secondary Database. Otherwise, it's always Primary Database!
		self::changeConnectionService($this->getDI()['defaultDb']);

		parent::initialize();

		// Set Relationship to other Models.
		$this->belongsTo('tenantId', 'PARCC\ADP\Models\Tenant', 'tenantId', ['alias' => 'Tenant']);
		$this->hasMany('userId', 'Student', 'createdByUserId');
		//$this->hasMany('userId', 'Student', 'updatedByUserId');
		$this->hasMany('userId', 'Test', 'createdByUserId');
		//$this->hasMany('userId', 'Test', 'updatedByUserId');
		$this->hasMany('userId', 'TestContent', 'createdByUserId');
		//$this->hasMany('userId', 'TestContent', 'updatedByUserId');
		$this->hasMany('userId', 'TestForm', 'createdByUserId');
		//$this->hasMany('userId', 'TestForm', 'updatedByUserId');
		$this->hasMany('userId', 'TestFormRevision', 'createdByUserId');
		//$this->hasMany('userId', 'TestFormRevision', 'updatedByUserId');
		$this->hasMany('userId', 'TestResults', 'createdByUserId');
		//$this->hasMany('userId', 'TestResults', 'updatedByUserId');
		$this->hasMany('userId', 'TestSession', 'createdByUserId');
		//$this->hasMany('userId', 'TestSession', 'updatedByUserId');
	}


	/**
	 * Returns the Database Table linked to the Model.
	 *
	 * @return string Database Table Name.
	 */
	public function getSource()
	{
		return 'user';
	}


	/**
	 * Column Mapping returns Keys as Field Names in the Database Table and Values as Application Variables.
	 *
	 * @return array Database Table Mapping to Table Columns.
	 */
	public function columnMap()
	{
		return [
			'uid' => 'userId',
			'fk_tenant_id' => 'tenantId',
			'username' => 'username',
			'password' => 'password',
			'secret' => 'secret',
			'type' => 'type',
			'active' => 'isActive',
			'created' => 'createdDateTime',
			'updated' => 'updatedDateTime'
		];
	}
}
