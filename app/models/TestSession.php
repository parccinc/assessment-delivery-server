<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Models;

/**
 * Class TestSession
 *
 * This is the Test Session Model. It contains all Test Battery Form Revision Session/Assignment related details that
 * are mapped to the Database.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @property-read Student          $Student          Reference by foreign key to the linked Student Model.
 * @property-read TestForm         $TestForm         Reference by foreign key to the linked Test Form Model.
 * @property-read TestFormRevision $TestFormRevision Reference by foreign key to the linked Test Form Revision Model.
 */
class TestSession extends BaseModel
{

	/**
	 * Test Session Properties.
	 *
	 * @var integer $testSessionId
	 */
	public $testSessionId;
	/**
	 * @var integer $tenantId
	 */
	public $tenantId;
	/**
	 * @var integer $externalTestSessionId
	 */
	public $externalTestSessionId;
	/**
	 * @var integer $parentStudentId
	 */
	public $parentStudentId;
	/**
	 * @var integer $parentTestFormId
	 */
	public $parentTestFormId;
	/**
	 * @var integer $parentTestFormRevisionId
	 */
	public $parentTestFormRevisionId;
	/**
	 * @var string $testKey
	 */
	public $testKey;
	/**
	 * @var string $token
	 */
	public $token;
	/**
	 * @var integer $tokenExpirationTimestamp
	 */
	public $tokenExpirationTimestamp;
	/**
	 * @var string $status
	 */
	public $status;
	/**
	 * @var string $startDateTime
	 */
	public $startDateTime;
	/**
	 * @var string $endDateTime
	 */
	public $endDateTime;
	/**
	 * @var boolean $isEnabledLineReader
	 */
	public $isEnabledLineReader;
	/**
	 * @var boolean $isEnabledTextToSpeech
	 */
	public $isEnabledTextToSpeech;
	/**
	 * @var boolean $isActive
	 */
	public $isActive;
	/**
	 * @var string $createdDateTime
	 */
	public $createdDateTime;
	/**
	 * @var integer $createdByUserId
	 */
	public $createdByUserId;
	/**
	 * @var string $updatedDateTime
	 */
	public $updatedDateTime;
	/**
	 * @var integer $updatedByUserId
	 */
	public $updatedByUserId;


	/**
	 * Sets Connection to the Database and Relationship to other Models.
	 * It also joins with other Models based on defined dependencies.
	 */
	public function initialize()
	{
		// Set Primary Database Connection as TestSession always uses Primary Database!
		self::changeConnectionService('db');

		parent::initialize();

		// Set Relationship to other Models.
		$this->belongsTo('tenantId', 'PARCC\ADP\Models\Tenant', 'tenantId', ['alias' => 'Tenant']);
		$this->belongsTo('parentStudentId', 'PARCC\ADP\Models\Student', 'studentId', ['alias' => 'Student']);
		$this->belongsTo('parentTestFormId', 'PARCC\ADP\Models\TestForm', 'formId', ['alias' => 'TestForm']);
		$this->belongsTo(
			'parentTestFormRevisionId',
			'PARCC\ADP\Models\TestFormRevision',
			'revisionId',
			['alias' => 'TestFormRevision']
		);
		$this->belongsTo('createdByUserId', 'PARCC\ADP\Models\User', 'userId', ['alias' => 'User']);
		//$this->belongsTo('updatedByUserId', 'PARCC\ADP\Models\User', 'userId', ['alias' => 'User']);
	}


	/**
	 * Returns the Database Table linked to the Model.
	 *
	 * @return string Database Table Name.
	 */
	public function getSource()
	{
		return 'test_session';
	}


	/**
	 * Column Mapping returns Keys as Field Names in the Database Table and Values as Application Variables.
	 *
	 * @return array Database Table Mapping to Table Columns.
	 */
	public function columnMap()
	{
		return [
			'tsid' => 'testSessionId',
			'fk_tenant_id' => 'tenantId',
			'external_tsid' => 'externalTestSessionId',
			'fk_student_id' => 'parentStudentId',
			'fk_test_form_id' => 'parentTestFormId',
			'fk_test_form_revision_id' => 'parentTestFormRevisionId',
			'test_key' => 'testKey',
			'token' => 'token',
			'token_expiration' => 'tokenExpirationTimestamp',
			'status' => 'status',
			'started' => 'startDateTime',
			'ended' => 'endDateTime',
			'enable_line_reader' => 'isEnabledLineReader',
			'enable_text_to_speech' => 'isEnabledTextToSpeech',
			'active' => 'isActive',
			'created' => 'createdDateTime',
			'fk_created_by' => 'createdByUserId',
			'updated' => 'updatedDateTime',
			'fk_updated_by' => 'updatedByUserId'
		];
	}
}
