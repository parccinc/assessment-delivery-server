<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Tests\Interfaces\Implementations;

use PARCC\ADP\Tests\Interfaces\HttpRequest;

/**
 * Class CurlRequest.
 *
 * @package PARCC\ADP\Tests
 */
class CurlRequest implements HttpRequest
{
	private $handle = null;

	/**
	 * CurlRequest constructor.
	 *
	 * @param string $url
	 * @param array  $options
	 */
	public function __construct($url, array $options = [])
	{
		$this->handle = curl_init($url);

		foreach ($options as $name => $value) {
			$this->setOption($name, $value);
		}
	}

	/**
	 * @param  mixed      $name
	 * @param  mixed      $value
	 * @return mixed|void
	 */
	public function setOption($name, $value)
	{
		curl_setopt($this->handle, $name, $value);
	}

	/**
	 * @return mixed
	 */
	public function execute()
	{
		return curl_exec($this->handle);
	}

	/**
	 * @return string
	 */
	public function getError()
	{
		return curl_error($this->handle);
	}

	/**
	 * @param  mixed $name
	 * @return mixed
	 */
	public function getInfo($name)
	{
		return curl_getinfo($this->handle, $name);
	}

	public function close()
	{
		curl_close($this->handle);
	}
}
