<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Tests\Services;

use Phalcon\Config;
use Phalcon\Exception;
use PARCC\ADP\Tests\UnitTestCase;
use PARCC\ADP\Services\HttpClient;

/**
 * Class HttpClientTest.
 *
 * @package PARCC\ADP\Tests
 */
class HttpClientTest extends UnitTestCase
{
	/**
	 * @var array $fileConfig
	 */
	protected $fileConfig;
	/**
	 * @var \PHPUnit_Framework_MockObject_MockBuilder $mockRequest
	 */
	protected $mockRequest;

	/**
	 * @covers PARCC\ADP\Services\HttpClient::request
	 */
	public function testRequestDefaultRequestReturnsTestSuccess()
	{
		$httpClient = new HttpClient($this->mockRequest);
		$result = $httpClient->request('', false);
		$this->assertObjectHasAttribute('testStatus', $result);
		$this->assertEquals('testSuccess', $result->testStatus);
	}

	/**
	 * @covers PARCC\ADP\Services\HttpClient::request
	 */
	public function testRequestFileConfigReturnsFilepathString()
	{
		$httpClient = new HttpClient($this->mockRequest);
		$result = $httpClient->request('', false, 'POST', 'data', ['Accept: application/json'], $this->fileConfig);
		$this->assertEquals($this->config['FILE_CONFIG'], $result);
	}

	/**
	 * @covers                         PARCC\ADP\Services\HttpClient::request
	 * @expectedException              Exception
	 * @expectedExceptionMessage       fakeURL
	 * @expectedExceptionMessageRegExp /^Error while making cUrl call to \(.*\)\./
	 */
	public function testRequestPersistentFileConfigThrowsGenericException()
	{
		$fileConfig = $this->fileConfig;
		$fileConfig['filePath'] = $this->config['PERSISTENT_FILE_CONFIG'];

		$httpClient = new HttpClient($this->mockRequest);
		$httpClient->request('fakeURL', false, 'POST', 'data', ['Accept: application/json'], $fileConfig);
	}

	/**
	 * @covers                         PARCC\ADP\Services\HttpClient::request
	 * @expectedException              Exception
	 * @expectedExceptionMessageRegExp /File \(.*\) already exist!$/
	 */
	public function testRequestPersistentFileConfigThrowsFileExistsException()
	{
		$fileConfig = $this->fileConfig;
		$fileConfig['filePath'] = $this->config['PERSISTENT_FILE_CONFIG'];

		$httpClient = new HttpClient($this->mockRequest);
		$httpClient->request('fakeURL', false, 'POST', 'data', ['Accept: application/json'], $fileConfig);
	}

	public function testRequestPostRequestNoDataPostOptSetPostFieldsNotSet()
	{
		$this->markTestSkipped();
	}

	/**
	 * Setup.
	 */
	public function setUp()
	{
		$this->mockRequest =
			$this->getMockBuilder('PARCC\ADP\Tests\Interfaces\Implementations\CurlRequest')
			     ->disableOriginalConstructor()
			     ->getMock();

		$this->mockRequest->method('execute')
		     ->willReturn(json_encode([
		         'testStatus' => 'testSuccess'
		     ]));

		$this->fileConfig = [
			'filePath' => $this->config['FILE_CONFIG'],
			'overwrite'	=> false
		];

		// Ensure the file config does not exist.
		if (file_exists($this->config['FILE_CONFIG'])) {
			unlink($this->config['FILE_CONFIG']);
		}

		// Create the persistent config file.
		if (!file_exists($this->config['PERSISTENT_FILE_CONFIG'])) {
			$fileHandler = fopen($this->config['PERSISTENT_FILE_CONFIG'], 'w+');
			fclose($fileHandler);
		}

		parent::setUp();
	}

	/**
	 * Tear Down.
	 */
	public function tearDown()
	{
		unset($this->request);

		// Cleanup files.
		foreach ([$this->config['FILE_CONFIG'], $this->config['PERSISTENT_FILE_CONFIG']] as $file) {
			if (file_exists($file)) {
				unlink($file);
			}
		}
		parent::tearDown();
	}
}
