<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

// Configuration for Unit Tests.
return [
	'LOCALHOST_URI' => 'http://localhost',
	'ADP_ROOT_URI' => ':8083/api',
	'LOGIN_URI' => '/login',
	'DEBUG_UNIT_TESTS' => false,
	'FILE_CONFIG' => __DIR__ . '\..\..\header-out.txt',
	'PERSISTENT_FILE_CONFIG' => __DIR__ . '\..\..\header-out-persistent.txt'
];
