<?php
/**
 * @apiDefine LDR User Authentication includes:
 *                <ol>
 *                  <li>Basic Authentication</li>
 *                  <li>HMAC Authentication</li>
 *                </ol>
 */
/**
 * @apiGroup ADP Publisher
 * @api {POST} /api/assignment Assignment Creation
 * @apiName Assignment Creation
 * @apiPermission LDR
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>LDR</b><br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Assignment web service is exclusively used by LDR for Test Assignments and Student details
 *                 publishing. It validates multiple parameters before it allows client to login into ADP Publisher
 *                 application.<br/>
 *                 Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/>
 *                 In order to create new Test Assignment, all Test Assignment details are required. Student details are
 *                 optional, but must be provided if referenced Student does not exist in ADP system. On the other hand,
 *                 if Student already exists in ADP, a valid reference for a Student is enough (Student ID). However, if
 *                 Student details have changed in the meantime, then all Student details are required to be provided
 *                 with Test details and ADP will update the existing Student details.<br/><br/>
 *                 Once Test Assignment Request is received, if correct, ADP Publisher will create a Request Job in its
 *                 Job Queue and will process it as soon as possible depending on its priority, and number and priority
 *                 of other pending Request Jobs in its Job Queue.<br/><br/>
 *                 In case of successful request, it returns an ADP Test Assignment ID, Date and Time when it was
 *                 created and its current Status.<br/>
 *                 In case of failure, it returns an error with error code and error description, HTTP status code and
 *                 HTTP status description, and current time on the server.<br/><br/>
 *                 This means that LDR should be able to roll back any changes if ADP doesn't return a confirmation
 *                 about successful creation of Test Assignment details and optionally creation/update of Student
 *                 details. If subsequent request for creation of the same Test Assignment is sent ADP Publisher will
 *                 simply return an error and won't process the request.<br/><br/>
 *                 More details about HMAC Authentication can be found under Authentication section.<br/>
 *                 All possible generic API Errors can be found under Errors section.<br/><br/>
 *                 These are possible Test Assignment Creation specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1803</td>
 *                      <td>Invalid Request.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case of missing index in the request.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1804</td>
 *                      <td>Invalid Request.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case of missing missing ADP Test Assignment Id in the request.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1805</td>
 *                      <td>Invalid Request.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case of missing index in the request.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1806</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case Test Assignment Id is missing or invalid.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1807</td>
 *                      <td>Data Not Found.</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case a test session cannot be found.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1809</td>
 *                      <td>Internal Error.</td>
 *                      <td>500</td>
 *                      <td>Internal Server Error/td>
 *                      <td>Returns JSON error which has a custom error message.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1810</td>
 *                      <td>Invalid Request.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case student ID is missing.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1811</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case student ID is not an unsigned integer.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1812</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case provided state is not valid.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1813</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case student's grade is provided but is not valid.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1814</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case student's date of birth is provided but is not a valid date.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1815</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case student's date of birth is provided but is not a valid
 *                          format.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1816</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case student's school name is provided but is not a valid format.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1817</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>ReturnsJSON error in case student's name is provided but is not a valid format.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1818</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case student's personal ID is provided but is not a valid format.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1819</td>
 *                      <td>Data Not Found.</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case the student can't be found.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1820</td>
 *                      <td>Invalid Request.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case of missing index in the request.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1821</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case test ID is not an unsigned integer.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1822</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case test form ID is not an unsigned integer.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1823</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case Test Key is not a valid format.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1824</td>
 *                      <td>Test Key already exists.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case if test key already exists.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1825</td>
 *                      <td>Data Not Found.</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case Test Session is not found.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1827</td>
 *                      <td>Internal Error.</td>
 *                      <td>500</td>
 *                      <td>Internal Server Error</td>
 *                      <td>Returns JSON error in case a Test Form does not belong to Test Battery.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1828</td>
 *                      <td>Data Not Found.</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case the student is not found.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1830</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case provided Test Status is invalid.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1831</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case provided LineReader value is not valid.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1832</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case provided TextToSpeech value is not valid.</td>
 *                   </tr>
 *                 </table>
 *                 PHP Code for Test Assignment Creation:
 *                 <pre class="pseudo-code">
 *                   date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server
 *                                                        uses NTP service.<br/>
 *                   <br/>
 *                   $testAssignmentId = ...<br/>
 *                   <br/>
 *                   $testId = ...<br/>
 *                   $testFormId = ...<br/>
 *                   $testKey = ... // Unique Test Key generated by LDR.<br/>
 *                   <br/>
 *                   $studentId = ...<br/>
 *                   $personalId = ...<br/>
 *                   $firstName = ...<br/>
 *                   $lastName = ...<br/>
 *                   $dateOfBirth = ...<br/>
 *                   $state = ...<br/>
 *                   $schoolName = ...<br/>
 *                   $grade = ...<br/>
 *                   $enableLineReader = ...<br/>
 *                   $enableTextToSpeech = ...<br/>
 *                   <br/>
 *                   $apiHost = "https://adp.parcc.dev";<br/>
 *                   $apiURI = "/api/assignment";<br/>
 *                   $username = "DocumentationLDR";<br/>
 *                   $password = "97f0ca00610888a0e9e58883dabc813b";<br/>
 *                   $secret = "6d393cf3b856c34ee5c55f7c50dfeb49";<br/>
 *                   <br/>
 *                   $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/>
 *                   $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix
 *                                                                              timestamp.<br/>
 *                   $hash = hash_hmac('sha256', $message, $secret);<br/>
 *                   $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/>
 *                   <br/>
 *                   $ch = curl_init($apiHost . $apiURI);<br/>
 *                   curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>
 *                   &nbsp;  "Content-Type": "application/json; charset=UTF-8",<br/>
 *                   &nbsp;  "Authorization: Basic " . base64_encode($username . ":" . $password),<br/>
 *                   &nbsp;  "Authentication:" . $authentication<br/>
 *                   ]);<br/>
 *                   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode({<br/>
 *                   &nbsp;  'testAssignmentId' => $testAssignmentId,<br/>
 *                   &nbsp;  'test' => {<br/>
 *                   &nbsp;    'testId' => $testId,<br/>
 *                   &nbsp;    'testFormId' => $testFormId,<br/>
 *                   &nbsp;    'testKey' => $testKey<br/>
 *                   &nbsp;    'enableLineReader' => $enableLineReader<br/>
 *                   &nbsp;    'enableTextToSpeech' => $enableTextToSpeech<br/>
 *                   &nbsp;  },<br/>
 *                   &nbsp;  'student' => {<br/>
 *                   &nbsp;    'studentId' => $studentId,<br/>
 *                   &nbsp;    'personalId' => $personalId,<br/>
 *                   &nbsp;    'firstName' => $firstName,<br/>
 *                   &nbsp;    'lastName' => $lastName,<br/>
 *                   &nbsp;    'dateOfBirth' => $dateOfBirth,<br/>
 *                   &nbsp;    'state' => $state,<br/>
 *                   &nbsp;    'schoolName' => $schoolName,<br/>
 *                   &nbsp;    'grade' => $grade<br/>
 *                   &nbsp;  }<br/>
 *                   }));<br/>
 *                   <br/>
 *                   $result = curl_exec($ch);<br/>
 *                   curl_close($ch);<br/>
 *                   ...
 *                 </pre><br/>
 *
 *
 * @apiHeader {String} username="DocumentationLDR" NOTE: This is a dummy field and is used only as a placeholder, so
 *                     Username can be entered in the form.<br/>
 *                     It's 20 characters long unique string.
 * @apiHeader {String} password="97f0ca00610888a0e9e58883dabc813b" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Password can be entered in
 *                     the form.<br/>
 *                     It's 60 characters long string.
 * @apiHeader {String} secret="6d393cf3b856c34ee5c55f7c50dfeb49" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Secret can be entered in the form.<br/>
 *                     It's 32 characters long MD5 hash string.
 * @apiHeader {String} Authentication="Base64EncodedStringXXXXXXXXXX==" HMAC Authentication using Secret key as salt for
 *                     SHA256 hashing of the request.
 * @apiHeader {String} Authorization="Basic Base64EncodedStringYYYYYYYYYY==" Basic Authentication using Username and
 *                     Password.
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * POST /api/assignment HTTP/1.1
 * Host: adp.parcc.dev
 * User-Agent: curl/7.40.0
 * Accept: * / *
 * Content-Type: application/json; charset=UTF-8
 * Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==
 * Authentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky
 * Content-Length: 89
 *
 *
 * @apiParam {Integer} testAssignmentId="100" Test Assignment ID is an ID used as a reference to LDR's Test Assignment
 *                     ID which is unique inside LDR, but may not be the same as those Test Assignments created by PRC
 *                     for Practice Tests and Quizzes.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {Integer} testId="1" Test Battery ID is the ID provided by ADP during Test Publishing and should be the
 *                     same as the ones used in LDR, so it is unique both inside ADP and LDR.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {Integer} testFormId="3" Test Battery Form ID is the ID provided by ADP during Test Publishing and should
 *                     be the same as the ones used in LDR, so it is unique both inside ADP and LDR.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {String}  testKey="ABCDEFGHIJ" Test Key is all uppercase, 10-character long string that is unique for each
 *                     Test Assignment. LDR generates Test Key when a Test Assignment is created and provides it to ADP.
 *                     LDR should not use Test Keys that begin with a predefined sequence of characters "PRACTxxxxx" as
 *                     these Test Keys are reserved for Practice Tests and Quizzes. These Test Keys are generated by ADP
 *                     Publisher when a Test Assignment for these types of Tests are created. Both ADP Publisher and LDR
 *                     should have this sequence configurable, so it can be easily changed to something else in the
 *                     future.<br/>
 *                     It's 10 characters long uppercase only unique string [A-Z].
 * @apiParam {Integer} studentId="3" Student ID is a unique ID for each Student assigned by LDR when Student is created.
 *                     ADP should use the same Student ID as LDR, so it is unique both inside ADP and LDR.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {String}  [personalId="IL-12345"] Personal ID is an arbitrary Student's ID used outside of ADS system, and
 *                     is imported into LDR from various external systems. It is also referenced as Student's Global
 *                     Unique Identifier, and can be Student' State ID, School ID, Local ID or some other kind of
 *                     Personal ID. Rules which state uses which ID are handled by LDR, and if Student has multiple IDs
 *                     LDR will decide which one will be provided to ADP to be used as Student's Personal ID during Test
 *                     Delivery.<br/>
 *                     It's 30 characters long string.
 * @apiParam {String}  [firstName="John"] First Name of the Student.<br/>
 *                     It's 35 characters long string.
 * @apiParam {String}  [lastName="Smith"] Last Name of the Student.<br/>
 *                     It's 35 characters long string.
 * @apiParam {String}  [dateOfBirth="2000-01-01"] Student's Date of Birth.<br/>
 *                     It's date in the ANSI SQL-92 format ('YYYY-MM-DD').
 * @apiParam {String}  [state="IL"] State in which Student's School is located (this doesn't necessarily mean the same
 *                     state where Student lives).<br/>
 *                     It's 2 character long predefined State abbreviation (all uppercase string).
 * @apiParam {String}  [schoolName="Evanston High School"] School Name which Student is attending.<br/>
 *                     It's 60 characters long string.
 * @apiParam {String}  [grade="K, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12"] Student's Grade.<br/>
 *                     It's 1 or 2 character long predefined string.
 * @apiParam {Boolean} [enableLineReader="true"] Indicator if Line Reader Tool should be available to Student while
 *                     taking a Test.<br/>
 *                     It's a boolean.
 * @apiParam {Boolean} [enableTextToSpeech="true"] Indicator if Text-to-Speech Tool should be available to Student while
 *                     taking a Test.<br/>
 *                     It's a boolean.
 *
 * @apiParamExample {JSON} Request Body Example for Existing Student
 *   {
 *     "testAssignmentId": 100,
 *     "test": {
 *       "testId": 1,
 *       "testFormId": 3,
 *       "testKey": "ABCDEFGHIJ",
 *       "enableLineReader": true,
 *       "enableTextToSpeech": false
 *     },
 *     "student": {
 *       "studentId": 3
 *     }
 *   }
 *
 *
 * @apiParamExample {JSON} Request Body Example for Non-Existing Student
 *   {
 *     "testAssignmentId": 100,
 *     "test": {
 *       "testId": 1,
 *       "testFormId": 3,
 *       "testKey": "ABCDEFGHIJ",
 *       "enableLineReader": true,
 *       "enableTextToSpeech": false
 *     },
 *     "student": {
 *       "studentId": 3,
 *       "personalId": "IL-12345",
 *       "firstName": "John",
 *       "lastName": "Smith",
 *       "dateOfBirth": "2000-01-01",
 *       "state": "IL",
 *       "schoolName": "Evanston High School",
 *       "grade": 10
 *     }
 *   }
 *
 *
 * @apiSuccess {JSON} data Dataset includes ADP Test Delivery's Test Assignment ID, Date and Time when Test Assignment
 *                         was created and its Status.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case SUCCESS).
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "testAssignmentId": 5,
 *     "createdTimestamp": "2015-05-05 05:05:05",
 *     "status": "Scheduled"
 *   }
 *
 * @apiSuccessExample Successful Response with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 3
 *     },
 *     "result": {
 *       "testAssignmentId": 5,
 *       "createdTimestamp": "2015-05-05 05:05:05",
 *       "status": "Scheduled"
 *     }
 *   }
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 412 Precondition Failed
 *   {
 *     "statusCode": 412,
 *     "statusDescription": "Precondition Failed",
 *     "errorCode": "ADP-1803",
 *     "errorDescription": "Invalid Request.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 412 Precondition Failed
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 412,
 *       "statusDescription": "Precondition Failed",
 *       "errorCode": "ADP-1803",
 *       "errorDescription": "Invalid Request.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function assignmentCreate()
{
}



/**********************************************************************************************************************/



/**
 * @apiGroup ADP Publisher
 * @api {PATCH} /api/assignment/{adpTestAssignmentId} Assignment Update
 * @apiName Assignment Update
 * @apiPermission LDR
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>LDR</b><br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Assignment web service is exclusively used by LDR for Test Assignments and Student details
 *                 publishing. It validates multiple parameters before it allows client to login into ADP Publisher
 *                 application.<br/>
 *                 Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/>
 *                 Test Assignment Update may include any of the following: only Test Status, only Test details (one
 *                 property or many), only Student details (one property or many), or any combination of these three
 *                 including all of them at once. In case Student already has at least one Test Assignment, Student
 *                 details can be updated either by calling Assignment Update or while creating a new Test Assignment.
 *                 In case Assignment Update is used, only one Test Assignment needs to be updated in order to have
 *                 Student details updated.<br/><br/>
 *                 In order to update existing Test Assignment and/or Student details, both Test Assignment ID and
 *                 Student ID must be provided as a reference. In addition to those, only those properties that need to
 *                 be updated are required, but there has to be at least one. Otherwise, ADP Publisher will return an
 *                 error as there's nothing to be updated. In addition, ADP Test Assignment ID must always be provided
 *                 as a reference.<br/>
 *                 Finally, Test Assignment can also be updated in order to change the Test Status (i.e. Reopen
 *                 Submitted Test, Cancel a Test, Confirm Test Submission and Complete the Test, or Reset Test Status
 *                 from 'InProgress' to 'Paused' in case of crash or if Student closed their browser by mistake).<br/>
 *                 <br/>
 *                 Only Test Assignments that are not Completed or are not already Canceled can be canceled. Only
 *                 Completed Test can be reopened in which case its status needs to be changed to 'Paused'. Only
 *                 Submitted Test can be Completed. Once Test Status changes to either Completed or Canceled, it will
 *                 automatically trigger Export of Test Results. Once completed, that Test cannot be reopened anymore
 *                 and a new Test Assignment would have to be created in order to re-take the same Test from scratch.
 *                 However, once new Test Assignment is started, a newer Test Battery Form Revision may be assigned
 *                 which may be different from the one originally taken.<br/><br/>
 *                 Once Test Assignment Request is received, if correct, ADP Publisher will create a Request Job in its
 *                 Job Queue and will process it as soon as possible depending on its priority, and number and priority
 *                 of other pending Request Jobs in its Job Queue.<br/><br/>
 *                 In case of successful request, it returns an ADP Test Assignment ID, Date and Time when it was
 *                 updated and its current Status.<br/>
 *                 In case of failure, it returns an error with error code and error description, HTTP status code and
 *                 HTTP status description, and current time on the server.<br/><br/>
 *                 This means that LDR should be able to roll back any changes if ADP doesn't return a confirmation
 *                 about successful update of Test Assignment and/or Student details.<br/><br/>
 *                 More details about HMAC Authentication can be found under Authentication section.<br/>
 *                 All possible generic API Errors can be found under Errors section.<br/><br/>
 *                 These are possible Test Assignment Update specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1803</td>
 *                      <td>Invalid Request.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case of missing index in the request.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1804</td>
 *                      <td>Invalid Request.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case of missing missing ADP Test Assignment Id in the request.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1805</td>
 *                      <td>Invalid Request.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case of missing index in the request.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1806</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case Test Assignment Id is missing or invalid.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1807</td>
 *                      <td>Data Not Found.</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case a test session cannot be found.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1809</td>
 *                      <td>Internal Error.</td>
 *                      <td>500</td>
 *                      <td>Internal Server Error/td>
 *                      <td>Returns JSON error which has a custom error message.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1810</td>
 *                      <td>Invalid Request.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case student ID is missing.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1811</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case student ID is not an unsigned integer.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1812</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case provided state is not valid.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1813</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case student's grade is provided but is not valid.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1814</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case student's date of birth is provided but is not a valid date.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1815</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case student's date of birth is provided but is not a valid
 *                          format.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1816</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case student's school name is provided but is not a valid format.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1817</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>ReturnsJSON error in case student's name is provided but is not a valid format.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1818</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case student's personal ID is provided but is not a valid format.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1819</td>
 *                      <td>Data Not Found.</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case the student can't be found.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1820</td>
 *                      <td>Invalid Request.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case of missing index in the request.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1821</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case test ID is not an unsigned integer.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1822</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case test form ID is not an unsigned integer.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1823</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case Test Key is not a valid format.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1824</td>
 *                      <td>Test Key already exists.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case if test key already exists.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1825</td>
 *                      <td>Data Not Found.</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case Test Session is not found.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1827</td>
 *                      <td>Internal Error.</td>
 *                      <td>500</td>
 *                      <td>Internal Server Error</td>
 *                      <td>Returns JSON error in case a Test Form does not belong to Test Battery.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1828</td>
 *                      <td>Data Not Found.</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case the student is not found.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1830</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case provided Test Status is invalid.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1831</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case provided LineReader value is not valid.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1832</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case provided TextToSpeech value is not valid.</td>
 *                   </tr>
 *                 </table>
 *                 PHP Code for Test Assignment Update:
 *                 <pre class="pseudo-code">
 *                   date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server
 *                                                        uses NTP service.<br/>
 *                   <br/>
 *                   $testAssignmentId = ...<br/>
 *                   $adpTestAssignmentId = ...; // ADP's Test Assignment ID that was provided when it was created the
 *                                                  first time.<br/>
 *                   $testStatus = ...; // Optional new Test Status requested by Proctor.<br/>
 *                   <br/>
 *                   $testId = ...<br/>
 *                   $testFormId = ...<br/>
 *                   $testKey = ... // Unique Test Key generated by LDR.<br/>
 *                   <br/>
 *                   $studentId = ...<br/>
 *                   $personalId = ...<br/>
 *                   $firstName = ...<br/>
 *                   $lastName = ...<br/>
 *                   $dateOfBirth = ...<br/>
 *                   $state = ...<br/>
 *                   $schoolName = ...<br/>
 *                   $grade = ...<br/>
 *                   $enableLineReader = ...<br/>
 *                   $enableTextToSpeech = ...<br/>
 *                   <br/>
 *                   $apiHost = "https://adp.parcc.dev";<br/>
 *                   $apiURI = "/api/assignment/" . $adpTestAssignmentId;<br/>
 *                   $username = "DocumentationLDR";<br/>
 *                   $password = "97f0ca00610888a0e9e58883dabc813b";<br/>
 *                   $secret = "6d393cf3b856c34ee5c55f7c50dfeb49";<br/>
 *                   <br/>
 *                   $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/>
 *                   $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix
 *                                                                              timestamp.<br/>
 *                   $hash = hash_hmac('sha256', $message, $secret);<br/>
 *                   $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/>
 *                   <br/>
 *                   $ch = curl_init($apiHost . $apiURI);<br/>
 *                   curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>
 *                   &nbsp;  "Content-Type": "application/json; charset=UTF-8",<br/>
 *                   &nbsp;  "Authorization: Basic " . base64_encode($username . ":" . $password),<br/>
 *                   &nbsp;  "Authentication:" . $authentication<br/>
 *                   ]);<br/>
 *                   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([<br/>
 *                   &nbsp;  'testAssignmentId' => $testAssignmentId,<br/>
 *                   &nbsp;  'status' => $testStatus,<br/>
 *                   &nbsp;  'test' => [<br/>
 *                   &nbsp;    'testId' => $testId,<br/>
 *                   &nbsp;    'testFormId' => $testFormId,<br/>
 *                   &nbsp;    'testKey' => $testKey<br/>
 *                   &nbsp;    'enableLineReader' => $enableLineReader<br/>
 *                   &nbsp;    'enableTextToSpeech' => $enableTextToSpeech<br/>
 *                   &nbsp;  ],<br/>
 *                   &nbsp;  'student' => [<br/>
 *                   &nbsp;    'studentId' => $studentId,<br/>
 *                   &nbsp;    'personalId' => $personalId,<br/>
 *                   &nbsp;    'firstName' => $firstName,<br/>
 *                   &nbsp;    'lastName' => $lastName,<br/>
 *                   &nbsp;    'dateOfBirth' => $dateOfBirth,<br/>
 *                   &nbsp;    'state' => $state,<br/>
 *                   &nbsp;    'schoolName' => $schoolName,<br/>
 *                   &nbsp;    'grade' => $grade<br/>
 *                   &nbsp;  ]<br/>
 *                   ]));<br/>
 *                   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');<br/>
 *                   <br/>
 *                   $result = curl_exec($ch);<br/>
 *                   curl_close($ch);<br/>
 *                   ...
 *                 </pre><br/>
 *
 *
 * @apiHeader {String} username="DocumentationLDR" NOTE: This is a dummy field and is used only as a placeholder, so
 *                     Username can be entered in the form.<br/>
 *                     It's 20 characters long unique string.
 * @apiHeader {String} password="97f0ca00610888a0e9e58883dabc813b" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Password can be entered in the form.<br/>
 *                     It's 60 characters long string.
 * @apiHeader {String} secret="6d393cf3b856c34ee5c55f7c50dfeb49" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Secret can be entered in the form.<br/>
 *                     It's 32 characters long MD5 hash string.
 * @apiHeader {String} Authentication="Base64EncodedStringXXXXXXXXXX==" HMAC Authentication using Secret key as salt
 *                     for SHA256 hashing of the request.
 * @apiHeader {String} Authorization="Basic Base64EncodedStringYYYYYYYYYY==" Basic Authentication using Username and
 *                     Password.
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * PATCH /api/assignment/5 HTTP/1.1
 * Host: adp.parcc.dev
 * User-Agent: curl/7.40.0
 * Accept: * / *
 * Content-Type: application/json; charset=UTF-8
 * Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==
 * Authentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky
 * Content-Length: 89
 *
 *
 * @apiParam {Integer} testAssignmentId="100" Test Assignment ID is an ID used as a reference to LDR's Test Assignment
 *                     ID which is unique inside LDR, but may not be the same as those Test Assignments created by PRC
 *                     for Practice Tests and Quizzes.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {Integer} adpTestAssignmentId="5" ADP Test Assignment ID is a unique ADP's ID provided when Test Assignment
 *                     was created. As ADP also has Practice Tests and Quizzes, this ID is different from LDR's Test
 *                     Assignment ID.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {Integer} [testStatus="'Paused', 'Completed', 'Canceled'"] Test Status determines current state of the Test
 *                     Assignment. Even though there are other possible Test States, LDR can only request changing it to
 *                     one of these 3. Once Test is Submitted or is already Canceled its Test Status cannot be changed
 *                     anymore. Only if Test is currently Submitted, LDR can either re-open it by changing its Test
 *                     Status back to 'Paused', or confirm/accept its submission by changing its status to 'Completed'.
 *                     These are the only cases when Test Status can be changed outside Test Driver. Any other changes
 *                     of Test Status can only be made by Test Driver.<br/>
 *                     It's one of the 3 predefined values.
 * @apiParam {Integer} testId="1" Test Battery ID is the ID provided by ADP during Test Publishing and should be the
 *                     same as the ones used in LDR, so it is unique both inside ADP and LDR.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {Integer} testFormId="3" Test Battery Form ID is the ID provided by ADP during Test Publishing and should
 *                     be the same as the ones used in LDR, so it is unique both inside ADP and LDR.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {String}  testKey="ABCDEFGHIJ" Test Key is all uppercase, 10-character long string that is unique for each
 *                     Test Assignment. LDR generates Test Key when a Test Assignment is created and provides it to ADP.
 *                     LDR should not use Test Keys that begin with a predefined sequence of characters "PRACTxxxxx" as
 *                     these Test Keys are reserved for Practice Tests and Quizzes. These Test Keys are generated by ADP
 *                     Publisher when a Test Assignment for these types of Tests are created. Both ADP Publisher and LDR
 *                     should have this sequence configurable, so it can be easily changed to something else in the
 *                     future.<br/>
 *                     It's 10 characters long uppercase only unique string [A-Z].
 * @apiParam {Integer} studentId="3" Student ID is a unique ID for each Student assigned by LDR when Student is created.
 *                     ADP should use the same Student ID as LDR, so it is unique both inside ADP and LDR.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {String}  [personalId="IL-12345"] Personal ID is an arbitrary Student's ID used outside of ADS system, and
 *                     is imported into LDR from various external systems. It is also referenced as Student's Global
 *                     Unique Identifier, and can be Student' State ID, School ID, Local ID or some other kind of
 *                     Personal ID. Rules which state uses which ID are handled by LDR, and if Student has multiple IDs
 *                     LDR will decide which one will be provided to ADP to be used as Student's Personal ID during Test
 *                     Delivery.<br/>
 *                     It's 30 characters long string.
 * @apiParam {String}  [firstName="John"] First Name of the Student.<br/>
 *                     It's 35 characters long string.
 * @apiParam {String}  [lastName="Smith"] Last Name of the Student.<br/>
 *                     It's 35 characters long string.
 * @apiParam {String}  [dateOfBirth="2000-01-01"] Student's Date of Birth.<br/>
 *                     It's date in the ANSI SQL-92 format ('YYYY-MM-DD').
 * @apiParam {String}  [state="IL"] State in which Student's School is located (this doesn't necessarily mean the same
 *                     state where Student lives).<br/>
 *                     It's 2 character long predefined State abbreviation (all uppercase string).
 * @apiParam {String}  [schoolName="Evanston High School"] School Name which Student is attending.<br/>
 *                     It's 60 characters long string.
 * @apiParam {String}  [grade="K, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12"] Student's Grade.<br/>
 *                     It's 1 or 2 character long predefined string.
 * @apiParam {Boolean} [enableLineReader="true"] Indicator if Line Reader Tool should be available to Student while
 *                     taking a Test.<br/>
 *                     It's a boolean.
 * @apiParam {Boolean} [enableTextToSpeech="true"] Indicator if Text-to-Speech Tool should be available to Student
 *                     while taking a Test.<br/>
 *                     It's a boolean.
 *
 * @apiParamExample {JSON} Request Body Example to Update Test Status only
 *   {
 *     "testAssignmentId": 100,
 *     "testStatus": "Completed",
 *     "student": {
 *       "studentId": 3
 *     }
 *   }
 *
 *
 * @apiParamExample {JSON} Request Body Example to Update Test only
 *   {
 *     "testAssignmentId": 100,
 *     "test": {
 *       "testId": 1,
 *       "testFormId": 3,
 *       "testKey": "ABCDEFGHIJ",
 *       "enableLineReader": true,
 *       "enableTextToSpeech": false
 *     },
 *     "student": {
 *       "studentId": 3
 *     }
 *   }
 *
 *
 * @apiParamExample {JSON} Request Body Example to Update Student only
 *   {
 *     "testAssignmentId": 100,
 *     "student": {
 *       "studentId": 3,
 *       "personalId": "IL-12345",
 *       "firstName": "John",
 *       "lastName": "Smith",
 *       "dateOfBirth": "2000-01-01",
 *       "state": "IL",
 *       "schoolName": "Evanston High School",
 *       "grade": 10
 *     }
 *   }
 *
 *
 * @apiParamExample {JSON} Request Body Example to Update everything
 *   {
 *     "testAssignmentId": 100,
 *     "testStatus": "Paused",
 *     "test": {
 *       "testId": 1,
 *       "testFormId": 3,
 *       "testKey": "ABCDEFGHIJ",
 *       "enableLineReader": true,
 *       "enableTextToSpeech": false
 *     },
 *     "student": {
 *       "studentId": 3,
 *       "personalId": "IL-12345",
 *       "firstName": "John",
 *       "lastName": "Smith",
 *       "dateOfBirth": "2000-01-01",
 *       "state": "IL",
 *       "schoolName": "Evanston High School",
 *       "grade": 10
 *     }
 *   }
 *
 *
 * @apiSuccess {JSON} data Dataset includes ADP Test Delivery's Test Assignment ID, Date and Time when Test Assignment
 *                         was updated and its Status.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case SUCCESS).
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "testAssignmentId": 5,
 *     "updatedTimestamp": "2015-05-05 05:05:05",
 *     "status": "Scheduled"
 *   }
 *
 * @apiSuccessExample Successful Response with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 3
 *     },
 *     "result": {
 *       "testAssignmentId": 5,
 *       "updatedTimestamp": "2015-05-05 05:05:05",
 *       "status": "Scheduled"
 *     }
 *   }
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 412 Precondition Failed
 *   {
 *     "statusCode": 412,
 *     "statusDescription": "Precondition Failed",
 *     "errorCode": "ADP-1803",
 *     "errorDescription": "Invalid Request.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 412 Precondition Failed
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 4
 *     },
 *     "result": {
 *       "statusCode": 412,
 *       "statusDescription": "Precondition Failed",
 *       "errorCode": "ADP-1803",
 *       "errorDescription": "Invalid Request.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function assignmentUpdate()
{
}



/**********************************************************************************************************************/



/**
 * @apiGroup ADP Publisher
 * @api {DELETE} /api/assignment/{adpTestAssignmentId} Assignment Deletion
 * @apiName Assignment Deletion
 * @apiPermission LDR
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>LDR</b><br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Assignment web service is exclusively used by LDR for Test Assignments and Student details
 *                 publishing. It validates multiple parameters before it allows client to login into ADP Publisher
 *                 application.<br/>
 *                 Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/>
 *                 In order to delete existing Test Assignment and/or Student details, both Test Assignment ID and
 *                 Student ID must be provided as a reference. In addition, ADP Test Assignment ID must always be
 *                 provided as a reference. ADP will delete Student only if there are no other active Test Assignments
 *                 assigned to the Student.<br/>
 *                 Finally, only Scheduled Test Assignment without any Test Results can be deleted, but those that are
 *                 already In Progress, are Paused, Submitted, Completed or Canceled cannot be deleted as they have
 *                 their Test Results which should never be deleted.<br/><br/>
 *                 Once Test Assignment Request is received, if correct, ADP Publisher will create a Request Job in its
 *                 Job Queue and will process it as soon as possible depending on its priority, and number and priority
 *                 of other pending Request Jobs in its Job Queue.<br/><br/>
 *                 In case of successful request, it returns an ADP Test Assignment ID, Date and Time when it was
 *                 updated and its current Status (in this case 'Deleted').<br/>
 *                 In case of failure, it returns an error with error code and error description, HTTP status code and
 *                 HTTP status description, and current time on the server.<br/><br/>
 *                 This means that LDR should be able to roll back any changes if ADP doesn't return a confirmation
 *                 about successful deletion of Test Assignment details and/or Student details.<br/>
 *                 If subsequent request for deletion of the same Test Assignment is sent, ADP Publisher will simply
 *                 return an error.<br/><br/>
 *                 More details about HMAC Authentication can be found under Authentication section.<br/>
 *                 All possible generic API Errors can be found under Errors section.<br/><br/>
 *                 These are possible Test Assignment Deletion specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1806</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case of missing or invalid format of the provided Test Assignment
 *                         details.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1810</td>
 *                     <td>Invalid Request.</td>
 *                     <td>412</td>
 *                     <td>Precondition Failed</td>
 *                     <td>Returns JSON error in case request body does not have the studentId.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1811</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case studentId format is not valid.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1850</td>
 *                     <td>Invalid Request.</td>
 *                     <td>412</td>
 *                     <td>Precondition Failed</td>
 *                     <td>Returns JSON error in case of missing or invalid format of the provided ADP Test Assignment
 *                         ID.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1851</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Precondition Failed</td>
 *                     <td>Returns JSON error in case the job already exists.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1852</td>
 *                     <td>Data Not Found.</td>
 *                     <td>404</td>
 *                     <td>Not Found</td>
 *                     <td>Returns JSON error in case the system is unable to find a test assignment to delete.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1853</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case the test assignment does not belong to actual user.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1854</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case the test assignment does not belong to the actual sutdent.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1855</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case the test assignment does not have the right test assignment
 *                         ID.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1856</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case the test assignment belongs to Practice Test.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1857</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns JSON error in case the the system is unable to delete assignment.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1858</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case the test assignment status is different than 'Scheduled'.</td>
 *                   </tr>
 *                 </table>
 *                 PHP Code for Test Assignment Deletion:
 *                 <pre class="pseudo-code">
 *                   date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server
 *                                                        uses NTP service.<br/>
 *                   <br/>
 *                   $testAssignmentId = ...<br/>
 *                   $adpTestAssignmentId = ...; // ADP's Test Assignment ID that was provided when it was created the
 *                                                  first time.<br/>
 *                   <br/>
 *                   $testId = ...<br/>
 *                   $testFormId = ...<br/>
 *                   $testKey = ... // Unique Test Key generated by LDR.<br/>
 *                   <br/>
 *                   $studentId = ...<br/>
 *                   <br/>
 *                   $apiHost = "https://adp.parcc.dev";<br/>
 *                   $apiURI = "/api/assignment/" . $adpTestAssignmentId;<br/>
 *                   $username = "DocumentationLDR";<br/>
 *                   $password = "97f0ca00610888a0e9e58883dabc813b";<br/>
 *                   $secret = "6d393cf3b856c34ee5c55f7c50dfeb49";<br/>
 *                   <br/>
 *                   $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/>
 *                   $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix
 *                                                                              timestamp.<br/>
 *                   $hash = hash_hmac('sha256', $message, $secret);<br/>
 *                   $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/>
 *                   <br/>
 *                   $ch = curl_init($apiHost . $apiURI);<br/>
 *                   curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>
 *                   &nbsp;  "Content-Type": "application/json; charset=UTF-8",<br/>
 *                   &nbsp;  "Authorization: Basic " . base64_encode($username . ":" . $password),<br/>
 *                   &nbsp;  "Authentication:" . $authentication<br/>
 *                   ]);<br/>
 *                   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode({<br/>
 *                   &nbsp;  'testAssignmentId' => $testAssignmentId,<br/>
 *                   &nbsp;  'status' => $testStatus,<br/>
 *                   &nbsp;  'test' => {<br/>
 *                   &nbsp;    'testId' => $testId,<br/>
 *                   &nbsp;    'testFormId' => $testFormId,<br/>
 *                   &nbsp;    'testKey' => $testKey<br/>
 *                   &nbsp;  },<br/>
 *                   &nbsp;  'student' => {<br/>
 *                   &nbsp;    'studentId' => $studentId<br/>
 *                   &nbsp;  }<br/>
 *                   }));<br/>
 *                   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');<br/>
 *                   <br/>
 *                   $result = curl_exec($ch);<br/>
 *                   curl_close($ch);<br/>
 *                   ...
 *                 </pre><br/>
 *
 *
 * @apiHeader {String} username="DocumentationLDR" NOTE: This is a dummy field and is used only as a placeholder, so
 *                     Username can be entered in the form.<br/>
 *                     It's 20 characters long unique string.
 * @apiHeader {String} password="97f0ca00610888a0e9e58883dabc813b" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Password can be entered in the form.<br/>
 *                     It's 60 characters long string.
 * @apiHeader {String} secret="6d393cf3b856c34ee5c55f7c50dfeb49" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Secret can be entered in the form.<br/>
 *                     It's 32 characters long MD5 hash string.
 * @apiHeader {String} Authentication="Base64EncodedStringXXXXXXXXXX==" HMAC Authentication using Secret key as salt for
 *                     SHA256 hashing of the request.
 * @apiHeader {String} Authorization="Basic Base64EncodedStringYYYYYYYYYY==" Basic Authentication using Username and
 *                     Password.
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * DELETE /api/assignment/5 HTTP/1.1
 * Host: adp.parcc.dev
 * User-Agent: curl/7.40.0
 * Accept: * / *
 * Content-Type: application/json; charset=UTF-8
 * Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==
 * Authentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky
 * Content-Length: 89
 *
 *
 * @apiParam {Integer} testAssignmentId="100" Test Assignment ID is an ID used as a reference to LDR's Test Assignment
 *                     ID which is unique inside LDR, but may not be the same as those Test Assignments created by PRC
 *                     for Practice Tests and Quizzes.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {Integer} adpTestAssignmentId="5" ADP Test Assignment ID is a unique ADP's ID provided when Test Assignment
 *                     was created. As ADP also has Practice
 *                     Tests and Quizzes, this ID is different from LDR's Test Assignment ID.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {Integer} studentId="3" Student ID is a unique ID for each Student assigned by LDR when Student is created.
 *                     ADP should use the same Student ID as LDR, so it is unique both inside ADP and LDR.<br/>
 *                     It's 32-bit unsigned unique integer.
 *
 * @apiParamExample {JSON} Request Body Example
 *   {
 *     "testAssignmentId": 100,
 *     "student": {
 *       "studentId": 3
 *     }
 *   }
 *
 *
 * @apiSuccess {JSON} data Dataset includes ADP Test Delivery's Test Assignment ID, Date and Time when Test Assignment
 *                         was updated and its Status.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case SUCCESS).
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "testAssignmentId": 5,
 *     "updatedTimestamp": "2015-05-05 05:05:05",
 *     "status": "Deleted"
 *   }
 *
 * @apiSuccessExample Successful Response with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 3
 *     },
 *     "result": {
 *       "testAssignmentId": 5,
 *       "updatedTimestamp": "2015-05-05 05:05:05",
 *       "status": "Deleted"
 *     }
 *   }
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 412 Precondition Failed
 *   {
 *     "statusCode": 412,
 *     "statusDescription": "Precondition Failed",
 *     "errorCode": "ADP-1810",
 *     "errorDescription": "Invalid Request.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 412 Precondition Failed
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 4
 *     },
 *     "result": {
 *       "statusCode": 412,
 *       "statusDescription": "Precondition Failed",
 *       "errorCode": "ADP-1810",
 *       "errorDescription": "Invalid Request.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function assignmentDelete()
{
}
