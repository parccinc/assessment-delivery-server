<?php
/**
 * @apiDefine TestDriver_Results User Authentication includes:
 *                               <ol>
 *                                 <li>Basic Authentication</li>
 *                                 <li>HMAC Authentication</li>
 *                                 <li>X-Requested-With header</li>
 *                                 <li>Token</li>
 *                                 <li>Test Battery Form Revision ID</li>
 *                               </ol>
 */
/**
 * @apiGroup ADP Test Delivery
 * @api {GET} /api/results/{token}/{testId} Results Download
 * @apiName Results Download
 * @apiPermission TestDriver_Results
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>Test Driver</b><br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Results web service is exclusively used by Test Delivery for uploading and downloading of Test
 *                 Results. It validates multiple parameters before it allows client to interact with Test Results.<br/>
 *                 Authentication includes: Basic Authentication, HMAC Authentication and Test Key validation.<br/><br/>
 *                 Each time Test Results are uploaded to the server, ADP Test Delivery creates a snapshot of Test
 *                 Results and keeps all previously uploaded snapshots of Test Results. In addition, it validates used
 *                 Token and if valid, the most recently uploaded Test Results can be downloaded later as part of Test
 *                 Resume process. However, if used Token has expired, or is not valid, ADP Test Delivery will accept
 *                 uploaded Test Results, but will not use them during Test Resume process &ndash; these Test Results
 *                 can be used for Test Resume only if manually activated (they are inactive by default), or can be used
 *                 for further analysis.<br/><br/>
 *                 In case of successful authentication, it returns the most recent snapshot of the Test Results in case
 *                 Test Results were previously uploaded successfully at least once.<br/>
 *                 In case of failure, it returns an error with error code and error description, HTTP status code and
 *                 HTTP status description, and current time on the server. In case of Results Download, these details
 *                 are returned within response header, but the response body is omitted.<br/><br/>
 *                 More details about HMAC Authentication can be found under Authentication section.<br/>
 *                 All possible generic API Errors can be found under Errors section.<br/><br/>
 *                 These are possible Content specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1400</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case Test is a Practice Test.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1401</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case Token doesn't match.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1402</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case Test Form Revision doesn't match.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1403</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case referenced Test is already Submitted and cannot be resumed.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1404</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case referenced Test is already Completed and cannot be resumed.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1405</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case referenced Test is Canceled and cannot be resumed.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1406</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case Test Token has expired.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1407</td>
 *                     <td>Data Not Found.</td>
 *                     <td>404</td>
 *                     <td>Not Found</td>
 *                     <td>Returns JSON error in case no Test Results were found.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-16xx</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns JSON error in case Test Results failed to be downloaded from AWS S3. Refer to S3
 *                         Errors for more details about possible errors.</td>
 *                   </tr>
 *                 </table>
 *                 JavaScript Code for Results Download:
 *                 <pre class="pseudo-code">
 *                   // Time Offset represent time difference between server and client in seconds. It is important that
 *                      all timestamps coming from the<br/>
 *                   // client side are synced with the server in UTC time zone as client can be in any (possibly wrong)
 *                      timezone, and client clock can<br/>
 *                   // be off or can be tampered with. Therefore, anything related to time and date must be relative to
 *                      a timestamp received form the<br/>
 *                   // server which is always synchronized with Atomic Clock Server using NTP protocol.<br/>
 *                   // Server time is always provided as 10-digit Unit timestamp in UTC timezone.<br/>
 *                   var timeOffset = 1000 * serverTime - new Date().getTime();<br/>
 *                   <br/>
 *                   var apiHost = "https://adp.parcc.dev";<br/>
 *                   var username = "DocumentationTD";<br/>
 *                   var password = "53c5de17b5488f1a8549e6db1786ca81";<br/>
 *                   var secret = "578cca1aa71f702c26cc81439392124c";<br/>
 *                   <br/>
 *                   var testKey = $('#testKey').val();<br/>
 *                   var token = $('#token').val();<br/>
 *                   var testId = $('#testId').val();<br/>
 *                   <br/>
 *                   var apiURI = "/api/results/" + token + "/" + testId;<br/>
 *                   <br/>
 *                   var timestamp = new Date().getTime() + timeOffset; // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   var nonce = Math.floor(Math.random() * 99999); // 1-5 digit long random integer in [0, 99999)
 *                                                                     range.<br/>
 *                   var message = apiURI + Math.round(timestamp / 1000) + nonce; // Message uses rounded 10-digit Unix
 *                                                                                   timestamp.<br/>
 *                   var hash = $.sha256hmac(secret, message);<br/>
 *                   var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(hash));<br/>
 *                   <br/>
 *                   $.ajax({<br/>
 *                   &nbsp;  url: apiHost + apiURI,<br/>
 *                   &nbsp;  type: 'GET',<br/>
 *                   &nbsp;  headers: {<br/>
 *                   &nbsp;    'Content-Type': "application/json; charset=UTF-8",<br/>
 *                   &nbsp;    'Authorization': "Basic " + btoa(username + ":" + password),<br/>
 *                   &nbsp;    'Authentication': authentication,<br/>
 *                   &nbsp;    'X-Requested-With': btoa(testKey)<br/>
 *                   &nbsp;  },<br/>
 *                   &nbsp;  ...<br/>
 *                   })
 *                 </pre><br/>
 *
 *
 * @apiHeader {String} username="DocumentationTD" NOTE: This is a dummy field and is used only as a placeholder, so
 *                     Username can be entered in the form.<br/>
 *                     It's 20 characters long unique string.
 * @apiHeader {String} password="53c5de17b5488f1a8549e6db1786ca81" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Password can be entered in the form.<br/>
 *                     It's 60 characters long string.
 * @apiHeader {String} secret="578cca1aa71f702c26cc81439392124c" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Secret can be entered in the form.<br/>
 *                     It's 32 characters long MD5 hash string.
 * @apiHeader {String} Authentication="Base64EncodedStringXXXXXXXXXX==" HMAC Authentication using Secret key as salt for
 *                     SHA256 hashing of the request.
 * @apiHeader {String} Authorization="Basic Base64EncodedStringYYYYYYYYYY==" Basic Authentication using Username and
 *                     Password.
 * @apiHeader {String} X-Requested-With="Base64EncodedStringZZZZZZZZZZ==" Specific custom header with given Test Key
 *                     which is allowed to be used only with valid CORS policy in place. This parameter is unique to
 *                     Login and Results web services.
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * GET /api/results/3c564954-0096-11e5-3123-10def1bd7269/3 HTTP/1.1
 * Host: adp.parcc.dev
 * Origin: https://adp.parcc.dev
 * User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
 * Accept: application/json, text/javascript; q=0.01
 * Accept-Language: en-US,en;q=0.5
 * Accept-Encoding: gzip, deflate
 * Content-Type: application/json; charset=UTF-8
 * Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==
 * Authentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky
 * X-Requested-With: QUFBQUFBQUFBQQ==
 * Referer: https://adp.parcc.dev/TestDriver/
 * Content-Length: 89
 * Connection: keep-alive
 * Pragma: no-cache
 * Cache-Control: no-cache
 *
 *
 * @apiParam {String}  testKey="DOCUMENTAT" Test Key is unique per test assignment for diagnostic tests, but is the same
 *                     for any practice test or quiz.<br/>
 *                     It's 10 characters long uppercase only unique string [A-Z].
 * @apiParam {String}  token="3c564954-0096-11e5-3123-10def1bd7269" Token is unique per test assignment, including
 *                     practice tests and quizzes. It is received as part of successful Login response. It also has an
 *                     expiration timestamp and once expired it will not be valid anymore.<br/>
 *                     It's 36 characters long unique GUID (lowercase only alphanumeric string with dashes).
 * @apiParam {Integer} testId="3" Test Battery Form Revision ID currently being taken by the Student. This is returned
 *                     by Login web service and is part of the Test Assignment.<br/>
 *                     It's 32-bit unsigned unique integer.
 *
 *
 * @apiParamExample {URL} Request URL Example
 *   GET /api/results/3c564954-0096-11e5-3123-10def1bd7269/3
 *
 *
 * @apiSuccess {JSON} data Dataset represents most recently uploaded Test Results.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case SUCCESS).
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "dynamic" : {
 *       "id" : "11",
 *       "state" : {
 *         "value" : null
 *       },
 *       "events" : [],
 *       "current" : 0,
 *       "remainingTime" : -4130
 *     },
 *     "testPart" : [
 *       {
 *         "dynamic" : {
 *           "state" : {
 *             "value" : null
 *           },
 *           "events" : [],
 *           "history" : [],
 *           "current" : 0
 *         },
 *         "assessmentSection" : [
 *           {
 *             "dynamic" : {
 *               "state" : {
 *                 "value" : null
 *               },
 *               "events" : [],
 *               "history" : [],
 *               "current" : 12
 *             },
 *             "assessmentItem" : [
 *               {
 *                 "dynamic" : {
 *                   "state" : {
 *                     "value" : {
 *                       "RESPONSE" : {
 *                         "response" : {
 *                           "base" : {
 *                             "integer" : 31
 *                           }
 *                         }
 *                       }
 *                     }
 *                   },
 *                   "response" : {
 *                     "value" : [
 *                       {
 *                         "RESPONSE" : {
 *                           "base" : {
 *                             "integer" : 31
 *                           }
 *                         }
 *                       }
 *                     ]
 *                   },
 *                   "score" : {
 *                     "value" : {
 *                       "RESPONSE" : {
 *                         "base" : {
 *                           "integer" : 31
 *                         }
 *                       },
 *                       "SCORE" : {
 *                         "base" : null
 *                       }
 *                     },
 *                     "error" : false
 *                   },
 *                   "events" : [
 *                     {
 *                       "type" : "render",
 *                       "value" : null,
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425089322380
 *                     }, {
 *                       "type" : "loaded",
 *                       "value" : null,
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425089322414
 *                     }, {
 *                       "type" : "init",
 *                       "value" : {
 *                         "RESPONSE" : {
 *                           "response" : {
 *                             "base" : {
 *                               "integer" : 0
 *                             }
 *                           }
 *                         }
 *                       },
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425089322419
 *                     }, {
 *                       "type" : "render",
 *                       "value" : null,
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425181075666
 *                     }, {
 *                       "type" : "loaded",
 *                       "value" : null,
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425181075673
 *                     }, {
 *                       "type" : "state",
 *                       "value" : {
 *                         "RESPONSE" : {
 *                           "response" : {
 *                             "base" : {
 *                               "integer" : 29
 *                             }
 *                           }
 *                         }
 *                       },
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425181079491
 *                     }, {
 *                       "type" : "response",
 *                       "value" : [
 *                         {
 *                           "RESPONSE" : {
 *                             "base" : {
 *                               "integer" : 29
 *                             }
 *                           }
 *                         }
 *                       ],
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425181079492
 *                     }
 *                   ],
 *                   "flagged" : true
 *                 }
 *               }
 *             ]
 *           }
 *         ]
 *       }
 *     ]
 *   }
 *
 *
 * @apiSuccessExample Successful Response with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 2
 *     },
 *     "result": {
 *       "dynamic" : {
 *         "id" : "11",
 *         "state" : {
 *           "value" : null
 *         },
 *         "events" : [],
 *         "current" : 0,
 *         "remainingTime" : -4130
 *       },
 *       "testPart" : [
 *         {
 *           "dynamic" : {
 *             "state" : {
 *               "value" : null
 *             },
 *             "events" : [],
 *             "history" : [],
 *             "current" : 0
 *           },
 *           "assessmentSection" : [
 *             {
 *               "dynamic" : {
 *                 "state" : {
 *                   "value" : null
 *                 },
 *                 "events" : [],
 *                 "history" : [],
 *                 "current" : 12
 *               },
 *               "assessmentItem" : [
 *                 {
 *                   "dynamic" : {
 *                     "state" : {
 *                       "value" : {
 *                         "RESPONSE" : {
 *                           "response" : {
 *                             "base" : {
 *                               "integer" : 31
 *                             }
 *                           }
 *                         }
 *                       }
 *                     },
 *                     "response" : {
 *                       "value" : [
 *                         {
 *                           "RESPONSE" : {
 *                             "base" : {
 *                               "integer" : 31
 *                             }
 *                           }
 *                         }
 *                       ]
 *                     },
 *                     "score" : {
 *                       "value" : {
 *                         "RESPONSE" : {
 *                           "base" : {
 *                             "integer" : 31
 *                           }
 *                         },
 *                         "SCORE" : {
 *                           "base" : null
 *                         }
 *                       },
 *                       "error" : false
 *                     },
 *                     "events" : [
 *                       {
 *                         "type" : "render",
 *                         "value" : null,
 *                         "selector" : {
 *                           "test" : 0,
 *                           "section" : 0,
 *                           "item" : 0
 *                         },
 *                         "time" : 1425089322380
 *                       }, {
 *                         "type" : "loaded",
 *                         "value" : null,
 *                         "selector" : {
 *                           "test" : 0,
 *                           "section" : 0,
 *                           "item" : 0
 *                         },
 *                         "time" : 1425089322414
 *                       }, {
 *                         "type" : "init",
 *                         "value" : {
 *                           "RESPONSE" : {
 *                             "response" : {
 *                               "base" : {
 *                                 "integer" : 0
 *                               }
 *                             }
 *                           }
 *                         },
 *                         "selector" : {
 *                           "test" : 0,
 *                           "section" : 0,
 *                           "item" : 0
 *                         },
 *                         "time" : 1425089322419
 *                       }, {
 *                         "type" : "render",
 *                         "value" : null,
 *                         "selector" : {
 *                           "test" : 0,
 *                           "section" : 0,
 *                           "item" : 0
 *                         },
 *                         "time" : 1425181075666
 *                       }, {
 *                         "type" : "loaded",
 *                         "value" : null,
 *                         "selector" : {
 *                           "test" : 0,
 *                           "section" : 0,
 *                           "item" : 0
 *                         },
 *                         "time" : 1425181075673
 *                       }, {
 *                         "type" : "state",
 *                         "value" : {
 *                           "RESPONSE" : {
 *                             "response" : {
 *                               "base" : {
 *                                 "integer" : 29
 *                               }
 *                             }
 *                           }
 *                         },
 *                         "selector" : {
 *                           "test" : 0,
 *                           "section" : 0,
 *                           "item" : 0
 *                         },
 *                         "time" : 1425181079491
 *                       }, {
 *                         "type" : "response",
 *                         "value" : [
 *                           {
 *                             "RESPONSE" : {
 *                               "base" : {
 *                                 "integer" : 29
 *                               }
 *                             }
 *                           }
 *                         ],
 *                         "selector" : {
 *                           "test" : 0,
 *                           "section" : 0,
 *                           "item" : 0
 *                         },
 *                         "time" : 1425181079492
 *                       }
 *                     ],
 *                     "flagged" : true
 *                   }
 *                 }
 *               ]
 *             }
 *           ]
 *         }
 *       ]
 *     }
 *   }
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 401 Unauthorized
 *   {
 *     "statusCode": 401,
 *     "statusDescription": "Unauthorized",
 *     "errorCode": "ADP-1400",
 *     "errorDescription": "Authorization Failed.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 401 Unauthorized
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 401,
 *       "statusDescription": "Unauthorized",
 *       "errorCode": "ADP-1400",
 *       "errorDescription": "Authorization Failed.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function downloadResults()
{
}



/**********************************************************************************************************************/



/**
 * @apiGroup ADP Test Delivery
 * @api {POST} /api/results/{token}/{testId}/{testStatus='InProgress'} Results Upload
 * @apiName Results Upload
 * @apiPermission TestDriver_Results
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>Test Driver</b><br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Results web service is exclusively used by Test Delivery for uploading and downloading of Test
 *                 Results. It validates multiple parameters before it allows client to interact with Test Results.<br/>
 *                 Authentication includes: Basic Authentication, HMAC Authentication and Test Key validation.<br/><br/>
 *                 Each time Test Results are uploaded to the server, ADP Test Delivery creates a snapshot of Test
 *                 Results and keeps all previously uploaded snapshots of Test Results. In addition, it validates used
 *                 Token and if valid, the most recently uploaded Test Results can be downloaded later as part of Test
 *                 Resume process. However, if used Token has expired, or is not valid, ADP Test Delivery will accept
 *                 uploaded Test Results, but will not use them during Test Resume process &ndash; these Test Results
 *                 can be used for Test Resume only if manually activated (they are inactive by default), or can be used
 *                 for further analysis.<br/><br/>
 *                 In case of successful authentication, it returns a confirmation about Results Upload, current Time on
 *                 the server as a Unix Timestamp and the current Test Status. The Server Timestamp is used by Test
 *                 Driver so it stays in sync with ADP Test Delivery and is constantly adjust its Time Offset. All used
 *                 timestamps in generated Test Results are synced with the server and are in UTC Time Zone, regardless
 *                 if the client computer has properly configured Date, Time and/or Time Zone, or if someone is trying
 *                 to tamper Test Results by manually changing Date and/or Time. The returned current Test Status is
 *                 used both as a confirmation, and as a request coming from the Proctor to change the current Test
 *                 Status to received Test Status (i.e. request to immediately pause the current test session in which
 *                 case Test Driver would enforce the Save & Exit procedure to immediately pauses the current Test
 *                 Session). In case of failure, it returns an error with error code and error description, HTTP status
 *                 code and HTTP status description, and current time on the server. In case of Results Download, these
 *                 details are returned within response header, but the response body is omitted.<br/><br/>
 *                 More details about HMAC Authentication can be found under Authentication section.<br/>
 *                 All possible generic API Errors can be found under Errors section.<br/><br/>
 *                 These are possible Content specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1450</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case Test is a Practice Test.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1451</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case referenced Test Token doesn't match Test Session.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1452</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case referenced Test Form Revision doesn't match Test Session.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1453</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case referenced Test is already Submitted and cannot be taken.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1454</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case referenced Test is already Completed and cannot be taken.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1455</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case referenced Test is Canceled and cannot be taken.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1456</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case Test that is not in progress was tried to be Paused.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1457</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns JSON error in case Test that is not in progress was tried to be Submitted.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1458</td>
 *                     <td>Invalid Request.</td>
 *                     <td>406</td>
 *                     <td>Not Acceptable</td>
 *                     <td>Returns JSON error in case provided Test Status is invalid (neither Paused nor
 *                         Submitted).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1459</td>
 *                     <td>Access Denied.</td>
 *                     <td>408</td>
 *                     <td>Request Timeout</td>
 *                     <td>Returns JSON error in case Test Token has expired and test status is Scheduled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1460</td>
 *                     <td>Access Denied.</td>
 *                     <td>408</td>
 *                     <td>Request Timeout</td>
 *                     <td>Returns JSON error in case Test Token has expired and test status is Paused.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1461</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns JSON error in case Results Schema cannot be loaded due to missing Results JSON Schema
 *                         file on the server.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1462</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns JSON error in case Results Schema cannot be loaded due to corrupted Results JSON
 *                         Schema file on the server.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1463</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns JSON error in case Results Schema cannot be loaded due to invalid Results JSON Schema
 *                         file structure on the server.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1464</td>
 *                     <td>Invalid Request.</td>
 *                     <td>412</td>
 *                     <td>Precondition Failed</td>
 *                     <td>Returns JSON error in case Results failed validation against Results JSON Schema and are not
 *                         saved.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1465</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns JSON error in case Test Results reference failed to be created in Database.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1466</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns JSON error in case Test Results reference failed to be updated in Database with Test
 *                         Results path.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1467</td>
 *                     <td>Access Denied.</td>
 *                     <td>408</td>
 *                     <td>Request Timeout</td>
 *                     <td>Returns JSON error in case Test Results were successfully saved, but System Paused the Test
 *                         by force due to expired Test Token.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-16xx</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns JSON error in case Test Results failed to be uploaded to AWS S3. Refer to S3 Errors
 *                         for more details about possible errors.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>TD-1001</td>
 *                     <td>There was a problem starting your test.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>This is Test Driver unique Error (not coming from ADP API) being displayed when Test is being
 *                         Started/Resumed and Results were successfully uploaded, but Test Status did not change to
 *                         "InProgress" as expected.</td>
 *                   </tr>
 *                 </table>
 *                 JavaScript Code for Results Upload:
 *                 <pre class="pseudo-code">
 *                   // Time Offset represent time difference between server and client in seconds. It is important that
 *                      all timestamps coming from the<br/>
 *                   // client side are synced with the server in UTC time zone as client can be in any (possibly wrong)
 *                      timezone, and client clock can<br/>
 *                   // be off or can be tampered with. Therefore, anything related to time and date must be relative to
 *                      a timestamp received form the<br/>
 *                   // server which is always synchronized with Atomic Clock Server using NTP protocol.<br/>
 *                   // Server time is always provided as 10-digit Unit timestamp in UTC timezone.<br/>
 *                   var timeOffset = 1000 * serverTime - new Date().getTime();<br/>
 *                   <br/>
 *                   var apiHost = "https://adp.parcc.dev";<br/>
 *                   var username = "DocumentationTD";<br/>
 *                   var password = "53c5de17b5488f1a8549e6db1786ca81";<br/>
 *                   var secret = "578cca1aa71f702c26cc81439392124c";<br/>
 *                   <br/>
 *                   var testKey = $('#testKey').val();<br/>
 *                   var token = $('#token').val();<br/>
 *                   var testId = $('#testId').val();<br/>
 *                   <br/>
 *                   var testStatus = $('#testStatus').val();<br/>
 *                   var resultsFile = $('#resultsFile').val();<br/>
 *                   <br/>
 *                   var apiURI = "/api/results/" + token + "/" + testId +
 *                                (testStatus <span class="exclamation-mark"/>== "" ? ("/" + testStatus) : "");<br/>
 *                   <br/>
 *                   var timestamp = new Date().getTime() + timeOffset; // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   var nonce = Math.floor(Math.random() * 99999); // 1-5 digit long random integer in [0, 99999)
 *                                                                     range.<br/>
 *                   var message = apiURI + Math.round(timestamp / 1000) + nonce; // Message uses rounded 10-digit Unix
 *                                                                                   timestamp.<br/>
 *                   var hash = $.sha256hmac(secret, message);<br/>
 *                   var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(hash));<br/>
 *                   <br/>
 *                   $.ajax({<br/>
 *                   &nbsp;  url: apiHost + apiURI,<br/>
 *                   &nbsp;  type: 'POST',<br/>
 *                   &nbsp;  headers: {<br/>
 *                   &nbsp;    'Content-Type': "application/json; charset=UTF-8",<br/>
 *                   &nbsp;    'Authorization': "Basic " + btoa(username + ":" + password),<br/>
 *                   &nbsp;    'Authentication': authentication,<br/>
 *                   &nbsp;    'X-Requested-With': btoa(testKey)<br/>
 *                   &nbsp;  },<br/>
 *                   &nbsp;  data: resultsFile,<br/>
 *                   &nbsp;  ...<br/>
 *                   })
 *                 </pre><br/>
 *
 *
 * @apiHeader {String} username="DocumentationTD" NOTE: This is a dummy field and is used only as a placeholder, so
 *                     Username can be entered in the form.<br/>
 *                     It's 20 characters long unique string.
 * @apiHeader {String} password="53c5de17b5488f1a8549e6db1786ca81" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Password can be entered in the form.<br/>
 *                     It's 60 characters long string.
 * @apiHeader {String} secret="578cca1aa71f702c26cc81439392124c" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Secret can be entered in the form.<br/>
 *                     It's 32 characters long MD5 hash string.
 * @apiHeader {String} Authentication="Base64EncodedStringXXXXXXXXXX==" HMAC Authentication using Secret key as salt for
 *                     SHA256 hashing of the request.
 * @apiHeader {String} Authorization="Basic Base64EncodedStringYYYYYYYYYY==" Basic Authentication using Username and
 *                     Password.
 * @apiHeader {String} X-Requested-With="Base64EncodedStringZZZZZZZZZZ==" Specific custom header with given Test Key
 *                     which is allowed to be used only with valid CORS policy in place. This parameter is unique to
 *                     Login and Results web services.
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * POST /api/results/3c564954-0096-11e5-3123-10def1bd7269/3/pause HTTP/1.1
 * Host: adp.parcc.dev
 * Origin: https://adp.parcc.dev
 * User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
 * Accept: application/json, text/javascript; q=0.01
 * Accept-Language: en-US,en;q=0.5
 * Accept-Encoding: gzip, deflate
 * Content-Type: application/json; charset=UTF-8
 * Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==
 * Authentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky
 * X-Requested-With: QUFBQUFBQUFBQQ==
 * Referer: https://adp.parcc.dev/TestDriver/
 * Content-Length: 89
 * Connection: keep-alive
 * Pragma: no-cache
 * Cache-Control: no-cache
 *
 *
 * @apiParam {String}  testKey="DOCUMENTAT" Test Key is unique per test assignment for diagnostic tests, but is the same
 *                     for any practice test or quiz.<br/>
 *                     It's 10 characters long uppercase only unique string [A-Z].
 * @apiParam {String}  token="3c564954-0096-11e5-3123-10def1bd7269" Token is unique per test assignment, including
 *                     practice tests and quizzes. It is received as part of successful Login response. It also has an
 *                     expiration timestamp and once expired it will not be valid anymore.<br/>
 *                     It's 36 characters long unique GUID (lowercase only alphanumeric string with dashes).
 * @apiParam {Integer} testId="3" Test Battery Form Revision ID currently being taken by the Student. This is returned
 *                     by Login web service and is part of the Test Assignment.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {String}  [testStatus="'', 'paused', 'submit'"] Test Status that should be updated upon successful upload
 *                     of Test Results. By default, it's omitted which means Test Status should be "InProgress". In case
 *                     Test Results are uploaded as part of Save & Exit process, it should be "pause", and in case of
 *                     Test Submission it should be "submit".<br/>
 *                     It's one of the 3 predefined values.
 * @apiParam {JSON}    resultsFile="" Actual Test Results being uploaded. ADP will validate uploaded Test Results
 *                     against Test Results JSON schema and if valid, it will save them as new snapshot which may be
 *                     used later for download to resume previously paused Test.
 *
 *
 * @apiParamExample {JSON} Request Body Example
 *   {
 *     "dynamic" : {
 *       "id" : "11",
 *       "state" : {
 *         "value" : null
 *       },
 *       "events" : [],
 *       "current" : 0,
 *       "remainingTime" : -4130
 *     },
 *     "testPart" : [
 *       {
 *         "dynamic" : {
 *           "state" : {
 *             "value" : null
 *           },
 *           "events" : [],
 *           "history" : [],
 *           "current" : 0
 *         },
 *         "assessmentSection" : [
 *           {
 *             "dynamic" : {
 *               "state" : {
 *                 "value" : null
 *               },
 *               "events" : [],
 *               "history" : [],
 *               "current" : 12
 *             },
 *             "assessmentItem" : [
 *               {
 *                 "dynamic" : {
 *                   "state" : {
 *                     "value" : {
 *                       "RESPONSE" : {
 *                         "response" : {
 *                           "base" : {
 *                             "integer" : 31
 *                           }
 *                         }
 *                       }
 *                     }
 *                   },
 *                   "response" : {
 *                     "value" : [
 *                       {
 *                         "RESPONSE" : {
 *                           "base" : {
 *                             "integer" : 31
 *                           }
 *                         }
 *                       }
 *                     ]
 *                   },
 *                   "score" : {
 *                     "value" : {
 *                       "RESPONSE" : {
 *                         "base" : {
 *                           "integer" : 31
 *                         }
 *                       },
 *                       "SCORE" : {
 *                         "base" : null
 *                       }
 *                     },
 *                     "error" : false
 *                   },
 *                   "events" : [
 *                     {
 *                       "type" : "render",
 *                       "value" : null,
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425089322380
 *                     }, {
 *                       "type" : "loaded",
 *                       "value" : null,
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425089322414
 *                     }, {
 *                       "type" : "init",
 *                       "value" : {
 *                         "RESPONSE" : {
 *                           "response" : {
 *                             "base" : {
 *                               "integer" : 0
 *                             }
 *                           }
 *                         }
 *                       },
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425089322419
 *                     }, {
 *                       "type" : "render",
 *                       "value" : null,
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425181075666
 *                     }, {
 *                       "type" : "loaded",
 *                       "value" : null,
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425181075673
 *                     }, {
 *                       "type" : "state",
 *                       "value" : {
 *                         "RESPONSE" : {
 *                           "response" : {
 *                             "base" : {
 *                               "integer" : 29
 *                             }
 *                           }
 *                         }
 *                       },
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425181079491
 *                     }, {
 *                       "type" : "response",
 *                       "value" : [
 *                         {
 *                           "RESPONSE" : {
 *                             "base" : {
 *                               "integer" : 29
 *                             }
 *                           }
 *                         }
 *                       ],
 *                       "selector" : {
 *                         "test" : 0,
 *                         "section" : 0,
 *                         "item" : 0
 *                       },
 *                       "time" : 1425181079492
 *                     }
 *                   ],
 *                   "flagged" : true
 *                 }
 *               }
 *             ]
 *           }
 *         ]
 *       }
 *     ]
 *   }
 *
 *
 * @apiSuccess {JSON} data Dataset represents most recently uploaded Test Results.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case SUCCESS).
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "api": {
 *       "expires": 1434252894,
 *       "time": 1434249325
 *     },
 *     "test": {
 *       "status": "InProgress"
 *     }
 *   }
 *
 *
 * @apiSuccessExample Successful Response with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 2
 *     },
 *     "result": {
 *       "api": {
 *         "expires": 1434252894,
 *         "time": 1434249325
 *       },
 *       "test": {
 *         "status": "InProgress"
 *       }
 *     }
 *   }
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 401 Unauthorized
 *   {
 *     "statusCode": 401,
 *     "statusDescription": "Unauthorized",
 *     "errorCode": "ADP-1450",
 *     "errorDescription": "Authorization Failed.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 401 Unauthorized
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 401,
 *       "statusDescription": "Unauthorized",
 *       "errorCode": "ADP-1450",
 *       "errorDescription": "Authorization Failed.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function uploadResults()
{
}
