<?php
/**
 * @apiGroup ADP Common
 * @api / S3 Errors
 * @apiName S3 Errors
 * @apiVersion 1.18.2
 *
 * @apiDescription Client: <b>ACR</b>, <b>LDR</b>, <b>PRC</b> and <b>Test Driver</b><br/><br/>
 *                 These are generic S3 errors returned by ADP S3 Library which may be caused by one of the following:
 *                 <ol>
 *                   <li>ADP configuration is missing AWS S3 credentials or contains invalid credentials.</li>
 *                   <li>AWS EC2 Server doesn't have assigned AWS IAM Role.</li>
 *                   <li>ADP API is not able to communicate with AWS S3 API (i.e. no network connectivity or connection
 *                       is blocked by firewall).</li>
 *                   <li>AWS API returned an unexpected response.</li>
 *                   <li>Request content was not found in AWS S3 (i.e. references in ADP database don't match content in
 *                       AWS S3).</li>
 *                   <li>Provided content failed to be uploaded to AWS S3 for various reasons.</li>
 *                   <li>An internal error/exception triggered by a request.</li>
 *                   <li>Something else.</li>
 *                 </ol>
 *                 In most cases ADP will just log these errors, but will return a different error depending on
 *                 where/how S3 Library is being utilized, but will always include the Error Code being provided by S3
 *                 Library and will add appropriate HTTP Code and HTTP Status, and will replace Error Description with a
 *                 custom one depending on what original action triggered S3 Error. Therefore, these descriptions are
 *                 just as a reference what will be included in the Application Log.<br/>
 *                 These are possible S3 errors:<br/><br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1600</td>
 *                     <td>S3 S3Authenticate Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response was received from AWS Metadata API while trying to retrieve
 *                         IAM Role.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1601</td>
 *                     <td>S3 S3Authenticate Error: [{AWSErrorCode}] {AWSErrorDescription}.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when an Error is received from AWS Metadata API while trying to retrieve IAM
 *                         Role.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1602</td>
 *                     <td>S3 S3Authenticate Error: [{AWSResponseCode}] Unexpected Instance Profile Response.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response Code was received from AWS Metadata API while trying to
 *                         retrieve IAM Role.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1603</td>
 *                     <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response without Confirmation Code was received from AWS Metadata API
 *                         while trying to retrieve IAM Role.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1604</td>
 *                     <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response without IAM Role was received from AWS Metadata API while
 *                         trying to retrieve IAM Role.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1605</td>
 *                     <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response without Instance Profile details was received from AWS
 *                         Metadata API while trying to retrieve IAM Role.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1606</td>
 *                     <td>S3 S3Authenticate Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response was received from AWS STS API while trying to assume assigned
 *                         IAM Role.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1607</td>
 *                     <td>S3 S3Authenticate Error: [{AWSErrorCode}] {AWSErrorDescription}.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when an Error was received from AWS STS API while trying to assume assigned IAM
 *                         Role.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1608</td>
 *                     <td>S3 S3Authenticate Error: [{AWSResponseCode}] Unexpected Instance Profile Response.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response Code was received from AWS STS API while trying to assume
 *                         assigned IAM Role.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1609</td>
 *                     <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response without Confirmation Code was received from AWS STS API while
 *                         trying to assume assigned IAM Role.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1610</td>
 *                     <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response was received from AWS STS API while trying to assume assigned
 *                         IAM Role without Expiration Timestamp for received temporary Credentials.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1611</td>
 *                     <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response was received from AWS STS API while trying to assume assigned
 *                         IAM Role without Access Key ID.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1612</td>
 *                     <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response was received from AWS STS API while trying to assume assigned
 *                         IAM Role without Secret Access Key.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1613</td>
 *                     <td>S3 GetObjectList Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response Code was received from AWS S3 API while trying to get list of
 *                         Bucket Keys.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1614</td>
 *                     <td>S3 GetObjectList Error: [{AWSErrorCode}] {AWSErrorDescription}.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when an Error was received from AWS S3 API while trying to get list of Bucket
 *                         Keys.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1615</td>
 *                     <td>S3 DownloadObject Error: Unable to open file {FilePath} for writing.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when not able to save a file downloaded from AWS S3 to the provided FilePath (due
 *                         to invalid file path, lack of file system permissions, insufficient available space or
 *                         something else).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1616</td>
 *                     <td>S3 DownloadObject Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response Code was received from AWS S3 API while trying to download a
 *                         file from AWS S3 to a custom file path.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1617</td>
 *                     <td>S3 DownloadObject Error: [{AWSErrorCode}] {AWSErrorDescription}.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when an Error was received from AWS S3 API while trying to download a file from AWS
 *                         S3 to a custom file path.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1618</td>
 *                     <td>S3 GetObjectDetails Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response Code was received from AWS S3 API while trying to get details
 *                         for a file located in AWS S3.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1619</td>
 *                     <td>S3 GetObjectDetails Error: [{AWSErrorCode}] {AWSErrorDescription}.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when an Error was received from AWS S3 API while trying to get details of a file
 *                         located in AWS S3.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1620</td>
 *                     <td>S3 UploadObject Error: Invalid Object.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when an invalid or missing Object was requested to be uploaded to AWS S3.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1621</td>
 *                     <td>S3 UploadObject Error: Missing Object parameters.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when an Object requested to be uploaded to AWS S3 is missing empty or has a length
 *                         of zero.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1622</td>
 *                     <td>S3 UploadObject Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response Code was received from AWS S3 API while trying to upload an
 *                         Object to AWS S3.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1623</td>
 *                     <td>S3 UploadObject Error: [{AWSErrorCode}] {AWSErrorDescription}.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when an Error was received from AWS S3 API while trying to upload an Object to AWS
 *                         S3.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1624</td>
 *                     <td>S3 UploadObjectFile Error: Unable to open file {FilePath}.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when an invalid File was requested to be uploaded to AWS S3 (i.e. file is missing
 *                         or is omitted).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1625</td>
 *                     <td>S3 UploadObjectResource Error: Invalid File Resource.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when an invalid File Resource/Stream is requested to be uploaded to AWS S3.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1626</td>
 *                     <td>S3 UploadObjectResource Error: Unable to obtain Resource Size for File Resource.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when requested File Resource/Stream was requested to be uploaded to AWS S3 that
 *                         cannot be opened, is empty or has a size of zero (can also be caused by lack of appropriate
 *                         permissions or if a file resource is being locked by another process).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1627</td>
 *                     <td>S3 DeleteObject Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when invalid response Code was received from AWS S3 API while trying to delete a
 *                         file located in AWS S3.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1628</td>
 *                     <td>S3 DeleteObject Error: [{AWSErrorCode}] {AWSErrorDescription}.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when an Error was received from AWS S3 API while trying to delete a file located in
 *                         AWS S3.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1629</td>
 *                     <td>S3 GetCloudFrontSignedURL Error: Unable to open AWS CloudFront SSH Private Key.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when AWS SSH Key needed to generate AWS CloudFront Signed URL is missing or cannot
 *                         be opened (i.e. due to missing file permissions or wrong file path).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1630</td>
 *                     <td>S3 GetCloudFrontSignedURL Error: Unable to read AWS CloudFront SSH Private Key.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when AWS SSH Key needed to generate AWS CloudFront Signed URL cannot be opened and
 *                         fully read (i.e. due to missing file permissions).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1631</td>
 *                     <td>S3 GetCloudFrontSignedURL Error: Unable to parse AWS CloudFront SSH Private Key.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when AWS SSH Key needed to generate AWS CloudFront Signed URL cannot be
 *                         successfully parsed or is an invalid SSH Key.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1632</td>
 *                     <td>S3 GetCloudFrontSignedURL Error: Unable to sign CloudFront Custom Policy Statement.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>Triggered when AWS CloudFront Signed URL cannot be successfully signed with provided AWS SSH
 *                         Key (i.e. due OpenSSL Library throwing an error or not being installed on the server).</td>
 *                   </tr>
 *                 </table>
 *
 *
 * @apiError {JSON}   data Dataset that has the statusCode, statusDescription, errorCode and errorDescription, and
 *                         current time on the server.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case ERROR).
 * @apiError {HTML}   N/A  Simple "404: Page Not Found" or "403: Access Denied" HTML page.
 * @apiError {Header} N/A  Only a response header is returned with HTTP Status and HTTP Status Code (if turned on), but
 *                         response body is omitted.
 *
 * @apiErrorExample Error Response without Envelope:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "statusCode": 500,
 *     "statusDescription": "Internal Server Error",
 *     "errorCode": "ADP-1600",
 *     "errorDescription": "Failed to save Results Details.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 500,
 *       "statusDescription": "Internal Server Error",
 *       "errorCode": "ADP-1600",
 *       "errorDescription": "Failed to save Results Details.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function s3Errors()
{
}
