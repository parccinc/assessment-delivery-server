<?php
/**
 * @apiGroup ADP Common
 * @api / General
 * @apiName General
 * @apiVersion 1.9.1
 *
 * @apiDescription Client: <b>ACR</b>, <b>LDR</b>, <b>PRC</b> and <b>Test Driver</b><br/><br/>
 *                 ADP consists of multiple components:
 *                 <ol>
 *                   <li>
 *                     <b>ADP Test Delivery</b> &ndash; responsible to provide support to Test Driver which at the high
 *                     level includes:
 *                     <ol>
 *                       <li>Authentication</li>
 *                       <li>Content Delivery</li>
 *                       <li>Results Upload and Download</li>
 *                       <li>Basic Proctoring Functionality</li>
 *                     </ol>
 *                   </li>
 *                   <li>
 *                     <b>ADP Publisher</b> &ndash; responsible to provide support for synchronizing data across ADS
 *                     system which at the high level includes:
 *                     <ol>
 *                       <li>Test Publishing</li>
 *                       <li>Test Assignments with Student Details</li>
 *                       <li>Results Export</li>
 *                     </ol>
 *                   </li>
 *                   <li><b>ADP Quiz</b> &ndash; responsible to provide support for PRC and synchronize data between PRC
 *                       and ADS systems (TBD).</li>
 *                 </ol>
 *                 ADP provides APIs for RESTful web services and by default all web services require Authentication.
 *                 More details about Authentication can be found under Authentication section.<br/><br/>
 *                 Most web services are stateless and can be called directly without prior call to any other web
 *                 service. The only exception are Test Delivery web services which because of lower security on the
 *                 Test Driver side require that Login web service is called first so ADP can generate a token that will
 *                 be used throughout test delivery session and is required for all other test delivery web services
 *                 (Content and Results).<br/><br/>
 *                 In general, ADP supports the following request methods: GET, POST, PUT, PATCH, DELETE, OPTIONS,
 *                 HEAD.<br/>
 *                 In addition, each web service by default supports OPTIONS request method in order to provide support
 *                 for pre-flight requests triggered by browsers (clients in general) due to CORS (Cross-Origin Resource
 *                 Sharing) security policy.<br/><br/>
 *                 Depending on the request type (GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD) and web service handling
 *                 particular request, ADP can return a response in one of the following formats:<br/>
 *                 <ul>
 *                   <li>JSON formatted response (this is the default response type). JSON responses also have optional
 *                       envelope which provides additional details, but it can be turned off.</li>
 *                   <li>HTML Page (simple "404: Page Not Found" or "403: Access Denied").</li>
 *                   <li>Header response which only returns response header, whereas response body is omitted.</li>
 *                   <li>302 Redirect (Resource Moved Temporary) which is only used for Content web service.</li>
 *                 </ul>
 *                 By default, ADP API also returns HTTP Status and HTTP Status Code in the response header for all
 *                 errors, but this is optional and can be turned off.
 */
function general()
{
}
