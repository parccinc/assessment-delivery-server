<?php
/**
 * @apiGroup ADP Common
 * @api / Generic Errors
 * @apiName Errors
 * @apiVersion 1.22.3
 *
 * @apiDescription Client: <b>ACR</b>, <b>LDR</b>, <b>PRC</b> and <b>Test Driver</b><br/><br/>
 *                 These are generic errors returned by ADP API which may be caused by one of the following:
 *                 <ol>
 *                   <li>Requested API route does not exist or a requested path is not correct (i.e. the order of URI
 *                       segments is not valid).</li>
 *                   <li>Request is invalid (i.e. doesn't have all expected parameters).</li>
 *                   <li>Request details are invalid (i.e. invalid JSON request).</li>
 *                   <li>Request details do not match data found in database (i.e. invalid username and/or
 *                       password).</li>
 *                   <li>Request is outdated (i.e. timestamp signature of the request is too old).</li>
 *                   <li>Request is rejected because of missing precondition (i.e. missing, invalid or expired
 *                       token).</li>
 *                   <li>An internal error/exception triggered by a request (i.e. unable to connect to database).</li>
 *                   <li>Something else.</li>
 *                 </ol>
 *                 These are possible common ADP errors:<br/><br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1000</td>
 *                     <td>The system is currently undergoing maintenance. We are sorry for the inconvenience.<br/>
 *                         Please try again later.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns 503 HTML page, but without actual Error (the actual Error is only logged into ADP
 *                         application log file) when trying to launch access Test Driver if Maintenance Mode is
 *                         enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1001</td>
 *                     <td>System is currently undergoing maintenance.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns JSON error for any POST, PUT, PATCH and DELETE requests if Maintenance Mode is
 *                         enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1002</td>
 *                     <td>System is currently undergoing maintenance.</td>
 *                     <td>200</td>
 *                     <td>OK</td>
 *                     <td>Returns error in response header without response body for any pre-flight (OPTIONS) request
 *                         if Maintenance Mode is enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1003</td>
 *                     <td>System is currently undergoing maintenance.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns error in response header without response body when Content is requested if
 *                         Maintenance Mode is enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1004</td>
 *                     <td>System is currently undergoing maintenance.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns JSON error when Results are tried to be downloaded if Maintenance Mode is
 *                         enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1005</td>
 *                     <td>The system is currently undergoing maintenance. We are sorry for the inconvenience.<br/>
 *                         Please try again later.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns 503 HTML page, but without actual Error (the actual Error is only logged into ADP
 *                         application log file) for any other GET request if Maintenance Mode is enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1006</td>
 *                     <td>System is currently undergoing maintenance.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns error in response header without response body in any other case if Maintenance Mode
 *                         is enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1007</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case Basic Authentication failed because of missing username.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1008</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case Basic Authentication failed because provided username doesn't
 *                         exist.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1009</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case Basic Authentication failed because user is deactivated.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1010</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case cached User is not a valid JSON Object.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1011</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case Basic Authentication failed because of missing password, or its
 *                         format is invalid (i.e. invalid length).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1012</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case Basic Authentication failed because password doesn't match with
 *                         the one found in database.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1013</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case HMAC Authentication is missing.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1014</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case HMAC Authentication failed because of mismatched number of
 *                         keys.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1015</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case HMAC Authentication failed because of missing or invalid
 *                         micro-time key.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1016</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case HMAC Authentication failed because of missing or invalid
 *                         nonce.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1017</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case HMAC Authentication failed because of missing or invalid Base64
 *                         encoded HMAC Hash.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1018</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error in case HMAC Authentication failed because of mismatched/invalid HMAC
 *                         Hash (didn't match hashing algorithm).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1019</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>408</td>
 *                     <td>Request Timeout</td>
 *                     <td>Returns JSON error in case HMAC Authentication failed because of outdated request (time based
 *                         request signature is too old).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1060</td>
 *                     <td>System is currently undergoing maintenance.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns JSON error for any Not Found POST, PUT, PATCH and DELETE requests if Maintenance Mode
 *                         is enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1061</td>
 *                     <td>System is currently undergoing maintenance.</td>
 *                     <td>200</td>
 *                     <td>OK</td>
 *                     <td>Returns error in response header without response body for any Not Found pre-flight (OPTIONS)
 *                         request if Maintenance Mode is enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1062</td>
 *                     <td>System is currently undergoing maintenance.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns error in response header without response body for any Not Found Content request if
 *                         Maintenance Mode is enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1063</td>
 *                     <td>System is currently undergoing maintenance.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns JSON error for Not Found Results download or upload if Maintenance Mode is
 *                         enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1064</td>
 *                     <td>The system is currently undergoing maintenance. We are sorry for the inconvenience.<br/>
 *                         Please try again later.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns 503 HTML page, but without actual Error (the actual Error is only logged into ADP
 *                         application log file) for any other Not Found GET request if Maintenance Mode is
 *                         enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1065</td>
 *                     <td>System is currently undergoing maintenance.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns error in response header without response body for any other Not Found request if
 *                         Maintenance Mode is enabled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1066</td>
 *                     <td>Data Not Found.</td>
 *                     <td>404</td>
 *                     <td>Not Found</td>
 *                     <td>Returns JSON error in case a resource (either some data or a file) for any POST, PUT, PATCH
 *                         or DELETE request was not found.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1067</td>
 *                     <td>Data Not Found.</td>
 *                     <td>200</td>
 *                     <td>OK</td>
 *                     <td>Returns error in response header without response body in case a resource (data) for OPTIONS
 *                         request was not found.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1068</td>
 *                     <td>Data Not Found.</td>
 *                     <td>404</td>
 *                     <td>Not Found</td>
 *                     <td>Returns only response header without body in case requested content resource was not
 *                         found.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1069</td>
 *                     <td>Data Not Found.</td>
 *                     <td>404</td>
 *                     <td>Not Found</td>
 *                     <td>Returns JSON error in case results upload or download for a Test Session was not found.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1070</td>
 *                     <td>Sorry, requested page was not found.</td>
 *                     <td>404</td>
 *                     <td>Not Found</td>
 *                     <td>Returns 404 HTML page, but without actual Error (the actual Error is only logged into ADP
 *                         application log file) in case any other GET request was not found.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1071</td>
 *                     <td>Data Not Found.</td>
 *                     <td>404</td>
 *                     <td>Not Found</td>
 *                     <td>Returns only response header without body in case any other resource (either data or a file)
 *                         was not found.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1080</td>
 *                     <td>Internal Error.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns JSON error in case a Database Connection failed to be opened.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1081</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns JSON error in case execution of SQL Query triggered a Database error.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1090</td>
 *                     <td>Sorry, requested data was not found.</td>
 *                     <td>404</td>
 *                     <td>Not Found</td>
 *                     <td>Returns error in case no matching Tenant was found in Application Configuration.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1091</td>
 *                     <td>Sorry, requested data was not found.</td>
 *                     <td>404</td>
 *                     <td>Not Found</td>
 *                     <td>Returns error in case no matching Tenant was found in Database.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1092</td>
 *                     <td>You are not authorized for access. We are sorry for the inconvenience.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns error in case Tenant is Deactivated.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1093</td>
 *                     <td>Server encountered Internal Error. We are sorry for the inconvenience.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns error in case  cached Tenant is not a valid JSON Object.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1100</td>
 *                     <td>You are not authorized to access this page.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP
 *                         application log file) in case HTML response is expected and ADP is not configured for Test
 *                         Delivery and/or Test Publishing when related web service was called.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1101</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns appropriate error in case non-HTML response is expected and ADP is not configured for
 *                         Test Delivery and/or Test Publishing when related web service was called.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1102</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns appropriate error in case System Configuration was not found (i.e. Login
 *                         Configuration or Test Driver Configuration).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1103</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns appropriate error in case System Configuration is not valid JSON object (i.e. Login
 *                         Configuration or Test Driver Configuration).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1104</td>
 *                     <td>You are not authorized to access this page.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP
 *                         application log file) in case HTML response is expected and User calling particular web
 *                         service doesn't have appropriate User Role (Type) which grants permissions to access that web
 *                         service.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1105</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns appropriate error in case User calling particular web service doesn't have
 *                         appropriate User Role (Type) which grants permissions to access that web service.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1106</td>
 *                     <td>Invalid Request.</td>
 *                     <td>400</td>
 *                     <td>Bad Request</td>
 *                     <td>Returns appropriate error in case Request has missing or invalid JSON structure.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1107</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns JSON error during Login or Results upload/download in case Test Key in the request
 *                         header is not in expected format.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1108</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP
 *                         application log file) in case HTML response is expected during launch of Practice Test when
 *                         referenced Test Key in the launch URL was not found in database.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1109</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns appropriate error during Login or Results upload/download in case Test Key in the
 *                         request header was not found in database.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1110</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP
 *                         application log file) in case HTML response is expected during launch of Practice Test when
 *                         referenced Test Assignment is Deactivated.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1111</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns appropriate error during Login or Results upload/download in case Test Assignment is
 *                         Deactivated.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1112</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns JSON error in case if invalid Response was generated by application.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>TD-1000</td>
 *                     <td>There was a problem starting your test.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>This is Test Driver unique Error (not coming from ADP API) being displayed when Test Driver
 *                         is not able to communicate to the Server or is not able to download required Content from AWS
 *                         (most probably either due to Configuration or Internet/Connectivity issue).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>TD-1001</td>
 *                     <td>There was a problem starting your test.</td>
 *                     <td>N/A</td>
 *                     <td>N/A</td>
 *                     <td>This is Test Driver unique Error (not coming from ADP API) being displayed when Test Driver
 *                         is trying to start or resume a test, but has not recived a confirmation that the test is in
 *                         progress.</td>
 *                   </tr>
 *                 </table>
 *
 *
 * @apiError {JSON}   data Dataset that has the statusCode, statusDescription, errorCode and errorDescription, and
 *                         current time on the server.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case ERROR).
 * @apiError {HTML}   N/A  Simple 404 "Page Not Found", 403 "Access Denied" or 503 "Service Unavailable" HTML page.
 * @apiError {Header} N/A  Only a response header is returned with HTTP Status and HTTP Status Code (if turned on), but
 *                         response body is omitted.
 *
 * @apiErrorExample Error Response without Envelope:
 *   HTTP/1.1 404 Not Found
 *   {
 *     "statusCode": 404,
 *     "statusDescription": "Not Found",
 *     "errorCode": "ADP-1026",
 *     "errorDescription": "Requested Resource Not Found.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 404 Not Found
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 404,
 *       "statusDescription": "Not Found",
 *       "errorCode": "ADP-1026",
 *       "errorDescription": "Requested Resource Not Found.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function errors()
{
}
