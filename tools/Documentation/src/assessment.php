<?php
/**
 * @apiDefine Student User Authentication:
 *                    Not needed.
 */
/**
 * @apiGroup ADP Test Delivery
 * @api {GET} /assessment Assessment
 * @apiName Assessment
 * @apiPermission Student
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>Student</b> - Launch of Test Driver for Diagnostic Test<br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Assessment web service is exclusively used by Students to launch Test Driver for Diagnostic Tests. It
 *                 uses 2 simple captcha protections to check if page is being loaded by humans and to prohibit bots
 *                 from launching Test Driver.<br/><br/>
 *                 It always returns HTML Page and should never return errors.<br/><br/>
 *                 More details about ADP Launching Test Driver can be found under Assessment Launch.<br/><br/>
 *
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * GET /assessment HTTP/1.1
 * Host: adp.parcc.dev
 * Origin: https://adp.parcc.dev
 * User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
 * Accept: application/json, text/javascript; q=0.01
 * Accept-Language: en-US,en;q=0.5
 * Accept-Encoding: gzip, deflate
 * Referer: https://adp.parcc.dev/
 * Content-Length: 89
 * Connection: keep-alive
 * Pragma: no-cache
 * Cache-Control: no-cache
 *
 *
 * @apiSuccess {HTML} N/A It always returns Test Driver Launch Page.<br/>
 *                        Anything else is considered as an error.
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   <!DOCTYPE html>
 *   <html...
 *
 */
function loadTestDriverStart()
{
}



/**********************************************************************************************************************/



/**
 * @apiGroup ADP Test Delivery
 * @api {POST} /assessment Assessment Launch
 * @apiName Assessment Launch
 * @apiPermission Student
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: Student - Launch of Test Driver for Diagnostic Test<br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Assessment Launch web service is exclusively used by Students when launching Test Driver. It can
 *                 either be launched using Assessment Launch Page provided by Assessment web service in case of
 *                 Diagnostic Tests, or from dedicated page in PRC for launching Practice Tests and Quizzes. It
 *                 validates 2 captcha parameters in case of Diagnostic Tests, or Test Key in case of Practice Tests and
 *                 Quizzes.<br/><br/>
 *                 In case of successful request, it returns HTML Page which loads Test Driver.<br/>
 *                 In case of failure, it returns a 403 Access Denied Page.<br/><br/>
 *                 These are possible Assessment Launch specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1500</td>
 *                     <td>There is a problem accessing this page. We are sorry for the inconvenience.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 HTML page, but without actual Error (the actual Error is only logged into ADP
 *                         application log file) in case of missing Tenant specific Test Driver Launch Theme (i.e.
 *                         someone misconfigured Tenant details).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1501</td>
 *                     <td>You are not authorized to access this page.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP
 *                         application log file) in case of malformed Test Driver launch Parameters (i.e. someone
 *                         tampering with the launch page).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1502</td>
 *                     <td>You are not authorized to access this page.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP
 *                         application log file) in case of malformed Test Driver launch Name (i.e. someone tampering
 *                         with the launch page).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1503</td>
 *                     <td>You are not authorized to access this page.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP
 *                         application log file) in case of malformed Test Driver launch Test Key (i.e. someone
 *                         tampering with the launch page).</td>
 *                   </tr>
 *                 </table><br/>
 *
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * POST /assessment HTTP/1.1
 * Host: adp.parcc.dev
 * Origin: https://adp.parcc.dev
 * User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
 * Accept: application/json, text/javascript; q=0.01
 * Accept-Language: en-US,en;q=0.5
 * Accept-Encoding: gzip, deflate
 * Referer: https://adp.parcc.dev/
 * Content-Type:application/x-www-form-urlencoded; charset=UTF-8
 * Content-Length: 89
 * Connection: keep-alive
 * Pragma: no-cache
 * Cache-Control: no-cache
 *
 *
 * @apiParam {String} testKey="PRACTABCDE" Test Key is all uppercase, 10-character long string that is unique for each
 *                    Test Assignment. If it's for Practice Test or Quiz (starts with "PRACTxxxxx"), Test Driver will
 *                    skip the Login screen and automatically login Anonymous Student. Otherwise, it will show Login
 *                    screen and Student will be required to enter Test Key and other Login details. However, in case of
 *                    Diagnostic Tests, this is not the actual Test Key, but a dummy one only used to validate launching
 *                    of Test Driver.<br/>
 *                    It's 10 characters long uppercase only unique string [A-Z].
 * @apiParam {String} name="" Name is dummy parameter used for captcha validation and should always be empty.<br/>
 *                    It's an empty string.
 *
 * @apiParamExample {Form Data} Request Body Example
 *   testKey=PRACTABCDE&name=
 *
 *
 * @apiSuccess {HTML} N/A HTML Page which loads Test Driver.
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   <!DOCTYPE html>
 *   <html...
 *
 *
 * @apiError {HTML} N/A 403 Access Denied HTML Page
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 403 Forbidden
 *   <!DOCTYPE html>
 *   <html...
 *
 *
 */
function loadTestDriver()
{
}
