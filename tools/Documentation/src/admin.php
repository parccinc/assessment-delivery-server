<?php
/**
 * @apiDefine ELB User Authentication:
 *                Not needed.
 */
/**
 * @apiGroup ADP Common
 * @api {GET} /api/admin/cache/clear Clear Cache
 * @apiName Clear Cache
 * @apiPermission IT_Dev
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>IT</b> - Clear Cache<br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Clear Cache web service is exclusively used by IT and Developers for Flushing Application Cache. It
 *                 doesn't use any authentication, but can only be successfully executed from the localhost for security
 *                 reasons.<br/>
 *                 In case of successful request, it returns a confirmation that Application Cache was cleared
 *                 successfully.<br/><br/>
 *                 These are possible Clear Cache specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0001</td>
 *                     <td>Access Denied.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns 401 JSON error if external request was made to administer Application (was not
 *                         initiated from localhost).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0005</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to clear Application Cache was not completed successfully
 *                         (keys are found in cache after cache was flushed).</td>
 *                   </tr>
 *                 </table><br/>
 *
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * GET /api/admin/cache/clear HTTP/1.1
 * Host: adp.parcc.dev
 * Origin: https://adp.parcc.dev
 * User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
 * Accept: application/json, text/javascript; q=0.01
 * Accept-Language: en-US,en;q=0.5
 * Accept-Encoding: gzip, deflate
 * Referer: https://adp.parcc.dev/
 * Content-Length: 89
 * Connection: keep-alive
 * Pragma: no-cache
 * Cache-Control: no-cache
 *
 *
 * @apiSuccess {HTML} N/A It returns a simple confirmation if Application Cache was successfully flushed.<br/>
 *                        Anything else is considered as an error.
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   Application Cache was cleared successfully.
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "statusCode": 500,
 *     "statusDescription": "Internal Server Error",
 *     "errorCode": "ADP-0001",
 *     "errorDescription": "Internal Error.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 500,
 *       "statusDescription": "Internal Server Error",
 *       "errorCode": "ADP-0001",
 *       "errorDescription": "Internal Error.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function clearCache()
{
}



/**********************************************************************************************************************/



/**
 * @apiGroup ADP Common
 * @api {GET} /api/admin/maintenance/status Maintenance Status
 * @apiName Maintenance Status
 * @apiPermission IT_Dev
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>IT</b> - Maintenance Status<br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Maintenance Status web service is exclusively used by IT and Developers for checking current Status
 *                 for Maintenance Mode. It doesn't use any authentication, but can only be successfully executed from
 *                 the localhost for security reasons.<br/>
 *                 In case of successful request, it returns a confirmation with current Status for Maintenance
 *                 Mode.<br/><br/>
 *                 These are possible Maintenance Status specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0001</td>
 *                     <td>Access Denied.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns 401 JSON error if external request was made to administer Application (was not
 *                         initiated from localhost).</td>
 *                   </tr>
 *                 </table><br/>
 *
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * GET /api/admin/maintenance/status HTTP/1.1
 * Host: adp.parcc.dev
 * Origin: https://adp.parcc.dev
 * User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
 * Accept: application/json, text/javascript; q=0.01
 * Accept-Language: en-US,en;q=0.5
 * Accept-Encoding: gzip, deflate
 * Referer: https://adp.parcc.dev/
 * Content-Length: 89
 * Connection: keep-alive
 * Pragma: no-cache
 * Cache-Control: no-cache
 *
 *
 * @apiSuccess {HTML} N/A It returns a simple confirmation with current Status for Maintenance Mode.<br/>
 *                        Anything else is considered as an error.
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   Maintenance Mode is Disabled.
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "statusCode": 500,
 *     "statusDescription": "Internal Server Error",
 *     "errorCode": "ADP-0001",
 *     "errorDescription": "Internal Error.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 500,
 *       "statusDescription": "Internal Server Error",
 *       "errorCode": "ADP-0001",
 *       "errorDescription": "Internal Error.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function maintenanceStatus()
{
}



/**********************************************************************************************************************/



/**
 * @apiGroup ADP Common
 * @api {GET} /api/admin/maintenance/enable Maintenance Enable
 * @apiName Maintenance Enable
 * @apiPermission IT_Dev
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>IT</b> - Maintenance Enable<br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Maintenance Enable web service is exclusively used by IT and Developers for Enabling Maintenance
 *                 Mode. It doesn't use any authentication, but can only be successfully executed from the localhost for
 *                 security reasons.<br/>
 *                 In case of successful request, it returns a confirmation with outcome of Enabling Maintenance
 *                 Mode.<br/><br/>
 *                 These are possible Maintenance Enable specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0001</td>
 *                     <td>Access Denied.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns 401 JSON error if external request was made to administer Application (was not
 *                         initiated from localhost).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0002</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to update Application Configuration Property failed because
 *                         given Application Configuration Property format is not supported (i.e. object or array is
 *                         provided).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0003</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to update Application Configuration Property failed because
 *                         Application Configuration File cannot be loaded.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0004</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to update Application Configuration Property failed because
 *                         Application Configuration File cannot be saved (i.e. due to invalid file permissions).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0005</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to clear Application Cache was not completed successfully
 *                         (keys are found in cache after cache was flushed).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0006</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to Enable Maintenance Mode failed to update Configuration
 *                         file.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0007</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to Enable Maintenance Mode failed unexpectedly
 *                         (Configuration file was updated, but Maintenance Mode was still not enabled).</td>
 *                   </tr>
 *                 </table><br/>
 *
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * GET /api/admin/maintenance/enable HTTP/1.1
 * Host: adp.parcc.dev
 * Origin: https://adp.parcc.dev
 * User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
 * Accept: application/json, text/javascript; q=0.01
 * Accept-Language: en-US,en;q=0.5
 * Accept-Encoding: gzip, deflate
 * Referer: https://adp.parcc.dev/
 * Content-Length: 89
 * Connection: keep-alive
 * Pragma: no-cache
 * Cache-Control: no-cache
 *
 *
 * @apiSuccess {HTML} N/A It returns a simple confirmation with current Status for Maintenance Mode.<br/>
 *                        Anything else is considered as an error.
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   Maintenance Mode is Enabled.
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "statusCode": 500,
 *     "statusDescription": "Internal Server Error",
 *     "errorCode": "ADP-0001",
 *     "errorDescription": "Internal Error.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 500,
 *       "statusDescription": "Internal Server Error",
 *       "errorCode": "ADP-0001",
 *       "errorDescription": "Internal Error.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function maintenanceEnable()
{
}



/**********************************************************************************************************************/



/**
 * @apiGroup ADP Common
 * @api {GET} /api/admin/maintenance/disable Maintenance Disable
 * @apiName Maintenance Disable
 * @apiPermission IT_Dev
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>IT</b> - Maintenance Disable<br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Maintenance Disable web service is exclusively used by IT and Developers for Disabling Maintenance
 *                 Mode. It doesn't use any authentication, but can only be successfully executed from the localhost for
 *                 security reasons.<br/>
 *                 In case of successful request, it returns a confirmation with outcome of Disabling Maintenance
 *                 Mode.<br/><br/>
 *                 These are possible Maintenance Disable specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0001</td>
 *                     <td>Access Denied.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns 401 JSON error if external request was made to administer Application (was not
 *                         initiated from localhost).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0002</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to update Application Configuration Property failed because
 *                         given Application Configuration Property format is not supported (i.e. object or array is
 *                         provided).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0003</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to update Application Configuration Property failed because
 *                         Application Configuration File cannot be loaded.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0004</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to update Application Configuration Property failed because
 *                         Application Configuration File cannot be saved (i.e. due to invalid file permissions).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0005</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to clear Application Cache was not completed successfully
 *                         (keys are found in cache after cache was flushed).</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0006</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to Disable Maintenance Mode failed to update Configuration
 *                         file.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0007</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns 500 JSON error if request to Disable Maintenance Mode failed unexpectedly
 *                         (Configuration file was updated, but Maintenance Mode was still not disabled).</td>
 *                   </tr>
 *                 </table><br/>
 *
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * GET /api/admin/maintenance/disable HTTP/1.1
 * Host: adp.parcc.dev
 * Origin: https://adp.parcc.dev
 * User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
 * Accept: application/json, text/javascript; q=0.01
 * Accept-Language: en-US,en;q=0.5
 * Accept-Encoding: gzip, deflate
 * Referer: https://adp.parcc.dev/
 * Content-Length: 89
 * Connection: keep-alive
 * Pragma: no-cache
 * Cache-Control: no-cache
 *
 *
 * @apiSuccess {HTML} N/A It returns a simple confirmation with current Status for Maintenance Mode.<br/>
 *                        Anything else is considered as an error.
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   Maintenance Mode is Disabled.
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "statusCode": 500,
 *     "statusDescription": "Internal Server Error",
 *     "errorCode": "ADP-0001",
 *     "errorDescription": "Internal Error.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 500,
 *       "statusDescription": "Internal Server Error",
 *       "errorCode": "ADP-0001",
 *       "errorDescription": "Internal Error.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function maintenanceDisable()
{
}



/**********************************************************************************************************************/



/**
 * @apiDefine IT_Dev User Authentication:
 *                   Not needed.
 */
/**
 * @apiGroup ADP Common
 * @api {GET} /api/admin/install Install
 * @apiName Install
 * @apiPermission IT_Dev
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>IT</b>/<b>Developers</b> - Application Installation<br/>
 *                 Status: <b style="color:#c00">In Development</b><br/><br/>
 *                 Application Installation web service is exclusively used by IT and Developers for Flushing
 *                 Application Cache. It doesn't use any authentication, but can only be successfully executed from the
 *                 localhost for security reasons.<br/>
 *                 In case of successful request, it returns a confirmation that Application was installed
 *                 successfully.<br/><br/>
 *                 These are possible Application Installation specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0001</td>
 *                     <td>Access Denied.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns 401 JSON error if external request was made to administer Application (was not
 *                         initiated from localhost).</td>
 *                   </tr>
 *                 </table><br/>
 *
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * GET /api/admin/install HTTP/1.1
 * Host: adp.parcc.dev
 * Origin: https://adp.parcc.dev
 * User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
 * Accept: application/json, text/javascript; q=0.01
 * Accept-Language: en-US,en;q=0.5
 * Accept-Encoding: gzip, deflate
 * Referer: https://adp.parcc.dev/
 * Content-Length: 89
 * Connection: keep-alive
 * Pragma: no-cache
 * Cache-Control: no-cache
 *
 *
 * @apiSuccess {HTML} N/A It returns a simple confirmation if Application was successfully installed.<br/>
 *                        Anything else is considered as an error.
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   Application was successfully installed.
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "statusCode": 500,
 *     "statusDescription": "Internal Server Error",
 *     "errorCode": "ADP-0001",
 *     "errorDescription": "Internal Error.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 500,
 *       "statusDescription": "Internal Server Error",
 *       "errorCode": "ADP-0001",
 *       "errorDescription": "Internal Error.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function install()
{
}



/**********************************************************************************************************************/



/**
 * @apiDefine IT_Dev User Authentication:
 *                   Not needed.
 */
/**
 * @apiGroup ADP Common
 * @api {GET} /api/admin/update Update
 * @apiName Update
 * @apiPermission IT_Dev
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>IT</b>/<b>Developers</b> - Application Update<br/>
 *                 Status: <b style="color:#c00">In Development</b><br/><br/>
 *                 Application Update web service is exclusively used by IT and Developers for Flushing Application
 *                 Cache. It doesn't use any authentication, but can only be successfully executed from the localhost
 *                 for security reasons.<br/>
 *                 In case of successful request, it returns a confirmation that Application was updated
 *                 successfully.<br/><br/>
 *                 These are possible Application Update specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-0001</td>
 *                     <td>Access Denied.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns 401 JSON error if external request was made to administer Application (was not
 *                         initiated from localhost).</td>
 *                   </tr>
 *                 </table><br/>
 *
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * GET /api/admin/update HTTP/1.1
 * Host: adp.parcc.dev
 * Origin: https://adp.parcc.dev
 * User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
 * Accept: application/json, text/javascript; q=0.01
 * Accept-Language: en-US,en;q=0.5
 * Accept-Encoding: gzip, deflate
 * Referer: https://adp.parcc.dev/
 * Content-Length: 89
 * Connection: keep-alive
 * Pragma: no-cache
 * Cache-Control: no-cache
 *
 *
 * @apiSuccess {HTML} N/A It returns a simple confirmation if Application was successfully updated.<br/>
 *                        Anything else is considered as an error.
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   Application was successfully installed.
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "statusCode": 500,
 *     "statusDescription": "Internal Server Error",
 *     "errorCode": "ADP-0001",
 *     "errorDescription": "Internal Error.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 500,
 *       "statusDescription": "Internal Server Error",
 *       "errorCode": "ADP-0001",
 *       "errorDescription": "Internal Error.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function update()
{
}
