define([
  'jquery'
], function($) {

  /**
   * BV: API Configuration.
   */
  var xdebug = '';
  //var xdebug = '?XDEBUG_SESSION_START=dbgp';
  /**
   * BV: Test Delivery.
   */
  var loginUsername = 'DocumentationTD';
  var loginPassword = '53c5de17b5488f1a8549e6db1786ca81';
  var loginSecret = '578cca1aa71f702c26cc81439392124c';
  var loginAuthentication = '[Dynamically generated upon request based on Username, Password and Secret]';
  var loginAuthorization = loginAuthentication;
  var loginXRequestedWith = '[Dynamically generated upon request based on Test Key]';
  var loginTestKey = 'DOCUMENTAT';
  var loginDateOfBirth = '2000-01-10';
  var loginStudentId = 1234567890;
  var loginState = 'IL';
  var loginToken = loginTestId = '[Dynamically populated upon successful Login]';
  var contentContentFile = '';
  var assessmentTestKey = 'PRACTXXXXX';
  /**
   * BV: Test Publishing.
   */
  var testUsername = 'DocumentationACR';
  var testPassword = '635fbb063cbc28e8bf03ed12ca4cca00';
  var testSecret = '51f48327961f6cb0da7d584a664cafba';
  var testAuthentication = loginAuthentication;
  var testAuthorization = testAuthentication;
  var testTestFormRevisionId = 'i123456789012';
  var testPublishedBy = 'John Smith';
  var testAdpTestFormRevisionId = 1;
  var testActive = 'false';
  var adpTestId = 1;
  var batteryId = 'i123456789012';
  /**
   * BV: Test Assignments.
   */
  var assignmentUsername = 'DocumentationLDR';
  var assignmentPassword = '97f0ca00610888a0e9e58883dabc813b';
  var assignmentSecret = '6d393cf3b856c34ee5c55f7c50dfeb49';
  var assignmentAuthentication = loginAuthentication;
  var assignmentAuthorization = assignmentAuthentication;
  var assignmentTestAssignmentId = 100;
  var assignmentAdpTestAssignmentId = 5;
  var assignmentTestStatus = '';
  var assignmentTestId = 1;
  var assignmentTestFormId = 3;
  var assignmentTestKey = 'ABCDEFGHIJ';
  var assignmentStudentId = 3;
  var assignmentPersonalId = 'IL-12345';
  var assignmentFirstName = 'John';
  var assignmentLastName = 'Smith';
  var assignmentDateOfBirth = '2000-01-01';
  var assignmentState = 'IL';
  var assignmentSchoolName = 'Evanston High School';
  var assignmentGrade = '10';
  var assignmentEnableLineReader = assignmentEnableTextToSpeech = 'true';
  /**
   * BV: Results Details.
   */
  var resultsUsername = 'DocumentationLDR';
  var resultsPassword = '97f0ca00610888a0e9e58883dabc813b';
  var resultsSecret = '6d393cf3b856c34ee5c55f7c50dfeb49';
  var resultsAuthentication = loginAuthentication;
  var resultsAuthorization = resultsAuthentication;
  var resultsAdpTestAssignmentId = 1;

  var initDynamic = function() {

      /**
       * BV: Fix invalid variable types.
       */
      $("span.add-on:contains('<p>String</p>')").html('String');
      $("span.add-on:contains('<p>Integer</p>')").html('Integer');
      $("span.add-on:contains('<p>Boolean</p>')").html('Boolean');
      $("span.add-on:contains('<p>JSON</p>')").html('JSON');

      /**
       * BV: Initialize all.
       */
      $(document).find('article').each(function() {
          var group = $(this).data("group");
          var name = $(this).data("name");
          var version = $(this).data("version");
          clearSampleRequest(group, name, version);
          initAdditionalFields(group, name, version);
      });
      /**
       * BV: Initialize additional fields.
       */
      function initAdditionalFields(group, name, version)
      {
        // Login.
        if (name === 'Login') {
          var $loginRoot = $('article[data-group="' + group + '"][data-name="Login"][data-version="' + version + '"]');
          var $paramFields = $loginRoot.find('.' + group + '-' + name + '-' + version.replace(/\./g, '_') + '-sample-request-param-fields');
          // Delete default control fields.
          $paramFields.find('.control-group').has('input[placeholder="state"]').remove();
          // Add custom control fields.
          $paramFields.append('<div class="control-group"><label class="control-label" for="sample-request-param-field-state">state</label><div class="controls"><div class="input-append"><select placeholder="state" class="input-xxlarge sample-request-param" data-sample-request-param-name="state" data-sample-request-param-group="sample-request-param-state"><option>AL</option><option>AK</option><option>AZ</option><option>AR</option><option>CA</option><option>CO</option><option>CT</option><option>DE</option><option>DD</option><option>DC</option><option>FL</option><option>GA</option><option>HI</option><option>ID</option><option selected>IL</option><option>IN</option><option>IA</option><option>KS</option><option>KY</option><option>LA</option><option>ME</option><option>MD</option><option>MA</option><option>MI</option><option>MN</option><option>MS</option><option>MO</option><option>MT</option><option>NE</option><option>NV</option><option>NH</option><option>NJ</option><option>NM</option><option>NY</option><option>NC</option><option>ND</option><option>OH</option><option>OK</option><option>OR</option><option>PA</option><option>PT</option><option>RI</option><option>SC</option><option>SD</option><option>TN</option><option>TX</option><option>UT</option><option>VT</option><option>VA</option><option>WA</option><option>WV</option><option>WI</option><option>WY</option></select><span class="add-on">String</span></div></div></div>');
        }
        // Content.
        else if (name === 'Content') {
          var $contentRoot = $('article[data-group="' + group + '"][data-name="Content"][data-version="' + version + '"]');
          $('<img class="sample-request-response-image" src="about:blank" alt=""/><audio class="sample-request-response-audio" src="about:blank" preload="auto" controls></audio><video class="sample-request-response-video" src="about:blank" controls></video>').insertBefore($contentRoot.find('.sample-request-response').find('pre'));
        }
        // Results Upload.
        else if (name === 'Results_Upload') {
          var $resultsUploadRoot = $('article[data-group="' + group + '"][data-name="Results_Upload"][data-version="' + version + '"]');
          var $paramFields = $resultsUploadRoot.find('.' + group + '-' + name + '-' + version.replace(/\./g, '_') + '-sample-request-param-fields');
          // Delete default control fields.
          $paramFields.find('.control-group').has('input[placeholder="testStatus"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="resultsFile"]').remove();
          // Add custom control fields.
          $paramFields.append('<div class="control-group"><label class="control-label" for="sample-request-param-field-testStatus">testStatus</label><div class="controls"><div class="input-append"><select placeholder="testStatus" class="input-xxlarge sample-request-param" data-sample-request-param-name="testStatus" data-sample-request-param-group="sample-request-param-testStatus"><option value="">InProgress</option><option value="pause">Paused</option><option value="submit">Submitted</option></select><span class="add-on">String</span></div></div></div>');
          $paramFields.append('<div class="control-group resultsFile"><label class="control-label" for="sample-request-param-field-resultsFile">resultsFile</label><div class="controls"><div class="input-append"><textarea rows="3" placeholder="resultsFile" class="input-xxlarge sample-request-param" data-sample-request-param-name="resultsFile" data-sample-request-param-group="sample-request-param-resultsFile"></textarea><span class="add-on">JSON</span></div></div></div>');
        }
        // Test Properties Update.
        else if (name === 'Test_Properties_Update') {
          var $testPropertiesRoot = $('article[data-group="' + group + '"][data-name="Test_Properties_Update"][data-version="' + version + '"]');
          var $paramFields = $testPropertiesRoot.find('.' + group + '-' + name + '-' + version.replace(/\./g, '_') + '-sample-request-param-fields');
          // Delete default control fields.
          $paramFields.find('.control-group').has('input[placeholder="program"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="scoreReport"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="subject"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="grade"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="itemSelectionAlgorithm"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="security"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="multimedia"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="scoring"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="permissions"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="privacy"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="isActive"]').remove();
          // Add custom control fields.
          $paramFields.find('.control-group').has('input[placeholder="name"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-program">program</label><div class="controls"><div class="input-append"><select placeholder="program" class="input-xxlarge sample-request-param" data-sample-request-param-name="program" data-sample-request-param-group="sample-request-param-program"><option></option><option value="Diagnostic Assessment">Diagnostic Assessment</option><option value="K2 Formative">K2 Formative</option><option value="Mid-Year/Interim">Mid-Year/Interim</option><option value="Practice Test">Practice Test</option><option value="Quiz Test">Quiz Test</option><option value="Speaking &amp; Listening">Speaking &amp; Listening</option><option value="Summative">Summative</option></select><span class="add-on">Boolean</span></div></div></div>');
          $paramFields.find('.control-group').has('select[placeholder="program"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-scoreReport">scoreReport</label><div class="controls"><div class="input-append"><select placeholder="scoreReport" class="input-xxlarge sample-request-param" data-sample-request-param-name="scoreReport" data-sample-request-param-group="sample-request-param-scoreReport"><option></option><option value="None">None</option><option value="Generic">Generic</option><option value="ELA Decoding">ELA Decoding</option><option value="ELA Reader Motivation Survey">ELA Reader Motivation Survey</option><option value="ELA Reader Comprehension">ELA Reader Comprehension</option><option value="ELA Vocabulary">ELA Vocabulary</option><option value="Math Comprehension">Math Comprehension</option><option value="Math Fluency">Math Fluency</option></select><span class="add-on">Boolean</span></div></div></div>');
          $paramFields.find('.control-group').has('select[placeholder="scoreReport"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-subject">subject</label><div class="controls"><div class="input-append"><select placeholder="subject" class="input-xxlarge sample-request-param" data-sample-request-param-name="subject" data-sample-request-param-group="sample-request-param-subject"><option></option><option value="ELA">ELA</option><option value="Math">Math</option><option value="N/A">N/A</option></select><span class="add-on">Boolean</span></div></div></div>');
          $paramFields.find('.control-group').has('select[placeholder="subject"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-grade">grade</label><div class="controls"><div class="input-append"><select placeholder="grade" class="input-xxlarge sample-request-param" data-sample-request-param-name="grade" data-sample-request-param-group="sample-request-param-grade"><option></option><option value="K">K</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="Multi-level">Multi-level</option></select><span class="add-on">Boolean</span></div></div></div>');
          $paramFields.find('.control-group').has('select[placeholder="grade"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-itemSelectionAlgorithm">itemSelectionAlgorithm</label><div class="controls"><div class="input-append"><select placeholder="itemSelectionAlgorithm" class="input-xxlarge sample-request-param" data-sample-request-param-name="itemSelectionAlgorithm" data-sample-request-param-group="sample-request-param-itemSelectionAlgorithm"><option></option><option value="Fixed">Fixed</option><option value="Adaptive">Adaptive</option></select><span class="add-on">Boolean</span></div></div></div>');
          $paramFields.find('.control-group').has('select[placeholder="itemSelectionAlgorithm"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-security">security</label><div class="controls"><div class="input-append"><select placeholder="security" class="input-xxlarge sample-request-param" data-sample-request-param-name="security" data-sample-request-param-group="sample-request-param-security"><option></option><option value="Non-Secure">Non-Secure</option><option value="Secure">Secure</option></select><span class="add-on">Boolean</span></div></div></div>');
          $paramFields.find('.control-group').has('select[placeholder="security"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-multimedia">multimedia</label><div class="controls"><div class="input-append"><select placeholder="multimedia" class="input-xxlarge sample-request-param" data-sample-request-param-name="multimedia" data-sample-request-param-group="sample-request-param-multimedia"><option></option><option value="OnDemand">OnDemand</option><option value="PreDownload">PreDownload</option><option value="Embedded">Embedded</option></select><span class="add-on">Boolean</span></div></div></div>');
          $paramFields.find('.control-group').has('select[placeholder="multimedia"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-scoring">scoring</label><div class="controls"><div class="input-append"><select placeholder="scoring" class="input-xxlarge sample-request-param" data-sample-request-param-name="scoring" data-sample-request-param-group="sample-request-param-scoring"><option></option><option value="Immediate">Immediate</option><option value="Delayed">Delayed</option></select><span class="add-on">Boolean</span></div></div></div>');
          $paramFields.find('.control-group').has('select[placeholder="scoring"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-permissions">permissions</label><div class="controls"><div class="input-append"><select placeholder="permissions" class="input-xxlarge sample-request-param" data-sample-request-param-name="permissions" data-sample-request-param-group="sample-request-param-permissions"><option></option><option value="Non-Restricted">Non-Restricted</option><option value="Restricted">Restricted</option></select><span class="add-on">Boolean</span></div></div></div>');
          $paramFields.find('.control-group').has('select[placeholder="permissions"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-privacy">privacy</label><div class="controls"><div class="input-append"><select placeholder="privacy" class="input-xxlarge sample-request-param" data-sample-request-param-name="privacy" data-sample-request-param-group="sample-request-param-privacy"><option></option><option value="Public">Public</option><option value="Private">Private</option></select><span class="add-on">Boolean</span></div></div></div>');
          $paramFields.append('<div class="control-group"><label class="control-label" for="sample-request-param-field-isActive">isActive</label><div class="controls"><div class="input-append"><select placeholder="isActive" class="input-xxlarge sample-request-param" data-sample-request-param-name="isActive" data-sample-request-param-group="sample-request-param-isActive"><option></option><option value="true">True</option><option value="false">False</option></select><span class="add-on">Boolean</span></div></div></div>');
        }
        // Test Activate/Deactivate.
        else if (name === 'Test_Activate_Deactivate') {
          var $testActivateDeactivateRoot = $('article[data-group="' + group + '"][data-name="Test_Activate_Deactivate"][data-version="' + version + '"]');
          var $paramFields = $testActivateDeactivateRoot.find('.' + group + '-' + name + '-' + version.replace(/\./g, '_') + '-sample-request-param-fields');
          // Delete default control fields.
          $paramFields.find('.control-group').has('input[placeholder="active"]').remove();
          // Add custom control fields.
          $paramFields.append('<div class="control-group"><label class="control-label" for="sample-request-param-field-active">active</label><div class="controls"><div class="input-append"><select placeholder="active" class="input-xxlarge sample-request-param" data-sample-request-param-name="active" data-sample-request-param-group="sample-request-param-active"><option value="true">True</option><option value="false" selected>False</option></select><span class="add-on">Boolean</span></div></div></div>');
        }
        // Test Assignment Creation.
        else if (name === 'Assignment_Creation') {
          var $assignmentCreationRoot = $('article[data-group="' + group + '"][data-name="Assignment_Creation"][data-version="' + version + '"]');
          var $paramFields = $assignmentCreationRoot.find('.' + group + '-' + name + '-' + version.replace(/\./g, '_') + '-sample-request-param-fields');
          // Delete default control fields.
          $paramFields.find('.control-group').has('input[placeholder="state"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="grade"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="enableLineReader"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="enableTextToSpeech"]').remove();
          // Add custom control fields.
          $paramFields.find('.control-group').has('input[placeholder="dateOfBirth"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-state">state</label><div class="controls"><div class="input-append"><select placeholder="state" class="input-xxlarge sample-request-param" data-sample-request-param-name="state" data-sample-request-param-group="sample-request-param-state"><option></option><option>AL</option><option>AK</option><option>AZ</option><option>AR</option><option>CA</option><option>CO</option><option>CT</option><option>DE</option><option>DD</option><option>DC</option><option>FL</option><option>GA</option><option>HI</option><option>ID</option><option selected>IL</option><option>IN</option><option>IA</option><option>KS</option><option>KY</option><option>LA</option><option>ME</option><option>MD</option><option>MA</option><option>MI</option><option>MN</option><option>MS</option><option>MO</option><option>MT</option><option>NE</option><option>NV</option><option>NH</option><option>NJ</option><option>NM</option><option>NY</option><option>NC</option><option>ND</option><option>OH</option><option>OK</option><option>OR</option><option>PA</option><option>PT</option><option>RI</option><option>SC</option><option>SD</option><option>TN</option><option>TX</option><option>UT</option><option>VT</option><option>VA</option><option>WA</option><option>WV</option><option>WI</option><option>WY</option></select><span class="add-on">String</span></div></div></div>');
          $paramFields.append('<div class="control-group"><label class="control-label" for="sample-request-param-field-grade">grade</label><div class="controls"><div class="input-append"><select placeholder="grade" class="input-xxlarge sample-request-param" data-sample-request-param-name="grade" data-sample-request-param-group="sample-request-param-grade"><option></option><option>K</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option selected>10</option><option>11</option><option>12</option></select><span class="add-on">String</span></div></div></div>');
          $paramFields.append('<div class="control-group"><label class="control-label" for="sample-request-param-field-enableLineReader">enableLineReader</label><div class="controls"><div class="input-append"><select placeholder="enableLineReader" class="input-xxlarge sample-request-param" data-sample-request-param-name="enableLineReader" data-sample-request-param-group="sample-request-param-enableLineReader"><option value="true" selected>True</option><option value="false">False</option></select><span class="add-on">Boolean</span></div></div></div>');
          $paramFields.append('<div class="control-group"><label class="control-label" for="sample-request-param-field-enableTextToSpeech">enableTextToSpeech</label><div class="controls"><div class="input-append"><select placeholder="enableTextToSpeech" class="input-xxlarge sample-request-param" data-sample-request-param-name="enableTextToSpeech" data-sample-request-param-group="sample-request-param-enableTextToSpeech"><option value="true" selected>True</option><option value="false">False</option></select><span class="add-on">Boolean</span></div></div></div>');
        }
        // Test Assignment Update.
        else if (name === 'Assignment_Update') {
          var $assignmentUpdateRoot = $('article[data-group="' + group + '"][data-name="Assignment_Update"][data-version="' + version + '"]');
          var $paramFields = $assignmentUpdateRoot.find('.' + group + '-' + name + '-' + version.replace(/\./g, '_') + '-sample-request-param-fields');
          // Delete default control fields.
          $paramFields.find('.control-group').has('input[placeholder="testStatus"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="state"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="grade"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="enableLineReader"]').remove();
          $paramFields.find('.control-group').has('input[placeholder="enableTextToSpeech"]').remove();
          // Add custom control fields.
          $paramFields.find('.control-group').has('input[placeholder="adpTestAssignmentId"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-testStatus">testStatus</label><div class="controls"><div class="input-append"><select placeholder="testStatus" class="input-xxlarge sample-request-param" data-sample-request-param-name="testStatus" data-sample-request-param-group="sample-request-param-testStatus"><option></option><option>Paused</option><option>Completed</option><option>Canceled</option></select><span class="add-on">String</span></div></div></div>');
          $paramFields.find('.control-group').has('input[placeholder="dateOfBirth"]').after('<div class="control-group"><label class="control-label" for="sample-request-param-field-state">state</label><div class="controls"><div class="input-append"><select placeholder="state" class="input-xxlarge sample-request-param" data-sample-request-param-name="state" data-sample-request-param-group="sample-request-param-state"><option></option><option>AL</option><option>AK</option><option>AZ</option><option>AR</option><option>CA</option><option>CO</option><option>CT</option><option>DE</option><option>DD</option><option>DC</option><option>FL</option><option>GA</option><option>HI</option><option>ID</option><option selected>IL</option><option>IN</option><option>IA</option><option>KS</option><option>KY</option><option>LA</option><option>ME</option><option>MD</option><option>MA</option><option>MI</option><option>MN</option><option>MS</option><option>MO</option><option>MT</option><option>NE</option><option>NV</option><option>NH</option><option>NJ</option><option>NM</option><option>NY</option><option>NC</option><option>ND</option><option>OH</option><option>OK</option><option>OR</option><option>PA</option><option>PT</option><option>RI</option><option>SC</option><option>SD</option><option>TN</option><option>TX</option><option>UT</option><option>VT</option><option>VA</option><option>WA</option><option>WV</option><option>WI</option><option>WY</option></select><span class="add-on">String</span></div></div></div>');
          $paramFields.append('<div class="control-group"><label class="control-label" for="sample-request-param-field-grade">grade</label><div class="controls"><div class="input-append"><select placeholder="grade" class="input-xxlarge sample-request-param" data-sample-request-param-name="grade" data-sample-request-param-group="sample-request-param-grade"><option></option><option>K</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option selected>10</option><option>11</option><option>12</option></select><span class="add-on">String</span></div></div></div>');
          $paramFields.append('<div class="control-group"><label class="control-label" for="sample-request-param-field-enableLineReader">enableLineReader</label><div class="controls"><div class="input-append"><select placeholder="enableLineReader" class="input-xxlarge sample-request-param" data-sample-request-param-name="enableLineReader" data-sample-request-param-group="sample-request-param-enableLineReader"><option value="true" selected>True</option><option value="false">False</option></select><span class="add-on">Boolean</span></div></div></div>');
          $paramFields.append('<div class="control-group"><label class="control-label" for="sample-request-param-field-enableTextToSpeech">enableTextToSpeech</label><div class="controls"><div class="input-append"><select placeholder="enableTextToSpeech" class="input-xxlarge sample-request-param" data-sample-request-param-name="enableTextToSpeech" data-sample-request-param-group="sample-request-param-enableTextToSpeech"><option value="true" selected>True</option><option value="false">False</option></select><span class="add-on">Boolean</span></div></div></div>');
        }
      }

      // Button send
      $(".sample-request-send").off("click");
      $(".sample-request-send").on("click", function(e) {
          e.preventDefault();
          var $root = $(this).parents("article");
          var group = $root.data("group");
          var name = $root.data("name");
          var version = $root.data("version");
          sendSampleRequest(group, name, version, $(this).data("sample-request-type"));
      });

      // Button clear
      $(".sample-request-clear").off("click");
      $(".sample-request-clear").on("click", function(e) {
          e.preventDefault();
          var $root = $(this).parents("article");
          var group = $root.data("group");
          var name = $root.data("name");
          var version = $root.data("version");
          clearSampleRequest(group, name, version);
      });
  }; // initDynamic

  function sendSampleRequest(group, name, version, type)
  {
      var $root = $('article[data-group="' + group + '"][data-name="' + name + '"][data-version="' + version + '"]');

      // Optional header
      var header = {};
      $root.find(".sample-request-header:checked").each(function(i, element) {
          var group = $(element).data("sample-request-header-group-id");
          $root.find("[data-sample-request-header-group=\"" + group + "\"]").each(function(i, element) {
            var key = $(element).data("sample-request-header-name");
            var value = element.value;
            header[key] = $.type(value) === "string" ? escapeHtml(value) : value;
          });
      });

      // create JSON dictionary of parameters
      var param = {};
      $root.find(".sample-request-param:checked").each(function(i, element) {
          var group = $(element).data("sample-request-param-group-id");
          $root.find("[data-sample-request-param-group=\"" + group + "\"]").each(function(i, element) {
            var key = $(element).data("sample-request-param-name");
            var value = element.value;
            param[key] = $.type(value) === "string" ? escapeHtml(value) : value;
          });
      });

      // grab user-inputted URL
      var url = $root.find(".sample-request-url").val();

      // Insert url parameter
      var pattern = pathToRegexp(url, null);
      var matches = pattern.exec(url);
      for (var i = 1; i < matches.length; i++) {
          var key = matches[i].substr(1);
          if (param[key] !== undefined) {
              url = url.replace(matches[i], encodeURIComponent(param[key]));

              // remove URL parameters from list
              delete param[key];
          }
      } // for

      /**
       * BV: Send Request.
       */
      // Admin Clear Cache.
      if (url.indexOf('/api/admin/cache/clear') !== -1) {
          param = undefined;
      }
      // Admin Maintenance Status.
      else if (url.indexOf('/api/admin/maintenance/status') !== -1) {
          param = undefined;
      }
      // Admin Maintenance Enable.
      else if (url.indexOf('/api/admin/maintenance/enable') !== -1) {
          param = undefined;
      }
      // Admin Maintenance Disable.
      else if (url.indexOf('/api/admin/maintenance/disable') !== -1) {
          param = undefined;
      }
      // Admin Install.
      else if (url.indexOf('/api/admin/install') !== -1) {
          param = undefined;
      }
      // Admin Update.
      else if (url.indexOf('/api/admin/update') !== -1) {
          param = undefined;
      }
      // Ping.
      else if (url.indexOf('/api/ping') !== -1) {
          param = undefined;
      }
      // Status.
      else if (url.indexOf('/api/ping/status') !== -1) {
          param = undefined;
      }
      // Login.
      else if (url.indexOf('/api/login') !== -1) {
          var username = header['username'];
          var password = header['password'];
          var secret = header['secret'];
          var testKey = param['testKey'];
          param['state'] = $root.find('select[placeholder="state"]').val();
          var timestamp = new Date().getTime();
          var nonce = Math.floor(Math.random() * 99999);
          var hash = '/api/login' + Math.round(timestamp / 1000) + nonce;
          var digest = $.sha256hmac(secret, hash);
          var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(digest));
          var credentials = username + ":" + password;
          $root.find('input[placeholder="Authentication"]').val(authentication);
          $root.find('input[placeholder="Authorization"]').val('Basic ' + btoa(credentials));
          $root.find('input[placeholder="X-Requested-With"]').val(btoa(testKey));
          header = {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Basic ' + btoa(credentials),
            'Authentication': authentication,
            'X-Requested-With': btoa(testKey)
          };
          if (!username || !password || !secret || !testKey || !param['dateOfBirth'] || !param['studentId'] || !param['state']) {
            alert("Please enter all Login details!");
          }
      }
      // Content.
      else if (url.indexOf('/api/content') !== -1) {
          if (!param['token'] || param['token'] === loginToken || !param['testId'] || param['testId'] === loginTestId) {
            alert("Please Login first!");
          }
          url = url.substring(0, url.indexOf('/api/content')) + '/api/content/' + param['token'] + '/' + param['testId'] +
                (param['contentFile'] ? ('/' + param['contentFile']) : '');
          $root.find(".sample-request-url").val(url);
          param = undefined;
      }
      // Results Upload.
      else if (url.indexOf('/api/results') !== -1 && param['testId'] !== undefined && type.toUpperCase() === 'POST') {
          var username = header['username'];
          var password = header['password'];
          var secret = header['secret'];
          var testKey = param['testKey'];
          var testStatus = $root.find('select[placeholder="testStatus"]').val();
          var resultsFile = $root.find('textarea[placeholder="resultsFile"]').val();
          var timestamp = new Date().getTime();
          var nonce = Math.floor(Math.random() * 99999);
          var hash = '/api/results/' + param['token'] + '/' + param['testId'] + (testStatus !== '' ? ('/' + testStatus) : '') + Math.round(timestamp / 1000) +
                     nonce;
          var digest = $.sha256hmac(secret, hash);
          var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(digest));
          var credentials = username + ":" + password;
          $root.find('input[placeholder="Authentication"]').val(authentication);
          $root.find('input[placeholder="Authorization"]').val('Basic ' + btoa(credentials));
          $root.find('input[placeholder="X-Requested-With"]').val(btoa(testKey));
          header = {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Basic ' + btoa(credentials),
            'Authentication': authentication,
            'X-Requested-With': btoa(testKey)
          };
          if (!username || !password || !secret || !testKey) {
            alert("Please enter all Login details!");
          }
          if (!param['token'] || param['token'] === loginToken || !param['testId'] || param['testId'] === loginTestId) {
            alert("Please Login first!");
          }
          url = url.substring(0, url.indexOf('/api/results')) + '/api/results/' + param['token'] + '/' + param['testId'] +
                (testStatus !== '' ? ('/' + testStatus) : '');
          $root.find(".sample-request-url").val(url);
          param = undefined;
          if (resultsFile !== '') {
            try {
              param = $.parseJSON(resultsFile);
            } catch (e) {
              param = resultsFile;
              alert("Please enter a valid Results File!");
            }
          }
      }
      // Results Download.
      else if (url.indexOf('/api/results') !== -1 && param['testId'] !== undefined && type.toUpperCase() === 'GET') {
          var username = header['username'];
          var password = header['password'];
          var secret = header['secret'];
          var testKey = param['testKey'];
          var timestamp = new Date().getTime();
          var nonce = Math.floor(Math.random() * 99999);
          var hash = '/api/results/' + param['token'] + '/' + param['testId'] + Math.round(timestamp / 1000) + nonce;
          var digest = $.sha256hmac(secret, hash);
          var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(digest));
          var credentials = username + ":" + password;
          $root.find('input[placeholder="Authentication"]').val(authentication);
          $root.find('input[placeholder="Authorization"]').val('Basic ' + btoa(credentials));
          $root.find('input[placeholder="X-Requested-With"]').val(btoa(testKey));
          header = {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Basic ' + btoa(credentials),
            'Authentication': authentication,
            'X-Requested-With': btoa(testKey)
          };
          if (!username || !password || !secret || !testKey) {
            alert("Please enter all Login details!");
          }
          if (!param['token'] || param['token'] === loginToken || !param['testId'] || param['testId'] === loginTestId) {
            alert("Please Login first!");
          }
          url = url.substring(0, url.indexOf('/api/results')) + '/api/results/' + param['token'] + '/' + param['testId'];
          $root.find(".sample-request-url").val(url);
          param = undefined;
      }
      // Assessment.
      else if (url.indexOf('/assessment') !== -1 && type.toUpperCase() === 'GET') {
          param = undefined;
      }
      // Assessment Launch.
      else if (url.indexOf('/assessment') !== -1 && type.toUpperCase() === 'POST') {
          data = "testKey=" + encodeURIComponent(param['testKey']) + '&name=' + encodeURIComponent(param['name']);
      }
      // Test Publishing.
      else if (url.indexOf('/api/test') !== -1 && type.toUpperCase() === 'POST') {
          var username = header['username'];
          var password = header['password'];
          var secret = header['secret'];
          var timestamp = new Date().getTime();
          var nonce = Math.floor(Math.random() * 99999);
          var hash = '/api/test' + Math.round(timestamp / 1000) + nonce;
          var digest = $.sha256hmac(secret, hash);
          var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(digest));
          var credentials = username + ":" + password;
          $root.find('input[placeholder="Authentication"]').val(authentication);
          $root.find('input[placeholder="Authorization"]').val('Basic ' + btoa(credentials));
          header = {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Basic ' + btoa(credentials),
            'Authentication': authentication
          };
          if (!username || !password || !secret || !param['testFormRevisionId'] || !param['publishedBy']) {
            alert("Please enter all Test details!");
          }
      }
      // Test Re-Publishing.
      else if (url.indexOf('/api/test') !== -1 && type.toUpperCase() === 'PUT') {
          var username = header['username'];
          var password = header['password'];
          var secret = header['secret'];
          var timestamp = new Date().getTime();
          var nonce = Math.floor(Math.random() * 99999);
          var hash = '/api/test/' + param['adpTestFormRevisionId'] + Math.round(timestamp / 1000) + nonce;
          var digest = $.sha256hmac(secret, hash);
          var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(digest));
          var credentials = username + ":" + password;
          $root.find('input[placeholder="Authentication"]').val(authentication);
          $root.find('input[placeholder="Authorization"]').val('Basic ' + btoa(credentials));
          header = {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Basic ' + btoa(credentials),
            'Authentication': authentication
          };
          if (!username || !password || !secret || !param['testFormRevisionId'] || !param['publishedBy']) {
            alert("Please enter all Test details!");
          }
          url = url.substring(0, url.indexOf('/api/test')) + '/api/test/' + param['adpTestFormRevisionId'];
          $root.find(".sample-request-url").val(url);
          param = {
            testFormRevisionId: param['testFormRevisionId'],
            publishedBy: param['publishedBy']
          };
      }
      // Test Properties Update.
      else if (url.indexOf('/api/test/properties') !== -1 && type.toUpperCase() === 'PATCH') {
          var username = header['username'];
          var password = header['password'];
          param['program'] = $root.find('select[placeholder="program"]').val();
          param['scoreReport'] = $root.find('select[placeholder="scoreReport"]').val();
          param['subject'] = $root.find('select[placeholder="subject"]').val();
          param['grade'] = $root.find('select[placeholder="grade"]').val();
          param['itemSelectionAlgorithm'] = $root.find('select[placeholder="itemSelectionAlgorithm"]').val();
          param['security'] = $root.find('select[placeholder="security"]').val();
          param['multimedia'] = $root.find('select[placeholder="multimedia"]').val();
          param['scoring'] = $root.find('select[placeholder="scoring"]').val();
          param['permissions'] = $root.find('select[placeholder="permissions"]').val();
          param['privacy'] = $root.find('select[placeholder="privacy"]').val();
          param['isActive'] = $root.find('select[placeholder="isActive"]').val();
          var secret = header['secret'];
          var timestamp = new Date().getTime();
          var nonce = Math.floor(Math.random() * 99999);
          var hash = '/api/test/properties' + param['adpTestId'] + Math.round(timestamp / 1000) + nonce;
          var digest = $.sha256hmac(secret, hash);
          var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(digest));
          var credentials = username + ":" + password;
          $root.find('input[placeholder="Authentication"]').val(authentication);
          $root.find('input[placeholder="Authorization"]').val('Basic ' + btoa(credentials));
          header = {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Basic ' + btoa(credentials),
            'Authentication': authentication
          };
          if (!username || !password || !secret || !param['adpTestId'] || !param['batteryId'] ||
              (!param['name'] && !param['program'] && !param['scoreReport'] && !param['subject'] && !param['grade'] &&
               !param['itemSelectionAlgorithm'] && !param['security'] && !param['multimedia'] && !param['scoring'] &&
               !param['permissions'] && !param['privacy'] && !param['description'] && !param['isActive']))
          {
            alert("Please enter all Test details!");
          }
          url = url.substring(0, url.indexOf('/api/test/properties')) + '/api/test/properties/' + param['adpTestId'];
          $root.find(".sample-request-url").val(url);
          param = {
            batteryId: param['batteryId'],
            program: param['program'],
            scoreReport: param['scoreReport'],
            subject: param['subject'],
            grade: param['grade'],
            itemSelectionAlgorithm: param['itemSelectionAlgorithm'],
            security: param['security'],
            multimedia: param['multimedia'],
            scoring: param['scoring'],
            permissions: param['permissions'],
            privacy: param['privacy'],
            isActive: param['isActive']
          };
      }
      // Test Activate/Deactivate.
      else if (url.indexOf('/api/test') !== -1 && type.toUpperCase() === 'PATCH') {
          var username = header['username'];
          var password = header['password'];
          var secret = header['secret'];
          var timestamp = new Date().getTime();
          var nonce = Math.floor(Math.random() * 99999);
          var hash = '/api/test/' + param['adpTestFormRevisionId'] + Math.round(timestamp / 1000) + nonce;
          var digest = $.sha256hmac(secret, hash);
          var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(digest));
          var credentials = username + ":" + password;
          $root.find('input[placeholder="Authentication"]').val(authentication);
          $root.find('input[placeholder="Authorization"]').val('Basic ' + btoa(credentials));
          header = {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Basic ' + btoa(credentials),
            'Authentication': authentication
          };
          if (!username || !password || !secret || !param['testFormRevisionId'] || !param['publishedBy']) {
            alert("Please enter all Test details!");
          }
          url = url.substring(0, url.indexOf('/api/test')) + '/api/test/' + param['adpTestFormRevisionId'];
          $root.find(".sample-request-url").val(url);
          param = {
            testFormRevisionId: param['testFormRevisionId'],
            publishedBy: param['publishedBy'],
            active: $root.find('select[placeholder="active"]').val()
          };
      }
      // Test Un-Publishing & Request Canceling.
      else if (url.indexOf('/api/test') !== -1 && type.toUpperCase() === 'DELETE') {
          var username = header['username'];
          var password = header['password'];
          var secret = header['secret'];
          var timestamp = new Date().getTime();
          var nonce = Math.floor(Math.random() * 99999);
          var hash = '/api/test/' + param['adpTestFormRevisionId'] + Math.round(timestamp / 1000) + nonce;
          var digest = $.sha256hmac(secret, hash);
          var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(digest));
          var credentials = username + ":" + password;
          $root.find('input[placeholder="Authentication"]').val(authentication);
          $root.find('input[placeholder="Authorization"]').val('Basic ' + btoa(credentials));
          header = {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Basic ' + btoa(credentials),
            'Authentication': authentication
          };
          if (!username || !password || !secret || !param['testFormRevisionId'] || !param['publishedBy']) {
            alert("Please enter all Test details!");
          }
          url = url.substring(0, url.indexOf('/api/test')) + '/api/test/' + param['adpTestFormRevisionId'];
          $root.find(".sample-request-url").val(url);
          if (param['requestId']) {
            param = {
              testFormRevisionId: param['testFormRevisionId'],
              publishedBy: param['publishedBy'],
              requestId: param['requestId']
            };
          } else {
            param = {
              testFormRevisionId: param['testFormRevisionId'],
              publishedBy: param['publishedBy']
            };
          }
      }
      // Test Assignment Creation.
      else if (url.indexOf('/api/assignment') !== -1 && type.toUpperCase() === 'POST') {
          var username = header['username'];
          var password = header['password'];
          var secret = header['secret'];
          param['state'] = $root.find('select[placeholder="state"]').val();
          param['grade'] = $root.find('select[placeholder="grade"]').val();
          var timestamp = new Date().getTime();
          var nonce = Math.floor(Math.random() * 99999);
          var hash = '/api/assignment' + Math.round(timestamp / 1000) + nonce;
          var digest = $.sha256hmac(secret, hash);
          var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(digest));
          var credentials = username + ":" + password;
          $root.find('input[placeholder="Authentication"]').val(authentication);
          $root.find('input[placeholder="Authorization"]').val('Basic ' + btoa(credentials));
          header = {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Basic ' + btoa(credentials),
            'Authentication': authentication
          };
          if (!username || !password || !secret || !param['testAssignmentId'] || !param['testId'] || !param['testFormId'] || !param['testKey'] ||
              !param['studentId'])
          {
            alert("Please enter all Test Assignment details!");
          }
          var studentDetails;
          if (param['personalId'] || param['firstName'] || param['lastName'] || param['dateOfBirth'] || param['state'] || param['schoolName'] || param['grade']) {
            studentDetails = {
              studentId: param['studentId'],
              personalId: param['personalId'],
              firstName: param['firstName'],
              lastName: param['lastName'],
              dateOfBirth: param['dateOfBirth'],
              state: param['state'],
              schoolName: param['schoolName'],
              grade: param['grade']
            };
          } else {
            studentDetails = {
              studentId: param['studentId']
            };
          }
          param = {
            testAssignmentId: param['testAssignmentId'],
            test: {
              testId: param['testId'],
              testFormId: param['testFormId'],
              testKey: param['testKey'],
              enableLineReader: $root.find('select[placeholder="enableLineReader"]').val(),
              enableTextToSpeech: $root.find('select[placeholder="enableTextToSpeech"]').val()
            },
            student: studentDetails
          };
      }
      // Test Assignment Update.
      else if (url.indexOf('/api/assignment') !== -1 && type.toUpperCase() === 'PATCH') {
          var username = header['username'];
          var password = header['password'];
          var secret = header['secret'];
          var testStatus = $root.find('select[placeholder="testStatus"]').val();
          param['state'] = $root.find('select[placeholder="state"]').val();
          param['grade'] = $root.find('select[placeholder="grade"]').val();
          var timestamp = new Date().getTime();
          var nonce = Math.floor(Math.random() * 99999);
          var hash = '/api/assignment' + Math.round(timestamp / 1000) + nonce;
          var digest = $.sha256hmac(secret, hash);
          var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(digest));
          var credentials = username + ":" + password;
          $root.find('input[placeholder="Authentication"]').val(authentication);
          $root.find('input[placeholder="Authorization"]').val('Basic ' + btoa(credentials));
          header = {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Basic ' + btoa(credentials),
            'Authentication': authentication
          };
          if (!username || !password || !secret || !param['testAssignmentId'] || !param['adpTestAssignmentId'] || !param['studentId'] || (!testStatus &&
              !param['testId'] && !param['testFormId'] && !param['testKey'] && !param['personalId'] && !param['firstName'] && !param['lastName'] &&
              !param['dateOfBirth'] && param['state'] === '' && !param['schoolName'] && param['grade'] === ''))
          {
            alert("Please enter all Test Assignment details!");
          }
          url = url.substring(0, url.indexOf('/api/assignment')) + '/api/assignment/' + param['adpTestAssignmentId'];
          $root.find(".sample-request-url").val(url);
          var status, testDetails, studentDetails;
          if (testStatus) {
            status = testStatus;
          }
          if (param['testId'] || param['testFormId'] || param['testKey']) {
            testDetails = {
              testId: param['testId'],
              testFormId: param['testFormId'],
              testKey: param['testKey'],
              enableLineReader: $root.find('select[placeholder="enableLineReader"]').val(),
              enableTextToSpeech: $root.find('select[placeholder="enableTextToSpeech"]').val()
            };
          }
          if (param['personalId'] || param['firstName'] || param['lastName'] || param['dateOfBirth'] || param['state'] || param['schoolName'] || param['grade']) {
            studentDetails = {
              studentId: param['studentId'],
              personalId: param['personalId'],
              firstName: param['firstName'],
              lastName: param['lastName'],
              dateOfBirth: param['dateOfBirth'],
              state: param['state'],
              schoolName: param['schoolName'],
              grade: param['grade']
            };
          } else {
            studentDetails = {
              studentId: param['studentId']
            };
          }
          param = {
            testAssignmentId: param['testAssignmentId'],
            testStatus: status,
            test: testDetails,
            student: studentDetails
          };
      }
      // Test Assignment Deletion.
      else if (url.indexOf('/api/assignment') !== -1 && type.toUpperCase() === 'DELETE') {
          var username = header['username'];
          var password = header['password'];
          var secret = header['secret'];
          var timestamp = new Date().getTime();
          var nonce = Math.floor(Math.random() * 99999);
          var hash = '/api/assignment' + Math.round(timestamp / 1000) + nonce;
          var digest = $.sha256hmac(secret, hash);
          var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(digest));
          var credentials = username + ":" + password;
          $root.find('input[placeholder="Authentication"]').val(authentication);
          $root.find('input[placeholder="Authorization"]').val('Basic ' + btoa(credentials));
          header = {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Basic ' + btoa(credentials),
            'Authentication': authentication
          };
          if (!username || !password || !secret || !param['testAssignmentId'] || !param['adpTestAssignmentId'] || !param['studentId']) {
            alert("Please enter all Test Assignment details!");
          }
          url = url.substring(0, url.indexOf('/api/assignment')) + '/api/assignment/' + param['adpTestAssignmentId'];
          $root.find(".sample-request-url").val(url);
          param = {
            testAssignmentId: param['testAssignmentId'],
            test: {
              testId: param['testId'],
              testFormId: param['testFormId'],
              testKey: param['testKey']
            },
            student: {
              studentId: param['studentId']
            }
          };
      }
      // Results Details.
      else if (url.indexOf('/api/results/') !== -1 && type.toUpperCase() === 'GET') {
          var username = header['username'];
          var password = header['password'];
          var secret = header['secret'];
          var timestamp = new Date().getTime();
          var nonce = Math.floor(Math.random() * 99999);
          var hash = '/api/results/' + param['adpTestAssignmentId'] + Math.round(timestamp / 1000) + nonce;
          var digest = $.sha256hmac(secret, hash);
          var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(digest));
          var credentials = username + ":" + password;
          $root.find('input[placeholder="Authentication"]').val(authentication);
          $root.find('input[placeholder="Authorization"]').val('Basic ' + btoa(credentials));
          header = {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Basic ' + btoa(credentials),
            'Authentication': authentication,
          };
          if (!param['adpTestAssignmentId']) {
            alert("Please enter Test Assignment ID!");
          }
          url = url.substring(0, url.indexOf('/api/results')) + '/api/results/' + param['adpTestAssignmentId'];
          $root.find(".sample-request-url").val(url);
          param = undefined;
      }
      // Don't make a request for General, Authentication and Errors.
      else {
          return;
      }

      // send AJAX request, catch success or error callback
      $.ajax({
          url: url + xdebug, // BV: Trigger Xdebug session.
          //dataType: "json", // BV: Only some requests are receiving back JSON response.
          // BV: Explicit "application/json" for any request triggers CORS policy.
          contentType: (type.toUpperCase() === "GET" || (url.indexOf('/assessment') !== -1 && type.toUpperCase() === 'POST') ?
                       "application/x-www-form-urlencoded; charset=UTF-8" : "application/json"),
          data: (url.indexOf('/assessment') !== -1 && type.toUpperCase() === 'POST') ? data : JSON.stringify(param),
          headers: header,
          type: type.toUpperCase(),
          success: displaySuccess,
          error: displayError
      });

      function displaySuccess(data) {
          // BV: Populate media placeholders for Content web service, if needed.
          var media;
          $root.find('.sample-request-response-image').attr('src', '').hide();
          $root.find('.sample-request-response-audio').attr('src', '').hide();
          $root.find('.sample-request-response-video').attr('src', '').hide();
          if (url.indexOf('/api/content') !== -1) {
            if (isFileType(url, '.jpg') || isFileType(url, '.jpeg') || isFileType(url, '.jpe') || isFileType(url, '.png')) {
              media = true;
              $root.find('.sample-request-response-image').attr('src', url).show();
            } else if(isFileType(url, '.mp3') || isFileType(url, '.ogg')) {
              media = true;
              $root.find('.sample-request-response-audio').attr('src', url).show();
            } else if(isFileType(url, '.mp4') || isFileType(url, '.webm')) {
              media = true;
              $root.find('.sample-request-response-video').attr('src', url).show();
            }
            if (media) {
              $root.find(".sample-request-response").find('pre').hide();
            } else {
              $root.find(".sample-request-response").find('pre').show();
            }
          }

          var jsonResponse;
          try {
            jsonResponse = JSON.stringify(data, null, 4);
          } catch (e) {
            jsonResponse = data;
          }
          $root.find(".sample-request-response").fadeTo(250, 1);
          $root.find(".sample-request-response-json").html(prettyPrintJSON(media ? "" : jsonResponse)); // BV: Added pretty printing of JSON response and Media detection.
          refreshScrollSpy();

          // BV: Auto-populate form fields that are dependent on Login response.
          if (url.indexOf('/api/login') !== -1) {
            var token, testId;
            try {
              token = data.result.api.token;
              testId = data.result.test.id;
            } catch(e) {
              try {
                token = data.api.token;
                testId = data.test.id;
              } catch(e) {
                token = '[ERROR]';
                testId = '[ERROR]';
              }
            }
            $('input[placeholder="token"]').val(token);
            $('input[placeholder="testId"]').val(testId);
            var $contentRoot = $('article[data-group="' + group + '"][data-name="Content"][data-version="' + version + '"]');
            var contentURL = $contentRoot.find(".sample-request-url").val();
            $contentRoot.find('.sample-request-url').val(
              contentURL.substring(0, contentURL.indexOf('/api/content')) + '/api/content/' + token + '/' + testId
            );
            var $resultsDownloadRoot = $('article[data-group="' + group + '"][data-name="Results_Download"][data-version="' + version + '"]');
            var resultsDownloadURL = $resultsDownloadRoot.find(".sample-request-url").val();
            $resultsDownloadRoot.find('.sample-request-url').val(
              resultsDownloadURL.substring(0, resultsDownloadURL.indexOf('/api/results')) + '/api/results/' + token + '/' + testId
            );
            var $resultsUploadRoot = $('article[data-group="' + group + '"][data-name="Results_Upload"][data-version="' + version + '"]');
            var resultsUploadURL = $resultsUploadRoot.find(".sample-request-url").val();
            $resultsUploadRoot.find('.sample-request-url').val(
              resultsUploadURL.substring(0, resultsUploadURL.indexOf('/api/results')) + '/api/results/' + token + '/' + testId
            );
          }
      };

      function displayError(jqXHR, textStatus, error) {
          var message = "Error " + jqXHR.status + ": " + error;
          var jsonResponse;
          try {
              jsonResponse = JSON.parse(jqXHR.responseText);
              jsonResponse = JSON.stringify(jsonResponse, null, 4);
          } catch (e) {
              jsonResponse = jqXHR.responseText;
          }

          if (jsonResponse)
              message += "<br>" + prettyPrintJSON(jsonResponse); // BV: Added pretty printing of JSON response.

          // flicker on previous error to make clear that there is a new response
          if($root.find(".sample-request-response").is(":visible"))
              $root.find(".sample-request-response").fadeTo(1, 0.1);

          $root.find(".sample-request-response").fadeTo(250, 1);
          $root.find(".sample-request-response-json").html(message);
          refreshScrollSpy();

          // BV: Auto-populate form fields that are dependent on Login response.
          if (url.indexOf('/api/login') !== -1) {
            var token = testId = '[ERROR]';
            $('input[placeholder="token"]').val(token);
            $('input[placeholder="testId"]').val(testId);
            var $contentRoot = $('article[data-group="' + group + '"][data-name="Content"][data-version="' + version + '"]');
            var contentURL = $contentRoot.find(".sample-request-url").val();
            $contentRoot.find('.sample-request-url').val(
              contentURL.substring(0, contentURL.indexOf('/api/content')) + '/api/content/' + token + '/' + testId
            );
            var $resultsDownloadRoot = $('article[data-group="' + group + '"][data-name="Results_Download"][data-version="' + version + '"]');
            var resultsDownloadURL = $resultsDownloadRoot.find(".sample-request-url").val();
            $resultsDownloadRoot.find('.sample-request-url').val(
              resultsDownloadURL.substring(0, resultsDownloadURL.indexOf('/api/results')) + '/api/results/' + token + '/' + testId
            );
            var $resultsUploadRoot = $('article[data-group="' + group + '"][data-name="Results_Upload"][data-version="' + version + '"]');
            var resultsUploadURL = $resultsUploadRoot.find(".sample-request-url").val();
            $resultsUploadRoot.find('.sample-request-url').val(
              resultsUploadURL.substring(0, resultsUploadURL.indexOf('/api/results')) + '/api/results/' + token + '/' + testId
            );
          }
          // BV: Reset media placeholders for Content responses.
          if (url.indexOf('/api/content') !== -1) {
            $root.find('.sample-request-response-image').attr('src', '').hide();
            $root.find('.sample-request-response-audio').attr('src', '').hide();
            $root.find('.sample-request-response-video').attr('src', '').hide();
            $root.find(".sample-request-response").find('pre').show();
          }
      };
  }

  function clearSampleRequest(group, name, version)
  {
      var $root = $('article[data-group="' + group + '"][data-name="' + name + '"][data-version="' + version + '"]');

      // hide sample response
      $root.find(".sample-request-response-json").html("");
      $root.find(".sample-request-response").hide();

      // reset value of parameters
      $root.find(".sample-request-param").each(function(i, element) {
          element.value = "";
      });

      // restore default URL
      var $urlElement = $root.find(".sample-request-url");
      $urlElement.val($urlElement.prop("defaultValue"));

      refreshScrollSpy();

      /**
       * BV: Reset forms.
       */
      // Login.
      if (name === 'Login') {
          var $loginRoot = $('article[data-group="' + group + '"][data-name="Login"][data-version="' + version + '"]');
          $loginRoot.find('input[placeholder="username"]').val(loginUsername);
          $loginRoot.find('input[placeholder="username"]').prop('required', true);
          $loginRoot.find('input[placeholder="username"]').prop('maxlength', 20);
          $loginRoot.find('input[placeholder="username"]').prop('pattern', '^[0-9a-zA-Z]{1,20}$');
          $loginRoot.find('input[placeholder="password"]').val(loginPassword);
          $loginRoot.find('input[placeholder="password"]').prop('required', true);
          $loginRoot.find('input[placeholder="password"]').prop('maxlength', 32);
          $loginRoot.find('input[placeholder="password"]').prop('pattern', '[0-9a-z]{32}');
          $loginRoot.find('input[placeholder="secret"]').val(loginSecret);
          $loginRoot.find('input[placeholder="secret"]').prop('required', true);
          $loginRoot.find('input[placeholder="secret"]').prop('maxlength', 32);
          $loginRoot.find('input[placeholder="secret"]').prop('pattern', '^[0-9a-z]{32}$');
          $loginRoot.find('input[placeholder="Authentication"]').val(loginAuthentication);
          $loginRoot.find('input[placeholder="Authentication"]').prop('readonly', true);
          $loginRoot.find('input[placeholder="Authorization"]').val(loginAuthorization);
          $loginRoot.find('input[placeholder="Authorization"]').prop('readonly', true);
          $loginRoot.find('input[placeholder="X-Requested-With"]').val(loginXRequestedWith);
          $loginRoot.find('input[placeholder="X-Requested-With"]').prop('readonly', true);
          $loginRoot.find('input[placeholder="testKey"]').val(loginTestKey);
          $loginRoot.find('input[placeholder="testKey"]').prop('required', true);
          $loginRoot.find('input[placeholder="testKey"]').prop('maxlength', 10);
          $loginRoot.find('input[placeholder="testKey"]').prop('pattern', '^[A-Z]{10}$');
          $loginRoot.find('input[placeholder="dateOfBirth"]').val(loginDateOfBirth);
          $loginRoot.find('input[placeholder="dateOfBirth"]').prop('required', true);
          $loginRoot.find('input[placeholder="dateOfBirth"]').prop('maxlength', 10);
          $loginRoot.find('input[placeholder="dateOfBirth"]').prop('pattern', '^(199\d|20\d\d)-(0\d|10|11|12)-(0\d|1\d|2\d|30|31)$');
          $loginRoot.find('input[placeholder="studentId"]').val(loginStudentId);
          $loginRoot.find('input[placeholder="studentId"]').prop('required', true);
          $loginRoot.find('input[placeholder="studentId"]').prop('maxlength', 30);
          $loginRoot.find('input[placeholder="studentId"]').prop('pattern', '^[0-9a-zA-Z]{0,30}');
          $loginRoot.find('select[placeholder="state"]').val(loginState);
      }
      // Content.
      else if (name === 'Content') {
          var $contentRoot = $('article[data-group="' + group + '"][data-name="Content"][data-version="' + version + '"]');
          $contentRoot.find('input[placeholder="token"]').val(loginToken);
          $contentRoot.find('input[placeholder="token"]').prop('required', true);
          $contentRoot.find('input[placeholder="token"]').prop('maxlength', 32);
          $contentRoot.find('input[placeholder="token"]').prop('pattern', '^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$');
          $contentRoot.find('input[placeholder="testId"]').val(loginTestId);
          $contentRoot.find('input[placeholder="testId"]').prop('required', true);
          $contentRoot.find('input[placeholder="testId"]').prop('pattern', '^[1-4294967295]$');
          $contentRoot.find('input[placeholder="contentFile"]').val(contentContentFile);
          $contentRoot.find('input[placeholder="contentFile"]').prop('pattern', '^[^\|\*\?\\:<>/$\x22]*[^\.\|\*\?\\:<>/$\x22]+$');
      }
      // Results Upload.
      else if (name === 'Results_Upload') {
          var $resultsUploadRoot = $('article[data-group="' + group + '"][data-name="Results_Upload"][data-version="' + version + '"]');
          $resultsUploadRoot.find('input[placeholder="username"]').val(loginUsername);
          $resultsUploadRoot.find('input[placeholder="username"]').prop('required', true);
          $resultsUploadRoot.find('input[placeholder="username"]').prop('maxlength', 20);
          $resultsUploadRoot.find('input[placeholder="username"]').prop('pattern', '^[0-9a-zA-Z]{1,20}$');
          $resultsUploadRoot.find('input[placeholder="password"]').val(loginPassword);
          $resultsUploadRoot.find('input[placeholder="password"]').prop('required', true);
          $resultsUploadRoot.find('input[placeholder="password"]').prop('maxlength', 32);
          $resultsUploadRoot.find('input[placeholder="password"]').prop('pattern', '^[0-9a-z]{32}$');
          $resultsUploadRoot.find('input[placeholder="secret"]').val(loginSecret);
          $resultsUploadRoot.find('input[placeholder="secret"]').prop('required', true);
          $resultsUploadRoot.find('input[placeholder="secret"]').prop('maxlength', 32);
          $resultsUploadRoot.find('input[placeholder="secret"]').prop('pattern', '^[0-9a-z]{32}$');
          $resultsUploadRoot.find('input[placeholder="Authentication"]').val(loginAuthentication);
          $resultsUploadRoot.find('input[placeholder="Authentication"]').prop('readonly', true);
          $resultsUploadRoot.find('input[placeholder="Authorization"]').val(loginAuthorization);
          $resultsUploadRoot.find('input[placeholder="Authorization"]').prop('readonly', true);
          $resultsUploadRoot.find('input[placeholder="X-Requested-With"]').val(loginXRequestedWith);
          $resultsUploadRoot.find('input[placeholder="X-Requested-With"]').prop('readonly', true);
          $resultsUploadRoot.find('input[placeholder="testKey"]').val(loginTestKey);
          $resultsUploadRoot.find('input[placeholder="testKey"]').prop('required', true);
          $resultsUploadRoot.find('input[placeholder="testKey"]').prop('maxlength', 10);
          $resultsUploadRoot.find('input[placeholder="testKey"]').prop('pattern', '^[A-Z]{10}$');
          $resultsUploadRoot.find('input[placeholder="token"]').val(loginToken);
          $resultsUploadRoot.find('input[placeholder="token"]').prop('required', true);
          $resultsUploadRoot.find('input[placeholder="token"]').prop('maxlength', 32);
          $resultsUploadRoot.find('input[placeholder="token"]').prop('pattern', '^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$');
          $resultsUploadRoot.find('input[placeholder="testId"]').val(loginTestId);
          $resultsUploadRoot.find('input[placeholder="testId"]').prop('required', true);
          $resultsUploadRoot.find('input[placeholder="testId"]').prop('pattern', '^[1-4294967295]$');
      }
      // Results Download.
      else if (name === 'Results_Download') {
          var $resultsDownloadRoot = $('article[data-group="' + group + '"][data-name="Results_Download"][data-version="' + version + '"]');
          $resultsDownloadRoot.find('input[placeholder="username"]').val(loginUsername);
          $resultsDownloadRoot.find('input[placeholder="username"]').prop('required', true);
          $resultsDownloadRoot.find('input[placeholder="username"]').prop('maxlength', 20);
          $resultsDownloadRoot.find('input[placeholder="username"]').prop('pattern', '^[0-9a-zA-Z]{1,20}$');
          $resultsDownloadRoot.find('input[placeholder="password"]').val(loginPassword);
          $resultsDownloadRoot.find('input[placeholder="password"]').prop('required', true);
          $resultsDownloadRoot.find('input[placeholder="password"]').prop('maxlength', 32);
          $resultsDownloadRoot.find('input[placeholder="password"]').prop('pattern', '^[0-9a-z]{32}$');
          $resultsDownloadRoot.find('input[placeholder="secret"]').val(loginSecret);
          $resultsDownloadRoot.find('input[placeholder="secret"]').prop('required', true);
          $resultsDownloadRoot.find('input[placeholder="secret"]').prop('maxlength', 32);
          $resultsDownloadRoot.find('input[placeholder="secret"]').prop('pattern', '^[0-9a-f]{32}$');
          $resultsDownloadRoot.find('input[placeholder="Authentication"]').val(loginAuthentication);
          $resultsDownloadRoot.find('input[placeholder="Authentication"]').prop('readonly', true);
          $resultsDownloadRoot.find('input[placeholder="Authorization"]').val(loginAuthorization);
          $resultsDownloadRoot.find('input[placeholder="Authorization"]').prop('readonly', true);
          $resultsDownloadRoot.find('input[placeholder="X-Requested-With"]').val(loginXRequestedWith);
          $resultsDownloadRoot.find('input[placeholder="X-Requested-With"]').prop('readonly', true);
          $resultsDownloadRoot.find('input[placeholder="testKey"]').val(loginTestKey);
          $resultsDownloadRoot.find('input[placeholder="testKey"]').prop('required', true);
          $resultsDownloadRoot.find('input[placeholder="testKey"]').prop('maxlength', 10);
          $resultsDownloadRoot.find('input[placeholder="testKey"]').prop('pattern', '^[A-Z]{10}$');
          $resultsDownloadRoot.find('input[placeholder="token"]').val(loginToken);
          $resultsDownloadRoot.find('input[placeholder="token"]').prop('required', true);
          $resultsDownloadRoot.find('input[placeholder="token"]').prop('maxlength', 32);
          $resultsDownloadRoot.find('input[placeholder="token"]').prop('pattern', '^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$');
          $resultsDownloadRoot.find('input[placeholder="testId"]').val(loginTestId);
          $resultsDownloadRoot.find('input[placeholder="testId"]').prop('required', true);
          $resultsDownloadRoot.find('input[placeholder="testId"]').prop('pattern', '^[1-4294967295]$');
      }
      // Assessment Launch.
      else if (name === 'Assessment_Launch') {
          var $assessmentLaunchRoot = $('article[data-group="' + group + '"][data-name="Assessment_Launch"][data-version="' + version + '"]');
          $assessmentLaunchRoot.find('input[placeholder="testKey"]').val(assessmentTestKey);
          $assessmentLaunchRoot.find('input[placeholder="testKey"]').prop('required', true);
          $assessmentLaunchRoot.find('input[placeholder="testKey"]').prop('maxlength', 10);
          $assessmentLaunchRoot.find('input[placeholder="testKey"]').prop('pattern', '^[A-Z]{10}$');
      }
      // Test Publishing.
      else if (name === 'Test_Publishing') {
          var $testPublishingRoot = $('article[data-group="' + group + '"][data-name="Test_Publishing"][data-version="' + version + '"]');
          $testPublishingRoot.find('input[placeholder="username"]').val(testUsername);
          $testPublishingRoot.find('input[placeholder="username"]').prop('required', true);
          $testPublishingRoot.find('input[placeholder="username"]').prop('maxlength', 20);
          $testPublishingRoot.find('input[placeholder="username"]').prop('pattern', '^[0-9a-zA-Z]{1,20}$');
          $testPublishingRoot.find('input[placeholder="password"]').val(testPassword);
          $testPublishingRoot.find('input[placeholder="password"]').prop('required', true);
          $testPublishingRoot.find('input[placeholder="password"]').prop('maxlength', 32);
          $testPublishingRoot.find('input[placeholder="password"]').prop('pattern', '^[0-9a-z]{32}$');
          $testPublishingRoot.find('input[placeholder="secret"]').val(testSecret);
          $testPublishingRoot.find('input[placeholder="secret"]').prop('required', true);
          $testPublishingRoot.find('input[placeholder="secret"]').prop('maxlength', 32);
          $testPublishingRoot.find('input[placeholder="secret"]').prop('pattern', '^[0-9a-f]{32}$');
          $testPublishingRoot.find('input[placeholder="Authentication"]').val(testAuthentication);
          $testPublishingRoot.find('input[placeholder="Authentication"]').prop('readonly', true);
          $testPublishingRoot.find('input[placeholder="Authorization"]').val(testAuthorization);
          $testPublishingRoot.find('input[placeholder="Authorization"]').prop('readonly', true);
          $testPublishingRoot.find('input[placeholder="testFormRevisionId"]').val(testTestFormRevisionId);
          $testPublishingRoot.find('input[placeholder="testFormRevisionId"]').prop('required', true);
          $testPublishingRoot.find('input[placeholder="testFormRevisionId"]').prop('maxlength', 26);
          $testPublishingRoot.find('input[placeholder="testFormRevisionId"]').prop('pattern', '^i[0-9]{12,25}$');
          $testPublishingRoot.find('input[placeholder="publishedBy"]').val(testPublishedBy);
          $testPublishingRoot.find('input[placeholder="publishedBy"]').prop('required', true);
          $testPublishingRoot.find('input[placeholder="publishedBy"]').prop('maxlength', 100);
          $testPublishingRoot.find('input[placeholder="publishedBy"]').prop('pattern', '^.{1,100}$');
      }
      // Test Re-Publishing.
      else if (name === 'Test_Re_Publishing') {
          var $testRePublishingRoot = $('article[data-group="' + group + '"][data-name="Test_Re_Publishing"][data-version="' + version + '"]');
          $testRePublishingRoot.find('input[placeholder="username"]').val(testUsername);
          $testRePublishingRoot.find('input[placeholder="username"]').prop('required', true);
          $testRePublishingRoot.find('input[placeholder="username"]').prop('maxlength', 20);
          $testRePublishingRoot.find('input[placeholder="username"]').prop('pattern', '^[0-9a-zA-Z]{1,20}$');
          $testRePublishingRoot.find('input[placeholder="password"]').val(testPassword);
          $testRePublishingRoot.find('input[placeholder="password"]').prop('required', true);
          $testRePublishingRoot.find('input[placeholder="password"]').prop('maxlength', 32);
          $testRePublishingRoot.find('input[placeholder="password"]').prop('pattern', '^[0-9a-z]{32}$');
          $testRePublishingRoot.find('input[placeholder="secret"]').val(testSecret);
          $testRePublishingRoot.find('input[placeholder="secret"]').prop('required', true);
          $testRePublishingRoot.find('input[placeholder="secret"]').prop('maxlength', 32);
          $testRePublishingRoot.find('input[placeholder="secret"]').prop('pattern', '^[0-9a-f]{32}$');
          $testRePublishingRoot.find('input[placeholder="Authentication"]').val(testAuthentication);
          $testRePublishingRoot.find('input[placeholder="Authentication"]').prop('readonly', true);
          $testRePublishingRoot.find('input[placeholder="Authorization"]').val(testAuthorization);
          $testRePublishingRoot.find('input[placeholder="Authorization"]').prop('readonly', true);
          $testRePublishingRoot.find('input[placeholder="testFormRevisionId"]').val(testTestFormRevisionId);
          $testRePublishingRoot.find('input[placeholder="testFormRevisionId"]').prop('required', true);
          $testRePublishingRoot.find('input[placeholder="testFormRevisionId"]').prop('maxlength', 26);
          $testRePublishingRoot.find('input[placeholder="testFormRevisionId"]').prop('pattern', '^i[0-9]{12,25}$');
          $testRePublishingRoot.find('input[placeholder="publishedBy"]').val(testPublishedBy);
          $testRePublishingRoot.find('input[placeholder="publishedBy"]').prop('required', true);
          $testRePublishingRoot.find('input[placeholder="publishedBy"]').prop('maxlength', 100);
          $testRePublishingRoot.find('input[placeholder="publishedBy"]').prop('pattern', '^.{1,100}$');
          $testRePublishingRoot.find('input[placeholder="adpTestFormRevisionId"]').val(testAdpTestFormRevisionId);
          $testRePublishingRoot.find('input[placeholder="adpTestFormRevisionId"]').prop('required', true);
          $testRePublishingRoot.find('input[placeholder="adpTestFormRevisionId"]').prop('pattern', '^[1-4294967295]$');
      }
      // Test Activate/Deactivate.
      else if (name === 'Test_Properties_Update') {
          var $testProeprtiesUpdateRoot = $('article[data-group="' + group + '"][data-name="Test_Properties_Update"][data-version="' + version + '"]');
          $testProeprtiesUpdateRoot.find('input[placeholder="username"]').val(testUsername);
          $testProeprtiesUpdateRoot.find('input[placeholder="username"]').prop('required', true);
          $testProeprtiesUpdateRoot.find('input[placeholder="username"]').prop('maxlength', 20);
          $testProeprtiesUpdateRoot.find('input[placeholder="username"]').prop('pattern', '^[0-9a-zA-Z]{1,20}$');
          $testProeprtiesUpdateRoot.find('input[placeholder="password"]').val(testPassword);
          $testProeprtiesUpdateRoot.find('input[placeholder="password"]').prop('required', true);
          $testProeprtiesUpdateRoot.find('input[placeholder="password"]').prop('maxlength', 32);
          $testProeprtiesUpdateRoot.find('input[placeholder="password"]').prop('pattern', '^[0-9a-z]{32}$');
          $testProeprtiesUpdateRoot.find('input[placeholder="secret"]').val(testSecret);
          $testProeprtiesUpdateRoot.find('input[placeholder="secret"]').prop('required', true);
          $testProeprtiesUpdateRoot.find('input[placeholder="secret"]').prop('maxlength', 32);
          $testProeprtiesUpdateRoot.find('input[placeholder="secret"]').prop('pattern', '^[0-9a-z]{32}$');
          $testProeprtiesUpdateRoot.find('input[placeholder="Authentication"]').val(testAuthentication);
          $testProeprtiesUpdateRoot.find('input[placeholder="Authentication"]').prop('readonly', true);
          $testProeprtiesUpdateRoot.find('input[placeholder="Authorization"]').val(testAuthorization);
          $testProeprtiesUpdateRoot.find('input[placeholder="Authorization"]').prop('readonly', true);
          $testProeprtiesUpdateRoot.find('input[placeholder="adpTestId"]').val(adpTestId);
          $testProeprtiesUpdateRoot.find('input[placeholder="adpTestId"]').prop('required', true);
          $testProeprtiesUpdateRoot.find('input[placeholder="adpTestId"]').prop('pattern', '^[1-4294967295]$');
          $testProeprtiesUpdateRoot.find('input[placeholder="batteryId"]').val(batteryId);
          $testProeprtiesUpdateRoot.find('input[placeholder="batteryId"]').prop('required', true);
          $testProeprtiesUpdateRoot.find('input[placeholder="batteryId"]').prop('maxlength', 26);
          $testProeprtiesUpdateRoot.find('input[placeholder="batteryId"]').prop('pattern', '^i[0-9]{12,25}$');
          $testProeprtiesUpdateRoot.find('input[placeholder="name"]').prop('maxlength', 100);
          $testProeprtiesUpdateRoot.find('input[placeholder="name"]').prop('pattern', '^.{1,100}$');
          $testProeprtiesUpdateRoot.find('input[placeholder="decription"]').prop('maxlength', 4096);
          $testProeprtiesUpdateRoot.find('input[placeholder="decription"]').prop('pattern', '^.{1,4096}$');
      }
      // Test Activate/Deactivate.
      else if (name === 'Test_Activate_Deactivate') {
          var $testActivateDeactivateRoot = $('article[data-group="' + group + '"][data-name="Test_Activate_Deactivate"][data-version="' + version + '"]');
          $testActivateDeactivateRoot.find('input[placeholder="username"]').val(testUsername);
          $testActivateDeactivateRoot.find('input[placeholder="username"]').prop('required', true);
          $testActivateDeactivateRoot.find('input[placeholder="username"]').prop('maxlength', 20);
          $testActivateDeactivateRoot.find('input[placeholder="username"]').prop('pattern', '^[0-9a-zA-Z]{1,20}$');
          $testActivateDeactivateRoot.find('input[placeholder="password"]').val(testPassword);
          $testActivateDeactivateRoot.find('input[placeholder="password"]').prop('required', true);
          $testActivateDeactivateRoot.find('input[placeholder="password"]').prop('maxlength', 32);
          $testActivateDeactivateRoot.find('input[placeholder="password"]').prop('pattern', '^[0-9a-f]{32}$');
          $testActivateDeactivateRoot.find('input[placeholder="secret"]').val(testSecret);
          $testActivateDeactivateRoot.find('input[placeholder="secret"]').prop('required', true);
          $testActivateDeactivateRoot.find('input[placeholder="secret"]').prop('maxlength', 32);
          $testActivateDeactivateRoot.find('input[placeholder="secret"]').prop('pattern', '^[0-9a-f]{32}$');
          $testActivateDeactivateRoot.find('input[placeholder="Authentication"]').val(testAuthentication);
          $testActivateDeactivateRoot.find('input[placeholder="Authentication"]').prop('readonly', true);
          $testActivateDeactivateRoot.find('input[placeholder="Authorization"]').val(testAuthorization);
          $testActivateDeactivateRoot.find('input[placeholder="Authorization"]').prop('readonly', true);
          $testActivateDeactivateRoot.find('input[placeholder="testFormRevisionId"]').val(testTestFormRevisionId);
          $testActivateDeactivateRoot.find('input[placeholder="testFormRevisionId"]').prop('required', true);
          $testActivateDeactivateRoot.find('input[placeholder="testFormRevisionId"]').prop('maxlength', 26);
          $testActivateDeactivateRoot.find('input[placeholder="testFormRevisionId"]').prop('pattern', '^i[0-9]{12,25}$');
          $testActivateDeactivateRoot.find('input[placeholder="publishedBy"]').val(testPublishedBy);
          $testActivateDeactivateRoot.find('input[placeholder="publishedBy"]').prop('required', true);
          $testActivateDeactivateRoot.find('input[placeholder="publishedBy"]').prop('maxlength', 100);
          $testActivateDeactivateRoot.find('input[placeholder="publishedBy"]').prop('pattern', '^.{1,100}$');
          $testActivateDeactivateRoot.find('input[placeholder="adpTestFormRevisionId"]').val(testAdpTestFormRevisionId);
          $testActivateDeactivateRoot.find('input[placeholder="adpTestFormRevisionId"]').prop('required', true);
          $testActivateDeactivateRoot.find('input[placeholder="adpTestFormRevisionId"]').prop('pattern', '^[1-4294967295]$');
          $testActivateDeactivateRoot.find('select[placeholder="active"]').val(testActive);
      }
      // Test Un-Publishing & Request Canceling.
      else if (name === 'Test_Un_Publishing') {
          var $testUnPublishingRoot = $('article[data-group="' + group + '"][data-name="Test_Un_Publishing"][data-version="' + version + '"]');
          $testUnPublishingRoot.find('input[placeholder="username"]').val(testUsername);
          $testUnPublishingRoot.find('input[placeholder="username"]').prop('required', true);
          $testUnPublishingRoot.find('input[placeholder="username"]').prop('maxlength', 20);
          $testUnPublishingRoot.find('input[placeholder="username"]').prop('pattern', '^[0-9a-zA-Z]{1,20}$');
          $testUnPublishingRoot.find('input[placeholder="password"]').val(testPassword);
          $testUnPublishingRoot.find('input[placeholder="password"]').prop('required', true);
          $testUnPublishingRoot.find('input[placeholder="password"]').prop('maxlength', 32);
          $testUnPublishingRoot.find('input[placeholder="password"]').prop('pattern', '^[0-9a-z]{32}$');
          $testUnPublishingRoot.find('input[placeholder="secret"]').val(testSecret);
          $testUnPublishingRoot.find('input[placeholder="secret"]').prop('required', true);
          $testUnPublishingRoot.find('input[placeholder="secret"]').prop('maxlength', 32);
          $testUnPublishingRoot.find('input[placeholder="secret"]').prop('pattern', '^[0-9a-z]{32}$');
          $testUnPublishingRoot.find('input[placeholder="Authentication"]').val(testAuthentication);
          $testUnPublishingRoot.find('input[placeholder="Authentication"]').prop('readonly', true);
          $testUnPublishingRoot.find('input[placeholder="Authorization"]').val(testAuthorization);
          $testUnPublishingRoot.find('input[placeholder="Authorization"]').prop('readonly', true);
          $testUnPublishingRoot.find('input[placeholder="testFormRevisionId"]').val(testTestFormRevisionId);
          $testUnPublishingRoot.find('input[placeholder="testFormRevisionId"]').prop('required', true);
          $testUnPublishingRoot.find('input[placeholder="testFormRevisionId"]').prop('maxlength', 26);
          $testUnPublishingRoot.find('input[placeholder="testFormRevisionId"]').prop('pattern', '^i[0-9]{12,25}$');
          $testUnPublishingRoot.find('input[placeholder="publishedBy"]').val(testPublishedBy);
          $testUnPublishingRoot.find('input[placeholder="publishedBy"]').prop('required', true);
          $testUnPublishingRoot.find('input[placeholder="publishedBy"]').prop('maxlength', 100);
          $testUnPublishingRoot.find('input[placeholder="publishedBy"]').prop('pattern', '^.{1,100}$');
          $testUnPublishingRoot.find('input[placeholder="adpTestFormRevisionId"]').val(testAdpTestFormRevisionId);
          $testUnPublishingRoot.find('input[placeholder="adpTestFormRevisionId"]').prop('required', true);
          $testUnPublishingRoot.find('input[placeholder="adpTestFormRevisionId"]').prop('pattern', '^[1-4294967295]$');
          $testUnPublishingRoot.find('input[placeholder="requestId"]').val('');
          $testUnPublishingRoot.find('input[placeholder="requestId"]').prop('pattern', '^.{1,4096}$');
      }
      // Test Assignment Creation.
      else if (name === 'Assignment_Creation') {
          var $assignmentCreationRoot = $('article[data-group="' + group + '"][data-name="Assignment_Creation"][data-version="' + version + '"]');
          $assignmentCreationRoot.find('input[placeholder="username"]').val(assignmentUsername);
          $assignmentCreationRoot.find('input[placeholder="username"]').prop('required', true);
          $assignmentCreationRoot.find('input[placeholder="username"]').prop('maxlength', 20);
          $assignmentCreationRoot.find('input[placeholder="username"]').prop('pattern', '^[0-9a-zA-Z]{1,20}$');
          $assignmentCreationRoot.find('input[placeholder="password"]').val(assignmentPassword);
          $assignmentCreationRoot.find('input[placeholder="password"]').prop('required', true);
          $assignmentCreationRoot.find('input[placeholder="password"]').prop('maxlength', 32);
          $assignmentCreationRoot.find('input[placeholder="password"]').prop('pattern', '^[0-9a-z]{32}$');
          $assignmentCreationRoot.find('input[placeholder="secret"]').val(assignmentSecret);
          $assignmentCreationRoot.find('input[placeholder="secret"]').prop('required', true);
          $assignmentCreationRoot.find('input[placeholder="secret"]').prop('maxlength', 32);
          $assignmentCreationRoot.find('input[placeholder="secret"]').prop('pattern', '^[0-9a-z]{32}$');
          $assignmentCreationRoot.find('input[placeholder="Authentication"]').val(assignmentAuthentication);
          $assignmentCreationRoot.find('input[placeholder="Authentication"]').prop('readonly', true);
          $assignmentCreationRoot.find('input[placeholder="Authorization"]').val(assignmentAuthorization);
          $assignmentCreationRoot.find('input[placeholder="Authorization"]').prop('readonly', true);
          $assignmentCreationRoot.find('input[placeholder="testAssignmentId"]').val(assignmentTestAssignmentId);
          $assignmentCreationRoot.find('input[placeholder="testAssignmentId"]').prop('required', true);
          $assignmentCreationRoot.find('input[placeholder="testAssignmentId"]').prop('pattern', '^.{1,4096}$');
          $assignmentCreationRoot.find('input[placeholder="testId"]').val(assignmentTestId);
          $assignmentCreationRoot.find('input[placeholder="testId"]').prop('required', true);
          $assignmentCreationRoot.find('input[placeholder="testId"]').prop('pattern', '^.{1,4096}$');
          $assignmentCreationRoot.find('input[placeholder="testFormId"]').val(assignmentTestFormId);
          $assignmentCreationRoot.find('input[placeholder="testFormId"]').prop('required', true);
          $assignmentCreationRoot.find('input[placeholder="testFormId"]').prop('pattern', '^.{1,4096}$');
          $assignmentCreationRoot.find('input[placeholder="testKey"]').val(assignmentTestKey);
          $assignmentCreationRoot.find('input[placeholder="testKey"]').prop('required', true);
          $assignmentCreationRoot.find('input[placeholder="testKey"]').prop('pattern', '^(?!PRACT)[A-Z]{10}$');
          $assignmentCreationRoot.find('input[placeholder="studentId"]').val(assignmentStudentId);
          $assignmentCreationRoot.find('input[placeholder="studentId"]').prop('required', true);
          $assignmentCreationRoot.find('input[placeholder="studentId"]').prop('pattern', '^.{1,4096}$');
          $assignmentCreationRoot.find('input[placeholder="personalId"]').val(assignmentPersonalId);
          $assignmentCreationRoot.find('input[placeholder="personalId"]').prop('maxlength', 30);
          $assignmentCreationRoot.find('input[placeholder="personalId"]').prop('pattern', '^[0-9a-zA-Z]{0,30}$');
          $assignmentCreationRoot.find('input[placeholder="firstName"]').val(assignmentFirstName);
          $assignmentCreationRoot.find('input[placeholder="firstName"]').prop('maxlength', 35);
          $assignmentCreationRoot.find('input[placeholder="firstName"]').prop('pattern', '^.{1,35}$');
          $assignmentCreationRoot.find('input[placeholder="lastName"]').val(assignmentLastName);
          $assignmentCreationRoot.find('input[placeholder="lastName"]').prop('maxlength', 35);
          $assignmentCreationRoot.find('input[placeholder="lastName"]').prop('pattern', '^.{1,35}$');
          $assignmentCreationRoot.find('input[placeholder="dateOfBirth"]').val(assignmentDateOfBirth);
          $assignmentCreationRoot.find('input[placeholder="dateOfBirth"]').prop('maxlength', 10);
          $assignmentCreationRoot.find('input[placeholder="dateOfBirth"]').prop('pattern', '^(199\d|20\d\d)-(0\d|10|11|12)-(0\d|1\d|2\d|30|31)$');
          $assignmentCreationRoot.find('select[placeholder="state"]').val(assignmentState);
          $assignmentCreationRoot.find('input[placeholder="schoolName"]').val(assignmentSchoolName);
          $assignmentCreationRoot.find('input[placeholder="schoolName"]').prop('maxlength', 60);
          $assignmentCreationRoot.find('input[placeholder="schoolName"]').prop('pattern', '^.{1,60}$');
          $assignmentCreationRoot.find('select[placeholder="grade"]').val(assignmentGrade);
          $assignmentCreationRoot.find('select[placeholder="enableLineReader"]').val(assignmentEnableLineReader);
          $assignmentCreationRoot.find('select[placeholder="enableTextToSpeech"]').val(assignmentEnableTextToSpeech);
      }
      // Test Assignment Update.
      else if (name === 'Assignment_Update') {
          var $assignmentUpdateRoot = $('article[data-group="' + group + '"][data-name="Assignment_Update"][data-version="' + version + '"]');
          $assignmentUpdateRoot.find('input[placeholder="username"]').val(assignmentUsername);
          $assignmentUpdateRoot.find('input[placeholder="username"]').prop('required', true);
          $assignmentUpdateRoot.find('input[placeholder="username"]').prop('maxlength', 20);
          $assignmentUpdateRoot.find('input[placeholder="username"]').prop('pattern', '^[0-9a-zA-Z]{1,20}$');
          $assignmentUpdateRoot.find('input[placeholder="password"]').val(assignmentPassword);
          $assignmentUpdateRoot.find('input[placeholder="password"]').prop('required', true);
          $assignmentUpdateRoot.find('input[placeholder="password"]').prop('maxlength', 32);
          $assignmentUpdateRoot.find('input[placeholder="password"]').prop('pattern', '^[0-9a-z]{32}$');
          $assignmentUpdateRoot.find('input[placeholder="secret"]').val(assignmentSecret);
          $assignmentUpdateRoot.find('input[placeholder="secret"]').prop('required', true);
          $assignmentUpdateRoot.find('input[placeholder="secret"]').prop('maxlength', 32);
          $assignmentUpdateRoot.find('input[placeholder="secret"]').prop('pattern', '^[0-9a-z]{32}$');
          $assignmentUpdateRoot.find('input[placeholder="Authentication"]').val(assignmentAuthentication);
          $assignmentUpdateRoot.find('input[placeholder="Authentication"]').prop('readonly', true);
          $assignmentUpdateRoot.find('input[placeholder="Authorization"]').val(assignmentAuthorization);
          $assignmentUpdateRoot.find('input[placeholder="Authorization"]').prop('readonly', true);
          $assignmentUpdateRoot.find('input[placeholder="testAssignmentId"]').val(assignmentTestAssignmentId);
          $assignmentUpdateRoot.find('input[placeholder="testAssignmentId"]').prop('required', true);
          $assignmentUpdateRoot.find('input[placeholder="testAssignmentId"]').prop('pattern', '^.{1,4096}$');
          $assignmentUpdateRoot.find('input[placeholder="adpTestAssignmentId"]').val(assignmentAdpTestAssignmentId);
          $assignmentUpdateRoot.find('input[placeholder="adpTestAssignmentId"]').prop('required', true);
          $assignmentUpdateRoot.find('input[placeholder="adpTestAssignmentId"]').prop('pattern', '^.{1,4096}$');
          $assignmentUpdateRoot.find('select[placeholder="testStatus"]').val(assignmentTestStatus);
          $assignmentUpdateRoot.find('input[placeholder="testId"]').val(assignmentTestId);
          $assignmentUpdateRoot.find('input[placeholder="testId"]').prop('pattern', '^.{1,4096}$');
          $assignmentUpdateRoot.find('input[placeholder="testFormId"]').val(assignmentTestFormId);
          $assignmentUpdateRoot.find('input[placeholder="testFormId"]').prop('pattern', '^.{1,4096}$');
          $assignmentUpdateRoot.find('input[placeholder="testKey"]').val(assignmentTestKey);
          $assignmentUpdateRoot.find('input[placeholder="testKey"]').prop('pattern', '^(?!PRACT)[A-Z]{10}$');
          $assignmentUpdateRoot.find('input[placeholder="studentId"]').val(assignmentStudentId);
          $assignmentUpdateRoot.find('input[placeholder="studentId"]').prop('required', true);
          $assignmentUpdateRoot.find('input[placeholder="studentId"]').prop('pattern', '^.{1,4096}$');
          $assignmentUpdateRoot.find('input[placeholder="personalId"]').val(assignmentPersonalId);
          $assignmentUpdateRoot.find('input[placeholder="personalId"]').prop('maxlength', 30);
          $assignmentUpdateRoot.find('input[placeholder="personalId"]').prop('pattern', '^[0-9a-zA-Z]{0,30}$');
          $assignmentUpdateRoot.find('input[placeholder="firstName"]').val(assignmentFirstName);
          $assignmentUpdateRoot.find('input[placeholder="firstName"]').prop('maxlength', 35);
          $assignmentUpdateRoot.find('input[placeholder="firstName"]').prop('pattern', '^.{1,35}$');
          $assignmentUpdateRoot.find('input[placeholder="lastName"]').val(assignmentLastName);
          $assignmentUpdateRoot.find('input[placeholder="lastName"]').prop('maxlength', 35);
          $assignmentUpdateRoot.find('input[placeholder="lastName"]').prop('pattern', '^.{1,35}$');
          $assignmentUpdateRoot.find('input[placeholder="dateOfBirth"]').val(assignmentDateOfBirth);
          $assignmentUpdateRoot.find('input[placeholder="dateOfBirth"]').prop('maxlength', 10);
          $assignmentUpdateRoot.find('input[placeholder="dateOfBirth"]').prop('pattern', '^(199\d|20\d\d)-(0\d|10|11|12)-(0\d|1\d|2\d|30|31)$');
          $assignmentUpdateRoot.find('select[placeholder="state"]').val(assignmentState);
          $assignmentUpdateRoot.find('input[placeholder="schoolName"]').val(assignmentSchoolName);
          $assignmentUpdateRoot.find('input[placeholder="schoolName"]').prop('maxlength', 60);
          $assignmentUpdateRoot.find('input[placeholder="schoolName"]').prop('pattern', '^.{1,60}$');
          $assignmentUpdateRoot.find('select[placeholder="grade"]').val(assignmentGrade);
          $assignmentUpdateRoot.find('select[placeholder="enableLineReader"]').val(assignmentEnableLineReader);
          $assignmentUpdateRoot.find('select[placeholder="enableTextToSpeech"]').val(assignmentEnableTextToSpeech);
      }
      // Test Assignment Deletion.
      else if (name === 'Assignment_Deletion') {
          var $assignmentDeletionRoot = $('article[data-group="' + group + '"][data-name="Assignment_Deletion"][data-version="' + version + '"]');
          $assignmentDeletionRoot.find('input[placeholder="username"]').val(assignmentUsername);
          $assignmentDeletionRoot.find('input[placeholder="username"]').prop('required', true);
          $assignmentDeletionRoot.find('input[placeholder="username"]').prop('maxlength', 20);
          $assignmentDeletionRoot.find('input[placeholder="username"]').prop('pattern', '^[0-9a-zA-Z]{1,20}$');
          $assignmentDeletionRoot.find('input[placeholder="password"]').val(assignmentPassword);
          $assignmentDeletionRoot.find('input[placeholder="password"]').prop('required', true);
          $assignmentDeletionRoot.find('input[placeholder="password"]').prop('maxlength', 32);
          $assignmentDeletionRoot.find('input[placeholder="password"]').prop('pattern', '^[0-9a-f]{32}$');
          $assignmentDeletionRoot.find('input[placeholder="secret"]').val(assignmentSecret);
          $assignmentDeletionRoot.find('input[placeholder="secret"]').prop('required', true);
          $assignmentDeletionRoot.find('input[placeholder="secret"]').prop('maxlength', 32);
          $assignmentDeletionRoot.find('input[placeholder="secret"]').prop('pattern', '^[0-9a-f]{32}$');
          $assignmentDeletionRoot.find('input[placeholder="Authentication"]').val(assignmentAuthentication);
          $assignmentDeletionRoot.find('input[placeholder="Authentication"]').prop('readonly', true);
          $assignmentDeletionRoot.find('input[placeholder="Authorization"]').val(assignmentAuthorization);
          $assignmentDeletionRoot.find('input[placeholder="Authorization"]').prop('readonly', true);
          $assignmentDeletionRoot.find('input[placeholder="testAssignmentId"]').val(assignmentTestAssignmentId);
          $assignmentDeletionRoot.find('input[placeholder="testAssignmentId"]').prop('required', true);
          $assignmentDeletionRoot.find('input[placeholder="testAssignmentId"]').prop('pattern', '^.{1,4096}$');
          $assignmentDeletionRoot.find('input[placeholder="adpTestAssignmentId"]').val(assignmentAdpTestAssignmentId);
          $assignmentDeletionRoot.find('input[placeholder="adpTestAssignmentId"]').prop('required', true);
          $assignmentDeletionRoot.find('input[placeholder="adpTestAssignmentId"]').prop('pattern', '^.{1,4096}$');
          $assignmentDeletionRoot.find('input[placeholder="testId"]').val(assignmentTestId);
          $assignmentDeletionRoot.find('input[placeholder="testId"]').prop('required', true);
          $assignmentDeletionRoot.find('input[placeholder="testId"]').prop('pattern', '^.{1,4096}$');
          $assignmentDeletionRoot.find('input[placeholder="testFormId"]').val(assignmentTestFormId);
          $assignmentDeletionRoot.find('input[placeholder="testFormId"]').prop('required', true);
          $assignmentDeletionRoot.find('input[placeholder="testFormId"]').prop('pattern', '^.{1,4096}$');
          $assignmentDeletionRoot.find('input[placeholder="testKey"]').val(assignmentTestKey);
          $assignmentDeletionRoot.find('input[placeholder="testKey"]').prop('required', true);
          $assignmentDeletionRoot.find('input[placeholder="testKey"]').prop('pattern', '^(?!PRACT)[A-Z]{10}$');
          $assignmentDeletionRoot.find('input[placeholder="studentId"]').val(assignmentStudentId);
          $assignmentDeletionRoot.find('input[placeholder="studentId"]').prop('required', true);
          $assignmentDeletionRoot.find('input[placeholder="studentId"]').prop('pattern', '^.{1,4096}$');
      }
      // Results Details.
      else if (name === 'Results_Details') {
          var $resultsDetailsRoot = $('article[data-group="' + group + '"][data-name="Results_Details"][data-version="' + version + '"]');
          $resultsDetailsRoot.find('input[placeholder="username"]').val(resultsUsername);
          $resultsDetailsRoot.find('input[placeholder="username"]').prop('required', true);
          $resultsDetailsRoot.find('input[placeholder="username"]').prop('maxlength', 20);
          $resultsDetailsRoot.find('input[placeholder="username"]').prop('pattern', '^[0-9a-zA-Z]{1,20}$');
          $resultsDetailsRoot.find('input[placeholder="password"]').val(resultsPassword);
          $resultsDetailsRoot.find('input[placeholder="password"]').prop('required', true);
          $resultsDetailsRoot.find('input[placeholder="password"]').prop('maxlength', 32);
          $resultsDetailsRoot.find('input[placeholder="password"]').prop('pattern', '^[0-9a-z]{32}$');
          $resultsDetailsRoot.find('input[placeholder="secret"]').val(resultsSecret);
          $resultsDetailsRoot.find('input[placeholder="secret"]').prop('required', true);
          $resultsDetailsRoot.find('input[placeholder="secret"]').prop('maxlength', 32);
          $resultsDetailsRoot.find('input[placeholder="secret"]').prop('pattern', '^[0-9a-f]{32}$');
          $resultsDetailsRoot.find('input[placeholder="Authentication"]').val(resultsAuthentication);
          $resultsDetailsRoot.find('input[placeholder="Authentication"]').prop('readonly', true);
          $resultsDetailsRoot.find('input[placeholder="Authorization"]').val(resultsAuthorization);
          $resultsDetailsRoot.find('input[placeholder="Authorization"]').prop('readonly', true);
          $resultsDetailsRoot.find('input[placeholder="adpTestAssignmentId"]').val(resultsAdpTestAssignmentId);
          $resultsDetailsRoot.find('input[placeholder="adpTestAssignmentId"]').prop('required', true);
          $resultsDetailsRoot.find('input[placeholder="adpTestAssignmentId"]').prop('pattern', '^.{1,4096}$');
      }
  }

  function refreshScrollSpy()
  {
      $('[data-spy="scroll"]').each(function () {
          $(this).scrollspy("refresh");
      });
  }

  function escapeHtml(str) {
      var div = document.createElement("div");
      div.appendChild(document.createTextNode(str));
      return div.innerHTML;
  }

  /**
   * Exports.
   */
  return {
      initDynamic: initDynamic
  };

  /**
   * BV: Validate file type based on its extension.
   */
  function isFileType(url, fileExtension) {
    if (url.indexOf(fileExtension) !== -1 && url.length >= fileExtension.length) {
        if (url.substring(url.length - fileExtension.length) === fileExtension) {
            return true;
        }
    }
    return false;
  }

});
