define({ "api": [
  {
    "group": "ADP_Common",
    "type": "",
    "url": "/",
    "title": "Authentication",
    "name": "Authentication",
    "version": "1.9.3",
    "description": "<p>Client: <b>ACR</b>, <b>LDR</b>, <b>PRC</b> and <b>Test Driver</b><br/><br/> All ADP web service use Core Authentication and the only exception is Content web service. Some web services have additional steps for authentication and require additional details (i.e. Login and Results).<br/> Core ADP Authentication consists of: <ol> <li>Basic Authentication</li> <li>HMAC Authentication</li> </ol><br/> <b>Basic Authentication</b>:<br/><br/> Basic Authentication is a simple authentication based on username and password sent in &quot;Authorization&quot; request header. All passwords are 32-characters long lowercase alphanumeric strings generated as random MD5 hashes. This can be further strengthened in the future to increase security, if necessary. The value in the header is string &quot;Basic &quot; followed by Base64 encoded digest (concatenated username and password separated by single colon): <pre class=\"pseudo-code\">&quot;Authorization&quot;: &quot;Basic &quot; + Base64Encode(Username + &quot;:&quot; + Password)</pre> Example: <pre>Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==</pre><br/> <b>HMAC Authentication</b>:<br/><br/> HMAC Authentication is more complex authentication based Keyed Hash Message Authentication Code that uses Secret as salt and is signed with a timestamp, so once generated it is valid only for a short period of time.<br/><br/> It requires calculation of Message Authentication Code (MAC) which involves cryptographic hash function with combination with a secret cryptographic key. As with any MAC, it is used to simultaneously verify both the data integrity and the authentication of a message. SHA-256 cryptographic hash function is used in the calculation of the HMAC. The cryptographic strength of the HMAC depends upon the cryptographic strength of the underlying hash function, the size of its hash output, and on the size and quality of the key, so if necessary, the security can be further increased in the future by replacing SHA-256 with a stronger cryptographic hash function and/or by increasing the complexity of the HMAC algorithm which generates the input digest.<br/><br/> An iterative hash function breaks up a message into blocks of a fixed size and iterates over them with a compression function. SHA-256 cryptographic hash function operates on 512-bit blocks, and the size of the output (HMAC Hash) is the same and is fixed to 256 bits which means it produces a 64-characters long lowercase alphanumeric string. This string can further be truncated at certain position in order to increase complexity of HMAC Authentication algorithm in the future (if necessary), so there is a lot of space for further increasing security levels in the future for adding support for High-Stakes Testing, if needed.<br/><br/> The current implementation of HMAC relies on SHA-256 hashing using Secret as salt. Secret is 32-characters long lowercase alphanumeric string generated as random MD5 hash which should be stored on the client side in a secure place. As Test Driver can run in a regular browser and its Secret cannot be stored in a highly secured manner, it is the only exception, so it has to provide additional authentication details for the extended authentication algorithm which include one or more of the following depending on the web service: Token, Test Key, Personal ID, Date of Birth and/or State. This only applies to Login, Content and Results web services which are further explained within their sections.<br/> <br/> In addition, HMAC Authentication uses Unix timestamp to sign the request, so it's valid only for a short period of time.<br/> As all timestamps in ADS must be in UTC timezone, timestamp is a 13-digit Unix timestamp in a UTC timezone (frequently called micro-time). In addition, HMAC Authentication uses Nonce which is a random integer in the range [0, 99999):<br/> <pre class=\"pseudo-code\"> Nonce = Floor(Random() * 99999)<br/> Message = apiURI + Round(Microtime('UTC') / 1000) + Nonce<br/> Hash = SHA-256-HMAC(Secret, Message)<br/><br/> &quot;Authentication&quot;: Base64Encode(Microtime('UTC')+ &quot;:&quot; + Nonce + &quot;:&quot; + Base64Encode(Hash)) </pre> Example: <pre>Authentication: MTQzMTg3ODczMzYzOTo3MjQyNjpPRGN3TXpoak16Um1ZMlJoWXpoak1ESTFaR1JpWXpneVlqSmxNbVl6 TkRabFlUWmlZMkZqWTJFM01HRTBaR0U1TVdZNE0ySTNPVEZsWWpreVl6Sm1NUT09</pre> JavaScript Code for Authentication: <pre class=\"pseudo-code\"> // Time Offset represent time difference between server and client in seconds. It is important that all timestamps coming from the<br/> // client side are synced with the server in UTC time zone as client can be in any (possibly wrong) timezone, and client clock can<br/> // be off or can be tampered with. Therefore, anything related to time and date must be relative to a timestamp received form the<br/> // server which is always synchronized with Atomic Clock Server using NTP protocol.<br/> // Server time is always provided as 10-digit Unit timestamp in UTC timezone.<br/> var timeOffset = 1000 * serverTime - new Date().getTime();<br/> <br/> var apiHost = &quot;https://adp.parcc.dev&quot;;<br/> var apiURI = &quot;/api/{api_route}&quot;;<br/> var username = &quot;DocumentationTestDriver&quot;;<br/> var password = &quot;53c5de17b5488f1a8549e6db1786ca81&quot;;<br/> var secret = &quot;578cca1aa71f702c26cc81439392124c&quot;;<br/> <br/> var timestamp = new Date().getTime() + timeOffset; // 13-digit Unix timestamp in UTC timezone.<br/> var nonce = Math.floor(Math.random() * 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> var message = apiURI + Math.round(timestamp / 1000) + nonce; // Message uses rounded 10-digit Unix timestamp.<br/> var hash = $.sha256hmac(secret, message);<br/> var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(hash));<br/> <br/> $.ajax({<br/>    url: apiHost + apiURI,<br/>    type: requestMethod,<br/>    headers: {<br/>      'Content-Type': &quot;application/json; charset=UTF-8&quot;,<br/>      'Authorization': &quot;Basic &quot; + btoa(username + &quot;:&quot; + password),<br/>      'Authentication': authentication,<br/>      ...<br/>    },<br/>    data: data,<br/>    ...<br/> }) </pre> PHP Code for Authentication: <pre class=\"pseudo-code\"> date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server uses NTP service.<br/> <br/> $apiHost = &quot;https://adp.parcc.dev&quot;;<br/> $apiURI = &quot;/api/{api_route}&quot;;<br/> $username = &quot;DocumentationTestDriver&quot;;<br/> $password = &quot;53c5de17b5488f1a8549e6db1786ca81&quot;;<br/> $secret = &quot;578cca1aa71f702c26cc81439392124c&quot;;<br/> <br/> $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/> $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix timestamp.<br/> $hash = hash_hmac('sha256', $message, $secret);<br/> $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/> <br/> $ch = curl_init($apiHost . $apiURI);<br/> curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>    &quot;Content-Type&quot;: &quot;application/json; charset=UTF-8&quot;,<br/>    &quot;Authorization: Basic &quot; . base64_encode($username . &quot;:&quot; . $password),<br/>    &quot;Authentication:&quot; . $authentication,<br/>    ...<br/> ]);<br/> ...<br/> $result = curl_exec($ch);<br/> curl_close($ch);<br/> ... </pre></p>",
    "filename": "src/authentication.php",
    "groupTitle": "ADP_Common",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/"
      }
    ]
  },
  {
    "group": "ADP_Common",
    "type": "GET",
    "url": "/api/admin/cache/clear",
    "title": "Clear Cache",
    "name": "Clear_Cache",
    "permission": [
      {
        "name": "IT_Dev",
        "title": "User Authentication:",
        "description": "<p>Not needed.</p>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>IT</b> - Clear Cache<br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Clear Cache web service is exclusively used by IT and Developers for Flushing Application Cache. It doesn't use any authentication, but can only be successfully executed from the localhost for security reasons.<br/> In case of successful request, it returns a confirmation that Application Cache was cleared successfully.<br/><br/> These are possible Clear Cache specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-0001</td> <td>Access Denied.</td> <td>401</td> <td>Unauthorized</td> <td>Returns 401 JSON error if external request was made to administer Application (was not initiated from localhost).</td> </tr> <tr> <td>ADP-0005</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to clear Application Cache was not completed successfully (keys are found in cache after cache was flushed).</td> </tr> </table><br/></p>",
    "header": {
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nGET /api/admin/cache/clear HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nReferer: https://adp.parcc.dev/\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "HTML",
            "optional": false,
            "field": "N/A",
            "description": "<p>It returns a simple confirmation if Application Cache was successfully flushed.<br/> Anything else is considered as an error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\nApplication Cache was cleared successfully.",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"statusCode\": 500,\n  \"statusDescription\": \"Internal Server Error\",\n  \"errorCode\": \"ADP-0001\",\n  \"errorDescription\": \"Internal Error.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 500,\n    \"statusDescription\": \"Internal Server Error\",\n    \"errorCode\": \"ADP-0001\",\n    \"errorDescription\": \"Internal Error.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/admin.php",
    "groupTitle": "ADP_Common",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/admin/cache/clear"
      }
    ]
  },
  {
    "group": "ADP_Common",
    "type": "",
    "url": "/",
    "title": "Generic Errors",
    "name": "Errors",
    "version": "1.22.3",
    "description": "<p>Client: <b>ACR</b>, <b>LDR</b>, <b>PRC</b> and <b>Test Driver</b><br/><br/> These are generic errors returned by ADP API which may be caused by one of the following: <ol> <li>Requested API route does not exist or a requested path is not correct (i.e. the order of URI segments is not valid).</li> <li>Request is invalid (i.e. doesn't have all expected parameters).</li> <li>Request details are invalid (i.e. invalid JSON request).</li> <li>Request details do not match data found in database (i.e. invalid username and/or password).</li> <li>Request is outdated (i.e. timestamp signature of the request is too old).</li> <li>Request is rejected because of missing precondition (i.e. missing, invalid or expired token).</li> <li>An internal error/exception triggered by a request (i.e. unable to connect to database).</li> <li>Something else.</li> </ol> These are possible common ADP errors:<br/><br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1000</td> <td>The system is currently undergoing maintenance. We are sorry for the inconvenience.<br/> Please try again later.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns 503 HTML page, but without actual Error (the actual Error is only logged into ADP application log file) when trying to launch access Test Driver if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1001</td> <td>System is currently undergoing maintenance.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns JSON error for any POST, PUT, PATCH and DELETE requests if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1002</td> <td>System is currently undergoing maintenance.</td> <td>200</td> <td>OK</td> <td>Returns error in response header without response body for any pre-flight (OPTIONS) request if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1003</td> <td>System is currently undergoing maintenance.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns error in response header without response body when Content is requested if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1004</td> <td>System is currently undergoing maintenance.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns JSON error when Results are tried to be downloaded if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1005</td> <td>The system is currently undergoing maintenance. We are sorry for the inconvenience.<br/> Please try again later.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns 503 HTML page, but without actual Error (the actual Error is only logged into ADP application log file) for any other GET request if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1006</td> <td>System is currently undergoing maintenance.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns error in response header without response body in any other case if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1007</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Basic Authentication failed because of missing username.</td> </tr> <tr> <td>ADP-1008</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Basic Authentication failed because provided username doesn't exist.</td> </tr> <tr> <td>ADP-1009</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Basic Authentication failed because user is deactivated.</td> </tr> <tr> <td>ADP-1010</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case cached User is not a valid JSON Object.</td> </tr> <tr> <td>ADP-1011</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Basic Authentication failed because of missing password, or its format is invalid (i.e. invalid length).</td> </tr> <tr> <td>ADP-1012</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Basic Authentication failed because password doesn't match with the one found in database.</td> </tr> <tr> <td>ADP-1013</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case HMAC Authentication is missing.</td> </tr> <tr> <td>ADP-1014</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case HMAC Authentication failed because of mismatched number of keys.</td> </tr> <tr> <td>ADP-1015</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case HMAC Authentication failed because of missing or invalid micro-time key.</td> </tr> <tr> <td>ADP-1016</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case HMAC Authentication failed because of missing or invalid nonce.</td> </tr> <tr> <td>ADP-1017</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case HMAC Authentication failed because of missing or invalid Base64 encoded HMAC Hash.</td> </tr> <tr> <td>ADP-1018</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case HMAC Authentication failed because of mismatched/invalid HMAC Hash (didn't match hashing algorithm).</td> </tr> <tr> <td>ADP-1019</td> <td>Authorization Failed.</td> <td>408</td> <td>Request Timeout</td> <td>Returns JSON error in case HMAC Authentication failed because of outdated request (time based request signature is too old).</td> </tr> <tr> <td>ADP-1060</td> <td>System is currently undergoing maintenance.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns JSON error for any Not Found POST, PUT, PATCH and DELETE requests if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1061</td> <td>System is currently undergoing maintenance.</td> <td>200</td> <td>OK</td> <td>Returns error in response header without response body for any Not Found pre-flight (OPTIONS) request if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1062</td> <td>System is currently undergoing maintenance.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns error in response header without response body for any Not Found Content request if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1063</td> <td>System is currently undergoing maintenance.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns JSON error for Not Found Results download or upload if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1064</td> <td>The system is currently undergoing maintenance. We are sorry for the inconvenience.<br/> Please try again later.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns 503 HTML page, but without actual Error (the actual Error is only logged into ADP application log file) for any other Not Found GET request if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1065</td> <td>System is currently undergoing maintenance.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns error in response header without response body for any other Not Found request if Maintenance Mode is enabled.</td> </tr> <tr> <td>ADP-1066</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case a resource (either some data or a file) for any POST, PUT, PATCH or DELETE request was not found.</td> </tr> <tr> <td>ADP-1067</td> <td>Data Not Found.</td> <td>200</td> <td>OK</td> <td>Returns error in response header without response body in case a resource (data) for OPTIONS request was not found.</td> </tr> <tr> <td>ADP-1068</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns only response header without body in case requested content resource was not found.</td> </tr> <tr> <td>ADP-1069</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case results upload or download for a Test Session was not found.</td> </tr> <tr> <td>ADP-1070</td> <td>Sorry, requested page was not found.</td> <td>404</td> <td>Not Found</td> <td>Returns 404 HTML page, but without actual Error (the actual Error is only logged into ADP application log file) in case any other GET request was not found.</td> </tr> <tr> <td>ADP-1071</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns only response header without body in case any other resource (either data or a file) was not found.</td> </tr> <tr> <td>ADP-1080</td> <td>Internal Error.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns JSON error in case a Database Connection failed to be opened.</td> </tr> <tr> <td>ADP-1081</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case execution of SQL Query triggered a Database error.</td> </tr> <tr> <td>ADP-1090</td> <td>Sorry, requested data was not found.</td> <td>404</td> <td>Not Found</td> <td>Returns error in case no matching Tenant was found in Application Configuration.</td> </tr> <tr> <td>ADP-1091</td> <td>Sorry, requested data was not found.</td> <td>404</td> <td>Not Found</td> <td>Returns error in case no matching Tenant was found in Database.</td> </tr> <tr> <td>ADP-1092</td> <td>You are not authorized for access. We are sorry for the inconvenience.</td> <td>403</td> <td>Forbidden</td> <td>Returns error in case Tenant is Deactivated.</td> </tr> <tr> <td>ADP-1093</td> <td>Server encountered Internal Error. We are sorry for the inconvenience.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns error in case  cached Tenant is not a valid JSON Object.</td> </tr> <tr> <td>ADP-1100</td> <td>You are not authorized to access this page.</td> <td>403</td> <td>Forbidden</td> <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP application log file) in case HTML response is expected and ADP is not configured for Test Delivery and/or Test Publishing when related web service was called.</td> </tr> <tr> <td>ADP-1101</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns appropriate error in case non-HTML response is expected and ADP is not configured for Test Delivery and/or Test Publishing when related web service was called.</td> </tr> <tr> <td>ADP-1102</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns appropriate error in case System Configuration was not found (i.e. Login Configuration or Test Driver Configuration).</td> </tr> <tr> <td>ADP-1103</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns appropriate error in case System Configuration is not valid JSON object (i.e. Login Configuration or Test Driver Configuration).</td> </tr> <tr> <td>ADP-1104</td> <td>You are not authorized to access this page.</td> <td>403</td> <td>Forbidden</td> <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP application log file) in case HTML response is expected and User calling particular web service doesn't have appropriate User Role (Type) which grants permissions to access that web service.</td> </tr> <tr> <td>ADP-1105</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns appropriate error in case User calling particular web service doesn't have appropriate User Role (Type) which grants permissions to access that web service.</td> </tr> <tr> <td>ADP-1106</td> <td>Invalid Request.</td> <td>400</td> <td>Bad Request</td> <td>Returns appropriate error in case Request has missing or invalid JSON structure.</td> </tr> <tr> <td>ADP-1107</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error during Login or Results upload/download in case Test Key in the request header is not in expected format.</td> </tr> <tr> <td>ADP-1108</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP application log file) in case HTML response is expected during launch of Practice Test when referenced Test Key in the launch URL was not found in database.</td> </tr> <tr> <td>ADP-1109</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns appropriate error during Login or Results upload/download in case Test Key in the request header was not found in database.</td> </tr> <tr> <td>ADP-1110</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP application log file) in case HTML response is expected during launch of Practice Test when referenced Test Assignment is Deactivated.</td> </tr> <tr> <td>ADP-1111</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns appropriate error during Login or Results upload/download in case Test Assignment is Deactivated.</td> </tr> <tr> <td>ADP-1112</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case if invalid Response was generated by application.</td> </tr> <tr> <td>TD-1000</td> <td>There was a problem starting your test.</td> <td>N/A</td> <td>N/A</td> <td>This is Test Driver unique Error (not coming from ADP API) being displayed when Test Driver is not able to communicate to the Server or is not able to download required Content from AWS (most probably either due to Configuration or Internet/Connectivity issue).</td> </tr> <tr> <td>TD-1001</td> <td>There was a problem starting your test.</td> <td>N/A</td> <td>N/A</td> <td>This is Test Driver unique Error (not coming from ADP API) being displayed when Test Driver is trying to start or resume a test, but has not recived a confirmation that the test is in progress.</td> </tr> </table></p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset that has the statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          },
          {
            "group": "Error 4xx",
            "type": "HTML",
            "optional": false,
            "field": "N/A",
            "description": "<p>Simple 404 &quot;Page Not Found&quot;, 403 &quot;Access Denied&quot; or 503 &quot;Service Unavailable&quot; HTML page.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response without Envelope:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"statusCode\": 404,\n  \"statusDescription\": \"Not Found\",\n  \"errorCode\": \"ADP-1026\",\n  \"errorDescription\": \"Requested Resource Not Found.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 404,\n    \"statusDescription\": \"Not Found\",\n    \"errorCode\": \"ADP-1026\",\n    \"errorDescription\": \"Requested Resource Not Found.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/errors.php",
    "groupTitle": "ADP_Common",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/"
      }
    ]
  },
  {
    "group": "ADP_Common",
    "type": "",
    "url": "/",
    "title": "General",
    "name": "General",
    "version": "1.9.1",
    "description": "<p>Client: <b>ACR</b>, <b>LDR</b>, <b>PRC</b> and <b>Test Driver</b><br/><br/> ADP consists of multiple components: <ol> <li> <b>ADP Test Delivery</b> – responsible to provide support to Test Driver which at the high level includes: <ol> <li>Authentication</li> <li>Content Delivery</li> <li>Results Upload and Download</li> <li>Basic Proctoring Functionality</li> </ol> </li> <li> <b>ADP Publisher</b> – responsible to provide support for synchronizing data across ADS system which at the high level includes: <ol> <li>Test Publishing</li> <li>Test Assignments with Student Details</li> <li>Results Export</li> </ol> </li> <li><b>ADP Quiz</b> – responsible to provide support for PRC and synchronize data between PRC and ADS systems (TBD).</li> </ol> ADP provides APIs for RESTful web services and by default all web services require Authentication. More details about Authentication can be found under Authentication section.<br/><br/> Most web services are stateless and can be called directly without prior call to any other web service. The only exception are Test Delivery web services which because of lower security on the Test Driver side require that Login web service is called first so ADP can generate a token that will be used throughout test delivery session and is required for all other test delivery web services (Content and Results).<br/><br/> In general, ADP supports the following request methods: GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD.<br/> In addition, each web service by default supports OPTIONS request method in order to provide support for pre-flight requests triggered by browsers (clients in general) due to CORS (Cross-Origin Resource Sharing) security policy.<br/><br/> Depending on the request type (GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD) and web service handling particular request, ADP can return a response in one of the following formats:<br/> <ul> <li>JSON formatted response (this is the default response type). JSON responses also have optional envelope which provides additional details, but it can be turned off.</li> <li>HTML Page (simple &quot;404: Page Not Found&quot; or &quot;403: Access Denied&quot;).</li> <li>Header response which only returns response header, whereas response body is omitted.</li> <li>302 Redirect (Resource Moved Temporary) which is only used for Content web service.</li> </ul> By default, ADP API also returns HTTP Status and HTTP Status Code in the response header for all errors, but this is optional and can be turned off.</p>",
    "filename": "src/general.php",
    "groupTitle": "ADP_Common",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/"
      }
    ]
  },
  {
    "group": "ADP_Common",
    "type": "GET",
    "url": "/api/admin/install",
    "title": "Install",
    "name": "Install",
    "permission": [
      {
        "name": "IT_Dev",
        "title": "User Authentication:",
        "description": "<p>Not needed.</p>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>IT</b>/<b>Developers</b> - Application Installation<br/> Status: <b style=\"color:#c00\">In Development</b><br/><br/> Application Installation web service is exclusively used by IT and Developers for Flushing Application Cache. It doesn't use any authentication, but can only be successfully executed from the localhost for security reasons.<br/> In case of successful request, it returns a confirmation that Application was installed successfully.<br/><br/> These are possible Application Installation specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-0001</td> <td>Access Denied.</td> <td>401</td> <td>Unauthorized</td> <td>Returns 401 JSON error if external request was made to administer Application (was not initiated from localhost).</td> </tr> </table><br/></p>",
    "header": {
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nGET /api/admin/install HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nReferer: https://adp.parcc.dev/\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "HTML",
            "optional": false,
            "field": "N/A",
            "description": "<p>It returns a simple confirmation if Application was successfully installed.<br/> Anything else is considered as an error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\nApplication was successfully installed.",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"statusCode\": 500,\n  \"statusDescription\": \"Internal Server Error\",\n  \"errorCode\": \"ADP-0001\",\n  \"errorDescription\": \"Internal Error.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 500,\n    \"statusDescription\": \"Internal Server Error\",\n    \"errorCode\": \"ADP-0001\",\n    \"errorDescription\": \"Internal Error.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/admin.php",
    "groupTitle": "ADP_Common",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/admin/install"
      }
    ]
  },
  {
    "group": "ADP_Common",
    "type": "GET",
    "url": "/api/admin/maintenance/disable",
    "title": "Maintenance Disable",
    "name": "Maintenance_Disable",
    "permission": [
      {
        "name": "IT_Dev",
        "title": "User Authentication:",
        "description": "<p>Not needed.</p>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>IT</b> - Maintenance Disable<br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Maintenance Disable web service is exclusively used by IT and Developers for Disabling Maintenance Mode. It doesn't use any authentication, but can only be successfully executed from the localhost for security reasons.<br/> In case of successful request, it returns a confirmation with outcome of Disabling Maintenance Mode.<br/><br/> These are possible Maintenance Disable specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-0001</td> <td>Access Denied.</td> <td>401</td> <td>Unauthorized</td> <td>Returns 401 JSON error if external request was made to administer Application (was not initiated from localhost).</td> </tr> <tr> <td>ADP-0002</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to update Application Configuration Property failed because given Application Configuration Property format is not supported (i.e. object or array is provided).</td> </tr> <tr> <td>ADP-0003</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to update Application Configuration Property failed because Application Configuration File cannot be loaded.</td> </tr> <tr> <td>ADP-0004</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to update Application Configuration Property failed because Application Configuration File cannot be saved (i.e. due to invalid file permissions).</td> </tr> <tr> <td>ADP-0005</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to clear Application Cache was not completed successfully (keys are found in cache after cache was flushed).</td> </tr> <tr> <td>ADP-0006</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to Disable Maintenance Mode failed to update Configuration file.</td> </tr> <tr> <td>ADP-0007</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to Disable Maintenance Mode failed unexpectedly (Configuration file was updated, but Maintenance Mode was still not disabled).</td> </tr> </table><br/></p>",
    "header": {
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nGET /api/admin/maintenance/disable HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nReferer: https://adp.parcc.dev/\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "HTML",
            "optional": false,
            "field": "N/A",
            "description": "<p>It returns a simple confirmation with current Status for Maintenance Mode.<br/> Anything else is considered as an error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\nMaintenance Mode is Disabled.",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"statusCode\": 500,\n  \"statusDescription\": \"Internal Server Error\",\n  \"errorCode\": \"ADP-0001\",\n  \"errorDescription\": \"Internal Error.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 500,\n    \"statusDescription\": \"Internal Server Error\",\n    \"errorCode\": \"ADP-0001\",\n    \"errorDescription\": \"Internal Error.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/admin.php",
    "groupTitle": "ADP_Common",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/admin/maintenance/disable"
      }
    ]
  },
  {
    "group": "ADP_Common",
    "type": "GET",
    "url": "/api/admin/maintenance/enable",
    "title": "Maintenance Enable",
    "name": "Maintenance_Enable",
    "permission": [
      {
        "name": "IT_Dev",
        "title": "User Authentication:",
        "description": "<p>Not needed.</p>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>IT</b> - Maintenance Enable<br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Maintenance Enable web service is exclusively used by IT and Developers for Enabling Maintenance Mode. It doesn't use any authentication, but can only be successfully executed from the localhost for security reasons.<br/> In case of successful request, it returns a confirmation with outcome of Enabling Maintenance Mode.<br/><br/> These are possible Maintenance Enable specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-0001</td> <td>Access Denied.</td> <td>401</td> <td>Unauthorized</td> <td>Returns 401 JSON error if external request was made to administer Application (was not initiated from localhost).</td> </tr> <tr> <td>ADP-0002</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to update Application Configuration Property failed because given Application Configuration Property format is not supported (i.e. object or array is provided).</td> </tr> <tr> <td>ADP-0003</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to update Application Configuration Property failed because Application Configuration File cannot be loaded.</td> </tr> <tr> <td>ADP-0004</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to update Application Configuration Property failed because Application Configuration File cannot be saved (i.e. due to invalid file permissions).</td> </tr> <tr> <td>ADP-0005</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to clear Application Cache was not completed successfully (keys are found in cache after cache was flushed).</td> </tr> <tr> <td>ADP-0006</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to Enable Maintenance Mode failed to update Configuration file.</td> </tr> <tr> <td>ADP-0007</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 JSON error if request to Enable Maintenance Mode failed unexpectedly (Configuration file was updated, but Maintenance Mode was still not enabled).</td> </tr> </table><br/></p>",
    "header": {
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nGET /api/admin/maintenance/enable HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nReferer: https://adp.parcc.dev/\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "HTML",
            "optional": false,
            "field": "N/A",
            "description": "<p>It returns a simple confirmation with current Status for Maintenance Mode.<br/> Anything else is considered as an error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\nMaintenance Mode is Enabled.",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"statusCode\": 500,\n  \"statusDescription\": \"Internal Server Error\",\n  \"errorCode\": \"ADP-0001\",\n  \"errorDescription\": \"Internal Error.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 500,\n    \"statusDescription\": \"Internal Server Error\",\n    \"errorCode\": \"ADP-0001\",\n    \"errorDescription\": \"Internal Error.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/admin.php",
    "groupTitle": "ADP_Common",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/admin/maintenance/enable"
      }
    ]
  },
  {
    "group": "ADP_Common",
    "type": "GET",
    "url": "/api/admin/maintenance/status",
    "title": "Maintenance Status",
    "name": "Maintenance_Status",
    "permission": [
      {
        "name": "IT_Dev",
        "title": "User Authentication:",
        "description": "<p>Not needed.</p>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>IT</b> - Maintenance Status<br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Maintenance Status web service is exclusively used by IT and Developers for checking current Status for Maintenance Mode. It doesn't use any authentication, but can only be successfully executed from the localhost for security reasons.<br/> In case of successful request, it returns a confirmation with current Status for Maintenance Mode.<br/><br/> These are possible Maintenance Status specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-0001</td> <td>Access Denied.</td> <td>401</td> <td>Unauthorized</td> <td>Returns 401 JSON error if external request was made to administer Application (was not initiated from localhost).</td> </tr> </table><br/></p>",
    "header": {
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nGET /api/admin/maintenance/status HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nReferer: https://adp.parcc.dev/\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "HTML",
            "optional": false,
            "field": "N/A",
            "description": "<p>It returns a simple confirmation with current Status for Maintenance Mode.<br/> Anything else is considered as an error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\nMaintenance Mode is Disabled.",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"statusCode\": 500,\n  \"statusDescription\": \"Internal Server Error\",\n  \"errorCode\": \"ADP-0001\",\n  \"errorDescription\": \"Internal Error.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 500,\n    \"statusDescription\": \"Internal Server Error\",\n    \"errorCode\": \"ADP-0001\",\n    \"errorDescription\": \"Internal Error.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/admin.php",
    "groupTitle": "ADP_Common",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/admin/maintenance/status"
      }
    ]
  },
  {
    "group": "ADP_Common",
    "type": "GET",
    "url": "/api/ping",
    "title": "Ping",
    "name": "Ping",
    "permission": [
      {
        "name": "ELB",
        "title": "User Authentication:",
        "description": "<p>Not needed.</p>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>ELB</b> - Health Check<br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Ping web service is exclusively used by ELB (Elastic Load Balancer) for Health Check. It doesn't use any authentication and is only used to confirm that the server is up and running and that ADP is responsive. However, this doesn't mean that it's fully functional.<br/> It always returns successful response and should never return errors.<br/><br/> More details about ADP Status Health Check can be found under Status.<br/><br/></p>",
    "header": {
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nGET /api/ping HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nReferer: https://adp.parcc.dev/\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "HTML",
            "optional": false,
            "field": "N/A",
            "description": "<p>It always returns &quot;OK&quot; as ELB only accepts responses with &quot;200 OK&quot; HTTP Status.<br/> Anything else is considered as an error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\nOK",
          "type": "json"
        }
      ]
    },
    "filename": "src/ping.php",
    "groupTitle": "ADP_Common",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/ping"
      }
    ]
  },
  {
    "group": "ADP_Common",
    "type": "",
    "url": "/",
    "title": "S3 Errors",
    "name": "S3_Errors",
    "version": "1.18.2",
    "description": "<p>Client: <b>ACR</b>, <b>LDR</b>, <b>PRC</b> and <b>Test Driver</b><br/><br/> These are generic S3 errors returned by ADP S3 Library which may be caused by one of the following: <ol> <li>ADP configuration is missing AWS S3 credentials or contains invalid credentials.</li> <li>AWS EC2 Server doesn't have assigned AWS IAM Role.</li> <li>ADP API is not able to communicate with AWS S3 API (i.e. no network connectivity or connection is blocked by firewall).</li> <li>AWS API returned an unexpected response.</li> <li>Request content was not found in AWS S3 (i.e. references in ADP database don't match content in AWS S3).</li> <li>Provided content failed to be uploaded to AWS S3 for various reasons.</li> <li>An internal error/exception triggered by a request.</li> <li>Something else.</li> </ol> In most cases ADP will just log these errors, but will return a different error depending on where/how S3 Library is being utilized, but will always include the Error Code being provided by S3 Library and will add appropriate HTTP Code and HTTP Status, and will replace Error Description with a custom one depending on what original action triggered S3 Error. Therefore, these descriptions are just as a reference what will be included in the Application Log.<br/> These are possible S3 errors:<br/><br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1600</td> <td>S3 S3Authenticate Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response was received from AWS Metadata API while trying to retrieve IAM Role.</td> </tr> <tr> <td>ADP-1601</td> <td>S3 S3Authenticate Error: [{AWSErrorCode}] {AWSErrorDescription}.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when an Error is received from AWS Metadata API while trying to retrieve IAM Role.</td> </tr> <tr> <td>ADP-1602</td> <td>S3 S3Authenticate Error: [{AWSResponseCode}] Unexpected Instance Profile Response.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response Code was received from AWS Metadata API while trying to retrieve IAM Role.</td> </tr> <tr> <td>ADP-1603</td> <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response without Confirmation Code was received from AWS Metadata API while trying to retrieve IAM Role.</td> </tr> <tr> <td>ADP-1604</td> <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response without IAM Role was received from AWS Metadata API while trying to retrieve IAM Role.</td> </tr> <tr> <td>ADP-1605</td> <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response without Instance Profile details was received from AWS Metadata API while trying to retrieve IAM Role.</td> </tr> <tr> <td>ADP-1606</td> <td>S3 S3Authenticate Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response was received from AWS STS API while trying to assume assigned IAM Role.</td> </tr> <tr> <td>ADP-1607</td> <td>S3 S3Authenticate Error: [{AWSErrorCode}] {AWSErrorDescription}.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when an Error was received from AWS STS API while trying to assume assigned IAM Role.</td> </tr> <tr> <td>ADP-1608</td> <td>S3 S3Authenticate Error: [{AWSResponseCode}] Unexpected Instance Profile Response.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response Code was received from AWS STS API while trying to assume assigned IAM Role.</td> </tr> <tr> <td>ADP-1609</td> <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response without Confirmation Code was received from AWS STS API while trying to assume assigned IAM Role.</td> </tr> <tr> <td>ADP-1610</td> <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response was received from AWS STS API while trying to assume assigned IAM Role without Expiration Timestamp for received temporary Credentials.</td> </tr> <tr> <td>ADP-1611</td> <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response was received from AWS STS API while trying to assume assigned IAM Role without Access Key ID.</td> </tr> <tr> <td>ADP-1612</td> <td>S3 S3Authenticate Error: Unexpected Instance Profile Response.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response was received from AWS STS API while trying to assume assigned IAM Role without Secret Access Key.</td> </tr> <tr> <td>ADP-1613</td> <td>S3 GetObjectList Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response Code was received from AWS S3 API while trying to get list of Bucket Keys.</td> </tr> <tr> <td>ADP-1614</td> <td>S3 GetObjectList Error: [{AWSErrorCode}] {AWSErrorDescription}.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when an Error was received from AWS S3 API while trying to get list of Bucket Keys.</td> </tr> <tr> <td>ADP-1615</td> <td>S3 DownloadObject Error: Unable to open file {FilePath} for writing.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when not able to save a file downloaded from AWS S3 to the provided FilePath (due to invalid file path, lack of file system permissions, insufficient available space or something else).</td> </tr> <tr> <td>ADP-1616</td> <td>S3 DownloadObject Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response Code was received from AWS S3 API while trying to download a file from AWS S3 to a custom file path.</td> </tr> <tr> <td>ADP-1617</td> <td>S3 DownloadObject Error: [{AWSErrorCode}] {AWSErrorDescription}.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when an Error was received from AWS S3 API while trying to download a file from AWS S3 to a custom file path.</td> </tr> <tr> <td>ADP-1618</td> <td>S3 GetObjectDetails Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response Code was received from AWS S3 API while trying to get details for a file located in AWS S3.</td> </tr> <tr> <td>ADP-1619</td> <td>S3 GetObjectDetails Error: [{AWSErrorCode}] {AWSErrorDescription}.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when an Error was received from AWS S3 API while trying to get details of a file located in AWS S3.</td> </tr> <tr> <td>ADP-1620</td> <td>S3 UploadObject Error: Invalid Object.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when an invalid or missing Object was requested to be uploaded to AWS S3.</td> </tr> <tr> <td>ADP-1621</td> <td>S3 UploadObject Error: Missing Object parameters.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when an Object requested to be uploaded to AWS S3 is missing empty or has a length of zero.</td> </tr> <tr> <td>ADP-1622</td> <td>S3 UploadObject Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response Code was received from AWS S3 API while trying to upload an Object to AWS S3.</td> </tr> <tr> <td>ADP-1623</td> <td>S3 UploadObject Error: [{AWSErrorCode}] {AWSErrorDescription}.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when an Error was received from AWS S3 API while trying to upload an Object to AWS S3.</td> </tr> <tr> <td>ADP-1624</td> <td>S3 UploadObjectFile Error: Unable to open file {FilePath}.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when an invalid File was requested to be uploaded to AWS S3 (i.e. file is missing or is omitted).</td> </tr> <tr> <td>ADP-1625</td> <td>S3 UploadObjectResource Error: Invalid File Resource.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when an invalid File Resource/Stream is requested to be uploaded to AWS S3.</td> </tr> <tr> <td>ADP-1626</td> <td>S3 UploadObjectResource Error: Unable to obtain Resource Size for File Resource.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when requested File Resource/Stream was requested to be uploaded to AWS S3 that cannot be opened, is empty or has a size of zero (can also be caused by lack of appropriate permissions or if a file resource is being locked by another process).</td> </tr> <tr> <td>ADP-1627</td> <td>S3 DeleteObject Error: [{AWSResponseCode}] Received unexpected HTTP Status.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when invalid response Code was received from AWS S3 API while trying to delete a file located in AWS S3.</td> </tr> <tr> <td>ADP-1628</td> <td>S3 DeleteObject Error: [{AWSErrorCode}] {AWSErrorDescription}.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when an Error was received from AWS S3 API while trying to delete a file located in AWS S3.</td> </tr> <tr> <td>ADP-1629</td> <td>S3 GetCloudFrontSignedURL Error: Unable to open AWS CloudFront SSH Private Key.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when AWS SSH Key needed to generate AWS CloudFront Signed URL is missing or cannot be opened (i.e. due to missing file permissions or wrong file path).</td> </tr> <tr> <td>ADP-1630</td> <td>S3 GetCloudFrontSignedURL Error: Unable to read AWS CloudFront SSH Private Key.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when AWS SSH Key needed to generate AWS CloudFront Signed URL cannot be opened and fully read (i.e. due to missing file permissions).</td> </tr> <tr> <td>ADP-1631</td> <td>S3 GetCloudFrontSignedURL Error: Unable to parse AWS CloudFront SSH Private Key.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when AWS SSH Key needed to generate AWS CloudFront Signed URL cannot be successfully parsed or is an invalid SSH Key.</td> </tr> <tr> <td>ADP-1632</td> <td>S3 GetCloudFrontSignedURL Error: Unable to sign CloudFront Custom Policy Statement.</td> <td>N/A</td> <td>N/A</td> <td>Triggered when AWS CloudFront Signed URL cannot be successfully signed with provided AWS SSH Key (i.e. due OpenSSL Library throwing an error or not being installed on the server).</td> </tr> </table></p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset that has the statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          },
          {
            "group": "Error 4xx",
            "type": "HTML",
            "optional": false,
            "field": "N/A",
            "description": "<p>Simple &quot;404: Page Not Found&quot; or &quot;403: Access Denied&quot; HTML page.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response without Envelope:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"statusCode\": 500,\n  \"statusDescription\": \"Internal Server Error\",\n  \"errorCode\": \"ADP-1600\",\n  \"errorDescription\": \"Failed to save Results Details.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 500,\n    \"statusDescription\": \"Internal Server Error\",\n    \"errorCode\": \"ADP-1600\",\n    \"errorDescription\": \"Failed to save Results Details.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/s3Errors.php",
    "groupTitle": "ADP_Common",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/"
      }
    ]
  },
  {
    "group": "ADP_Common",
    "type": "GET",
    "url": "/api/ping/status",
    "title": "Status",
    "name": "Status",
    "permission": [
      {
        "name": "IT_Dev",
        "title": "User Authentication:",
        "description": "<p>Not needed.</p>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>IT</b>/<b>Developers</b> - Health Check<br/> Status: <b style=\"color:#c00\">In Development</b><br/><br/> Status web service is exclusively used by IT and Developers for Status Health Check. It doesn't use any authentication and is only used to confirm that the server is up and running and that ADP is fully functional. It validates connections to MySQL Databases, Memcached instances, presence and correctness of the configuration file etc.<br/><br/> In case of successful request, it returns OK as the status if everything if working fine. Otherwise, it returns details which failed during Status Health Check.<br/><br/> These are possible Status specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1080</td> <td>Database Connection Error.</td> <td>503</td> <td>Service Unavailable</td> <td>Returns JSON error in case a Database Connection failed to be opened.</td> </tr> </table><br/></p>",
    "header": {
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nGET /api/ping/status HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nReferer: https://adp.parcc.dev/\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes simple &quot;OK&quot; as Status, API Version with Date and Database Schema Version with Date. Dates are Last Modified Dates of Version Files which is NOT a guarantee that it matches the actual Date of the actual version as the File(s) could be manually modified at any time!<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"apiVersion\": \"1.19.2 [11/16/2015]\",\n  \"dbVersion\": \"1.19.2 [11/17/2015]\",\n  \"apiVersionInstalled\": \"1.19.2 [11/16/2015]\",\n  \"dbVersionInstalled\": \"1.19.2 [11/17/2015]\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"status\": \"OK\"\n    \"apiVersion\": \"1.19.2 [11/16/2015]\",\n    \"dbVersion\": \"1.19.2 [11/17/2015]\",\n    \"apiVersionInstalled\": \"1.19.2 [11/16/2015]\",\n    \"dbVersionInstalled\": \"1.19.2 [11/17/2015]\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 503 Service Unavailable\n{\n  \"statusCode\": 503,\n  \"statusDescription\": \"Service Unavailable\",\n  \"errorCode\": \"ADP-1080\",\n  \"errorDescription\": \"Database Connection Error.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 503 Service Unavailable\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 503,\n    \"statusDescription\": \"Service Unavailable\",\n    \"errorCode\": \"ADP-1080\",\n    \"errorDescription\": \"Database Connection Error.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/ping.php",
    "groupTitle": "ADP_Common",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/ping/status"
      }
    ]
  },
  {
    "group": "ADP_Common",
    "type": "GET",
    "url": "/api/admin/update",
    "title": "Update",
    "name": "Update",
    "permission": [
      {
        "name": "IT_Dev",
        "title": "User Authentication:",
        "description": "<p>Not needed.</p>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>IT</b>/<b>Developers</b> - Application Update<br/> Status: <b style=\"color:#c00\">In Development</b><br/><br/> Application Update web service is exclusively used by IT and Developers for Flushing Application Cache. It doesn't use any authentication, but can only be successfully executed from the localhost for security reasons.<br/> In case of successful request, it returns a confirmation that Application was updated successfully.<br/><br/> These are possible Application Update specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-0001</td> <td>Access Denied.</td> <td>401</td> <td>Unauthorized</td> <td>Returns 401 JSON error if external request was made to administer Application (was not initiated from localhost).</td> </tr> </table><br/></p>",
    "header": {
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nGET /api/admin/update HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nReferer: https://adp.parcc.dev/\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "HTML",
            "optional": false,
            "field": "N/A",
            "description": "<p>It returns a simple confirmation if Application was successfully updated.<br/> Anything else is considered as an error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\nApplication was successfully installed.",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"statusCode\": 500,\n  \"statusDescription\": \"Internal Server Error\",\n  \"errorCode\": \"ADP-0001\",\n  \"errorDescription\": \"Internal Error.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 500,\n    \"statusDescription\": \"Internal Server Error\",\n    \"errorCode\": \"ADP-0001\",\n    \"errorDescription\": \"Internal Error.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/admin.php",
    "groupTitle": "ADP_Common",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/admin/update"
      }
    ]
  },
  {
    "group": "ADP_Publisher",
    "type": "POST",
    "url": "/api/assignment",
    "title": "Assignment Creation",
    "name": "Assignment_Creation",
    "permission": [
      {
        "name": "LDR",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>LDR</b><br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Assignment web service is exclusively used by LDR for Test Assignments and Student details publishing. It validates multiple parameters before it allows client to login into ADP Publisher application.<br/> Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/> In order to create new Test Assignment, all Test Assignment details are required. Student details are optional, but must be provided if referenced Student does not exist in ADP system. On the other hand, if Student already exists in ADP, a valid reference for a Student is enough (Student ID). However, if Student details have changed in the meantime, then all Student details are required to be provided with Test details and ADP will update the existing Student details.<br/><br/> Once Test Assignment Request is received, if correct, ADP Publisher will create a Request Job in its Job Queue and will process it as soon as possible depending on its priority, and number and priority of other pending Request Jobs in its Job Queue.<br/><br/> In case of successful request, it returns an ADP Test Assignment ID, Date and Time when it was created and its current Status.<br/> In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server.<br/><br/> This means that LDR should be able to roll back any changes if ADP doesn't return a confirmation about successful creation of Test Assignment details and optionally creation/update of Student details. If subsequent request for creation of the same Test Assignment is sent ADP Publisher will simply return an error and won't process the request.<br/><br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> These are possible Test Assignment Creation specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1803</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case of missing index in the request.</td> </tr> <tr> <td>ADP-1804</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case of missing missing ADP Test Assignment Id in the request.</td> </tr> <tr> <td>ADP-1805</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case of missing index in the request.</td> </tr> <tr> <td>ADP-1806</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test Assignment Id is missing or invalid.</td> </tr> <tr> <td>ADP-1807</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case a test session cannot be found.</td> </tr> <tr> <td>ADP-1809</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error/td&gt; <td>Returns JSON error which has a custom error message.</td> </tr> <tr> <td>ADP-1810</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case student ID is missing.</td> </tr> <tr> <td>ADP-1811</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case student ID is not an unsigned integer.</td> </tr> <tr> <td>ADP-1812</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case provided state is not valid.</td> </tr> <tr> <td>ADP-1813</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case student's grade is provided but is not valid.</td> </tr> <tr> <td>ADP-1814</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case student's date of birth is provided but is not a valid date.</td> </tr> <tr> <td>ADP-1815</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case student's date of birth is provided but is not a valid format.</td> </tr> <tr> <td>ADP-1816</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case student's school name is provided but is not a valid format.</td> </tr> <tr> <td>ADP-1817</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>ReturnsJSON error in case student's name is provided but is not a valid format.</td> </tr> <tr> <td>ADP-1818</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case student's personal ID is provided but is not a valid format.</td> </tr> <tr> <td>ADP-1819</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case the student can't be found.</td> </tr> <tr> <td>ADP-1820</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case of missing index in the request.</td> </tr> <tr> <td>ADP-1821</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case test ID is not an unsigned integer.</td> </tr> <tr> <td>ADP-1822</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case test form ID is not an unsigned integer.</td> </tr> <tr> <td>ADP-1823</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test Key is not a valid format.</td> </tr> <tr> <td>ADP-1824</td> <td>Test Key already exists.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case if test key already exists.</td> </tr> <tr> <td>ADP-1825</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case Test Session is not found.</td> </tr> <tr> <td>ADP-1827</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case a Test Form does not belong to Test Battery.</td> </tr> <tr> <td>ADP-1828</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case the student is not found.</td> </tr> <tr> <td>ADP-1830</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case provided Test Status is invalid.</td> </tr> <tr> <td>ADP-1831</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case provided LineReader value is not valid.</td> </tr> <tr> <td>ADP-1832</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case provided TextToSpeech value is not valid.</td> </tr> </table> PHP Code for Test Assignment Creation: <pre class=\"pseudo-code\"> date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server uses NTP service.<br/> <br/> $testAssignmentId = ...<br/> <br/> $testId = ...<br/> $testFormId = ...<br/> $testKey = ... // Unique Test Key generated by LDR.<br/> <br/> $studentId = ...<br/> $personalId = ...<br/> $firstName = ...<br/> $lastName = ...<br/> $dateOfBirth = ...<br/> $state = ...<br/> $schoolName = ...<br/> $grade = ...<br/> $enableLineReader = ...<br/> $enableTextToSpeech = ...<br/> <br/> $apiHost = &quot;https://adp.parcc.dev&quot;;<br/> $apiURI = &quot;/api/assignment&quot;;<br/> $username = &quot;DocumentationLDR&quot;;<br/> $password = &quot;97f0ca00610888a0e9e58883dabc813b&quot;;<br/> $secret = &quot;6d393cf3b856c34ee5c55f7c50dfeb49&quot;;<br/> <br/> $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/> $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix timestamp.<br/> $hash = hash_hmac('sha256', $message, $secret);<br/> $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/> <br/> $ch = curl_init($apiHost . $apiURI);<br/> curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>    &quot;Content-Type&quot;: &quot;application/json; charset=UTF-8&quot;,<br/>    &quot;Authorization: Basic &quot; . base64_encode($username . &quot;:&quot; . $password),<br/>    &quot;Authentication:&quot; . $authentication<br/> ]);<br/> curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode({<br/>    'testAssignmentId' =&gt; $testAssignmentId,<br/>    'test' =&gt; {<br/>      'testId' =&gt; $testId,<br/>      'testFormId' =&gt; $testFormId,<br/>      'testKey' =&gt; $testKey<br/>      'enableLineReader' =&gt; $enableLineReader<br/>      'enableTextToSpeech' =&gt; $enableTextToSpeech<br/>    },<br/>    'student' =&gt; {<br/>      'studentId' =&gt; $studentId,<br/>      'personalId' =&gt; $personalId,<br/>      'firstName' =&gt; $firstName,<br/>      'lastName' =&gt; $lastName,<br/>      'dateOfBirth' =&gt; $dateOfBirth,<br/>      'state' =&gt; $state,<br/>      'schoolName' =&gt; $schoolName,<br/>      'grade' =&gt; $grade<br/>    }<br/> }));<br/> <br/> $result = curl_exec($ch);<br/> curl_close($ch);<br/> ... </pre><br/></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "username",
            "defaultValue": "DocumentationLDR",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Username can be entered in the form.<br/> It's 20 characters long unique string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "password",
            "defaultValue": "97f0ca00610888a0e9e58883dabc813b",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Password can be entered in the form.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "secret",
            "defaultValue": "6d393cf3b856c34ee5c55f7c50dfeb49",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Secret can be entered in the form.<br/> It's 32 characters long MD5 hash string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "defaultValue": "Base64EncodedStringXXXXXXXXXX==",
            "description": "<p>HMAC Authentication using Secret key as salt for SHA256 hashing of the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Basic Base64EncodedStringYYYYYYYYYY==",
            "description": "<p>Basic Authentication using Username and Password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nPOST /api/assignment HTTP/1.1\nHost: adp.parcc.dev\nUser-Agent: curl/7.40.0\nAccept: * / *\nContent-Type: application/json; charset=UTF-8\nAuthorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==\nAuthentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky\nContent-Length: 89",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "testAssignmentId",
            "defaultValue": "100",
            "description": "<p>Test Assignment ID is an ID used as a reference to LDR's Test Assignment ID which is unique inside LDR, but may not be the same as those Test Assignments created by PRC for Practice Tests and Quizzes.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "testId",
            "defaultValue": "1",
            "description": "<p>Test Battery ID is the ID provided by ADP during Test Publishing and should be the same as the ones used in LDR, so it is unique both inside ADP and LDR.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "testFormId",
            "defaultValue": "3",
            "description": "<p>Test Battery Form ID is the ID provided by ADP during Test Publishing and should be the same as the ones used in LDR, so it is unique both inside ADP and LDR.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "testKey",
            "defaultValue": "ABCDEFGHIJ",
            "description": "<p>Test Key is all uppercase, 10-character long string that is unique for each Test Assignment. LDR generates Test Key when a Test Assignment is created and provides it to ADP. LDR should not use Test Keys that begin with a predefined sequence of characters &quot;PRACTxxxxx&quot; as these Test Keys are reserved for Practice Tests and Quizzes. These Test Keys are generated by ADP Publisher when a Test Assignment for these types of Tests are created. Both ADP Publisher and LDR should have this sequence configurable, so it can be easily changed to something else in the future.<br/> It's 10 characters long uppercase only unique string [A-Z].</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "studentId",
            "defaultValue": "3",
            "description": "<p>Student ID is a unique ID for each Student assigned by LDR when Student is created. ADP should use the same Student ID as LDR, so it is unique both inside ADP and LDR.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "personalId",
            "defaultValue": "IL-12345",
            "description": "<p>Personal ID is an arbitrary Student's ID used outside of ADS system, and is imported into LDR from various external systems. It is also referenced as Student's Global Unique Identifier, and can be Student' State ID, School ID, Local ID or some other kind of Personal ID. Rules which state uses which ID are handled by LDR, and if Student has multiple IDs LDR will decide which one will be provided to ADP to be used as Student's Personal ID during Test Delivery.<br/> It's 30 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "firstName",
            "defaultValue": "John",
            "description": "<p>First Name of the Student.<br/> It's 35 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "lastName",
            "defaultValue": "Smith",
            "description": "<p>Last Name of the Student.<br/> It's 35 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "dateOfBirth",
            "defaultValue": "2000-01-01",
            "description": "<p>Student's Date of Birth.<br/> It's date in the ANSI SQL-92 format ('YYYY-MM-DD').</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "state",
            "defaultValue": "IL",
            "description": "<p>State in which Student's School is located (this doesn't necessarily mean the same state where Student lives).<br/> It's 2 character long predefined State abbreviation (all uppercase string).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "schoolName",
            "defaultValue": "Evanston High School",
            "description": "<p>School Name which Student is attending.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "grade",
            "defaultValue": "K, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12",
            "description": "<p>Student's Grade.<br/> It's 1 or 2 character long predefined string.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "enableLineReader",
            "defaultValue": "true",
            "description": "<p>Indicator if Line Reader Tool should be available to Student while taking a Test.<br/> It's a boolean.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "enableTextToSpeech",
            "defaultValue": "true",
            "description": "<p>Indicator if Text-to-Speech Tool should be available to Student while taking a Test.<br/> It's a boolean.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Body Example for Existing Student",
          "content": "{\n  \"testAssignmentId\": 100,\n  \"test\": {\n    \"testId\": 1,\n    \"testFormId\": 3,\n    \"testKey\": \"ABCDEFGHIJ\",\n    \"enableLineReader\": true,\n    \"enableTextToSpeech\": false\n  },\n  \"student\": {\n    \"studentId\": 3\n  }\n}",
          "type": "JSON"
        },
        {
          "title": "Request Body Example for Non-Existing Student",
          "content": "{\n  \"testAssignmentId\": 100,\n  \"test\": {\n    \"testId\": 1,\n    \"testFormId\": 3,\n    \"testKey\": \"ABCDEFGHIJ\",\n    \"enableLineReader\": true,\n    \"enableTextToSpeech\": false\n  },\n  \"student\": {\n    \"studentId\": 3,\n    \"personalId\": \"IL-12345\",\n    \"firstName\": \"John\",\n    \"lastName\": \"Smith\",\n    \"dateOfBirth\": \"2000-01-01\",\n    \"state\": \"IL\",\n    \"schoolName\": \"Evanston High School\",\n    \"grade\": 10\n  }\n}",
          "type": "JSON"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes ADP Test Delivery's Test Assignment ID, Date and Time when Test Assignment was created and its Status.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"testAssignmentId\": 5,\n  \"createdTimestamp\": \"2015-05-05 05:05:05\",\n  \"status\": \"Scheduled\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 3\n  },\n  \"result\": {\n    \"testAssignmentId\": 5,\n    \"createdTimestamp\": \"2015-05-05 05:05:05\",\n    \"status\": \"Scheduled\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 412 Precondition Failed\n{\n  \"statusCode\": 412,\n  \"statusDescription\": \"Precondition Failed\",\n  \"errorCode\": \"ADP-1803\",\n  \"errorDescription\": \"Invalid Request.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 412 Precondition Failed\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 412,\n    \"statusDescription\": \"Precondition Failed\",\n    \"errorCode\": \"ADP-1803\",\n    \"errorDescription\": \"Invalid Request.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/assignment.php",
    "groupTitle": "ADP_Publisher",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/assignment"
      }
    ]
  },
  {
    "group": "ADP_Publisher",
    "type": "DELETE",
    "url": "/api/assignment/{adpTestAssignmentId}",
    "title": "Assignment Deletion",
    "name": "Assignment_Deletion",
    "permission": [
      {
        "name": "LDR",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>LDR</b><br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Assignment web service is exclusively used by LDR for Test Assignments and Student details publishing. It validates multiple parameters before it allows client to login into ADP Publisher application.<br/> Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/> In order to delete existing Test Assignment and/or Student details, both Test Assignment ID and Student ID must be provided as a reference. In addition, ADP Test Assignment ID must always be provided as a reference. ADP will delete Student only if there are no other active Test Assignments assigned to the Student.<br/> Finally, only Scheduled Test Assignment without any Test Results can be deleted, but those that are already In Progress, are Paused, Submitted, Completed or Canceled cannot be deleted as they have their Test Results which should never be deleted.<br/><br/> Once Test Assignment Request is received, if correct, ADP Publisher will create a Request Job in its Job Queue and will process it as soon as possible depending on its priority, and number and priority of other pending Request Jobs in its Job Queue.<br/><br/> In case of successful request, it returns an ADP Test Assignment ID, Date and Time when it was updated and its current Status (in this case 'Deleted').<br/> In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server.<br/><br/> This means that LDR should be able to roll back any changes if ADP doesn't return a confirmation about successful deletion of Test Assignment details and/or Student details.<br/> If subsequent request for deletion of the same Test Assignment is sent, ADP Publisher will simply return an error.<br/><br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> These are possible Test Assignment Deletion specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1806</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case of missing or invalid format of the provided Test Assignment details.</td> </tr> <tr> <td>ADP-1810</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case request body does not have the studentId.</td> </tr> <tr> <td>ADP-1811</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case studentId format is not valid.</td> </tr> <tr> <td>ADP-1850</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case of missing or invalid format of the provided ADP Test Assignment ID.</td> </tr> <tr> <td>ADP-1851</td> <td>Access Denied.</td> <td>403</td> <td>Precondition Failed</td> <td>Returns JSON error in case the job already exists.</td> </tr> <tr> <td>ADP-1852</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case the system is unable to find a test assignment to delete.</td> </tr> <tr> <td>ADP-1853</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case the test assignment does not belong to actual user.</td> </tr> <tr> <td>ADP-1854</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case the test assignment does not belong to the actual sutdent.</td> </tr> <tr> <td>ADP-1855</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case the test assignment does not have the right test assignment ID.</td> </tr> <tr> <td>ADP-1856</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case the test assignment belongs to Practice Test.</td> </tr> <tr> <td>ADP-1857</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case the the system is unable to delete assignment.</td> </tr> <tr> <td>ADP-1858</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case the test assignment status is different than 'Scheduled'.</td> </tr> </table> PHP Code for Test Assignment Deletion: <pre class=\"pseudo-code\"> date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server uses NTP service.<br/> <br/> $testAssignmentId = ...<br/> $adpTestAssignmentId = ...; // ADP's Test Assignment ID that was provided when it was created the first time.<br/> <br/> $testId = ...<br/> $testFormId = ...<br/> $testKey = ... // Unique Test Key generated by LDR.<br/> <br/> $studentId = ...<br/> <br/> $apiHost = &quot;https://adp.parcc.dev&quot;;<br/> $apiURI = &quot;/api/assignment/&quot; . $adpTestAssignmentId;<br/> $username = &quot;DocumentationLDR&quot;;<br/> $password = &quot;97f0ca00610888a0e9e58883dabc813b&quot;;<br/> $secret = &quot;6d393cf3b856c34ee5c55f7c50dfeb49&quot;;<br/> <br/> $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/> $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix timestamp.<br/> $hash = hash_hmac('sha256', $message, $secret);<br/> $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/> <br/> $ch = curl_init($apiHost . $apiURI);<br/> curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>    &quot;Content-Type&quot;: &quot;application/json; charset=UTF-8&quot;,<br/>    &quot;Authorization: Basic &quot; . base64_encode($username . &quot;:&quot; . $password),<br/>    &quot;Authentication:&quot; . $authentication<br/> ]);<br/> curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode({<br/>    'testAssignmentId' =&gt; $testAssignmentId,<br/>    'status' =&gt; $testStatus,<br/>    'test' =&gt; {<br/>      'testId' =&gt; $testId,<br/>      'testFormId' =&gt; $testFormId,<br/>      'testKey' =&gt; $testKey<br/>    },<br/>    'student' =&gt; {<br/>      'studentId' =&gt; $studentId<br/>    }<br/> }));<br/> curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');<br/> <br/> $result = curl_exec($ch);<br/> curl_close($ch);<br/> ... </pre><br/></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "username",
            "defaultValue": "DocumentationLDR",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Username can be entered in the form.<br/> It's 20 characters long unique string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "password",
            "defaultValue": "97f0ca00610888a0e9e58883dabc813b",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Password can be entered in the form.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "secret",
            "defaultValue": "6d393cf3b856c34ee5c55f7c50dfeb49",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Secret can be entered in the form.<br/> It's 32 characters long MD5 hash string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "defaultValue": "Base64EncodedStringXXXXXXXXXX==",
            "description": "<p>HMAC Authentication using Secret key as salt for SHA256 hashing of the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Basic Base64EncodedStringYYYYYYYYYY==",
            "description": "<p>Basic Authentication using Username and Password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nDELETE /api/assignment/5 HTTP/1.1\nHost: adp.parcc.dev\nUser-Agent: curl/7.40.0\nAccept: * / *\nContent-Type: application/json; charset=UTF-8\nAuthorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==\nAuthentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky\nContent-Length: 89",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "testAssignmentId",
            "defaultValue": "100",
            "description": "<p>Test Assignment ID is an ID used as a reference to LDR's Test Assignment ID which is unique inside LDR, but may not be the same as those Test Assignments created by PRC for Practice Tests and Quizzes.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "adpTestAssignmentId",
            "defaultValue": "5",
            "description": "<p>ADP Test Assignment ID is a unique ADP's ID provided when Test Assignment was created. As ADP also has Practice Tests and Quizzes, this ID is different from LDR's Test Assignment ID.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "studentId",
            "defaultValue": "3",
            "description": "<p>Student ID is a unique ID for each Student assigned by LDR when Student is created. ADP should use the same Student ID as LDR, so it is unique both inside ADP and LDR.<br/> It's 32-bit unsigned unique integer.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Body Example",
          "content": "{\n  \"testAssignmentId\": 100,\n  \"student\": {\n    \"studentId\": 3\n  }\n}",
          "type": "JSON"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes ADP Test Delivery's Test Assignment ID, Date and Time when Test Assignment was updated and its Status.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"testAssignmentId\": 5,\n  \"updatedTimestamp\": \"2015-05-05 05:05:05\",\n  \"status\": \"Deleted\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 3\n  },\n  \"result\": {\n    \"testAssignmentId\": 5,\n    \"updatedTimestamp\": \"2015-05-05 05:05:05\",\n    \"status\": \"Deleted\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 412 Precondition Failed\n{\n  \"statusCode\": 412,\n  \"statusDescription\": \"Precondition Failed\",\n  \"errorCode\": \"ADP-1810\",\n  \"errorDescription\": \"Invalid Request.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 412 Precondition Failed\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 4\n  },\n  \"result\": {\n    \"statusCode\": 412,\n    \"statusDescription\": \"Precondition Failed\",\n    \"errorCode\": \"ADP-1810\",\n    \"errorDescription\": \"Invalid Request.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/assignment.php",
    "groupTitle": "ADP_Publisher",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/assignment/{adpTestAssignmentId}"
      }
    ]
  },
  {
    "group": "ADP_Publisher",
    "type": "PATCH",
    "url": "/api/assignment/{adpTestAssignmentId}",
    "title": "Assignment Update",
    "name": "Assignment_Update",
    "permission": [
      {
        "name": "LDR",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>LDR</b><br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Assignment web service is exclusively used by LDR for Test Assignments and Student details publishing. It validates multiple parameters before it allows client to login into ADP Publisher application.<br/> Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/> Test Assignment Update may include any of the following: only Test Status, only Test details (one property or many), only Student details (one property or many), or any combination of these three including all of them at once. In case Student already has at least one Test Assignment, Student details can be updated either by calling Assignment Update or while creating a new Test Assignment. In case Assignment Update is used, only one Test Assignment needs to be updated in order to have Student details updated.<br/><br/> In order to update existing Test Assignment and/or Student details, both Test Assignment ID and Student ID must be provided as a reference. In addition to those, only those properties that need to be updated are required, but there has to be at least one. Otherwise, ADP Publisher will return an error as there's nothing to be updated. In addition, ADP Test Assignment ID must always be provided as a reference.<br/> Finally, Test Assignment can also be updated in order to change the Test Status (i.e. Reopen Submitted Test, Cancel a Test, Confirm Test Submission and Complete the Test, or Reset Test Status from 'InProgress' to 'Paused' in case of crash or if Student closed their browser by mistake).<br/> <br/> Only Test Assignments that are not Completed or are not already Canceled can be canceled. Only Completed Test can be reopened in which case its status needs to be changed to 'Paused'. Only Submitted Test can be Completed. Once Test Status changes to either Completed or Canceled, it will automatically trigger Export of Test Results. Once completed, that Test cannot be reopened anymore and a new Test Assignment would have to be created in order to re-take the same Test from scratch. However, once new Test Assignment is started, a newer Test Battery Form Revision may be assigned which may be different from the one originally taken.<br/><br/> Once Test Assignment Request is received, if correct, ADP Publisher will create a Request Job in its Job Queue and will process it as soon as possible depending on its priority, and number and priority of other pending Request Jobs in its Job Queue.<br/><br/> In case of successful request, it returns an ADP Test Assignment ID, Date and Time when it was updated and its current Status.<br/> In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server.<br/><br/> This means that LDR should be able to roll back any changes if ADP doesn't return a confirmation about successful update of Test Assignment and/or Student details.<br/><br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> These are possible Test Assignment Update specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1803</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case of missing index in the request.</td> </tr> <tr> <td>ADP-1804</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case of missing missing ADP Test Assignment Id in the request.</td> </tr> <tr> <td>ADP-1805</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case of missing index in the request.</td> </tr> <tr> <td>ADP-1806</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test Assignment Id is missing or invalid.</td> </tr> <tr> <td>ADP-1807</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case a test session cannot be found.</td> </tr> <tr> <td>ADP-1809</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error/td&gt; <td>Returns JSON error which has a custom error message.</td> </tr> <tr> <td>ADP-1810</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case student ID is missing.</td> </tr> <tr> <td>ADP-1811</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case student ID is not an unsigned integer.</td> </tr> <tr> <td>ADP-1812</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case provided state is not valid.</td> </tr> <tr> <td>ADP-1813</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case student's grade is provided but is not valid.</td> </tr> <tr> <td>ADP-1814</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case student's date of birth is provided but is not a valid date.</td> </tr> <tr> <td>ADP-1815</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case student's date of birth is provided but is not a valid format.</td> </tr> <tr> <td>ADP-1816</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case student's school name is provided but is not a valid format.</td> </tr> <tr> <td>ADP-1817</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>ReturnsJSON error in case student's name is provided but is not a valid format.</td> </tr> <tr> <td>ADP-1818</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case student's personal ID is provided but is not a valid format.</td> </tr> <tr> <td>ADP-1819</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case the student can't be found.</td> </tr> <tr> <td>ADP-1820</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case of missing index in the request.</td> </tr> <tr> <td>ADP-1821</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case test ID is not an unsigned integer.</td> </tr> <tr> <td>ADP-1822</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case test form ID is not an unsigned integer.</td> </tr> <tr> <td>ADP-1823</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test Key is not a valid format.</td> </tr> <tr> <td>ADP-1824</td> <td>Test Key already exists.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case if test key already exists.</td> </tr> <tr> <td>ADP-1825</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case Test Session is not found.</td> </tr> <tr> <td>ADP-1827</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case a Test Form does not belong to Test Battery.</td> </tr> <tr> <td>ADP-1828</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case the student is not found.</td> </tr> <tr> <td>ADP-1830</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case provided Test Status is invalid.</td> </tr> <tr> <td>ADP-1831</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case provided LineReader value is not valid.</td> </tr> <tr> <td>ADP-1832</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case provided TextToSpeech value is not valid.</td> </tr> </table> PHP Code for Test Assignment Update: <pre class=\"pseudo-code\"> date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server uses NTP service.<br/> <br/> $testAssignmentId = ...<br/> $adpTestAssignmentId = ...; // ADP's Test Assignment ID that was provided when it was created the first time.<br/> $testStatus = ...; // Optional new Test Status requested by Proctor.<br/> <br/> $testId = ...<br/> $testFormId = ...<br/> $testKey = ... // Unique Test Key generated by LDR.<br/> <br/> $studentId = ...<br/> $personalId = ...<br/> $firstName = ...<br/> $lastName = ...<br/> $dateOfBirth = ...<br/> $state = ...<br/> $schoolName = ...<br/> $grade = ...<br/> $enableLineReader = ...<br/> $enableTextToSpeech = ...<br/> <br/> $apiHost = &quot;https://adp.parcc.dev&quot;;<br/> $apiURI = &quot;/api/assignment/&quot; . $adpTestAssignmentId;<br/> $username = &quot;DocumentationLDR&quot;;<br/> $password = &quot;97f0ca00610888a0e9e58883dabc813b&quot;;<br/> $secret = &quot;6d393cf3b856c34ee5c55f7c50dfeb49&quot;;<br/> <br/> $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/> $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix timestamp.<br/> $hash = hash_hmac('sha256', $message, $secret);<br/> $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/> <br/> $ch = curl_init($apiHost . $apiURI);<br/> curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>    &quot;Content-Type&quot;: &quot;application/json; charset=UTF-8&quot;,<br/>    &quot;Authorization: Basic &quot; . base64_encode($username . &quot;:&quot; . $password),<br/>    &quot;Authentication:&quot; . $authentication<br/> ]);<br/> curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([<br/>    'testAssignmentId' =&gt; $testAssignmentId,<br/>    'status' =&gt; $testStatus,<br/>    'test' =&gt; [<br/>      'testId' =&gt; $testId,<br/>      'testFormId' =&gt; $testFormId,<br/>      'testKey' =&gt; $testKey<br/>      'enableLineReader' =&gt; $enableLineReader<br/>      'enableTextToSpeech' =&gt; $enableTextToSpeech<br/>    ],<br/>    'student' =&gt; [<br/>      'studentId' =&gt; $studentId,<br/>      'personalId' =&gt; $personalId,<br/>      'firstName' =&gt; $firstName,<br/>      'lastName' =&gt; $lastName,<br/>      'dateOfBirth' =&gt; $dateOfBirth,<br/>      'state' =&gt; $state,<br/>      'schoolName' =&gt; $schoolName,<br/>      'grade' =&gt; $grade<br/>    ]<br/> ]));<br/> curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');<br/> <br/> $result = curl_exec($ch);<br/> curl_close($ch);<br/> ... </pre><br/></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "username",
            "defaultValue": "DocumentationLDR",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Username can be entered in the form.<br/> It's 20 characters long unique string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "password",
            "defaultValue": "97f0ca00610888a0e9e58883dabc813b",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Password can be entered in the form.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "secret",
            "defaultValue": "6d393cf3b856c34ee5c55f7c50dfeb49",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Secret can be entered in the form.<br/> It's 32 characters long MD5 hash string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "defaultValue": "Base64EncodedStringXXXXXXXXXX==",
            "description": "<p>HMAC Authentication using Secret key as salt for SHA256 hashing of the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Basic Base64EncodedStringYYYYYYYYYY==",
            "description": "<p>Basic Authentication using Username and Password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nPATCH /api/assignment/5 HTTP/1.1\nHost: adp.parcc.dev\nUser-Agent: curl/7.40.0\nAccept: * / *\nContent-Type: application/json; charset=UTF-8\nAuthorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==\nAuthentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky\nContent-Length: 89",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "testAssignmentId",
            "defaultValue": "100",
            "description": "<p>Test Assignment ID is an ID used as a reference to LDR's Test Assignment ID which is unique inside LDR, but may not be the same as those Test Assignments created by PRC for Practice Tests and Quizzes.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "adpTestAssignmentId",
            "defaultValue": "5",
            "description": "<p>ADP Test Assignment ID is a unique ADP's ID provided when Test Assignment was created. As ADP also has Practice Tests and Quizzes, this ID is different from LDR's Test Assignment ID.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "testStatus",
            "defaultValue": "'Paused', 'Completed', 'Canceled'",
            "description": "<p>Test Status determines current state of the Test Assignment. Even though there are other possible Test States, LDR can only request changing it to one of these 3. Once Test is Submitted or is already Canceled its Test Status cannot be changed anymore. Only if Test is currently Submitted, LDR can either re-open it by changing its Test Status back to 'Paused', or confirm/accept its submission by changing its status to 'Completed'. These are the only cases when Test Status can be changed outside Test Driver. Any other changes of Test Status can only be made by Test Driver.<br/> It's one of the 3 predefined values.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "testId",
            "defaultValue": "1",
            "description": "<p>Test Battery ID is the ID provided by ADP during Test Publishing and should be the same as the ones used in LDR, so it is unique both inside ADP and LDR.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "testFormId",
            "defaultValue": "3",
            "description": "<p>Test Battery Form ID is the ID provided by ADP during Test Publishing and should be the same as the ones used in LDR, so it is unique both inside ADP and LDR.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "testKey",
            "defaultValue": "ABCDEFGHIJ",
            "description": "<p>Test Key is all uppercase, 10-character long string that is unique for each Test Assignment. LDR generates Test Key when a Test Assignment is created and provides it to ADP. LDR should not use Test Keys that begin with a predefined sequence of characters &quot;PRACTxxxxx&quot; as these Test Keys are reserved for Practice Tests and Quizzes. These Test Keys are generated by ADP Publisher when a Test Assignment for these types of Tests are created. Both ADP Publisher and LDR should have this sequence configurable, so it can be easily changed to something else in the future.<br/> It's 10 characters long uppercase only unique string [A-Z].</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "studentId",
            "defaultValue": "3",
            "description": "<p>Student ID is a unique ID for each Student assigned by LDR when Student is created. ADP should use the same Student ID as LDR, so it is unique both inside ADP and LDR.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "personalId",
            "defaultValue": "IL-12345",
            "description": "<p>Personal ID is an arbitrary Student's ID used outside of ADS system, and is imported into LDR from various external systems. It is also referenced as Student's Global Unique Identifier, and can be Student' State ID, School ID, Local ID or some other kind of Personal ID. Rules which state uses which ID are handled by LDR, and if Student has multiple IDs LDR will decide which one will be provided to ADP to be used as Student's Personal ID during Test Delivery.<br/> It's 30 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "firstName",
            "defaultValue": "John",
            "description": "<p>First Name of the Student.<br/> It's 35 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "lastName",
            "defaultValue": "Smith",
            "description": "<p>Last Name of the Student.<br/> It's 35 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "dateOfBirth",
            "defaultValue": "2000-01-01",
            "description": "<p>Student's Date of Birth.<br/> It's date in the ANSI SQL-92 format ('YYYY-MM-DD').</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "state",
            "defaultValue": "IL",
            "description": "<p>State in which Student's School is located (this doesn't necessarily mean the same state where Student lives).<br/> It's 2 character long predefined State abbreviation (all uppercase string).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "schoolName",
            "defaultValue": "Evanston High School",
            "description": "<p>School Name which Student is attending.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "grade",
            "defaultValue": "K, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12",
            "description": "<p>Student's Grade.<br/> It's 1 or 2 character long predefined string.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "enableLineReader",
            "defaultValue": "true",
            "description": "<p>Indicator if Line Reader Tool should be available to Student while taking a Test.<br/> It's a boolean.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "enableTextToSpeech",
            "defaultValue": "true",
            "description": "<p>Indicator if Text-to-Speech Tool should be available to Student while taking a Test.<br/> It's a boolean.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Body Example to Update Test Status only",
          "content": "{\n  \"testAssignmentId\": 100,\n  \"testStatus\": \"Completed\",\n  \"student\": {\n    \"studentId\": 3\n  }\n}",
          "type": "JSON"
        },
        {
          "title": "Request Body Example to Update Test only",
          "content": "{\n  \"testAssignmentId\": 100,\n  \"test\": {\n    \"testId\": 1,\n    \"testFormId\": 3,\n    \"testKey\": \"ABCDEFGHIJ\",\n    \"enableLineReader\": true,\n    \"enableTextToSpeech\": false\n  },\n  \"student\": {\n    \"studentId\": 3\n  }\n}",
          "type": "JSON"
        },
        {
          "title": "Request Body Example to Update Student only",
          "content": "{\n  \"testAssignmentId\": 100,\n  \"student\": {\n    \"studentId\": 3,\n    \"personalId\": \"IL-12345\",\n    \"firstName\": \"John\",\n    \"lastName\": \"Smith\",\n    \"dateOfBirth\": \"2000-01-01\",\n    \"state\": \"IL\",\n    \"schoolName\": \"Evanston High School\",\n    \"grade\": 10\n  }\n}",
          "type": "JSON"
        },
        {
          "title": "Request Body Example to Update everything",
          "content": "{\n  \"testAssignmentId\": 100,\n  \"testStatus\": \"Paused\",\n  \"test\": {\n    \"testId\": 1,\n    \"testFormId\": 3,\n    \"testKey\": \"ABCDEFGHIJ\",\n    \"enableLineReader\": true,\n    \"enableTextToSpeech\": false\n  },\n  \"student\": {\n    \"studentId\": 3,\n    \"personalId\": \"IL-12345\",\n    \"firstName\": \"John\",\n    \"lastName\": \"Smith\",\n    \"dateOfBirth\": \"2000-01-01\",\n    \"state\": \"IL\",\n    \"schoolName\": \"Evanston High School\",\n    \"grade\": 10\n  }\n}",
          "type": "JSON"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes ADP Test Delivery's Test Assignment ID, Date and Time when Test Assignment was updated and its Status.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"testAssignmentId\": 5,\n  \"updatedTimestamp\": \"2015-05-05 05:05:05\",\n  \"status\": \"Scheduled\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 3\n  },\n  \"result\": {\n    \"testAssignmentId\": 5,\n    \"updatedTimestamp\": \"2015-05-05 05:05:05\",\n    \"status\": \"Scheduled\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 412 Precondition Failed\n{\n  \"statusCode\": 412,\n  \"statusDescription\": \"Precondition Failed\",\n  \"errorCode\": \"ADP-1803\",\n  \"errorDescription\": \"Invalid Request.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 412 Precondition Failed\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 4\n  },\n  \"result\": {\n    \"statusCode\": 412,\n    \"statusDescription\": \"Precondition Failed\",\n    \"errorCode\": \"ADP-1803\",\n    \"errorDescription\": \"Invalid Request.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/assignment.php",
    "groupTitle": "ADP_Publisher",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/assignment/{adpTestAssignmentId}"
      }
    ]
  },
  {
    "group": "ADP_Publisher",
    "type": "GET",
    "url": "/api/results/{adpTestAssignmentId}",
    "title": "Results Details",
    "name": "Results_Details",
    "permission": [
      {
        "name": "LDR",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>LDR</b><br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Results Details web service is exclusively used by LDR for getting details for Final Test Results once Test is being Submitted or Canceled. It validates multiple parameters before it allows client to login into ADP Publisher application.<br/> Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/> In order to get details for Final Test Results, only Test Assignment ID is required. Test must be already Submitted or Canceled.<br/><br/> Once Results Details Request is received, if correct, ADP Publisher will create a Request Job in its Job Queue and will process it immediately.<br/><br/> In case of successful request, it returns LDR Test Assignment ID, ADP Test Results ID, Student ID, Test ID, Test Form ID, Test Form Revision ID, Date and Time when Test was started, Date and Time when Test was either Submitted or Canceled, and its current Status.<br/> In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server.<br/><br/> This means that LDR should be able to handle exception if ADP doesn't return expected Test Results Details and be able to make the same request later, if needed. In addition, ADP Publisher will not check if actual Test Results file is physically present inside AWS S3 and if Test Results are valid. This check was already performed previously, and it is expected that Test Results are still in place where they were initially saved. If subsequent request for getting Test Results Details is sent, ADP Publisher will return the same response.<br/><br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> These are possible Results Details specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1901</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case of error while saving the job details.</td> </tr> <tr> <td>ADP-1902</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case validation error.</td> </tr> <tr> <td>ADP-1903</td> <td>Data Not Found</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case Test Assignment is not found.</td> </tr> <tr> <td>ADP-1904</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case referenced Test Assignment status is 'Scheduled'.</td> </tr> <tr> <td>ADP-1911</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case of no results found for a Test Session.</td> </tr> <tr> <td>ADP-1912</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case web service is called for a Test Assignment that is not 'submitted', 'completed' or 'canceled'.</td> </tr> <tr> <td>ADP-1913</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case there is no form details for a Test Session.</td> </tr> </table> PHP Code for getting Test Results Details: <pre class=\"pseudo-code\"> date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server uses NTP service.<br/> <br/> $testAssignmentId = ...<br/> <br/> $apiHost = &quot;https://adp.parcc.dev&quot;;<br/> $apiURI = &quot;/api/results/&quot; . $testAssignmentId;<br/> $username = &quot;DocumentationLDR&quot;;<br/> $password = &quot;97f0ca00610888a0e9e58883dabc813b&quot;;<br/> $secret = &quot;6d393cf3b856c34ee5c55f7c50dfeb49&quot;;<br/> <br/> $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/> $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix timestamp.<br/> $hash = hash_hmac('sha256', $message, $secret);<br/> $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/> <br/> $ch = curl_init($apiHost . $apiURI);<br/> curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>    &quot;Content-Type&quot;: &quot;application/json; charset=UTF-8&quot;,<br/>    &quot;Authorization: Basic &quot; . base64_encode($username . &quot;:&quot; . $password),<br/>    &quot;Authentication:&quot; . $authentication<br/> ]);<br/> <br/> $result = curl_exec($ch);<br/> curl_close($ch);<br/> ... </pre><br/></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "username",
            "defaultValue": "DocumentationLDR",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Username can be entered in the form.<br/> It's 20 characters long unique string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "password",
            "defaultValue": "97f0ca00610888a0e9e58883dabc813b",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Password can be entered in the form.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "secret",
            "defaultValue": "6d393cf3b856c34ee5c55f7c50dfeb49",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Secret can be entered in the form.<br/> It's 32 characters long MD5 hash string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "defaultValue": "Base64EncodedStringXXXXXXXXXX==",
            "description": "<p>HMAC Authentication using Secret key as salt for SHA256 hashing of the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Basic Base64EncodedStringYYYYYYYYYY==",
            "description": "<p>Basic Authentication using Username and Password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nGET /api/results/1 HTTP/1.1\nHost: adp.parcc.dev\nUser-Agent: curl/7.40.0\nAccept: * / *\nContent-Type: application/json; charset=UTF-8\nAuthorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==\nAuthentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky\nContent-Length: 89",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "adpTestAssignmentId",
            "defaultValue": "1",
            "description": "<p>Test Assignment ID is an ID used as a reference to LDR's Test Assignment ID which is unique inside LDR, but may not be the same as those Test Assignments created by PRC for Practice Tests and Quizzes.<br/> It's 32-bit unsigned unique integer.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes LDR Test Assignment ID, ADP Test Results ID, Student ID, Test ID, Test Form ID, Test Form Revision ID, Date and Time when Test was started, Date and Time when Test was either Submitted or Canceled, and its Status.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"testAssignmentId\": 2,\n  \"studentId\": 3,\n  \"testId\": 1,\n  \"testFormId\": 3,\n  \"testFormRevisionId\": 5,\n  \"testResultsId\": 100,\n  \"startTimestamp\": \"2015-05-05 05:05:05\",\n  \"endTimestamp\": \"2015-05-05 06:05:05\",\n  \"status\": \"Submitted\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 9\n  },\n  \"result\": {\n    \"testAssignmentId\": 2,\n    \"studentId\": 3,\n    \"testId\": 1,\n    \"testFormId\": 3,\n    \"testFormRevisionId\": 5,\n    \"testResultsId\": 100,\n    \"startTimestamp\": \"2015-05-05 05:05:05\",\n    \"endTimestamp\": \"2015-05-05 06:05:05\",\n    \"status\": \"Submitted\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"statusCode\": 500,\n  \"statusDescription\": \"Internal Server Error\",\n  \"errorCode\": \"ADP-1901\",\n  \"errorDescription\": \"Internal Error.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 500,\n    \"statusDescription\": \"Internal Server Error\",\n    \"errorCode\": \"ADP-1901\",\n    \"errorDescription\": \"Internal Error.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/resultsDetails.php",
    "groupTitle": "ADP_Publisher",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/results/{adpTestAssignmentId}"
      }
    ]
  },
  {
    "group": "ADP_Publisher",
    "type": "PATCH",
    "url": "/api/test/{adpTestFormRevisionId}",
    "title": "Test Activate/Deactivate",
    "name": "Test_Activate_Deactivate",
    "permission": [
      {
        "name": "ACR_PRC",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>ACR</b> and <b>PRC</b><br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Test web service is exclusively used by ACR and PRC for Test Package Publishing. It validates multiple parameters before it allows client to login into ADP Publisher application.<br/> Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/> Once Test is Published, it is automatically made Active (Online) which means that Test Assignments can be created for it and it can be taken. However, if any issue is discovered with Published Test, it can be Deactivated (updated by making it Inactive/Offline). Once issue is resolved, it can be Activated (updated by making it Active/Online) again.<br/> Similar to publishing, a Request Job for Activating and Deactivating is created first, and is processed as soon as possible according to its priority and other pending Jobs in the ADP Publisher's Job Queue.<br/><br/> In case of successful request, it returns a Request Job ID which is required in case Requested Job needs to be canceled. It also returns Date and Time when that Request Job was created and current Status of the Requested Job. However, a Job can be canceled only if its processing hasn't started yet.<br/> In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server.<br/><br/> If subsequent request for Activating/Deactivating the same Test Package is sent while the previous one is still being processed, ADP Publisher will simply return the same request as the first time and will not create new Request Job. If that Test Package was already been successfully activated/deactivated, it will return ADP Test Delivery's Test Battery Form Revision ID, Date and Time when that Test Package was updated, and its current Status.<br/><br/> Once actual Test Activation/Deactivation is completed, ADP Publisher will send back a notification with details about the outcome of Test Activation/Deactivation.<br/> In case of successful Activation/Deactivation, it will provide Test Battery Form Revision ID, Date and Time when Test Package was updated. In case of Practice Test or Quiz, it will also return previously generated Test Key. If successfully activated/deactivated Test is a Diagnostic Test, it will also notify LDR about successful outcome, and in case of successfully activated/deactivated Practice Test or Quiz it will also notify PRC.<br/><br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> These are possible Test Activate/Deactivate specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <tr> <td>ADP-1753</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case of validation errors.</td> </tr> <tr> <td>ADP-1756</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case of a not found testFormRevision.</td> </tr> <tr> <td>ADP-1757</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case if user is unauthorized.</td> </tr> </table> PHP Code for Test Properties Update: <pre class=\"pseudo-code\"> date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server uses NTP service.<br/> <br/> $testFormRevisionId = ...<br/> $publishedBy = ...<br/> $adpTestFormRevisionId = ...; // ADP's Test Battery Form Revision ID that was provided when it was published the first time.<br/> <br/> $apiHost = &quot;https://adp.parcc.dev&quot;;<br/> $apiURI = &quot;/api/test/&quot; . $adpTestFormRevisionId;<br/> $username = &quot;DocumentationACR&quot;;<br/> $password = &quot;635fbb063cbc28e8bf03ed12ca4cca00&quot;;<br/> $secret = &quot;51f48327961f6cb0da7d584a664cafba&quot;;<br/> <br/> $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/> $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix timestamp.<br/> $hash = hash_hmac('sha256', $message, $secret);<br/> $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/> <br/> $ch = curl_init($apiHost . $apiURI);<br/> curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>    &quot;Content-Type&quot;: &quot;application/json; charset=UTF-8&quot;,<br/>    &quot;Authorization: Basic &quot; . base64_encode($username . &quot;:&quot; . $password),<br/>    &quot;Authentication:&quot; . $authentication<br/> ]);<br/> curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([<br/>    'testFormRevisionId' =&gt; $testFormRevisionId,<br/>    'publishedBy' =&gt; $publishedBy,<br/>    'active' =&gt; false,<br/> ]));<br/> curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');<br/> <br/> $result = curl_exec($ch);<br/> curl_close($ch);<br/> ... </pre><br/></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "username",
            "defaultValue": "DocumentationACR",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Username can be entered in the form.<br/> It's 20 characters long unique string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "password",
            "defaultValue": "635fbb063cbc28e8bf03ed12ca4cca00",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Password can be entered in the form.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "secret",
            "defaultValue": "51f48327961f6cb0da7d584a664cafba",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Secret can be entered in the form.<br/> It's 32 characters long MD5 hash string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "defaultValue": "Base64EncodedStringXXXXXXXXXX==",
            "description": "<p>HMAC Authentication using Secret key as salt for SHA256 hashing of the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Basic Base64EncodedStringYYYYYYYYYY==",
            "description": "<p>Basic Authentication using Username and Password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nPATCH /api/test/10 HTTP/1.1\nHost: adp.parcc.dev\nUser-Agent: curl/7.40.0\nAccept: * / *\nContent-Type: application/json; charset=UTF-8\nAuthorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==\nAuthentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky\nContent-Length: 89",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "testFormRevisionId",
            "defaultValue": "i123456789012",
            "description": "<p>Test Battery Form Revision ID is an ID used as a reference for Test Package being published which is unique in a system requesting publishing and is used inside ADP only as an external reference to the source where Test Package came from.<br/> It's 26 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "publishedBy",
            "defaultValue": "John Smith",
            "description": "<p>First and/or Last Name of the Content Author initiating Test Package publishing. If neither is available, this can also be an email or a username of the Content Author.<br/> It's 100 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "adpTestFormRevisionId",
            "defaultValue": "10",
            "description": "<p>ADP Test Battery Form Revision ID is an unique ID assigned by ADP when the Test Package was originally published. As multiple systems can publish their own Test Packages, ADP has to use its own IDs in order to ensure they are unique.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "active",
            "defaultValue": "true, false",
            "description": "<p>Active indicates if Published Test Battery Form Revision should be Activate or Deactivated (Online or Offline).<br/> It's boolean.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Body Example",
          "content": "{\n  \"testFormRevisionId\": \"i123456789012\",\n  \"publishedBy\": \"John Smith\",\n  \"active\": false\n}",
          "type": "JSON"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes ADP Publisher's Request Job ID, Date and Time when Request Job was created.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"requestId\": 789,\n  \"publishedTimestamp\": \"2015-05-05 05:05:05\",\n  \"status\": \"Scheduled\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 3\n  },\n  \"result\": {\n    \"requestId\": 789,\n    \"publishedTimestamp\": \"2015-05-05 05:05:05\",\n    \"status\": \"Scheduled\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Successful Notification:",
          "content": "HTTP/1.1 200 OK\n{\n  \"testFormRevisionId\": 10,\n  \"publishedTimestamp\": \"2015-05-05 06:06:06\",\n  \"status\": \"Inactive\",\n  \"testKey\": \"PRACT-ABCDE\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Notification with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 4\n  },\n  \"result\": {\n    \"testFormRevisionId\": 10,\n    \"publishedTimestamp\": \"2015-05-05 06:06:06\",\n    \"status\": \"Inactive\",\n    \"testKey\": \"PRACT-ABCDE\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"statusCode\": 403,\n  \"statusDescription\": \"Forbidden\",\n  \"errorCode\": \"ADP-1753\",\n  \"errorDescription\": \"Access Denied.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 403,\n    \"statusDescription\": \"Forbidden\",\n    \"errorCode\": \"ADP-1753\",\n    \"errorDescription\": \"Access Denied.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/test.php",
    "groupTitle": "ADP_Publisher",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/test/{adpTestFormRevisionId}"
      }
    ]
  },
  {
    "group": "ADP_Publisher",
    "type": "PATCH",
    "url": "/api/test/properties/{adpTestId}",
    "title": "Test Properties Update",
    "name": "Test_Properties_Update",
    "permission": [
      {
        "name": "ACR_PRC",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>ACR</b> and <b>PRC</b><br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Test web service is exclusively used by ACR and PRC for Test Package Publishing. It validates multiple parameters before it allows client to login into ADP Publisher application.<br/> Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/> Once Test is Published, it is automatically made Active (Online) which means that Test Assignments can be created, if the Test's permission is set to 'Restricted' the Test will not be viewable and available to teachers and will only be available to the content team, once content team check and make sure everything look fine, then they update that permission value to 'Non-Restricted' which makes the test available for teachers.<br/> Similar to publishing, a Request Job for Test Properties Update is created first, and is processed as soon as possible according to its priority and other pending Jobs in the ADP Publisher's Job Queue.<br/><br/> In case of successful request, it returns a Request Job ID which is required in case Requested Job needs to be canceled. It also returns Date and Time when that Request Job was created and current Status of the Requested Job. However, a Job can be canceled only if its processing hasn't started yet.<br/> In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server.<br/><br/> If subsequent request for patching the same Test is sent while the previous one is still being processed, ADP Publisher will simply return the same request as the first time and will not create new Request Job.<br/><br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> This web service could be extended to update the Test Battery Form and the Test Battery Form Revision properties in the future.<br/><br/> These are possible Test Properties Update specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <tr> <td>ADP-1760</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case if the data sent to the server is not a valid JSON object.</td> </tr> <tr> <td>ADP-1761</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case if the batteryId property is missing.</td> </tr> <tr> <td>ADP-1762</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case if the batteryId property is missing.</td> </tr> <tr> <td>ADP-1763</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case if battery is not found.</td> </tr> <tr> <td>ADP-1764</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case if isActive property has an invalid value.</td> </tr> <tr> <td>ADP-1765</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case If one of the not allowed properties is being updated./td&gt; </tr> <tr> <td>ADP-1766</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case if a value is not in the expected range.</td> </tr> <tr> <td>ADP-1767</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Battery name or description does not meet the validation requirements.</td> </tr> <tr> <td>ADP-1769</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case the system is not able to send the new test details to LDR.</td> </tr> </table> PHP Code for Test Activate/Deactivate: <pre class=\"pseudo-code\"> date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server uses NTP service.<br/> <br/> $adpTestId = ...<br/> $batteryId = ...<br/> $name = ...<br/> $program = ...<br/> $scoreReport = ...<br/> $subject = ...<br/> $grade = ...<br/> $itemSelectionAlgorithm = ...<br/> $security = ...<br/> $multimedia = ...<br/> $scoring = ...<br/> $permissions = ...<br/> $privacy = ...<br/> $description = ...<br/> $isActive = ...<br/> <br/> $apiHost = &quot;https://adp.parcc.dev&quot;;<br/> $apiURI = &quot;/api/test/properties/&quot; . $adpTestId;<br/> $username = &quot;DocumentationACR&quot;;<br/> $password = &quot;635fbb063cbc28e8bf03ed12ca4cca00&quot;;<br/> $secret = &quot;51f48327961f6cb0da7d584a664cafba&quot;;<br/> <br/> $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/> $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix timestamp.<br/> $hash = hash_hmac('sha256', $message, $secret);<br/> $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/> <br/> $ch = curl_init($apiHost . $apiURI);<br/> curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>    &quot;Content-Type&quot;: &quot;application/json; charset=UTF-8&quot;,<br/>    &quot;Authorization: Basic &quot; . base64_encode($username . &quot;:&quot; . $password),<br/>    &quot;Authentication:&quot; . $authentication<br/> ]);<br/> curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([<br/>    'batteryId' =&gt; $batteryId,<br/>    'name' =&gt; $name,<br/>    'program' =&gt; $program,<br/>    'scoreReport' =&gt; $scoreReport,<br/>    'subject' =&gt; $subject,<br/>    'grade' =&gt; $grade,<br/>    'itemSelectionAlgorithm' =&gt; $itemSelectionAlgorithm,<br/>    'security' =&gt; $security,<br/>    'multimedia' =&gt; $multimedia,<br/>    'scoring' =&gt; $scoring,<br/>    'permissions' =&gt; $permissions,<br/>    'privacy' =&gt; $privacy,<br/>    'description' =&gt; $description,<br/>    'isActive' =&gt; $isActive<br/> ]));<br/> curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');<br/> <br/> $result = curl_exec($ch);<br/> curl_close($ch);<br/> ... </pre><br/></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "username",
            "defaultValue": "DocumentationACR",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Username can be entered in the form.<br/> It's 20 characters long unique string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "password",
            "defaultValue": "635fbb063cbc28e8bf03ed12ca4cca00",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Password can be entered in the form.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "secret",
            "defaultValue": "51f48327961f6cb0da7d584a664cafba",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Secret can be entered in the form.<br/> It's 32 characters long MD5 hash string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "defaultValue": "Base64EncodedStringXXXXXXXXXX==",
            "description": "<p>HMAC Authentication using Secret key as salt for SHA256 hashing of the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Basic Base64EncodedStringYYYYYYYYYY==",
            "description": "<p>Basic Authentication using Username and Password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nPATCH /api/test/properties/10 HTTP/1.1\nHost: adp.parcc.dev\nUser-Agent: curl/7.40.0\nAccept: * / *\nContent-Type: application/json; charset=UTF-8\nAuthorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==\nAuthentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky\nContent-Length: 89",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "adpTestId",
            "defaultValue": "1",
            "description": "<p>ADP Test Battery ID is an unique ID assigned by ADP when the Test Package was originally published. As multiple systems can publish their own Test Packages, ADP has to use its own IDs in order to ensure they are unique.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "batteryId",
            "defaultValue": "i123456789012",
            "description": "<p>Test Battery ID is an ID used as a reference for the published Test which is unique in a system requesting publishing and is used inside ADP only as an external reference to the source where Test came from.<br/> It's 26 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "defaultValue": "Math Test",
            "description": "<p>Test Battery's Name.<br/> It's 100 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "program",
            "defaultValue": "'Diagnostic Assessment','K2 Formative','Mid-Year/Interim','Practice Test','Quiz Test','Speaking & Listening','Summative'",
            "description": "<p>Test Battery's Program.<br/> It's one of the the above predefined strings.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "scoreReport",
            "defaultValue": "'None','Generic','ELA Decoding','ELA Reader Motivation Survey','ELA Reader Comprehension','ELA Vocabulary','Math Comprehension','Math Fluency'",
            "description": "<p>Test Battery's Score Report Type.<br/> It's one of the the above predefined strings.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "subject",
            "defaultValue": "'ELA','Math','Science','N/A'",
            "description": "<p>Test Battery's Subject.<br/> It's one of the the above predefined strings.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "grade",
            "defaultValue": "'K','1','2','3','4','5','6','7','8','9','10','11','12','Multi-level'",
            "description": "<p>Test Battery's Grade.<br/> It's one of the the above predefined strings.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "itemSelectionAlgorithm",
            "defaultValue": "'Fixed','Adaptive'",
            "description": "<p>Test Battery's Item Selection Algorithm.</br> It's one of the the above predefined strings.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "security",
            "defaultValue": "'Non-Secure','Secure'",
            "description": "<p>Indicator if Secure Browser is required for delivery of all Test Battery Form Revisions of the Test Battery. ’Non-Secure’ Test Batteries can be delivered in any Browser, whereas ’Secure’ can only be delivered inside the Secure Browser!<br/> It's one of the the above predefined strings.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "multimedia",
            "defaultValue": "'OnDemand','PreDownload','Embedded'",
            "description": "<p>The Item Selection Algorithm this property is not allowed to be updated at the moment, and updating it would trigger an error.<br/> It's one of the the above predefined strings.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "scoring",
            "defaultValue": "'Immediate','Delayed'",
            "description": "<p>Test Battery’s Scoring Methods. ’Immediate’ means that Test Driver will perform all required Scoring for the Test Battery. ’Delayed’ means that Test Driver will perform as much as possible of automated Scoring, but some additional Scoring is required after Test Delivery (this may be either Automated Delayed Scoring, Hand Scoring, or combination of both).<br/> It's one of the the above predefined strings.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "permissions",
            "defaultValue": "'Non-Restricted','Restricted'",
            "description": "<p>Indicator if Test Battery requires additional Permissions for Test Battery Form Assignments. ’Non-Restricted’ means that the Test Battery is available to all Rostering Users and all of them can create Test Battery Form Assignments for the related Test Battery. ’Restricted’ on the other hand requires special permissions, and only Rostering Users with such permissions can create Test Battery Form Assignments for the related Test Battery. This Indicator is only used by external Rostering System and does not affect System in any way.<br/> It's one of the the above predefined strings.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "privacy",
            "defaultValue": "'Public','Private'",
            "description": "<p>Indicator if Test Battery’s Content has special Privacy, License, Copyright or any other restrictions. This Indicator is only used by external Rostering System and does not affect System in any way.<br/> It's one of the the above predefined strings.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "description",
            "defaultValue": "Test Description...",
            "description": "<p>Test Battery’s Description.<br/> It's 4096 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "isActive",
            "defaultValue": "true, false",
            "description": "<p>Indicates if Test Battery should be Active or Inactive (Online or Offline).<br/> It's boolean.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Body Example",
          "content": "{\n  \"batteryId\": \"i1446833225635899\",\n  \"name\": \"Math Test\",\n  \"program\": \"Diagnostic Assessment\",\n  \"scoreReport\": \"Generic\",\n  \"subject\": \"Math\",\n  \"grade\": \"3\",\n  \"itemSelectionAlgorithm\": \"Fixed\",\n  \"security\": \"Non-Secure\",\n  \"multimedia\": \"Embedded\",\n  \"scoring\": \"Delayed\",\n  \"permissions\": \"Non-Restricted\",\n  \"privacy\": \"Private\",\n  \"description\": \"This is a simple Math Test.\",\n  \"isActive\": true\n}",
          "type": "JSON"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes ADP Publisher's Request Job ID, Date and Time when Request Job was created.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"requestId\": 789,\n  \"publishedTimestamp\": \"2015-05-05 05:05:05\",\n  \"status\": \"Scheduled\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 3\n  },\n  \"result\": {\n    \"requestId\": 789,\n    \"publishedTimestamp\": \"2015-05-05 05:05:05\",\n    \"status\": \"Scheduled\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"statusCode\": 403,\n  \"statusDescription\": \"Forbidden\",\n  \"errorCode\": \"ADP-1703\",\n  \"errorDescription\": \"Access Denied.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 403,\n    \"statusDescription\": \"Forbidden\",\n    \"errorCode\": \"ADP-1703\",\n    \"errorDescription\": \"Access Denied.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/test.php",
    "groupTitle": "ADP_Publisher",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/test/properties/{adpTestId}"
      }
    ]
  },
  {
    "group": "ADP_Publisher",
    "type": "POST",
    "url": "/api/test",
    "title": "Test Publishing",
    "name": "Test_Publishing",
    "permission": [
      {
        "name": "ACR_PRC",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>ACR</b> and <b>PRC</b><br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Test web service is exclusively used by ACR and PRC for Test Package Publishing. It validates multiple parameters before it allows client to login into ADP Publisher application.<br/> Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/> For Practice Tests and Quizzes, it also automatically generates Test Assignment with Test Key and Token.<br/> Once Test Publishing Request is received, if correct, ADP Publisher will create a Request Job in its Job Queue and will process it as soon as possible depending on its priority, and number and priority of other pending Request Jobs in its Job Queue.<br/><br/> In case of successful request, it returns a Request Job ID which is required in case Requested Job needs to be canceled. It also returns Date and Time when that Request Job was created and current Status of the Requested Job. However, a Job can be canceled only if its processing hasn't started yet.<br/> In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server.<br/><br/> If subsequent request for publishing the same Test Package is sent while the previous one is still being processed, ADP Publisher will simply return the same request as the first time and will not create new Request Job. If that Test Package was already been successfully published, it will return ADP Test Delivery's Test Battery Form Revision ID, Date and Time when that Test Package was published, and its current Status.<br/><br/> Once actual Test Publishing is completed, ADP Publisher will send back a notification with details about the outcome of Test Publishing.<br/> In case of successful publishing, it will provide Test Battery Form Revision ID, Date and Time when Test Package was published. In case of Practice Test or Quiz, it will also return generated Test Key. If successfully published Test is a Diagnostic Test, it will also notify LDR about successful outcome, and in case of successfully published Practice Test or Quiz it will also notify PRC.<br/> <br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> These are possible Test Publishing specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <tr> <td>ADP-1703</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case of a validation error.</td> </tr> <tr> <td>ADP-1707</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error that will contain a custom error message.</td> </tr> <tr> <td>ADP-1708</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case if the combination of name, grade and subject was already published.</td> </tr> <tr> <td>ADP-1709</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case the form name already exist for the actual battery.</td> </tr> </table> PHP Code for Test Publishing: <pre class=\"pseudo-code\"> date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server uses NTP service.<br/> <br/> $testFormRevisionId = ...<br/> $publishedBy = ...<br/> <br/> $apiHost = &quot;https://adp.parcc.dev&quot;;<br/> $apiURI = &quot;/api/test&quot;;<br/> $username = &quot;DocumentationACR&quot;;<br/> $password = &quot;635fbb063cbc28e8bf03ed12ca4cca00&quot;;<br/> $secret = &quot;51f48327961f6cb0da7d584a664cafba&quot;;<br/> <br/> $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/> $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix timestamp.<br/> $hash = hash_hmac('sha256', $message, $secret);<br/> $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/> <br/> $ch = curl_init($apiHost . $apiURI);<br/> curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>    &quot;Content-Type&quot;: &quot;application/json; charset=UTF-8&quot;,<br/>    &quot;Authorization: Basic &quot; . base64_encode($username . &quot;:&quot; . $password),<br/>    &quot;Authentication:&quot; . $authentication<br/> ]);<br/> curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([<br/>    'testFormRevisionId' =&gt; $testFormRevisionId,<br/>    'publishedBy' =&gt; $publishedBy<br/> ]));<br/> <br/> $result = curl_exec($ch);<br/> curl_close($ch);<br/> ... </pre><br/></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "username",
            "defaultValue": "DocumentationACR",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Username can be entered in the form.<br/> It's 20 characters long unique string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "password",
            "defaultValue": "635fbb063cbc28e8bf03ed12ca4cca00",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Password can be entered in the form.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "secret",
            "defaultValue": "51f48327961f6cb0da7d584a664cafba",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Secret can be entered in the form.<br/> It's 32 characters long MD5 hash string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "defaultValue": "Base64EncodedStringXXXXXXXXXX==",
            "description": "<p>HMAC Authentication using Secret key as salt for SHA256 hashing of the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Basic Base64EncodedStringYYYYYYYYYY==",
            "description": "<p>Basic Authentication using Username and Password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nPOST /api/test HTTP/1.1\nHost: adp.parcc.dev\nUser-Agent: curl/7.40.0\nAccept: * / *\nContent-Type: application/json; charset=UTF-8\nAuthorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==\nAuthentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky\nContent-Length: 89",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "testFormRevisionId",
            "defaultValue": "i123456789012",
            "description": "<p>Test Battery Form Revision ID is an ID used as a reference for Test Package being published which is unique in a system requesting publishing and is used inside ADP only as an external reference to the source where Test Package came from.<br/> It's 26 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "publishedBy",
            "defaultValue": "John Smith",
            "description": "<p>First and/or Last Name of the Content Author initiating Test Package publishing. If neither is available, this can also be an email or a username of the Content Author.<br/> It's 100 characters long string.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Body Example",
          "content": "{\n  \"testFormRevisionId\": \"i123456789012\",\n  \"publishedBy\": \"John Smith\"\n}",
          "type": "JSON"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes ADP Publisher's Request Job ID, Date and Time when Request Job was created.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"requestId\": 123,\n  \"publishedTimestamp\": \"2015-05-05 05:05:05\",\n  \"status\": \"Scheduled\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 3\n  },\n  \"result\": {\n    \"requestId\": 123,\n    \"publishedTimestamp\": \"2015-05-05 05:05:05\",\n    \"status\": \"Scheduled\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Successful Notification:",
          "content": "HTTP/1.1 200 OK\n{\n  \"testFormRevisionId\": 10,\n  \"publishedTimestamp\": \"2015-05-05 06:06:06\",\n  \"status\": \"Published\",\n  \"testKey\": \"PRACT-ABCDE\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Notification with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 4\n  },\n  \"result\": {\n    \"testFormRevisionId\": 10,\n    \"publishedTimestamp\": \"2015-05-05 06:06:06\",\n    \"status\": \"Published\",\n    \"testKey\": \"PRACT-ABCDE\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"statusCode\": 403,\n  \"statusDescription\": \"Forbidden\",\n  \"errorCode\": \"ADP-1703\",\n  \"errorDescription\": \"Access Denied.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 403,\n    \"statusDescription\": \"Forbidden\",\n    \"errorCode\": \"ADP-1703\",\n    \"errorDescription\": \"Access Denied.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/test.php",
    "groupTitle": "ADP_Publisher",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/test"
      }
    ]
  },
  {
    "group": "ADP_Publisher",
    "type": "PUT",
    "url": "/api/test/{adpTestFormRevisionId}",
    "title": "Test Re-Publishing",
    "name": "Test_Re_Publishing",
    "permission": [
      {
        "name": "ACR_PRC",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>ACR</b> and <b>PRC</b><br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Test web service is exclusively used by ACR and PRC for Test Package Publishing. It validates multiple parameters before it allows client to login into ADP Publisher application.<br/> Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/> For Practice Tests and Quizzes, it also automatically generates Test Assignment with Test Key and Token.<br/> Once Test Re-Publishing Request is received, it is processed in almost identical way as Test Publishing Request. The only difference is that the referenced Test Package already exists in ADP (or at least some parts of it), so Re-Publishing will simply repeat the whole publishing process in order to re-create any potentially missing data references inside ADP database and/or re-deploy any potentially missing files.<br/><br/> In case of successful request, it returns a Request Job ID which is required in case Requested Job needs to be canceled. It also returns Date and Time when that Request Job was created and current Status of the Requested Job. However, a Job can be canceled only if its processing hasn't started yet.<br/> In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server.<br/><br/> If subsequent request for re-publishing the same Test Package is sent while the previous one is still being processed, ADP Publisher will simply return the same request as the first time and will not create new Request Job. If that Test Package was already been successfully re-published, it will return ADP Test Delivery's Test Battery Form Revision ID, Date and Time when that Test Package was updated, and its current Status.<br/><br/> Once actual Test Re-Publishing is completed, ADP Publisher will send back a notification with details about the outcome of Test Re-Publishing.<br/> In case of successful re-publishing, it will provide Test Battery Form Revision ID, Date and Time when Test Package was re-published. In case of Practice Test or Quiz, it will also return previously generated Test Key (it will not generate a new one). If successfully re-published Test is a Diagnostic Test, it will also notify LDR about successful outcome, and in case of successfully re-published Practice Test or Quiz it will also notify PRC.<br/><br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> These are possible Test Re-Publishing specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <tr> <tr> <td>ADP-1703</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case of a validation error.</td> </tr> <tr> <td>ADP-1707</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error that will contain a custom error message.</td> </tr> <tr> <td>ADP-1708</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case if the combination of name, grade and subject was already published.</td> </tr> <tr> <td>ADP-1709</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case the form name already exist for the actual battery.</td> </tr> </table> PHP Code for Test Re-Publishing: <pre class=\"pseudo-code\"> date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server uses NTP service.<br/> <br/> $testFormRevisionId = ...<br/> $publishedBy = ...<br/> $adpTestFormRevisionId = ...; // ADP's Test Battery Form Revision ID that was provided when it was published the first time.<br/> <br/> $apiHost = &quot;https://adp.parcc.dev&quot;;<br/> $apiURI = &quot;/api/test/&quot; . $adpTestFormRevisionId;<br/> $username = &quot;DocumentationACR&quot;;<br/> $password = &quot;635fbb063cbc28e8bf03ed12ca4cca00&quot;;<br/> $secret = &quot;51f48327961f6cb0da7d584a664cafba&quot;;<br/> <br/> $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/> $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix timestamp.<br/> $hash = hash_hmac('sha256', $message, $secret);<br/> $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/> <br/> $ch = curl_init($apiHost . $apiURI);<br/> curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>    &quot;Content-Type&quot;: &quot;application/json; charset=UTF-8&quot;,<br/>    &quot;Authorization: Basic &quot; . base64_encode($username . &quot;:&quot; . $password),<br/>    &quot;Authentication:&quot; . $authentication<br/> ]);<br/> curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([<br/>    'testFormRevisionId' =&gt; $testFormRevisionId,<br/>    'publishedBy' =&gt; $publishedBy<br/> ]));<br/> curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');<br/> <br/> $result = curl_exec($ch);<br/> curl_close($ch);<br/> ... </pre><br/></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "username",
            "defaultValue": "DocumentationACR",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Username can be entered in the form.<br/> It's 20 characters long unique string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "password",
            "defaultValue": "635fbb063cbc28e8bf03ed12ca4cca00",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Password can be entered in the form.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "secret",
            "defaultValue": "51f48327961f6cb0da7d584a664cafba",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Secret can be entered in the form.<br/> It's 32 characters long MD5 hash string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "defaultValue": "Base64EncodedStringXXXXXXXXXX==",
            "description": "<p>HMAC Authentication using Secret key as salt for SHA256 hashing of the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Basic Base64EncodedStringYYYYYYYYYY==",
            "description": "<p>Basic Authentication using Username and Password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nPUT /api/test/10 HTTP/1.1\nHost: adp.parcc.dev\nUser-Agent: curl/7.40.0\nAccept: * / *\nContent-Type: application/json; charset=UTF-8\nAuthorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==\nAuthentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky\nContent-Length: 89",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "testFormRevisionId",
            "defaultValue": "i123456789012",
            "description": "<p>Test Battery Form Revision ID is an ID used as a reference for Test Package being published which is unique in a system requesting publishing and is used inside ADP only as an external reference to the source where Test Package came from.<br/> It's 26 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "publishedBy",
            "defaultValue": "John Smith",
            "description": "<p>First and/or Last Name of the Content Author initiating Test Package publishing. If neither is available, this can also be an email or a username of the Content Author.<br/> It's 100 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "adpTestFormRevisionId",
            "defaultValue": "10",
            "description": "<p>ADP Test Battery Form Revision ID is an unique ID assigned by ADP when the Test Package was originally published. As multiple systems can publish their own Test Packages, ADP has to use its own IDs in order to ensure they are unique.<br/> It's 32-bit unsigned unique integer.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Body Example",
          "content": "{\n  \"testFormRevisionId\": \"i123456789012\",\n  \"publishedBy\": \"John Smith\"\n}",
          "type": "JSON"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes ADP Publisher's Request Job ID, Date and Time when Request Job was created.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"requestId\": 456,\n  \"publishedTimestamp\": \"2015-05-05 05:05:05\",\n  \"status\": \"Scheduled\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 3\n  },\n  \"result\": {\n    \"requestId\": 456,\n    \"publishedTimestamp\": \"2015-05-05 05:05:05\",\n    \"status\": \"Scheduled\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Successful Notification:",
          "content": "HTTP/1.1 200 OK\n{\n  \"testFormRevisionId\": 10,\n  \"publishedTimestamp\": \"2015-05-05 06:06:06\",\n  \"status\": \"Published\",\n  \"testKey\": \"PRACT-ABCDE\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Notification with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 4\n  },\n  \"result\": {\n    \"testFormRevisionId\": 10,\n    \"publishedTimestamp\": \"2015-05-05 06:06:06\",\n    \"status\": \"Published\",\n    \"testKey\": \"PRACT-ABCDE\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"statusCode\": 403,\n  \"statusDescription\": \"Forbidden\",\n  \"errorCode\": \"ADP-1703\",\n  \"errorDescription\": \"Access Denied.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 403,\n    \"statusDescription\": \"Forbidden\",\n    \"errorCode\": \"ADP-1703\",\n    \"errorDescription\": \"Access Denied.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/test.php",
    "groupTitle": "ADP_Publisher",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/test/{adpTestFormRevisionId}"
      }
    ]
  },
  {
    "group": "ADP_Publisher",
    "type": "DELETE",
    "url": "/api/test/{adpTestFormRevisionId}",
    "title": "Test Un-Publishing",
    "name": "Test_Un_Publishing",
    "permission": [
      {
        "name": "ACR_PRC",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>ACR</b> and <b>PRC</b><br/> Status: <b style=\"color:#c00\">TBD</b><br/><br/> Test web service is exclusively used by ACR and PRC for Test Package Publishing. It validates multiple parameters before it allows client to login into ADP Publisher application.<br/> Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/> Similar to publishing, a Request Job for Un-Publishing is created first, and is processed as soon as possible according to its priority and other pending Jobs in the ADP Publisher's Job Queue. If a Request Job ID is given, then instead of Un-Publishing ADP Publisher will just cancel previously created Request Job.<br/><br/> In case of successful request, it returns a Request Job ID which is required in case Requested Job needs to be canceled. It also returns Date and Time when that Request Job was created and current Status of the Requested Job. However, a Job can be canceled only if its processing hasn't started yet.<br/> In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server.<br/><br/> If subsequent request for Un-Publishing the same Test Package is sent while the previous one is still being processed, ADP Publisher will simply return the same request as the first time and will not create new Request Job. If that Test Package was already been successfully un-published, it will return an error as ADP Publisher won't be able to find any references for such Test Package as all of them were already deleted.<br/><br/> Once actual Test Un-Publishing is completed, ADP Publisher will send back a notification with details about the outcome of Test Un-Publishing.<br/> In case of successful Un-Publishing, it will provide Test Battery Form Revision ID, Date and Time when Test Package was un-published. If successfully un-published test is a Diagnostic Test, it will also notify LDR about successful outcome, and in case of successfully un-published Practice Test or Quiz it will also notify PRC.<br/><br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> These are possible Test Un-Publishing specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1700</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case of missing or invalid format of the provided Test Battery Form Revision details.</td> </tr> </table> PHP Code for Test Un-Publishing: <pre class=\"pseudo-code\"> date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server uses NTP service.<br/> <br/> $testFormRevisionId = ...<br/> $publishedBy = ...<br/> $adpRequestId = ...; // ADP's Publishing Request Job ID that was provided when it was requested the first time.<br/> $adpTestFormRevisionId = ...; // ADP's Test Battery Form Revision ID that was provided when it was published the first time.<br/> <br/> $apiHost = &quot;https://adp.parcc.dev&quot;;<br/> $apiURI = &quot;/api/test/&quot; . $adpTestFormRevisionId;<br/> $username = &quot;DocumentationACR&quot;;<br/> $password = &quot;635fbb063cbc28e8bf03ed12ca4cca00&quot;;<br/> $secret = &quot;51f48327961f6cb0da7d584a664cafba&quot;;<br/> <br/> $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/> $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix timestamp.<br/> $hash = hash_hmac('sha256', $message, $secret);<br/> $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/> <br/> $ch = curl_init($apiHost . $apiURI);<br/> curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>    &quot;Content-Type&quot;: &quot;application/json; charset=UTF-8&quot;,<br/>    &quot;Authorization: Basic &quot; . base64_encode($username . &quot;:&quot; . $password),<br/>    &quot;Authentication:&quot; . $authentication<br/> ]);<br/> curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([<br/>    'testFormRevisionId' =&gt; $testFormRevisionId,<br/>    'publishedBy' =&gt; $publishedBy,<br/>    'requestId' =&gt; $adpRequestId // This is optional: If provided, instead of Un-Publishing, ADP Publisher will just cancel the<br/>                                 // previous request (if it isn't already started or completed).<br/> ]));<br/> curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');<br/> <br/> $result = curl_exec($ch);<br/> curl_close($ch);<br/> ... </pre><br/></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "username",
            "defaultValue": "DocumentationACR",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Username can be entered in the form.<br/> It's 20 characters long unique string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "password",
            "defaultValue": "635fbb063cbc28e8bf03ed12ca4cca00",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Password can be entered in the form.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "secret",
            "defaultValue": "51f48327961f6cb0da7d584a664cafba",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Secret can be entered in the form.<br/> It's 32 characters long MD5 hash string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "defaultValue": "Base64EncodedStringXXXXXXXXXX==",
            "description": "<p>HMAC Authentication using Secret key as salt for SHA256 hashing of the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Basic Base64EncodedStringYYYYYYYYYY==",
            "description": "<p>Basic Authentication using Username and Password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nDELETE /api/test/10 HTTP/1.1\nHost: adp.parcc.dev\nUser-Agent: curl/7.40.0\nAccept: * / *\nContent-Type: application/json; charset=UTF-8\nAuthorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==\nAuthentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky\nContent-Length: 89",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "testFormRevisionId",
            "defaultValue": "i123456789012",
            "description": "<p>Test Battery Form Revision ID is an ID used as a reference for Test Package being published which is unique in a system requesting publishing and is used inside ADP only as an external reference to the source where Test Package came from. <br/> It's 26 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "publishedBy",
            "defaultValue": "John Smith",
            "description": "<p>First and/or Last Name of the Content Author initiating Test Package publishing. If neither is available, this can also be an email or a username of the Content Author.<br/> It's 100 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "adpTestFormRevisionId",
            "defaultValue": "10",
            "description": "<p>ADP Test Battery Form Revision ID is an unique ID assigned by ADP when the Test Package was originally published. As multiple systems can publish their own Test Packages, ADP has to use its own IDs in order to ensure they are unique.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "requestId",
            "defaultValue": "789",
            "description": "<p>ADP Publisher's Request Job ID is an unique ID assigned by ADP Publisher when the original Request was created.<br/> <b>NOTE:</b> It must be provided in case original Request needs to be canceled (deleted) which is only possible in case its processing hasn't started yet.<br/> It's 32-bit unsigned unique integer.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Body Example",
          "content": "{\n  \"testFormRevisionId\": \"i123456789012\",\n  \"publishedBy\": \"John Smith\",\n  \"requestId\": 789\n}",
          "type": "JSON"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes ADP Publisher's Request Job ID, Date and Time when Request Job was created.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"requestId\": 789,\n  \"publishedTimestamp\": \"2015-05-05 05:05:05\",\n  \"status\": \"Scheduled\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 3\n  },\n  \"result\": {\n    \"requestId\": 789,\n    \"publishedTimestamp\": \"2015-05-05 05:05:05\",\n    \"status\": \"Scheduled\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Successful Notification:",
          "content": "HTTP/1.1 200 OK\n{\n  \"testFormRevisionId\": 10,\n  \"publishedTimestamp\": \"2015-05-05 06:06:06\",\n  \"status\": \"Unpublished\"\n}",
          "type": "json"
        },
        {
          "title": "Successful Notification with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 3\n  },\n  \"result\": {\n    \"testFormRevisionId\": 10,\n    \"publishedTimestamp\": \"2015-05-05 06:06:06\",\n    \"status\": \"Unpublished\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"statusCode\": 400,\n  \"statusDescription\": \"Bad Request\",\n  \"errorCode\": \"ADP-1701\",\n  \"errorDescription\": \"User is not defined.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 400,\n    \"statusDescription\": \"Bad Request\",\n    \"errorCode\": \"ADP-1701\",\n    \"errorDescription\": \"User is not defined.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/test.php",
    "groupTitle": "ADP_Publisher",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/test/{adpTestFormRevisionId}"
      }
    ]
  },
  {
    "group": "ADP_Quiz",
    "type": "GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD",
    "url": "/api/*",
    "title": "TBD",
    "name": "TBD",
    "permission": [
      {
        "name": "PRC",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li> </ol>"
      }
    ],
    "version": "1.0.0",
    "description": "<p>Client: <b>PRC</b><br/> Status: <b style=\"color:#c00\">TBD</b><br/><br/></p>",
    "filename": "src/quiz.php",
    "groupTitle": "ADP_Quiz",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/*"
      }
    ]
  },
  {
    "group": "ADP_Test_Delivery",
    "type": "GET",
    "url": "/assessment",
    "title": "Assessment",
    "name": "Assessment",
    "permission": [
      {
        "name": "Student",
        "title": "User Authentication:",
        "description": "<p>Not needed.</p>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>Student</b> - Launch of Test Driver for Diagnostic Test<br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Assessment web service is exclusively used by Students to launch Test Driver for Diagnostic Tests. It uses 2 simple captcha protections to check if page is being loaded by humans and to prohibit bots from launching Test Driver.<br/><br/> It always returns HTML Page and should never return errors.<br/><br/> More details about ADP Launching Test Driver can be found under Assessment Launch.<br/><br/></p>",
    "header": {
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nGET /assessment HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nReferer: https://adp.parcc.dev/\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "HTML",
            "optional": false,
            "field": "N/A",
            "description": "<p>It always returns Test Driver Launch Page.<br/> Anything else is considered as an error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n<!DOCTYPE html>\n<html...",
          "type": "json"
        }
      ]
    },
    "filename": "src/assessment.php",
    "groupTitle": "ADP_Test_Delivery",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/assessment"
      }
    ]
  },
  {
    "group": "ADP_Test_Delivery",
    "type": "POST",
    "url": "/assessment",
    "title": "Assessment Launch",
    "name": "Assessment_Launch",
    "permission": [
      {
        "name": "Student",
        "title": "User Authentication:",
        "description": "<p>Not needed.</p>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: Student - Launch of Test Driver for Diagnostic Test<br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Assessment Launch web service is exclusively used by Students when launching Test Driver. It can either be launched using Assessment Launch Page provided by Assessment web service in case of Diagnostic Tests, or from dedicated page in PRC for launching Practice Tests and Quizzes. It validates 2 captcha parameters in case of Diagnostic Tests, or Test Key in case of Practice Tests and Quizzes.<br/><br/> In case of successful request, it returns HTML Page which loads Test Driver.<br/> In case of failure, it returns a 403 Access Denied Page.<br/><br/> These are possible Assessment Launch specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1500</td> <td>There is a problem accessing this page. We are sorry for the inconvenience.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns 500 HTML page, but without actual Error (the actual Error is only logged into ADP application log file) in case of missing Tenant specific Test Driver Launch Theme (i.e. someone misconfigured Tenant details).</td> </tr> <tr> <td>ADP-1501</td> <td>You are not authorized to access this page.</td> <td>403</td> <td>Forbidden</td> <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP application log file) in case of malformed Test Driver launch Parameters (i.e. someone tampering with the launch page).</td> </tr> <tr> <td>ADP-1502</td> <td>You are not authorized to access this page.</td> <td>403</td> <td>Forbidden</td> <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP application log file) in case of malformed Test Driver launch Name (i.e. someone tampering with the launch page).</td> </tr> <tr> <td>ADP-1503</td> <td>You are not authorized to access this page.</td> <td>403</td> <td>Forbidden</td> <td>Returns 403 HTML page, but without actual Error (the actual Error is only logged into ADP application log file) in case of malformed Test Driver launch Test Key (i.e. someone tampering with the launch page).</td> </tr> </table><br/></p>",
    "header": {
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nPOST /assessment HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nReferer: https://adp.parcc.dev/\nContent-Type:application/x-www-form-urlencoded; charset=UTF-8\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "testKey",
            "defaultValue": "PRACTABCDE",
            "description": "<p>Test Key is all uppercase, 10-character long string that is unique for each Test Assignment. If it's for Practice Test or Quiz (starts with &quot;PRACTxxxxx&quot;), Test Driver will skip the Login screen and automatically login Anonymous Student. Otherwise, it will show Login screen and Student will be required to enter Test Key and other Login details. However, in case of Diagnostic Tests, this is not the actual Test Key, but a dummy one only used to validate launching of Test Driver.<br/> It's 10 characters long uppercase only unique string [A-Z].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name is dummy parameter used for captcha validation and should always be empty.<br/> It's an empty string.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "{Form Data} Request Body Example",
          "content": "testKey=PRACTABCDE&name=",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "HTML",
            "optional": false,
            "field": "N/A",
            "description": "<p>HTML Page which loads Test Driver.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n<!DOCTYPE html>\n<html...",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "HTML",
            "optional": false,
            "field": "N/A",
            "description": "<p>403 Access Denied HTML Page</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 403 Forbidden\n<!DOCTYPE html>\n<html...",
          "type": "json"
        }
      ]
    },
    "filename": "src/assessment.php",
    "groupTitle": "ADP_Test_Delivery",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/assessment"
      }
    ]
  },
  {
    "group": "ADP_Test_Delivery",
    "type": "GET",
    "url": "/api/content/{token}/{testId}/{contentFile='test.json'}",
    "title": "Content Download",
    "name": "Content",
    "permission": [
      {
        "name": "TestDriver_Content",
        "title": "User Authentication includes:",
        "description": "<ol>  <li>Token</li>  <li>Test Battery Form Revision ID</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>Test Driver</b><br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Content web service is exclusively used by Test Delivery for getting test content. It validates multiple parameters before it allows client to login into ADP Test Delivery application.<br/><br/> In case of successful authentication, it returns a 302 Redirect with a URL that will return the actual requested content file resource.<br/> In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server. These details are returned within response header, but the response body is omitted.<br/><br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> These are possible Content specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1300</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns error in response header without response body in case Token doesn't belong to any Test Assignment.</td> </tr> <tr> <td>ADP-1301</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns error in response header without response body in case Test Assignment is Deactivated.</td> </tr> <tr> <td>ADP-1302</td> <td>Access Denied.</td> <td>403</td> <td>Unauthorized</td> <td>Returns error in response header without response body in case Test Form Revision doesn't match Test Assignment.</td> </tr> <tr> <td>ADP-1303</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns error in response header without response body in case referenced Test is already Submitted.</td> </tr> <tr> <td>ADP-1304</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns error in response header without response body in case referenced Test is already Completed.</td> </tr> <tr> <td>ADP-1305</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns error in response header without response body in case referenced Test is Canceled.</td> </tr> <tr> <td>ADP-1306</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns error in response header without response body in case Token has expired.</td> </tr> <tr> <td>ADP-1307</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns error in response header without response body in case requested File Resource was not found.</td> </tr> <tr> <td>ADP-1308</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns error in response header without response body in case Test Content is Deactivated.</td> </tr> <tr> <td>ADP-16xx</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns error in response header without response body in case of failure to generate AWS CloudFront Signed URL. Refer to S3 Errors for more details about possible errors.</td> </tr> </table> JavaScript Code for Content: <pre class=\"pseudo-code\"> var apiHost = &quot;https://adp.parcc.dev&quot;;<br/> var apiURI = &quot;/api/content&quot;;<br/> <br/> var token = $('#token').val();<br/> var testId = $('#testId').val();<br/> <br/> $.ajax({<br/>    url: apiHost + apiURI + &quot;/&quot; + token + &quot;/&quot; + testId + (contentFile <span class=\"exclamation-mark\"/>== &quot;&quot; ? (&quot;/&quot; + contentFile) : &quot;&quot;),<br/>    type: 'GET',<br/>    ...<br/> }) </pre><br/></p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "defaultValue": "3c564954-0096-11e5-3123-10def1bd7269",
            "description": "<p>Token is unique per test assignment, including practice tests and quizzes. It is received as part of successful Login response. It also has an expiration timestamp and once expired it will not be valid anymore.<br/> It's 36 characters long unique GUID (lowercase only alphanumeric string with dashes).</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "testId",
            "defaultValue": "3",
            "description": "<p>Test Battery Form Revision ID currently being taken by the Student. This is returned by Login web service and is part of the Test Assignment.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "contentFile",
            "defaultValue": "sample.jpg, sample.png, sample.mp3, sample.mp4, sample.webm",
            "description": "<p>Content File Resource being requested. This is optional parameter and if omitted, by default &quot;test.json&quot; will be returned which is the test definition file which includes references to all other Content File Resources. All Content File Resource files are prefixed with 3 indexes separated by underscores (i.e. 0_0_0_) which indicate Test Part, Test Section and Test Item respectively – these are the sequence numbers within the current Test.<br/> It's 256 characters long string.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request URL Example",
          "content": "GET /api/content/3c564954-0096-11e5-3123-10def1bd7269/3/0_0_0_flower.jpg",
          "type": "URL"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "URL",
            "optional": false,
            "field": "N/A",
            "description": "<p>URL pointing to the requested Content File Resource. This is a signed URL for AWS CloudFront and is pointing to AWS S3 bucket which contains the actual file. Signed URL has expiration timestamp after which it is not valid anymore. Upon successful Login, ADP Test Delivery will generate AWS CloudFront signature for all URLs that apply to the related Test Battery Form Revision and those will be valid as long as related Token is valid. Once Token and URL Signature expire, a new Login is necessary in order to generate new ones.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 302 Found\nLocation: https://d111111abcdef8.cloudfront.net/flower.jpg?Policy=eyANCiAgICEXAMPLEW1lbnQiOiBbeyANCiAgICAgICJgTf\nsZXNvdXJjZSI6Imh0dHA6Ly9kemJlc3FtN3VuMW0wLmNsb3VkZnJvbnQubmV0L2RlbW8ucGhwIiwgDQogICAgICAiQ29uZGl0aW9uIjp7IA0KtR3\niCAgICAgICAgIklwQWRkcmVzcyI6eyJBV1M6U291cmNlSXAiOiIyMDcuMTcxLjE4MC4xMDEvMzIifSwNCiAgICAgICAgICJEYXRlR3JlYXRlc4Fd\nlRoYW4iOnsiQVdTOkVwb2NoVGltZSI6MTI5Njg2MDE3Nn0sDQogICAgICAgICAiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjEyOT2eF\nY4NjAyMjZ9DQogICAgICB9IA0KICAgfV0gDQp9DQo&Signature=nitfHRCrtziwO2HwPfWwyYDhUF5EwRunQAj19DzZrvDh6hQ73lDxar3UoufG\ncvvRQVw6EkCGdpGQyyOSKQimTxAnW7d8F5Kkai9HVx0FIu5jcQb0UEmatEXAMPLE3ReXySpLSMj0yCd3ZAB4UcBCAqEijkytL6f3fVYNGQI62RfT\n&Key-Pair-Id=APKA9ONS7QCOWEXAMPLE",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Header",
            "optional": false,
            "field": "N/A",
            "description": "<p>Response Header contains Status Code, Status Description, Error Code and Error Description, and current time on the server. Response Body is omitted.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 401 Unauthorized\nX-Error-Code: ADP-1300\nX-Error-Description: Access Denied.\nX-Time: 1426619032",
          "type": "json"
        }
      ]
    },
    "filename": "src/content.php",
    "groupTitle": "ADP_Test_Delivery",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/content/{token}/{testId}/{contentFile='test.json'}"
      }
    ]
  },
  {
    "group": "ADP_Test_Delivery",
    "type": "POST",
    "url": "/api/login",
    "title": "Login",
    "name": "Login",
    "permission": [
      {
        "name": "TestDriver_Login",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li>   <li>X-Requested-With header</li>   <li>Test Key</li>   <li>Personal ID</li>   <li>Date of Birth</li>   <li>State</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>Test Driver</b><br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Login web service is exclusively used by Test Delivery for initial authentication. It validates multiple parameters before it allows client to login into ADP Test Delivery application.<br/> Authentication includes: Basic Authentication, HMAC Authentication and Test Key validation.<br/><br/> For Diagnostic Tests, it also automatically assigns Test Battery Form Revision upon first successful login by the student. Test Battery Form Revisions for Practice Tests and Quizzes are assigned during publishing process which also generates Test Key for such tests. On the other hand, Test Keys for Diagnostic Tests are generated by LDR during Test Battery Form assignment process and are passed to ADP Test Delivery together with Student details.<br/><br/> In case of successful authentication, it returns a Token which is required for Test Driver to call other web services (Content and Results), API details, Test details, Student details, and Test Driver configuration with additional details.<br/> In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server.<br/><br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> These are possible Login specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1200</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Test Key is missing from the request body or is empty.</td> </tr> <tr> <td>ADP-1201</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case State is missing from the request body or is empty.</td> </tr> <tr> <td>ADP-1202</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Student's Personal Identifier is missing from the request body.</td> </tr> <tr> <td>ADP-1203</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Student's Date of Birth is missing from the request body.</td> </tr> <tr> <td>ADP-1204</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Test Key is in invalid format (i.e. contains invalid characters or has incorrect length).</td> </tr> <tr> <td>ADP-1205</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case State is invalid (invalid State abbreviation - only 50 states + DC + DoDEA + PT are allowed).</td> </tr> <tr> <td>ADP-1206</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Test Key doesn't match.</td> </tr> <tr> <td>ADP-1207</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Login Configuration doesn't exist for given State (Test Driver should always be synced with ADP as it always gets list of supported States from ADP, so this is only if someone is trying to tamper data).</td> </tr> <tr> <td>ADP-1208</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case Test Assignment without assigned Test Form Revision cannot be retrieved from database (i.e. corrupted database or record was deleted in the meantime).</td> </tr> <tr> <td>ADP-1209</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case Test Assignment with assigned Test Form Revision cannot be retrieved from database (i.e. corrupted database or record was deleted in the meantime).</td> </tr> <tr> <td>ADP-1210</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Student's Personal Identifier is in invalid format (not a 30-character long alphanumeric string).</td> </tr> <tr> <td>ADP-1211</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Student's Personal Identifier doesn't match.</td> </tr> <tr> <td>ADP-1212</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Student's Personal Identifier is not empty (according to Login Configuration it should be empty).</td> </tr> <tr> <td>ADP-1213</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Student's Date of Birth is in invalid format (not in a 'YYYY-MM-DD' format).</td> </tr> <tr> <td>ADP-1214</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Student's Date of Birth doesn't match.</td> </tr> <tr> <td>ADP-1215</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Student's Date of Birth is not empty (according to Login Configuration it should be empty).</td> </tr> <tr> <td>ADP-1216</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Student is deactivated.</td> </tr> <tr> <td>ADP-1217</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case State doesn't match Student's record.</td> </tr> <tr> <td>ADP-1218</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case Test details cannot be retrieved from database (i.e. foreign key points to a non-existing record).</td> </tr> <tr> <td>ADP-1219</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test is deactivated.</td> </tr> <tr> <td>ADP-1220</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case Test Form details cannot be retrieved from database (i.e. foreign key points to a non-existing record).</td> </tr> <tr> <td>ADP-1221</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test Form is deactivated.</td> </tr> <tr> <td>ADP-1222</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case no active Test Form Revision was found while trying to assign Test Form Revision to the Test Assignment.</td> </tr> <tr> <td>ADP-1223</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case Test Form Revision details cannot be retrieved from database (i.e. foreign key points to a non-existing record).</td> </tr> <tr> <td>ADP-1224</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test Form Revision is deactivated.</td> </tr> <tr> <td>ADP-1225</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test is already Submitted.</td> </tr> <tr> <td>ADP-1226</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test is already Completed.</td> </tr> <tr> <td>ADP-1227</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test is Canceled.</td> </tr> <tr> <td>ADP-1228</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test is already in progress.</td> </tr> <tr> <td>ADP-1229</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test is not yet In Progress, but there is a valid Test Session Token which hasn't expired yet (when there was a recent successful Login and Student is currently on one of the pretest pages).</td> </tr> <tr> <td>ADP-1230</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case of multiple consecutive failures to generate a unique Test Session Token (number of retries depends on the configuration).</td> </tr> </table> JavaScript Code for Login: <pre class=\"pseudo-code\"> // Time Offset represent time difference between server and client in seconds. It is important that all timestamps coming from the<br/> // client side are synced with the server in UTC time zone as client can be in any (possibly wrong) timezone, and client clock can<br/> // be off or can be tampered with. Therefore, anything related to time and date must be relative to a timestamp received form the<br/> // server which is always synchronized with Atomic Clock Server using NTP protocol.<br/> // Server time is always provided as 10-digit Unit timestamp in UTC timezone.<br/> var timeOffset = 1000 * serverTime - new Date().getTime();<br/> <br/> var apiHost = &quot;https://adp.parcc.dev&quot;;<br/> var apiURI = &quot;/api/login&quot;;<br/> var username = &quot;DocumentationTD&quot;;<br/> var password = &quot;53c5de17b5488f1a8549e6db1786ca81&quot;;<br/> var secret = &quot;578cca1aa71f702c26cc81439392124c&quot;;<br/> <br/> var testKey = $('#testKey').val();<br/> var dateOfBirth = $('#dateOfBirth').val();<br/> var studentId = $('#studentId').val();<br/> var state = $('#state').val();<br/> <br/> var timestamp = new Date().getTime() + timeOffset; // 13-digit Unix timestamp in UTC timezone.<br/> var nonce = Math.floor(Math.random() * 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> var message = apiURI + Math.round(timestamp / 1000) + nonce; // Message uses rounded 10-digit Unix timestamp.<br/> var hash = $.sha256hmac(secret, message);<br/> var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(hash));<br/> <br/> $.ajax({<br/>    url: apiHost + apiURI,<br/>    type: 'POST',<br/>    headers: {<br/>      'Content-Type': &quot;application/json; charset=UTF-8&quot;,<br/>      'Authorization': &quot;Basic &quot; + btoa(username + &quot;:&quot; + password),<br/>      'Authentication': authentication,<br/>      'X-Requested-With': btoa(testKey)<br/>    },<br/>    data: JSON.stringify({<br/>      testKey: testKey,<br/>      dateOfBirth: dateOfBirth,<br/>      studentId: studentId,<br/>      state: state<br/>    }),<br/>    ...<br/> }) </pre><br/></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "username",
            "defaultValue": "DocumentationTD",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Username can be entered in the form.<br/> It's 20 characters long unique string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "password",
            "defaultValue": "53c5de17b5488f1a8549e6db1786ca81",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Password can be entered in the form.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "secret",
            "defaultValue": "578cca1aa71f702c26cc81439392124c",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Secret can be entered in the form.<br/> It's 32 characters long MD5 hash string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "defaultValue": "Base64EncodedStringXXXXXXXXXX==",
            "description": "<p>HMAC Authentication using Secret key as salt for SHA256 hashing of the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Basic Base64EncodedStringYYYYYYYYYY==",
            "description": "<p>Basic Authentication using Username and Password.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-Requested-With",
            "defaultValue": "Base64EncodedStringZZZZZZZZZZ==",
            "description": "<p>Specific custom header with given Test Key which is allowed to be used only with valid CORS policy in place. This parameter is unique to Login and Results web services.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nPOST /api/login HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nContent-Type: application/json; charset=UTF-8\nAuthorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==\nAuthentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky\nX-Requested-With: QUFBQUFBQUFBQQ==\nReferer: https://adp.parcc.dev/TestDriver/\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "testKey",
            "defaultValue": "DOCUMENTAT",
            "description": "<p>Test Key is unique per test assignment for diagnostic tests, but is the same for any practice test or quiz.<br/> It's 10 characters long uppercase only unique string [A-Z].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dateOfBirth",
            "defaultValue": "2000-01-10",
            "description": "<p>Student's DOB. Should always be sent, even if empty.<br/> It's a date in the ANSI SQL-92 format ('YYYY-MM-DD').</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "studentId",
            "defaultValue": "1234567890",
            "description": "<p>Student's unique personal ID. Should always be sent, even if empty.<br/> It's 30 characters long string.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "state",
            "defaultValue": "IL",
            "description": "<p>State of student's school. Should always be sent, even if empty (i.e. in case of Practice Tests or Quizzes).<br/> It's 2 character long State predefined abbreviation (all uppercase string).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Body Example",
          "content": "{\n  \"testKey\": \"DOCUMENTAT\",\n  \"dateOfBirth\": \"2000-01-10\",\n  \"studentId\": \"1234567890\",\n  \"state\": \"IL\"\n}",
          "type": "JSON"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes Test, Candidate, Config, Result and Meta details.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"api\": {\n    \"catUrl\":\"https://int.dcm.adp.parcc-ads.breaktech.org\",\n    \"token\": \"0f6b5bf8-ccd8-11e4-b622-5453edaa89e5\",\n    \"expires\": 1426622632,\n    \"time\": 1426619032\n  },\n  \"config\": {\n    \"uploadResults\": {\n      \"timeInterval\": 60,\n      \"navto\": false,\n      \"navfrom\": false,\n      \"state\": false,\n      \"response\": false\n    }\n  },\n  \"candidate\": {\n    \"firstName\": \"John\",\n    \"lastName\": \"Smith\",\n    \"grade\": \"11\",\n    \"school\": \"Evanston Elementary\"\n  },\n  \"test\": {\n    \"id\": 1,\n    \"name\": \"Math Test G11\",\n    \"secure\": \"Non-Secure\",\n    \"media\": \"OnDemand\",\n    \"scoring\": \"Immediate\",\n    \"scoreReport\": \"None\",\n    \"itemSelection\": \"Fixed\",\n    \"grade\": \"11\",\n    \"description\": \"This is a sample Math Test.\",\n    \"status\": \"Scheduled\"\n  },\n  \"tools\": {\n    \"lineReader\": true,\n    \"textToSpeech\": true\n  }\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"api\": {\n      \"catUrl\":\"https://int.dcm.adp.parcc-ads.breaktech.org\",\n      \"token\": \"0f6b5bf8-ccd8-11e4-b622-5453edaa89e5\",\n      \"expires\": 1426622632,\n      \"time\": 1426619032\n    },\n    \"config\": {\n      \"uploadResults\": {\n        \"timeInterval\": 60,\n        \"navto\": false,\n        \"navfrom\": false,\n        \"state\": false,\n        \"response\": false\n      }\n    },\n    \"candidate\": {\n      \"firstName\": \"John\",\n      \"lastName\": \"Smith\",\n      \"grade\": \"11\",\n      \"school\": \"Evanston Elementary\"\n    },\n    \"test\": {\n      \"id\": 1,\n      \"name\": \"Math Test G11\",\n      \"secure\": \"Non-Secure\",\n      \"media\": \"OnDemand\",\n      \"scoring\": \"Immediate\",\n      \"scoreReport\": \"None\",\n      \"itemSelection\": \"Fixed\",\n      \"grade\": \"11\",\n      \"description\": \"This is a sample Math Test.\",\n      \"status\": \"Scheduled\"\n    },\n    \"tools\": {\n      \"lineReader\": true,\n      \"textToSpeech\": true\n    }\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"statusCode\": 401,\n  \"statusDescription\": \"Unauthorized\",\n  \"errorCode\": \"ADP-1200\",\n  \"errorDescription\": \"Missing Test Key.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 401,\n    \"statusDescription\": \"Unauthorized\",\n    \"errorCode\": \"ADP-1200\",\n    \"errorDescription\": \"Missing Test Key.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/login.php",
    "groupTitle": "ADP_Test_Delivery",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/login"
      }
    ]
  },
  {
    "group": "ADP_Test_Delivery",
    "type": "GET",
    "url": "/api/results/{token}/{testId}",
    "title": "Results Download",
    "name": "Results_Download",
    "permission": [
      {
        "name": "TestDriver_Results",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li>   <li>X-Requested-With header</li>   <li>Token</li>   <li>Test Battery Form Revision ID</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>Test Driver</b><br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Results web service is exclusively used by Test Delivery for uploading and downloading of Test Results. It validates multiple parameters before it allows client to interact with Test Results.<br/> Authentication includes: Basic Authentication, HMAC Authentication and Test Key validation.<br/><br/> Each time Test Results are uploaded to the server, ADP Test Delivery creates a snapshot of Test Results and keeps all previously uploaded snapshots of Test Results. In addition, it validates used Token and if valid, the most recently uploaded Test Results can be downloaded later as part of Test Resume process. However, if used Token has expired, or is not valid, ADP Test Delivery will accept uploaded Test Results, but will not use them during Test Resume process – these Test Results can be used for Test Resume only if manually activated (they are inactive by default), or can be used for further analysis.<br/><br/> In case of successful authentication, it returns the most recent snapshot of the Test Results in case Test Results were previously uploaded successfully at least once.<br/> In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server. In case of Results Download, these details are returned within response header, but the response body is omitted.<br/><br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> These are possible Content specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1400</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test is a Practice Test.</td> </tr> <tr> <td>ADP-1401</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Token doesn't match.</td> </tr> <tr> <td>ADP-1402</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Test Form Revision doesn't match.</td> </tr> <tr> <td>ADP-1403</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case referenced Test is already Submitted and cannot be resumed.</td> </tr> <tr> <td>ADP-1404</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case referenced Test is already Completed and cannot be resumed.</td> </tr> <tr> <td>ADP-1405</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case referenced Test is Canceled and cannot be resumed.</td> </tr> <tr> <td>ADP-1406</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case Test Token has expired.</td> </tr> <tr> <td>ADP-1407</td> <td>Data Not Found.</td> <td>404</td> <td>Not Found</td> <td>Returns JSON error in case no Test Results were found.</td> </tr> <tr> <td>ADP-16xx</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case Test Results failed to be downloaded from AWS S3. Refer to S3 Errors for more details about possible errors.</td> </tr> </table> JavaScript Code for Results Download: <pre class=\"pseudo-code\"> // Time Offset represent time difference between server and client in seconds. It is important that all timestamps coming from the<br/> // client side are synced with the server in UTC time zone as client can be in any (possibly wrong) timezone, and client clock can<br/> // be off or can be tampered with. Therefore, anything related to time and date must be relative to a timestamp received form the<br/> // server which is always synchronized with Atomic Clock Server using NTP protocol.<br/> // Server time is always provided as 10-digit Unit timestamp in UTC timezone.<br/> var timeOffset = 1000 * serverTime - new Date().getTime();<br/> <br/> var apiHost = &quot;https://adp.parcc.dev&quot;;<br/> var username = &quot;DocumentationTD&quot;;<br/> var password = &quot;53c5de17b5488f1a8549e6db1786ca81&quot;;<br/> var secret = &quot;578cca1aa71f702c26cc81439392124c&quot;;<br/> <br/> var testKey = $('#testKey').val();<br/> var token = $('#token').val();<br/> var testId = $('#testId').val();<br/> <br/> var apiURI = &quot;/api/results/&quot; + token + &quot;/&quot; + testId;<br/> <br/> var timestamp = new Date().getTime() + timeOffset; // 13-digit Unix timestamp in UTC timezone.<br/> var nonce = Math.floor(Math.random() * 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> var message = apiURI + Math.round(timestamp / 1000) + nonce; // Message uses rounded 10-digit Unix timestamp.<br/> var hash = $.sha256hmac(secret, message);<br/> var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(hash));<br/> <br/> $.ajax({<br/>    url: apiHost + apiURI,<br/>    type: 'GET',<br/>    headers: {<br/>      'Content-Type': &quot;application/json; charset=UTF-8&quot;,<br/>      'Authorization': &quot;Basic &quot; + btoa(username + &quot;:&quot; + password),<br/>      'Authentication': authentication,<br/>      'X-Requested-With': btoa(testKey)<br/>    },<br/>    ...<br/> }) </pre><br/></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "username",
            "defaultValue": "DocumentationTD",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Username can be entered in the form.<br/> It's 20 characters long unique string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "password",
            "defaultValue": "53c5de17b5488f1a8549e6db1786ca81",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Password can be entered in the form.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "secret",
            "defaultValue": "578cca1aa71f702c26cc81439392124c",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Secret can be entered in the form.<br/> It's 32 characters long MD5 hash string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "defaultValue": "Base64EncodedStringXXXXXXXXXX==",
            "description": "<p>HMAC Authentication using Secret key as salt for SHA256 hashing of the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Basic Base64EncodedStringYYYYYYYYYY==",
            "description": "<p>Basic Authentication using Username and Password.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-Requested-With",
            "defaultValue": "Base64EncodedStringZZZZZZZZZZ==",
            "description": "<p>Specific custom header with given Test Key which is allowed to be used only with valid CORS policy in place. This parameter is unique to Login and Results web services.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nGET /api/results/3c564954-0096-11e5-3123-10def1bd7269/3 HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nContent-Type: application/json; charset=UTF-8\nAuthorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==\nAuthentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky\nX-Requested-With: QUFBQUFBQUFBQQ==\nReferer: https://adp.parcc.dev/TestDriver/\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "testKey",
            "defaultValue": "DOCUMENTAT",
            "description": "<p>Test Key is unique per test assignment for diagnostic tests, but is the same for any practice test or quiz.<br/> It's 10 characters long uppercase only unique string [A-Z].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "defaultValue": "3c564954-0096-11e5-3123-10def1bd7269",
            "description": "<p>Token is unique per test assignment, including practice tests and quizzes. It is received as part of successful Login response. It also has an expiration timestamp and once expired it will not be valid anymore.<br/> It's 36 characters long unique GUID (lowercase only alphanumeric string with dashes).</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "testId",
            "defaultValue": "3",
            "description": "<p>Test Battery Form Revision ID currently being taken by the Student. This is returned by Login web service and is part of the Test Assignment.<br/> It's 32-bit unsigned unique integer.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request URL Example",
          "content": "GET /api/results/3c564954-0096-11e5-3123-10def1bd7269/3",
          "type": "URL"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset represents most recently uploaded Test Results.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"dynamic\" : {\n    \"id\" : \"11\",\n    \"state\" : {\n      \"value\" : null\n    },\n    \"events\" : [],\n    \"current\" : 0,\n    \"remainingTime\" : -4130\n  },\n  \"testPart\" : [\n    {\n      \"dynamic\" : {\n        \"state\" : {\n          \"value\" : null\n        },\n        \"events\" : [],\n        \"history\" : [],\n        \"current\" : 0\n      },\n      \"assessmentSection\" : [\n        {\n          \"dynamic\" : {\n            \"state\" : {\n              \"value\" : null\n            },\n            \"events\" : [],\n            \"history\" : [],\n            \"current\" : 12\n          },\n          \"assessmentItem\" : [\n            {\n              \"dynamic\" : {\n                \"state\" : {\n                  \"value\" : {\n                    \"RESPONSE\" : {\n                      \"response\" : {\n                        \"base\" : {\n                          \"integer\" : 31\n                        }\n                      }\n                    }\n                  }\n                },\n                \"response\" : {\n                  \"value\" : [\n                    {\n                      \"RESPONSE\" : {\n                        \"base\" : {\n                          \"integer\" : 31\n                        }\n                      }\n                    }\n                  ]\n                },\n                \"score\" : {\n                  \"value\" : {\n                    \"RESPONSE\" : {\n                      \"base\" : {\n                        \"integer\" : 31\n                      }\n                    },\n                    \"SCORE\" : {\n                      \"base\" : null\n                    }\n                  },\n                  \"error\" : false\n                },\n                \"events\" : [\n                  {\n                    \"type\" : \"render\",\n                    \"value\" : null,\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425089322380\n                  }, {\n                    \"type\" : \"loaded\",\n                    \"value\" : null,\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425089322414\n                  }, {\n                    \"type\" : \"init\",\n                    \"value\" : {\n                      \"RESPONSE\" : {\n                        \"response\" : {\n                          \"base\" : {\n                            \"integer\" : 0\n                          }\n                        }\n                      }\n                    },\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425089322419\n                  }, {\n                    \"type\" : \"render\",\n                    \"value\" : null,\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425181075666\n                  }, {\n                    \"type\" : \"loaded\",\n                    \"value\" : null,\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425181075673\n                  }, {\n                    \"type\" : \"state\",\n                    \"value\" : {\n                      \"RESPONSE\" : {\n                        \"response\" : {\n                          \"base\" : {\n                            \"integer\" : 29\n                          }\n                        }\n                      }\n                    },\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425181079491\n                  }, {\n                    \"type\" : \"response\",\n                    \"value\" : [\n                      {\n                        \"RESPONSE\" : {\n                          \"base\" : {\n                            \"integer\" : 29\n                          }\n                        }\n                      }\n                    ],\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425181079492\n                  }\n                ],\n                \"flagged\" : true\n              }\n            }\n          ]\n        }\n      ]\n    }\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 2\n  },\n  \"result\": {\n    \"dynamic\" : {\n      \"id\" : \"11\",\n      \"state\" : {\n        \"value\" : null\n      },\n      \"events\" : [],\n      \"current\" : 0,\n      \"remainingTime\" : -4130\n    },\n    \"testPart\" : [\n      {\n        \"dynamic\" : {\n          \"state\" : {\n            \"value\" : null\n          },\n          \"events\" : [],\n          \"history\" : [],\n          \"current\" : 0\n        },\n        \"assessmentSection\" : [\n          {\n            \"dynamic\" : {\n              \"state\" : {\n                \"value\" : null\n              },\n              \"events\" : [],\n              \"history\" : [],\n              \"current\" : 12\n            },\n            \"assessmentItem\" : [\n              {\n                \"dynamic\" : {\n                  \"state\" : {\n                    \"value\" : {\n                      \"RESPONSE\" : {\n                        \"response\" : {\n                          \"base\" : {\n                            \"integer\" : 31\n                          }\n                        }\n                      }\n                    }\n                  },\n                  \"response\" : {\n                    \"value\" : [\n                      {\n                        \"RESPONSE\" : {\n                          \"base\" : {\n                            \"integer\" : 31\n                          }\n                        }\n                      }\n                    ]\n                  },\n                  \"score\" : {\n                    \"value\" : {\n                      \"RESPONSE\" : {\n                        \"base\" : {\n                          \"integer\" : 31\n                        }\n                      },\n                      \"SCORE\" : {\n                        \"base\" : null\n                      }\n                    },\n                    \"error\" : false\n                  },\n                  \"events\" : [\n                    {\n                      \"type\" : \"render\",\n                      \"value\" : null,\n                      \"selector\" : {\n                        \"test\" : 0,\n                        \"section\" : 0,\n                        \"item\" : 0\n                      },\n                      \"time\" : 1425089322380\n                    }, {\n                      \"type\" : \"loaded\",\n                      \"value\" : null,\n                      \"selector\" : {\n                        \"test\" : 0,\n                        \"section\" : 0,\n                        \"item\" : 0\n                      },\n                      \"time\" : 1425089322414\n                    }, {\n                      \"type\" : \"init\",\n                      \"value\" : {\n                        \"RESPONSE\" : {\n                          \"response\" : {\n                            \"base\" : {\n                              \"integer\" : 0\n                            }\n                          }\n                        }\n                      },\n                      \"selector\" : {\n                        \"test\" : 0,\n                        \"section\" : 0,\n                        \"item\" : 0\n                      },\n                      \"time\" : 1425089322419\n                    }, {\n                      \"type\" : \"render\",\n                      \"value\" : null,\n                      \"selector\" : {\n                        \"test\" : 0,\n                        \"section\" : 0,\n                        \"item\" : 0\n                      },\n                      \"time\" : 1425181075666\n                    }, {\n                      \"type\" : \"loaded\",\n                      \"value\" : null,\n                      \"selector\" : {\n                        \"test\" : 0,\n                        \"section\" : 0,\n                        \"item\" : 0\n                      },\n                      \"time\" : 1425181075673\n                    }, {\n                      \"type\" : \"state\",\n                      \"value\" : {\n                        \"RESPONSE\" : {\n                          \"response\" : {\n                            \"base\" : {\n                              \"integer\" : 29\n                            }\n                          }\n                        }\n                      },\n                      \"selector\" : {\n                        \"test\" : 0,\n                        \"section\" : 0,\n                        \"item\" : 0\n                      },\n                      \"time\" : 1425181079491\n                    }, {\n                      \"type\" : \"response\",\n                      \"value\" : [\n                        {\n                          \"RESPONSE\" : {\n                            \"base\" : {\n                              \"integer\" : 29\n                            }\n                          }\n                        }\n                      ],\n                      \"selector\" : {\n                        \"test\" : 0,\n                        \"section\" : 0,\n                        \"item\" : 0\n                      },\n                      \"time\" : 1425181079492\n                    }\n                  ],\n                  \"flagged\" : true\n                }\n              }\n            ]\n          }\n        ]\n      }\n    ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"statusCode\": 401,\n  \"statusDescription\": \"Unauthorized\",\n  \"errorCode\": \"ADP-1400\",\n  \"errorDescription\": \"Authorization Failed.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 401,\n    \"statusDescription\": \"Unauthorized\",\n    \"errorCode\": \"ADP-1400\",\n    \"errorDescription\": \"Authorization Failed.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/results.php",
    "groupTitle": "ADP_Test_Delivery",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/results/{token}/{testId}"
      }
    ]
  },
  {
    "group": "ADP_Test_Delivery",
    "type": "POST",
    "url": "/api/results/{token}/{testId}/{testStatus='InProgress'}",
    "title": "Results Upload",
    "name": "Results_Upload",
    "permission": [
      {
        "name": "TestDriver_Results",
        "title": "User Authentication includes:",
        "description": "<ol>   <li>Basic Authentication</li>   <li>HMAC Authentication</li>   <li>X-Requested-With header</li>   <li>Token</li>   <li>Test Battery Form Revision ID</li> </ol>"
      }
    ],
    "version": "1.26.2",
    "description": "<p>Client: <b>Test Driver</b><br/> Status: <b style=\"color:green\">Completed</b><br/><br/> Results web service is exclusively used by Test Delivery for uploading and downloading of Test Results. It validates multiple parameters before it allows client to interact with Test Results.<br/> Authentication includes: Basic Authentication, HMAC Authentication and Test Key validation.<br/><br/> Each time Test Results are uploaded to the server, ADP Test Delivery creates a snapshot of Test Results and keeps all previously uploaded snapshots of Test Results. In addition, it validates used Token and if valid, the most recently uploaded Test Results can be downloaded later as part of Test Resume process. However, if used Token has expired, or is not valid, ADP Test Delivery will accept uploaded Test Results, but will not use them during Test Resume process – these Test Results can be used for Test Resume only if manually activated (they are inactive by default), or can be used for further analysis.<br/><br/> In case of successful authentication, it returns a confirmation about Results Upload, current Time on the server as a Unix Timestamp and the current Test Status. The Server Timestamp is used by Test Driver so it stays in sync with ADP Test Delivery and is constantly adjust its Time Offset. All used timestamps in generated Test Results are synced with the server and are in UTC Time Zone, regardless if the client computer has properly configured Date, Time and/or Time Zone, or if someone is trying to tamper Test Results by manually changing Date and/or Time. The returned current Test Status is used both as a confirmation, and as a request coming from the Proctor to change the current Test Status to received Test Status (i.e. request to immediately pause the current test session in which case Test Driver would enforce the Save &amp; Exit procedure to immediately pauses the current Test Session). In case of failure, it returns an error with error code and error description, HTTP status code and HTTP status description, and current time on the server. In case of Results Download, these details are returned within response header, but the response body is omitted.<br/><br/> More details about HMAC Authentication can be found under Authentication section.<br/> All possible generic API Errors can be found under Errors section.<br/><br/> These are possible Content specific errors:<br/> <table> <tr> <th>Error Code</th> <th>Error Description</th> <th>HTTP Code</th> <th>HTTP Status</th> <th>Note</th> </tr> <tr> <td>ADP-1450</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test is a Practice Test.</td> </tr> <tr> <td>ADP-1451</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case referenced Test Token doesn't match Test Session.</td> </tr> <tr> <td>ADP-1452</td> <td>Authorization Failed.</td> <td>401</td> <td>Unauthorized</td> <td>Returns JSON error in case referenced Test Form Revision doesn't match Test Session.</td> </tr> <tr> <td>ADP-1453</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case referenced Test is already Submitted and cannot be taken.</td> </tr> <tr> <td>ADP-1454</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case referenced Test is already Completed and cannot be taken.</td> </tr> <tr> <td>ADP-1455</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case referenced Test is Canceled and cannot be taken.</td> </tr> <tr> <td>ADP-1456</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test that is not in progress was tried to be Paused.</td> </tr> <tr> <td>ADP-1457</td> <td>Access Denied.</td> <td>403</td> <td>Forbidden</td> <td>Returns JSON error in case Test that is not in progress was tried to be Submitted.</td> </tr> <tr> <td>ADP-1458</td> <td>Invalid Request.</td> <td>406</td> <td>Not Acceptable</td> <td>Returns JSON error in case provided Test Status is invalid (neither Paused nor Submitted).</td> </tr> <tr> <td>ADP-1459</td> <td>Access Denied.</td> <td>408</td> <td>Request Timeout</td> <td>Returns JSON error in case Test Token has expired and test status is Scheduled.</td> </tr> <tr> <td>ADP-1460</td> <td>Access Denied.</td> <td>408</td> <td>Request Timeout</td> <td>Returns JSON error in case Test Token has expired and test status is Paused.</td> </tr> <tr> <td>ADP-1461</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case Results Schema cannot be loaded due to missing Results JSON Schema file on the server.</td> </tr> <tr> <td>ADP-1462</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case Results Schema cannot be loaded due to corrupted Results JSON Schema file on the server.</td> </tr> <tr> <td>ADP-1463</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case Results Schema cannot be loaded due to invalid Results JSON Schema file structure on the server.</td> </tr> <tr> <td>ADP-1464</td> <td>Invalid Request.</td> <td>412</td> <td>Precondition Failed</td> <td>Returns JSON error in case Results failed validation against Results JSON Schema and are not saved.</td> </tr> <tr> <td>ADP-1465</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case Test Results reference failed to be created in Database.</td> </tr> <tr> <td>ADP-1466</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case Test Results reference failed to be updated in Database with Test Results path.</td> </tr> <tr> <td>ADP-1467</td> <td>Access Denied.</td> <td>408</td> <td>Request Timeout</td> <td>Returns JSON error in case Test Results were successfully saved, but System Paused the Test by force due to expired Test Token.</td> </tr> <tr> <td>ADP-16xx</td> <td>Internal Error.</td> <td>500</td> <td>Internal Server Error</td> <td>Returns JSON error in case Test Results failed to be uploaded to AWS S3. Refer to S3 Errors for more details about possible errors.</td> </tr> <tr> <td>TD-1001</td> <td>There was a problem starting your test.</td> <td>N/A</td> <td>N/A</td> <td>This is Test Driver unique Error (not coming from ADP API) being displayed when Test is being Started/Resumed and Results were successfully uploaded, but Test Status did not change to &quot;InProgress&quot; as expected.</td> </tr> </table> JavaScript Code for Results Upload: <pre class=\"pseudo-code\"> // Time Offset represent time difference between server and client in seconds. It is important that all timestamps coming from the<br/> // client side are synced with the server in UTC time zone as client can be in any (possibly wrong) timezone, and client clock can<br/> // be off or can be tampered with. Therefore, anything related to time and date must be relative to a timestamp received form the<br/> // server which is always synchronized with Atomic Clock Server using NTP protocol.<br/> // Server time is always provided as 10-digit Unit timestamp in UTC timezone.<br/> var timeOffset = 1000 * serverTime - new Date().getTime();<br/> <br/> var apiHost = &quot;https://adp.parcc.dev&quot;;<br/> var username = &quot;DocumentationTD&quot;;<br/> var password = &quot;53c5de17b5488f1a8549e6db1786ca81&quot;;<br/> var secret = &quot;578cca1aa71f702c26cc81439392124c&quot;;<br/> <br/> var testKey = $('#testKey').val();<br/> var token = $('#token').val();<br/> var testId = $('#testId').val();<br/> <br/> var testStatus = $('#testStatus').val();<br/> var resultsFile = $('#resultsFile').val();<br/> <br/> var apiURI = &quot;/api/results/&quot; + token + &quot;/&quot; + testId + (testStatus <span class=\"exclamation-mark\"/>== &quot;&quot; ? (&quot;/&quot; + testStatus) : &quot;&quot;);<br/> <br/> var timestamp = new Date().getTime() + timeOffset; // 13-digit Unix timestamp in UTC timezone.<br/> var nonce = Math.floor(Math.random() * 99999); // 1-5 digit long random integer in [0, 99999) range.<br/> var message = apiURI + Math.round(timestamp / 1000) + nonce; // Message uses rounded 10-digit Unix timestamp.<br/> var hash = $.sha256hmac(secret, message);<br/> var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(hash));<br/> <br/> $.ajax({<br/>    url: apiHost + apiURI,<br/>    type: 'POST',<br/>    headers: {<br/>      'Content-Type': &quot;application/json; charset=UTF-8&quot;,<br/>      'Authorization': &quot;Basic &quot; + btoa(username + &quot;:&quot; + password),<br/>      'Authentication': authentication,<br/>      'X-Requested-With': btoa(testKey)<br/>    },<br/>    data: resultsFile,<br/>    ...<br/> }) </pre><br/></p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "username",
            "defaultValue": "DocumentationTD",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Username can be entered in the form.<br/> It's 20 characters long unique string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "password",
            "defaultValue": "53c5de17b5488f1a8549e6db1786ca81",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Password can be entered in the form.<br/> It's 60 characters long string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "secret",
            "defaultValue": "578cca1aa71f702c26cc81439392124c",
            "description": "<p>NOTE: This is a dummy field and is used only as a placeholder, so Secret can be entered in the form.<br/> It's 32 characters long MD5 hash string.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "defaultValue": "Base64EncodedStringXXXXXXXXXX==",
            "description": "<p>HMAC Authentication using Secret key as salt for SHA256 hashing of the request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Basic Base64EncodedStringYYYYYYYYYY==",
            "description": "<p>Basic Authentication using Username and Password.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-Requested-With",
            "defaultValue": "Base64EncodedStringZZZZZZZZZZ==",
            "description": "<p>Specific custom header with given Test Key which is allowed to be used only with valid CORS policy in place. This parameter is unique to Login and Results web services.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Headers Example",
          "content": "\nPOST /api/results/3c564954-0096-11e5-3123-10def1bd7269/3/pause HTTP/1.1\nHost: adp.parcc.dev\nOrigin: https://adp.parcc.dev\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0\nAccept: application/json, text/javascript; q=0.01\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nContent-Type: application/json; charset=UTF-8\nAuthorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==\nAuthentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky\nX-Requested-With: QUFBQUFBQUFBQQ==\nReferer: https://adp.parcc.dev/TestDriver/\nContent-Length: 89\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache",
          "type": "String"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "testKey",
            "defaultValue": "DOCUMENTAT",
            "description": "<p>Test Key is unique per test assignment for diagnostic tests, but is the same for any practice test or quiz.<br/> It's 10 characters long uppercase only unique string [A-Z].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "defaultValue": "3c564954-0096-11e5-3123-10def1bd7269",
            "description": "<p>Token is unique per test assignment, including practice tests and quizzes. It is received as part of successful Login response. It also has an expiration timestamp and once expired it will not be valid anymore.<br/> It's 36 characters long unique GUID (lowercase only alphanumeric string with dashes).</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "testId",
            "defaultValue": "3",
            "description": "<p>Test Battery Form Revision ID currently being taken by the Student. This is returned by Login web service and is part of the Test Assignment.<br/> It's 32-bit unsigned unique integer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "testStatus",
            "defaultValue": "'', 'paused', 'submit'",
            "description": "<p>Test Status that should be updated upon successful upload of Test Results. By default, it's omitted which means Test Status should be &quot;InProgress&quot;. In case Test Results are uploaded as part of Save &amp; Exit process, it should be &quot;pause&quot;, and in case of Test Submission it should be &quot;submit&quot;.<br/> It's one of the 3 predefined values.</p>"
          },
          {
            "group": "Parameter",
            "type": "JSON",
            "optional": false,
            "field": "resultsFile",
            "description": "<p>Actual Test Results being uploaded. ADP will validate uploaded Test Results against Test Results JSON schema and if valid, it will save them as new snapshot which may be used later for download to resume previously paused Test.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request Body Example",
          "content": "{\n  \"dynamic\" : {\n    \"id\" : \"11\",\n    \"state\" : {\n      \"value\" : null\n    },\n    \"events\" : [],\n    \"current\" : 0,\n    \"remainingTime\" : -4130\n  },\n  \"testPart\" : [\n    {\n      \"dynamic\" : {\n        \"state\" : {\n          \"value\" : null\n        },\n        \"events\" : [],\n        \"history\" : [],\n        \"current\" : 0\n      },\n      \"assessmentSection\" : [\n        {\n          \"dynamic\" : {\n            \"state\" : {\n              \"value\" : null\n            },\n            \"events\" : [],\n            \"history\" : [],\n            \"current\" : 12\n          },\n          \"assessmentItem\" : [\n            {\n              \"dynamic\" : {\n                \"state\" : {\n                  \"value\" : {\n                    \"RESPONSE\" : {\n                      \"response\" : {\n                        \"base\" : {\n                          \"integer\" : 31\n                        }\n                      }\n                    }\n                  }\n                },\n                \"response\" : {\n                  \"value\" : [\n                    {\n                      \"RESPONSE\" : {\n                        \"base\" : {\n                          \"integer\" : 31\n                        }\n                      }\n                    }\n                  ]\n                },\n                \"score\" : {\n                  \"value\" : {\n                    \"RESPONSE\" : {\n                      \"base\" : {\n                        \"integer\" : 31\n                      }\n                    },\n                    \"SCORE\" : {\n                      \"base\" : null\n                    }\n                  },\n                  \"error\" : false\n                },\n                \"events\" : [\n                  {\n                    \"type\" : \"render\",\n                    \"value\" : null,\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425089322380\n                  }, {\n                    \"type\" : \"loaded\",\n                    \"value\" : null,\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425089322414\n                  }, {\n                    \"type\" : \"init\",\n                    \"value\" : {\n                      \"RESPONSE\" : {\n                        \"response\" : {\n                          \"base\" : {\n                            \"integer\" : 0\n                          }\n                        }\n                      }\n                    },\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425089322419\n                  }, {\n                    \"type\" : \"render\",\n                    \"value\" : null,\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425181075666\n                  }, {\n                    \"type\" : \"loaded\",\n                    \"value\" : null,\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425181075673\n                  }, {\n                    \"type\" : \"state\",\n                    \"value\" : {\n                      \"RESPONSE\" : {\n                        \"response\" : {\n                          \"base\" : {\n                            \"integer\" : 29\n                          }\n                        }\n                      }\n                    },\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425181079491\n                  }, {\n                    \"type\" : \"response\",\n                    \"value\" : [\n                      {\n                        \"RESPONSE\" : {\n                          \"base\" : {\n                            \"integer\" : 29\n                          }\n                        }\n                      }\n                    ],\n                    \"selector\" : {\n                      \"test\" : 0,\n                      \"section\" : 0,\n                      \"item\" : 0\n                    },\n                    \"time\" : 1425181079492\n                  }\n                ],\n                \"flagged\" : true\n              }\n            }\n          ]\n        }\n      ]\n    }\n  ]\n}",
          "type": "JSON"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset represents most recently uploaded Test Results.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case SUCCESS).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Successful Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"api\": {\n    \"expires\": 1434252894,\n    \"time\": 1434249325\n  },\n  \"test\": {\n    \"status\": \"InProgress\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Successful Response with Envelope:",
          "content": "HTTP/1.1 200 OK\n{\n  \"meta\": {\n    \"status\": \"SUCCESS\",\n    \"count\": 2\n  },\n  \"result\": {\n    \"api\": {\n      \"expires\": 1434252894,\n      \"time\": 1434249325\n    },\n    \"test\": {\n      \"status\": \"InProgress\"\n    }\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "JSON",
            "optional": false,
            "field": "data",
            "description": "<p>Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current time on the server.<br/> It may contain ENVELOPE in which case it will also indicate number of child nodes in the response and its status (in this case ERROR).</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"statusCode\": 401,\n  \"statusDescription\": \"Unauthorized\",\n  \"errorCode\": \"ADP-1450\",\n  \"errorDescription\": \"Authorization Failed.\",\n  \"time\": 1426619032\n}",
          "type": "json"
        },
        {
          "title": "Error Response with Envelope:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"meta\": {\n    \"status\": \"ERROR\",\n    \"count\": 5\n  },\n  \"result\": {\n    \"statusCode\": 401,\n    \"statusDescription\": \"Unauthorized\",\n    \"errorCode\": \"ADP-1450\",\n    \"errorDescription\": \"Authorization Failed.\",\n    \"time\": 1426619032\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/results.php",
    "groupTitle": "ADP_Test_Delivery",
    "sampleRequest": [
      {
        "url": "http://int.tds.adp.parcc-ads.breaktech.org/api/results/{token}/{testId}/{testStatus='InProgress'}"
      }
    ]
  }
] });