@echo off
title PARCC-ADP Documentation Generator
cls
echo Generating ADP API Documentation...

:: Generate documentation.
call apidoc -i src -o .
:: Replace generated files with patched ones.
copy patch\index.html .
copy patch\main.js .
copy patch\utils\send_sample_request.js utils

:: Add additional files.
copy patch\jquery.sha256.min.js vendor

echo.
pause