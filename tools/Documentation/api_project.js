define({
  "name": "ADP Web Services Documentation",
  "version": "1.26.2",
  "description": "ADP Test Delivery & ADP Publisher APIs.",
  "title": "ADP Web Services Documentation",
  "url": "https://adp.parcc.dev",
  "sampleUrl": "http://int.tds.adp.parcc-ads.breaktech.org",
  "order": [
    "ADP_Common",
    "General",
    "Authentication",
    "Errors",
    "S3_Errors",
    "Install",
    "Update",
    "Clear_Cache",
    "Maintenance_Status",
    "Maintenance_Enable",
    "Maintenance_Disable",
    "Ping",
    "Status",
    "ADP_Test_Delivery",
    "Login",
    "Content",
    "Results_Download",
    "Results_Upload",
    "Assessment",
    "Assessment_Launch",
    "ADP_Publisher",
    "Test_Publishing",
    "Test_Re_Publishing",
    "Test_Properties_Update",
    "Test_Activate_Deactivate",
    "Test_Un_Publishing",
    "Assignment_Creation",
    "Assignment_Update",
    "Assignment_Deletion",
    "Results_Details",
    "ADP_Quiz",
    "TBD"
  ],
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2016-04-12T04:09:46.689Z",
    "url": "http://apidocjs.com",
    "version": "0.13.1"
  }
});