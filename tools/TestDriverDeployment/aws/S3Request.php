<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Tools\AWS;

use stdClass;

/**
 * Class S3Request
 *
 * This is the AWS S3 API RESTful HTTP Request and Response Handler. It relies on CURL library.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
final class S3Request
{
	/**
	 * @var boolean $isS3Request Indicates if Request is to S3 API (or to STS otherwise) (@default=true).
	 */
	private $isS3Request;
	/**
	 * @var string $method HTTP Request Method (GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD).
	 */
	private $method;
	/**
	 * @var string $key Object Key (URI).
	 */
	private $key;
	/**
	 * @var string $resource Final Object URI.
	 */
	private $resource = '';
	/**
	 * @var array $parameters Additional Request Parameters.
	 */
	private $parameters = [];
	/**
	 * @var array $headers HTTP Request Headers (i.e. "Content-Type", "Content-Disposition", "Content-Encoding" etc.).
	 */
	private $headers = [
		'Host' => '',
		'Date' => '',
		'Content-Type' => '',
		'Content-MD5' => ''
	];
	/**
	 * @var array $metaHeaders AWS S3 Specific AMZ Metadata Headers ("x-amz-meta-*" Headers).
	 */
	private $metaHeaders = [];

	/**
	 * @var mixed $fileHandler File Handler of the Object being uploaded, OR Object being downloaded.
	 */
	public $fileHandler = false;
	/**
	 * @var array $data Data Object being uploaded.
	 */
	public $data = false;
	/**
	 * @var integer $size Size of the Object being uploaded.
	 */
	public $size = 0;
	/**
	 * @var object $response AWS S3 API Response.
	 */
	public $response;


	/**
	 * Constructor.
	 *
	 * Sets S3 API Request Parameters.
	 *
	 * @param string  $method      HTTP Request Method
	 *                             (@example: {'GET'|'POST'|'PUT'|'PATCH'|'DELETE'|'OPTIONS'|'HEAD'}).
	 * @param string  $endpoint    AWS S3 URL (may also contain AWS S3 Bucket Name if $bucket parameter is empty)
	 *                             (@example:
	 *                             {'s3.amazonaws.com'|'{bucket}.s3.amazonaws.com'|'s3.amazonaws.com/{bucket}'}).
	 * @param string  $bucket      AWS S3 Bucket Name (may be omitted if included as part of $endpoint) (@default='').
	 * @param string  $key         Object Key (URI) (@default='').
	 * @param boolean $isS3Request Indicates if Request is to S3 API (or to STS otherwise) (@default=true).
	 */
	public function __construct($method, $endpoint, $bucket = '', $key = '', $isS3Request = true)
	{
		// Set Request Method and flag if Request is for S3 API.
		$this->method = $method;
		$this->isS3Request = $isS3Request;

		// Check if Request is for S3 API.
		if ($isS3Request === true) {
			// Add leading slash to the Key, if necessary.
			if (mb_strpos($key, '/') !== 0) {
				$key = '/' . $key;
			}
			$this->key = strtr(rawurlencode($key), ['%2F' => '/']);

			// Check if S3 Bucket is provided.
			if ($bucket !== '') {
				// Check if S3 Bucket Name is a valid subdomain and if it can be accessed as a Virtual Host.
				if ($this->isValidS3VirtualHost($bucket)) {
					// In case S3 Bucket should be accessed as a Virtual Host.
					$this->headers['Host'] = $bucket . '.' . $endpoint;
					$this->resource = '/' . $bucket . $this->key;
				} else {
					// In case S3 Bucket should be accessed using Path-style URL.
					$this->headers['Host'] = $endpoint;
					$this->key = '/' . $bucket . $this->key;
					$this->resource = $this->key;
				}
			} else {
				// In case S3 Bucket is provided as part of $endpoint.
				$this->headers['Host'] = $endpoint;
				$this->resource = $this->key;
			}

			// Generate Request Timestamp.
			$this->headers['Date'] = gmdate('D, d M Y H:i:s T');

		} else {
			// In case Request is for STS API.
			$this->headers['Host'] = $endpoint;
		}

		// Initiate S3 API Response.
		$this->response = new stdClass();
		$this->response->error = false;
		$this->response->body = null;
		$this->response->headers = [];
	}


	/**
	 * Validates if given AWS S3 Bucket Name is a valid Virtual Host Name (Subdomain to be used to access S3)
	 * (@example: '{bucket}.s3.aws.com').
	 *
	 * @param  string  $bucket S3 Bucket Name.
	 * @return boolean         Returns TRUE if given Bucket Name confirms to AWS S3 naming convention; FALSE otherwise.
	 */
	private function isValidS3VirtualHost($bucket)
	{
		// Check if given S3 Bucket is a valid Virtual Host Name.
		if (isset($bucket[63]) || preg_match('/[^a-z0-9\.-]/', $bucket) > 0) {
			return false;
		} elseif (S3::$useHttps && mb_strstr($bucket, '.') !== false) {
			return false;
		} elseif (mb_strstr($bucket, '-.') !== false) {
			return false;
		} elseif (mb_strstr($bucket, '..') !== false) {
			return false;
		} elseif (!preg_match('/^[0-9a-z]/', $bucket)) {
			return false;
		} elseif (!preg_match('/[0-9a-z]$/', $bucket)) {
			return false;
		} else {
			return true;
		}
	}


	/**
	 * Sets Additional S3 Request Parameters.
	 *
	 * @param string $key   Parameter Name.
	 * @param string $value Parameter Value.
	 */
	public function setParameter($key, $value)
	{
		$this->parameters[$key] = $value;
	}


	/**
	 * Sets S3 Request Headers. These Headers are mostly used as Response Headers when uploaded Object is being
	 * downloaded by the Client (i.e. "Content-Type", "Content-Disposition", "Content-Encoding" etc.).
	 *
	 * @param string $key   Header Name.
	 * @param string $value Header Value.
	 */
	public function setHeader($key, $value)
	{
		$this->headers[$key] = $value;
	}


	/**
	 * Sets S3 API Request AMZ Meta Headers. These Headers are AWS S3 Specific AMZ Metadata Headers
	 * ("x-amz-meta-*" Headers). Some of them might be used as Response Headers when uploaded Object is being downloaded
	 * by the Client.
	 *
	 * @param string $key   Header Name.
	 * @param string $value Header Value.
	 */
	public function setMetaHeader($key, $value)
	{
		$this->metaHeaders[$key] = $value;
	}


	/** @noinspection PhpUnusedPrivateMethodInspection
	 * Processes S3 API Response Headers.
	 *
	 * @internal callable                        Used as CURL Response Header Callback.
	 * @param    resource $curlHandler           CURL Request Handler.
	 * @param    string   $responseHeaders       CURL Response Headers.
	 * @return   integer  $responseHeadersLength Returns the size (number of Bytes) written by the callback method.
	 */
	private function responseHeaderCallback(
		/** @noinspection PhpUnusedParameterInspection */ &$curlHandler,
		&$responseHeaders
	) {
		// Calculate the size of the Response Headers:
		$headersLength = strlen($responseHeaders);

		// Check if valid Response Headers are received (i.e. not just a simple "\r\n" confirmation).
		if ($headersLength <= 2) {
			// Return Size of received Response Headers.
			return $headersLength;
		}

		// Check if Response Headers only contain HTTP Status Code.
		if (mb_strpos($responseHeaders, 'HTTP') === 0) {
			// Parse HTTP Status Code.
			$this->response->code = (int) substr((string) $responseHeaders, 9, 3);
		} else {
			// Trim Response Headers.
			$responseHeaders = trim($responseHeaders);

			// Stop further processing if no valid Response Headers are found.
			if (mb_strpos($responseHeaders, ': ') === false) {
				// Return Size of received Response Headers.
				return $headersLength;
			}

			// Parse Response Headers.
			list($responseHeader, $responseHeaderValue) = explode(': ', $responseHeaders, 2);

			// Check if relevant Response Headers are received, and ignore all the others!
			if ($responseHeader === 'Cache-Control') {
				// Content Caching Directive.
				$this->response->headers['caching'] = (string) $responseHeaderValue;
			} elseif ($responseHeader === 'Content-Encoding') {
				// Content Encoding.
				$this->response->headers['encoding'] = (string) $responseHeaderValue;
			} elseif ($responseHeader === 'Content-Length') {
				// Content Size.
				$this->response->headers['size'] = (int) $responseHeaderValue;
			} elseif ($responseHeader === 'Content-Type') {
				// Content Type.
				$this->response->headers['type'] = (string) $responseHeaderValue;
			} elseif ($responseHeader === 'Date') {
				// Response Timestamp. This is only the timestamp when Response was generated!
				$this->response->headers['date'] = strtotime((string) $responseHeaderValue);
			} elseif ($responseHeader === 'ETag') {
				// Content ETag (MD5 Checksum).
				if ($responseHeaderValue{0} === '"') {
					// In case Etag is surrounded by double quotes.
					$this->response->headers['md5'] = mb_substr((string) $responseHeaderValue, 1, -1);
				} else {
					// In case Etag is not quoted.
					$this->response->headers['md5'] = (string) $responseHeaderValue;
				}
			} elseif ($responseHeader === 'Expires') {
				// Content Expiration Directive.
				$this->response->headers['expires'] = (string) $responseHeaderValue;
			} elseif ($responseHeader === 'Last-Modified') {
				// Last Modified Timestamp of the Content.
				$this->response->headers['timestamp'] = strtotime((string) $responseHeaderValue);
			} elseif ($responseHeader === 'x-amz-server-side-encryption') {
				// Server-Side Encryption Type of the Content.
				$this->response->headers['x-amz-server-side-encryption'] = (string) $responseHeaderValue;
			} elseif (preg_match('/^x-amz-meta-.*$/', (string) $responseHeader)) {
				// AWS S3 Specific AMZ Metadata of the Content.
				$this->response->headers[$responseHeader] = (string) $responseHeaderValue;
			}
		}

		// Return Size of received Response Headers.
		return $headersLength;
	}


	/** @noinspection PhpUnusedPrivateMethodInspection
	 * Processes S3 API Response Body.
	 *
	 * @internal callable               Used as CURL Response Write Callback.
	 * @param    resource $curlHandler  CURL Request Handler.
	 * @param    string   $responseBody CURL Response Body.
	 * @return   integer                Returns the size (number of Bytes) written by the callback method.
	 */
	private function responseWriteCallback(
		/** @noinspection PhpUnusedParameterInspection */ &$curlHandler,
		&$responseBody
	) {
		// Check if Response contains an Object which needs to be saved into a File Resource.
		if ($this->fileHandler !== false && in_array($this->response->code, [200, 206])) {
			// Save Object into existing File Resource (reference provided as part of S3Request initialization).
			return fwrite($this->fileHandler, (string) $responseBody);
		} else {
			// Append Response Body as S3 Response Property.
			$this->response->body .= (string) $responseBody;
		}

		// Return Size of received Response Body.
		return strlen($responseBody);
	}


	/**
	 * Sends the S3 Request and processes the S3 API Response.
	 *
	 * @return void S3Request class doesn't return anything as the actual Response is contained within the class itself.
	 */
	public function send()
	{
		// Check if Authentication should be performed before the actual S3 Request is being initiated. In case IAM Role
		// is used for Authentication, fresh Authentication Credentials should be acquired in order to successfully
		// complete S3 Request.
		if ($this->isS3Request === true) {
			// Authenticate against STS API, if necessary.
			S3::authenticate();

			// Check if Access Key ID and Secret Access Key are provided.
			if (empty(S3::$accessKeyId) || empty(S3::$secretAccessKey)) {
				// Return an Error in case Authentication Credentials are missing.
				$this->response->error = [
					'code' => 401,
					'message' => 'S3 Authentication Credentials are missing!'
				];

				return;

			} elseif (!empty(S3::$securityToken)) {
				// Add AWS STS Security Token to AMZ Metadata Headers, if necessary.
				$this->setMetaHeader('x-amz-security-token', S3::$securityToken);
			}

			// Initialize CURL Request.
			$curlHandler = curl_init();

			// Set CURL User Agent.
			curl_setopt($curlHandler, CURLOPT_USERAGENT, 'S3/Php');

			// Check if S3 Request should use HTTPS Protocol.
			if (S3::$useHttps === true) {
				// Set HTTPS Protocol Version.
				curl_setopt($curlHandler, CURLOPT_SSLVERSION, S3::USE_SSL_VERSION);

				// Set SSL Validation for AWS S3 Host and Peer.
				curl_setopt($curlHandler, CURLOPT_SSL_VERIFYHOST, S3::$useSslHostValidation === true ? 2 : 0);
				curl_setopt($curlHandler, CURLOPT_SSL_VERIFYPEER, S3::$useSslPeerValidation === true ? 1 : 0);
			}

			// Initialize Request Query with Additional Parameters.
			$query = '';

			// Check if Additional Request Parameters need to be set.
			if (count($this->parameters) > 0) {
				// Loop through Additional Parameters and append them to the Request Query.
				foreach ($this->parameters as $parameter => $parameterValue) {
					// Append Additional Parameter.
					$query .= '&' . $parameter;
					// Append Additional Parameter Value, if exists (including empty string).
					if (isset($parameterValue)) {
						$query .= '=' . rawurlencode($parameterValue);
					}
				}

				// Check if Object Key already has Request Query and prepend appropriate character to the Request Query
				// with Additional Parameters.
				if (mb_strpos($this->key, '?') === false) {
					$query = '?' . $query;
				} else {
					$query = '&' . $query;
				}

				// Append Additional Parameters to the Object Key.
				$this->key .= $query;
			}

			// Set Request URL for S3.
			curl_setopt(
				$curlHandler,
				CURLOPT_URL,
				(S3::$useHttps ? 'https://' : 'http://') . $this->headers['Host'] . $this->key
			);

			// Initialize Request Headers and AMZ Metadata Headers.
			$headers = [];
			$metaHeaders = [];

			// Loop through S3 Request AMZ Metadata Headers.
			foreach ($this->metaHeaders as $metaHeader => $metaHeaderValue) {
				// Skip AMZ Metadata Header if its value is an empty string.
				if (isset($metaHeaderValue[0])) {
					// Append AMZ Metadata Header to S3 Request Headers.
					$headers[] = $metaHeader . ': ' . $metaHeaderValue;
					// Append AMZ Metadata Header to AMZ Metadata Headers.
					$metaHeaders[] = mb_strtolower($metaHeader) . ':' . $metaHeaderValue;
				}
			}

			// Loop through S3 Request Headers.
			foreach ($this->headers as $header => $headerValue) {
				// Skip Header if its value is an empty string.
				if (isset($headerValue[0])) {
					// Append Header to S3 Request Headers.
					$headers[] = $header . ': ' . $headerValue;
				}
			}

			// Check if S3 Request contains any AMZ Metadata Headers.
			if (count($metaHeaders) > 0) {
				// Sort all S3 Request AMZ Metadata Headers according to AWS S3 API Authentication Signature
				// requirements.
				sort($metaHeaders);
				// Generate AMZ Metadata Headers.
				$metaHeaders = "\n" . implode("\n", $metaHeaders);
			} else {
				$metaHeaders = '';
			}

			// Check if Additional Request Parameters contain any of the special parameters that need to be included in
			// the S3 Request Authentication Signature.
			if (isset($this->parameters['acl']) || isset($this->parameters['location']) ||
				isset($this->parameters['torrent']) || isset($this->parameters['website']) ||
				isset($this->parameters['logging'])
			) {
				// Append Request Query with Additional Parameters to the Request Resource.
				$this->resource .= $query;
			}

			// Generate HMAC-SHA1 Hash as S3 Request Authentication Signature.
			$headers[] = 'Authorization: AWS ' . S3::$accessKeyId . ':' . S3::getHmacHash(
				$this->method . "\n" .
				$this->headers['Content-MD5'] . "\n" .
				$this->headers['Content-Type'] . "\n" .
				$this->headers['Date'] . $metaHeaders . "\n" .
				$this->resource
			);

			// Set S3 Request Headers.
			curl_setopt($curlHandler, CURLOPT_HTTPHEADER, $headers);

		} else {
			// Initialize CURL Request for AWS STS in order to acquire fresh temporary Authentication Credentials for
			// S3 API.
			$curlHandler = curl_init();

			// Set CURL User Agent.
			curl_setopt($curlHandler, CURLOPT_USERAGENT, 'S3/Php');

			// Set Request URL for AWS STS.
			curl_setopt($curlHandler, CURLOPT_URL, $this->headers['Host']);
		}


		// Configure S3 Response handling.
		curl_setopt($curlHandler, CURLOPT_HEADER, false);
		curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, false);
		curl_setopt($curlHandler, CURLOPT_FOLLOWLOCATION, true);

		// Set Callback Methods for handling S3 Response Body and Response Headers.
		curl_setopt($curlHandler, CURLOPT_WRITEFUNCTION, [&$this, 'responseWriteCallback']);
		curl_setopt($curlHandler, CURLOPT_HEADERFUNCTION, [&$this, 'responseHeaderCallback']);

		// Check HTTP Request Method and set additional properties if needed.
		switch ($this->method) {
			case 'GET':
				break;
			case 'HEAD':
				curl_setopt($curlHandler, CURLOPT_CUSTOMREQUEST, 'HEAD');
				curl_setopt($curlHandler, CURLOPT_NOBODY, true);
				break;
			case 'POST':
			case 'PUT':
			    // Check the S3 Request Body Type.
				if ($this->fileHandler !== false) {
					// In case of a File Resource being uploaded to S3.
					curl_setopt($curlHandler, CURLOPT_PUT, true);
					curl_setopt($curlHandler, CURLOPT_INFILE, $this->fileHandler);
					if ($this->size >= 0) {
						curl_setopt($curlHandler, CURLOPT_INFILESIZE, $this->size);
					}
				} elseif ($this->data !== false) {
					// In case of Raw Data being sent to S3.
					curl_setopt($curlHandler, CURLOPT_CUSTOMREQUEST, $this->method);
					curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $this->data);
				} else {
					// In case no data is being sent and Request contains only Request Headers.
					curl_setopt($curlHandler, CURLOPT_CUSTOMREQUEST, $this->method);
				}
				break;
			case 'DELETE':
				curl_setopt($curlHandler, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;
			default:
				// In case of OPTIONS request.
				break;
		}


		// Execute CURL as S3 Request and check if it was successful.
		if (curl_exec($curlHandler) === true) {
			// Parse S3 Response HTTP Status Code.
			$this->response->code = curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);
		} else {
			// Parse S3 Response Error.
			$this->response->error = [
				'code' => curl_errno($curlHandler),
				'message' => curl_error($curlHandler),
				'resource' => $this->resource
			];
		}

		// Close CURL Handler. Warnings and Errors must be silenced, so Application can handle them appropriately and be
		// able to recover without affecting the regular logical flow.
		@curl_close($curlHandler);


		// Parse S3 Response Body if received as XML and no CURL Error was been already triggered.
		if ($this->response->error === false && isset($this->response->headers['type']) &&
		    $this->response->headers['type'] === 'application/xml' && isset($this->response->body)
		) {
			// Parse S3 Response XML.
			$this->response->body = simplexml_load_string($this->response->body);

			// Check if S3 Response XML actually contains an Error.
			if (!in_array($this->response->code, [200, 204, 206]) &&
			    isset($this->response->body->Code, $this->response->body->Message)
			) {
				// Parse S3 Response XML Error.
				$this->response->error = [
					'code' => (string) $this->response->body->Code,
					'message' => (string) $this->response->body->Message
				];

				// Check if S3 Response XML Error contains a reference to a Resource.
				if (isset($this->response->body->Resource)) {
					// Parse referenced Resource from S3 Response XML.
					$this->response->error['resource'] = (string) $this->response->body->Resource;
				}

				// Remove S3 Response Body as it's redundant in case an Error was returned.
				unset($this->response->body);
			}
		}

		// Check if S3 Request was using a File Resource.
		if ($this->fileHandler !== false && is_resource($this->fileHandler)) {
			// Release the used File Resource. Warnings and Errors must be silenced, so Application can handle them
			// appropriately and be able to recover without affecting the regular logical flow.
			@fclose($this->fileHandler);
		}
	}
}
