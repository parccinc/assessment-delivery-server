<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Test Driver Deployment Settings for particular Environment.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 */
return [
	// Flag if 'OAT' folder should be skipped (@default=false).
	'skipOATFolder'      => false,
	// AWS S3 Access Key ID (should be empty if AWS IAM Role is assigned to the server!) (@default='').
	's3AccessKeyId'      => '',
	// AWS S3 Secret Access Key (should be empty if AWS IAM Role is assigned to the server!) (@default='').
	's3SecretAccessKey'  => '',
	// AWS S3 Test Driver Bucket Name.
	's3TestDriverBucket' => '',
	// AWS S3 Test Driver Prefix.
	's3TestDriverPrefix' => '',
	// AWS CloudFront URL (without trailing slash!).
	'cloudFrontURL'      => 'https://*.cloudfront.net',
	// Absolute path to target folder where Release version of Test Driver is located
	// (@default='/var/www/html/TD/release').
	'sourcePath'         => '/var/www/html/TD/release',
	// Absolute path to temp folder where Test Driver deployment package will be generated
	// (@default='/var/www/html/TD/temp').
	'tempPath'           => '/var/www/html/TD/temp',
	// Name of the Log file (@default='deploy_{timestamp}.log').
	'logFile'            => 'deploy_{timestamp}.log',
	// List of File Extensions for which both original and GZipped (compressed) version of the file should be uploaded
	// to AWS S3, so Browser can download compressed version if it supports GZip. Do NOT change!
	'gzipFileExtensions' => ['htm', 'html', 'xhtml', 'txt', 'js', 'json', 'css', 'xml', 'ttf', 'otf', 'eot', 'woff',
	                         'svg', 'ico']
];
