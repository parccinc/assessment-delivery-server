@echo off
cls
title Uninstalling PARCC-Vagrant Virtual Machine

:: Stop Vagrant Box.
echo Stopping PARCC-Vagrant Box...
vagrant halt
echo.

:: Destroy Vagrant Box (in VirtualBox).
echo Destroying PARCC-Vagrant Vagrant Box...
vagrant destroy -f
echo.

:: Uninstall Vagrant Box (in Vagrant).
echo Uninstalling PARCC-Vagrant Vagrant Box...
vagrant box remove PARCC-Vagrant
rmdir /s /q .vagrant\machines\PARCC-Vagrant\virtualbox .vagrant\machines\PARCC-Vagrant .vagrant\machines .vagrant
echo.

:: Remove DNS entries.
echo Removing DNS entries from Hosts file...
SET HOSTS=%WINDIR%\system32\drivers\etc\hosts
findstr /m /r /c:"^127.0.0.1	adp.parcc.dev ps.parcc.dev acr.parcc.dev	adp.isbe.dev ps.isbe.dev acr.isbe.dev$" ^
	%HOSTS%
if %errorlevel%==0 (
	findstr /v /r /c:"^127.0.0.1	adp.parcc.dev ps.parcc.dev acr.parcc.dev	adp.isbe.dev ps.isbe.dev acr.isbe.dev$"^
		%HOSTS% > %HOSTS%2
	del %HOSTS%
	ren %HOSTS%2 hosts
)
echo.

echo Uninstallation completed.
echo.
pause
