@echo off
title Stopping PARCC-Vagrant Virtual Machine
:: Stop Vagrant Box.
echo Stopping PARCC-Vagrant Box...
vagrant halt
echo.
pause
