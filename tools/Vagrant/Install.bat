@echo off
cls
title Installing PARCC-Vagrant Virtual Machine

:: Combine Vagrant Box file (due to 100MB limitation on Github it had to be split).
echo Combining PARCC-Vagrant Vagrant Box...
copy /B PARCC-Vagrant.00* PARCC-Vagrant.box
echo.

:: Install Vagrant Box.
echo Installing PARCC-Vagrant Vagrant Box...
vagrant box add PARCC-Vagrant PARCC-Vagrant.box
echo.

:: Delete temporary Vagrant Box.
del PARCC-Vagrant.box

:: Add DNS entries.
echo Adding DNS entries in Hosts file...
SET HOSTS=%WINDIR%\system32\drivers\etc\hosts
findstr /m /r /c:"^127.0.0.1	adp.parcc.dev ps.parcc.dev acr.parcc.dev	adp.isbe.dev ps.isbe.dev acr.isbe.dev$" ^
	%HOSTS%
if %errorlevel%==0 (
	echo DNS entries already exist. Skipping...
) else (
	echo.
	echo 127.0.0.1	adp.parcc.dev ps.parcc.dev acr.parcc.dev	adp.isbe.dev ps.isbe.dev acr.isbe.dev>> %HOSTS%
)
echo.

:: Start Vagrant Box.
call Start.bat
