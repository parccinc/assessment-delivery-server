@echo off
title SSH Connection to PARCC-Vagrant Virtual Machine
:: Connect to Vagrant Box over SSH.
echo Connecting to PARCC-Vagrant Box...
set PATH=%PATH%;C:\Program Files (x86)\Git\bin
vagrant ssh
