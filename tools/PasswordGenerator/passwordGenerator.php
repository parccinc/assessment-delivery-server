<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Password Generator is used to generate Login Credentials for ADP API.
 * Password should be handed to the End User. Password Hash and Secret should be manually imported into Database for the
 * related User.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */

// For security reasons limit access strictly to command line!
if (PHP_SAPI !== 'cli') {
	exit("This tool is only available from the command line!");
}


$targetTime = 0.01; // 10 milliseconds.
$cost = 4; // Minimum allowed cost for BCRYPT.

// Calculate the maximum cost that satisfies the targeted execution time.
do {
	$startTime = microtime(true);
	password_hash('test', PASSWORD_BCRYPT, ['cost' => $cost]);
	$endTime = microtime(true);
	echo "\nTesting Cost: {$cost} [", ($endTime - $startTime), "s]";
	$cost++;
} while ($endTime - $startTime < $targetTime);

echo "\nMinimum Cost Found: ", ($cost - 1), " [", ($endTime - $startTime), "s]\n";

// Generate Password, its Hash using calculated cost, and related Secret.
$password = md5(mt_rand());
echo "\nPassword     : ", $password,
	 "\nPassword Hash: ", password_hash($password, PASSWORD_BCRYPT, ['cost' => $cost]),
	 "\nSecret       : ", md5(mt_rand());
