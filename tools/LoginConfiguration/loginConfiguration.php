<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

// Report on ALL Errors!
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


use Phalcon\Config;
use Phalcon\Db\Adapter\Pdo\Mysql as MysqlConnection;
use Phalcon\Di\FactoryDefault as DefaultDI;

/**
 * Login Configuration is used to change and import configuration for Test Driver Login rules based on Student's State.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */

// For security reasons limit access strictly to command line!
if (PHP_SAPI !== 'cli') {
	exit("This tool is only available from the command line!");
}


// All 50 States + District of Columbia + DoDEA with their Abbreviations.
$states = [
	'AL' => 'Alabama',
	'AK' => 'Alaska',
	'AZ' => 'Arizona',
	'AR' => 'Arkansas',
	'CA' => 'California',
	'CO' => 'Colorado',
	'CT' => 'Connecticut',
	'DC' => 'District of Columbia',
	'DD' => 'Department of Defense Education Activity',
	'DE' => 'Delaware',
	'FL' => 'Florida',
	'GA' => 'Georgia',
	'HI' => 'Hawaii',
	'ID' => 'Idaho',
	'IL' => 'Illinois',
	'IN' => 'Indiana',
	'IA' => 'Iowa',
	'KS' => 'Kansas',
	'KY' => 'Kentucky',
	'LA' => 'Louisiana',
	'ME' => 'Maine',
	'MD' => 'Maryland',
	'MA' => 'Massachusetts',
	'MI' => 'Michigan',
	'MN' => 'Minnesota',
	'MS' => 'Mississippi',
	'MO' => 'Missouri',
	'MT' => 'Montana',
	'NE' => 'Nebraska',
	'NV' => 'Nevada',
	'NH' => 'New Hampshire',
	'NJ' => 'New Jersey',
	'NM' => 'New Mexico',
	'NY' => 'New York',
	'NC' => 'North Carolina',
	'ND' => 'North Dakota',
	'OH' => 'Ohio',
	'OK' => 'Oklahoma',
	'OR' => 'Oregon',
	'PA' => 'Pennsylvania',
	'PT' => 'Practice Test',
	'RI' => 'Rhode Island',
	'SC' => 'South Carolina',
	'SD' => 'South Dakota',
	'TN' => 'Tennessee',
	'TX' => 'Texas',
	'UT' => 'Utah',
	'VT' => 'Vermont',
	'VA' => 'Virginia',
	'WA' => 'Washington',
	'WV' => 'West Virginia',
	'WI' => 'Wisconsin',
	'WY' => 'Wyoming'
];


/**
 * Login Configuration.
 *
 * It includes a list of States and required fields for Login for each State (Key is the State Abbreviation):
 * @var string  $name State Full Name.
 * @var boolean $pid  Flag if related State requires Student's Personal ID for Login.
 * @var boolean $dob  Flag if related State requires Student's Date of Birth for Login.
 */

// Try to load Login Configuration files.
if (file_exists('loginConfiguration.json')) {
	$loginConfiguration = file_get_contents('loginConfiguration.json');
} else {
	// In case Login Configuration file is missing.
	exit("Login Configuration ERROR:\nFile 'loginConfiguration.json' is missing!");
}
if (file_exists('loginConfigurationPractice.json')) {
	$loginConfigurationPractice = file_get_contents('loginConfigurationPractice.json');
} else {
	// In case Login Configuration for Practice Tests file is missing.
	exit("Login Configuration ERROR:\nFile 'loginConfigurationPractice.json' is missing!");
}

// Check if Login Configuration files were successfully open.
if ($loginConfiguration === false) {
	// In case Login Configuration file failed to be opened.
	exit("Login Configuration ERROR:\nFile 'loginConfiguration.json' cannot be opened!");
} elseif ($loginConfigurationPractice === false) {
	// In case Login Configuration for Practice Tests file failed to be opened.
	exit("Login Configuration ERROR:\nFile 'loginConfigurationPractice.json' cannot be opened!");
}

// Try to parse loaded Login Configuration files.
$loginConfiguration = json_decode($loginConfiguration, true);
$loginConfigurationPractice = json_decode($loginConfigurationPractice, true);

// Check if Login Configuration files were successfully parsed.
if (!is_array($loginConfiguration)) {
	// In case Login Configuration file was not successfully parsed.
	exit("Login Configuration ERROR:\nFile 'loginConfiguration.json' is invalid!");
} elseif (!is_array($loginConfigurationPractice)) {
	// In case Login Configuration for Practice Tests file was not successfully parsed.
	exit("Login Configuration ERROR:\nFile 'loginConfigurationPractice.json' is invalid!");
}


/**
 * @var DefaultDI $di Default Dependency Injector.
 */
$di = new DefaultDI();

/**
 * Returns Application Configuration.
 *
 * @return Config Application Configuration.
 */
$di->setShared('config', function () {
	// Retrieve Application configuration.
	return require_once dirname(dirname(__DIR__)) . '/app/config/config.php';
});


// Validate Login Configuration first!
foreach ([$loginConfiguration, $loginConfigurationPractice] as $loginConfigurationFile) {
	// Validate current Login Configuration.
	foreach ($loginConfigurationFile as $state => $stateLoginConfiguration) {
		// Loop through all configured States and validate their Login Configuration.
		if (!array_key_exists($state, $states)) {
			// In case State Abbreviation is not in expected format (is not 2 uppercase characters).
			exit("Login Configuration ERROR:\n{$state} Abbreviation is misconfigured!");
		} elseif (!array_key_exists('name', $stateLoginConfiguration)) {
			// In case State doesn't have its Full Name.
			exit("Login Configuration ERROR:\n{$state} is missing its Full Name!");
		} elseif ($stateLoginConfiguration['name'] !== $states[$state]) {
			// In case State Full Name is invalid.
			exit("Login Configuration ERROR:\n{$state} Full Name is misconfigured!");
		} elseif (!array_key_exists('pid', $stateLoginConfiguration)) {
			// In case State doesn't have its Personal ID configuration.
			exit("Login Configuration ERROR:\n{$state} is missing its Personal ID configuration!");
		} elseif ($stateLoginConfiguration['pid'] !== true && $stateLoginConfiguration['pid'] !== false) {
			// In case State Personal ID configuration is invalid (is not a Boolean).
			exit("Login Configuration ERROR:\n{$state} Personal ID configuration is misconfigured!");
		} elseif (!array_key_exists('dob', $stateLoginConfiguration)) {
			// In case State doesn't have its Date of Birth configuration.
			exit("Login Configuration ERROR:\n{$state} is missing its Date of Birth configuration!");
		} elseif ($stateLoginConfiguration['dob'] !== true && $stateLoginConfiguration['dob'] !== false) {
			// In case State Date of Birth configuration is invalid (is not a Boolean).
			exit("Login Configuration ERROR:\n{$state} Date of Birth configuration is misconfigured!");
		}
	}
}


// Request User to enter Target Tenant ID.
echo "Please enter the Tenant ID you want Login Configuration to import for:\n";
// Retrieve User Input.
$stdin = fopen("php://stdin", 'r');
// Trim EOL character from User's input.
$tenantId = trim(fgets($stdin), "\n\r");
fclose($stdin);
// Check if entered Tenant ID is valid.
if (!preg_match('/^\d+$/', $tenantId)) {
	// In case entered Tenant ID is not a valid integer.
	exit("You have entered an invalid Tenant ID: '{$tenantId}'");
}


// Try to import Login Configuration into Database.
try {
	// Connect to Database.
	$db = new MysqlConnection((array) $di['config']->database);

	// Import Login Configuration.
	$success = $db->execute(
		'REPLACE INTO system VALUES(?, ?, ?, UTC_TIMESTAMP(), UTC_TIMESTAMP())',
		[
			'loginConfiguration',
			(int) $tenantId,
			json_encode($loginConfiguration)
		]
	);

	// Check if import of Login Configuration was successful.
	if ($success === true && $db->affectedRows() > 0) {
		// Import Login Configuration for Practice Tests.
		$success = $db->execute(
			'REPLACE INTO system VALUES(?, ?, ?, UTC_TIMESTAMP(), UTC_TIMESTAMP())',
			[
				'loginConfigurationPractice',
				(int) $tenantId,
				json_encode($loginConfigurationPractice)
			]
		);
	}

	// Check if import of Login Configuration was successful.
	if ($success === true && $db->affectedRows() > 0) {
		echo 'Login Configuration was imported successfully.';
	} else {
		echo 'There was an ERROR while importing Login Configuration!';
	}

	// Close Database Connection.
	$db->close();
	unset($db);

} catch (PDOException $exception) {
	// Check if PDOException is triggered from the client side (i.e. failed to establish Database Connection), OR it was
	// caused by an invalid SQL query.
	if ($exception->getCode() >= 2000 && $exception->getCode() <= 2061) {
		echo 'Failed to import Login Configuration due to Database Connection ERROR!';
	} else {
		echo 'Failed to import Login Configuration due to Database SQL ERROR!';
	}
}
