<?php
/*
 * This file is part of ADP.
 *
 * ADP is a proprietary software owned by PARCC (Partnership for Assessment of Readiness for College and Careers).
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Class AdminController
 *
 * This is the RESTful Controller for handling Administration Requests to Install Application, Update Application or
 * Clear APCu Cache. For security reasons these Requests are only available from localhost!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */

$filePath = 'html/index.html';


// Try to Read referenced File Content.
$fileContent = file_get_contents($filePath);

// Check if File Content was successfully read.
if ($fileContent === false) {
	// Throw Exception if File Content failed to be loaded.
	throw new Exception('Unable to Read File.');
}

// Try to Search and Replace referenced String in the File Content.
$fileContent = strtr($fileContent, ['branch: dev-bojan' => 'branch: dev']);
$fileContent = strtr($fileContent, ['-dirty<br/>' => '<br/>']);

// Try to Write File Content and check if it was written successfully.
if (!file_put_contents($filePath, $fileContent)) {
	// Throw Exception if File Content failed to be saved.
	throw new Exception('Unable to Write to File.');
}
