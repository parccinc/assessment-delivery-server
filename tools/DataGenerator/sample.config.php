<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Data Generator Settings for particular Environment.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Kendall Parks <kendall.parks@breaktech.com>
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */

return [
	// Database Host (@default='').
	'dbHost'                      => '',
	// Database Name (@default='').
	'dbName'                      => '',
	// Database Username (@default='').
	'dbUsername'                  => '',
	// Database Password (@default='').
	'dbPassword'                  => '',
	// Indicates if Database Tables should be truncated first, OR existing Data should be preserved. When enabled, ALL
	// Data is deleted! (@default=false).
	'truncateTables'              => false,
	// Number of Log Records that should be generated (@default=0).
	'numberOfLogs'                => 0,
	// Number of Student Records that should be generated (@default=0).
	'numberOfStudent'             => 0,
	// Number of Test Records that should be generated (@default=0).
	'numberOfTests'               => 0,
	// Number of Test Content Records that should be generated (@default=0).
	'numberOfTestContents'        => 0,
	// Number of Test Form Records that should be generated (@default=0).
	'numberOfTestForms'           => 0,
	// Number of Test Form Revision Records that should be generated (@default=0).
	'numberOfTestFormRevisions'   => 0,
	// Number of Test Results Records that should be generated (@default=0).
	'numberOfTestResults'         => 0,
	// Number of Test Results Archive Records that should be generated (@default=0).
	'numberOfTestResultArchives'  => 0,
	// Number of Test Session Records that should be generated (@default=0).
	'numberOfTestSessions'        => 0,
	// Number of Test Session Archive Records that should be generated (@default=0).
	'numberOfTestSessionArchives' => 0,
	// Number of User Records that should be generated (@default=0).
	'numberOfUsers'               => 0
];
