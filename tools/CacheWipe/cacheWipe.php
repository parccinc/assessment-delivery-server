<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

// Report on ALL Errors!
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


use Phalcon\Cache\Backend\Apc;
use Phalcon\Cache\Frontend\Data;

/**
 * Cache Wipe is used to invalidate all Application Cache.
 * In order to work, this requires APCu to be enabled for CLI, so this should be added into php.ini file:
 *   apc.enable_cli = 1
 * Or simply run provided scripts which will automatically enable APCu for CLI.
 * However, this only works on Windows! On Linux, CLI and CGI SAPIs do NOT share the same memory (data)!!!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */

// For security reasons limit access strictly to command line!
if (PHP_SAPI !== 'cli') {
	exit("This tool is only available from the command line!");
}

// Clear all APCu Cache!
$apcCache = new Apc(new Data());
$apcCache->flush();
echo "All APCu Cache was deleted!\n";
