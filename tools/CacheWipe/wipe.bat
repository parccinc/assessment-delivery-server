@echo off
cls
title PARCC-ADP Cache Wipe
echo Deleting ADP Cache...
echo.
php -d apc.enable_cli=1 cacheWipe.php
echo.
echo.
pause
