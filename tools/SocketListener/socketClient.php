<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use PARCC\ADP\Tools\Logger\SocketListener;

// For security reasons limit access strictly to command line!
if (PHP_SAPI !== 'cli') {
	exit("This tool is only available from the command line!");
}

require_once 'logger/SocketListener.php';


/**
 * Default Parameters:
 *  - Host     : 127.0.0.1
 *  - Port     : 9999
 *
 * @example: php SocketListener.php -h localhost -p 9999
 */
global $argv;


// Retrieve command line arguments.
$arguments = $argv;

array_shift($arguments);
$parameters = [];

// Loop through arguments, validate and set them up.
while (!empty($arguments)) {
	$current = array_shift($arguments);

	if ($current == '--') {
		break;
	}

	$key = null;
	if (substr($current, 0, 1) == '-') {
		$key = substr($current, 1);
	}

	if (empty($arguments) and $key !== 'c') {
		exit("Missing parameter value for {$key}.\n");
	}

	$value = array_shift($arguments);
	$parameters[$key] = $value;
}

$url = 'udp://' . (isset($parameters['h']) ? $parameters['h'] : '127.0.0.1') . ':' .
	   (isset($parameters['p']) ? $parameters['p'] : '9999');
echo "Listening to {$url}\n";

$udr = new SocketListener($url);
$udr->listen();
