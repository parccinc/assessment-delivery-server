<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Tools\Logger;

/**
 * Class SocketListener
 *
 * This is UDP Client for Socket Logger. It simply connects to Socket Connection and listens over UDP for any streamed
 * Log Entries coming from the Application, if enabled. Socket Logger is only available in Debug Mode and should not be
 * enabled in Production!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class SocketListener
{

	/**
	 * @const integer MAXIMUM_DATA_PACKAGE_SIZE Maximum supported data packet size allowed by UDP protocol.
	 */
	const MAXIMUM_DATA_PACKAGE_SIZE = 65507;

	/**
	 * @var array $color List of colors used for each Log Entry Type (based on Log Severity).
	 */
	protected static $color = [
		0 => '1;35', // EMERGENCY: light purple
		1 => '0;35', // CRITICAL : purple
		2 => '0;36', // ALERT    : cyan
		3 => '1;31', // ERROR    : light red
		4 => '0;31', // WARNING  : red
		5 => '1;37', // NOTICE   : white
		6 => '0;37', // INFO     : light gray
		7 => '1;30', // DEBUG    : dark gray
		8 => '1;30', // CUSTOM   : dark gray
		9 => '1;30', // SPECIAL  : dark gray
	];

	/**
	 * @var array $supportedShells List of Shells supporting colors.
	 */
	protected static $supportedShells = [
		'xterm',
		'xterm-256color',
		'xterm-color'
	];

	/**
	 * @var resource|boolean $socket Socket Stream, OR FALSE on Error.
	 */
	protected $socket;


	/**
	 * Constructor.
	 *
	 * Sets the URL and opens Socket Connection.
	 *
	 * @param string $url Socket Connection URL.
	 */
	public function __construct($url)
	{
		// Try to open Socket Connection.
		$this->socket = stream_socket_server($url, $errorCode, $errorDescription, STREAM_SERVER_BIND);

		// In case of failure to open Socket Connection, return an Error.
		if (!$this->socket) {
			exit("$errorDescription ($errorCode)");
		}
	}


	/**
	 * Identifies if CLI SAPI runs in a Shell that supports ANSI colors.
	 *
	 * @return boolean Returns TRUE if Shell supports ANSI color. Otherwise, it returns FALSE.
	 */
	private function isSupportedShell()
	{
		// Check if Environment of Server Global Variables include Shell details.
		if (isset($_ENV['TERM'])) {
			// Check if used Shell is in the list of those supporting colors.
			if (in_array($_ENV['TERM'], self::$supportedShells) === true) {
				// Confirm that Shell supports ANSI colors.
				return true;
			}

		} elseif (isset($_SERVER['TERM'])) {
			// Check if used Shell is in the list of those supporting colors.
			if (in_array($_SERVER['TERM'], self::$supportedShells) === true) {
				// Confirm that Shell supports ANSI colors.
				return true;
			}
		}

		// Confirm that Shell does not supports ANSI colors.
		return false;
	}


	/**
	 * Socket Connection Listener.
	 * It outputs data received from Socket Stream, formats and renders it to the console.
	 */
	public function listen()
	{
		// Start listening for Log Entries.
		do {
			// Retrieve data.
			$data = stream_socket_recvfrom($this->socket, self::MAXIMUM_DATA_PACKAGE_SIZE, 0, $peer);
			$message = json_decode($data, true);

			// Check if data was successfully retrieved.
			if (empty($message)) {
				// Throw an Error Message in case data packet failed to be retrieved.
				$message = [
					't' => 0,
					'm' => "ERROR RETRIEVING MESSAGE!"
				];
			}

			// Check if CLI SAPI Shell supports ANSI colors.
			if ($this->isSupportedShell() === true) {
				// Return Log Message decorated with colors.
				echo "\033[", self::$color[$message['t']], "m", $message['m'], "\033[0m\n";
			} else {
				// Print Log Message without colors.
				echo $message['m'], "\n";
			}

		} while ($data !== false);
	}
}
