/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * API Test Client is used for testing web services and should be disabled in Production!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author bojan.vulevic@breaktech.com (Bojan Vulevic)
 */


/**
 * API Configuration.
 */
/*global $:true, console:true, JSON:true, serverTime:true */
/*jslint browser:true */
/*eslint no-console: 0 */
/*eslint-disable no-unused-vars */
var apiHost = '';
//var apiHost = 'http://localhost:8080';
//var apiHost = 'https://int.tds.adp.parcc-ads.breaktech.org';
//var apiHost = 'https://stable.tds.adp.parcc-ads.breaktech.org';
var xdebug = '';
//var xdebug = '?XDEBUG_SESSION_START=dbgp';
var timeOffset = 1000 * serverTime - new Date().getTime();
console.log('Time Offset: ' + timeOffset);


/**
 * Helpers.
 *
 */


/**
 * @param  {boolean}  value Value to be parsed as Boolean.
 * @return {?boolean}       Returns actual value for given Boolean (TRUE|FALSE\NULL).
 */
function parseBool(value) {
	'use strict';

	switch (value) {
		case 'true':
			return true;
		case 'false':
			return false;
		default:
			return null;
	}
}


/**
 * @param {!number} newServerTime 10-digit integer representing Unix Timestamp.
 */
function updateServerTime(newServerTime) {
	'use strict';

	// Check if new server time is a valid Unix Timestamp.
	if (newServerTime > 1000000000 && newServerTime < 9999999999) {
		// Set new Server Time.
		serverTime = newServerTime;
		// Recalculate current Time Offset (in case local Clock got changed in the meantime or if it's drifting).
		timeOffset = 1000 * serverTime - new Date().getTime();
		console.log('Server Time updated to: ' + serverTime);
		console.log('New Time Offset: ' + timeOffset);
	}
}


/**
 * @param {string} html HTML that should be embedded into iFrame.
 */
function iframeHTML(html) {
	'use strict';

	var el = document.getElementById('html'), iframe;

	if (html === '') {
		$('#html').hide();
	} else {
		//noinspection JSLint
		reset(false);
		$('#html').show();
	}

	if (el.contentWindow) {
		iframe = el.contentWindow;
	} else if (el.contentDocument.document) {
		iframe = el.contentDocument.document;
	} else {
		iframe = el.contentDocument;
	}

	iframe.document.open();
	iframe.document.write(html);
	iframe.document.close();

	if (iframe.document.body === undefined || iframe.document.body === null) {
		iframe.onload = function() {
			el.height = iframe.document.body.scrollHeight;
		};
	} else {
		el.height = iframe.document.body.scrollHeight;
	}
}


//noinspection GjsLint
/**
 * @param {?boolean=} resetHeader Indicates if HTML within iFrame should be removed.
 */
function reset(resetHeader) { //jshint ignore:line
	'use strict';

	var image = $('#image');

	if (resetHeader === undefined || resetHeader === true) {
		$('#responseHeaders').html('');
	}
	$('#responseBody').html('');

	if (image.attr('src') !== '#') {
		image.attr('src', '#');
	}
	image.hide();

	$('#audio').removeAttr('src').hide();
	$('#video').removeAttr('src').hide();

	iframeHTML('');
}


/**
 * @param  {object|string} data Response Data that is either JSON Object or String.
 * @return {string}             Returns HTML formatted representation of Response Data.
 */
function prettyPrintJSON(data) {
	'use strict';

	var json = data;

	// Check if given data is JSON Object.
	if (typeof json !== 'object') {
		return json;
	}

	if (data !== 'string') {
		json = JSON.stringify(data, null, 2);
	}
	json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');

	return json.replace(
		/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
		function(match) {

			var cssClass = 'number';

			if (/^"/.test(match)) {
				if (/:$/.test(match)) {
					cssClass = 'key';
				} else {
					cssClass = 'string';
				}
			} else if (/true|false/.test(match)) {
				cssClass = 'boolean';
			} else if (/null/.test(match)) {
				cssClass = 'null';
			}

			return '<span class="' + cssClass + '">' + match + '<\/span>';
		}
	);
}


/**
 * @param  {string} data Response Header Data.
 * @return {string}      Returns HTML formatted representation of Response Header Data.
 */
function prettyPrintHeaders(data) {
	'use strict';

	return data.replace(/Access-Control-Allow-Methods:/i, '<span class="key">Access-Control-Allow-Methods:<\/span>')
		.replace(/Access-Control-Allow-Origin:/i, '<span class="key">Access-Control-Allow-Origin:<\/span>')
		.replace(/Status:/i, '<span class="key">Status:<\/span>')
		.replace(/X-Error-Code:/i, '<span class="key">X-Error-Code:<\/span>')
		.replace(/X-Error-Description:/i, '<span class="key">X-Error-Description:<\/span>');
}


/**
 * @param  {string}   url           Content File URL.
 * @param  {string}   fileExtension File Extension.
 * @return {!boolean}               Returns TRUE if File Extension matches Content URL. Otherwise, it returns FALSE.
 */
function isFileType(url, fileExtension) {
	'use strict';

	if (url.indexOf(fileExtension) !== -1 && url.length >= fileExtension.length) {
		if (url.substring(url.length - fileExtension.length) === fileExtension) {

			return true;
		}
	}

	return false;
}


//noinspection JSValidateJSDoc
/**
 * @param {object}   event    onKeyPress Event.
 * @param {callback} callback Callback Function.
 * @param {string}   argument Callback Function Argument.
 */
function enter(event, callback, argument) { //jshint ignore:line
	'use strict';

	var checkbox;
	// Check if Enter Key or Space Key was pressed.
	if (event.which === 10 || event.which === 13) {
		// Stop further Event Propagation if Enter Key was pressed.
		event.preventDefault();
		// Execute Callback Function.
		callback(argument);
	} else if (event.which === 32) {
		// Stop further Event Propagation if Space Key was pressed.
		event.preventDefault();
		// Check if current target is Checkbox.
		/**
		 * @param {object} event.currentTarget.control DOM Element triggering Event.
		 */
		if (event.currentTarget.control.type === 'checkbox') {
			checkbox = $('#' + event.currentTarget.htmlFor);
			// Check/Uncheck current Target Checkbox.
			checkbox.prop('checked', !checkbox.prop('checked'));
		}
	}
}


$(function() {
	'use strict';

	$('legend').click(function() {
		$(this).next().slideToggle($(this).next().find('pre').html() === '' ? 100 : 500);
	});
});

$(document).ready(function() {
	'use strict';

	reset();
});


/**
 * Admin.
 *
 * @param {string} method HTTP Request Method {'GET'|'OPTIONS'}.
 */
function admin(method) { //jshint ignore:line
	'use strict';

	var apiURI = '/api/admin/' + $('#adminOperation').val();

	$.ajax({
		url: apiHost + apiURI + xdebug,
		type: method
	}).always(function(data, textStatus, jqXHR) {
		/**
		 * @param {object} data.responseJSON JSON Response in case of Success.
		 * @param {object} data.responseText JSON Response in case of Error.
		 */
		var responseType, headers, json = '[EMPTY]';
		reset();
		if (textStatus === 'success') {
			responseType = jqXHR.getResponseHeader('Content-Type');
			if (data !== '') {
				json = data;
			}
		} else if (data !== undefined) {
			responseType = data.getResponseHeader('Content-Type');
			if (data.responseJSON !== undefined && data.responseJSON !== '') {
				json = data.responseJSON;
			} else if (data.responseText !== undefined && data.responseText !== '') {
				json = data.responseText;
			}
		}
		if (responseType === null) {
			$('#responseBody').html('[ERROR]');
		} else if (responseType.indexOf('text/html') === 0 && json.indexOf('<html') > -1) {
			iframeHTML(json);
		} else {
			json = prettyPrintJSON(json);
			$('#responseBody').html(json);
		}

		try {
			headers = jqXHR.getAllResponseHeaders();
		} catch (e) {
			// In case of Error.
			headers = data.getAllResponseHeaders();
		}
		if (!headers) {
			headers = '[ERROR]';
		}
		$('#responseHeaders').html(prettyPrintHeaders(headers));
	});
}


/**
 * Ping.
 *
 * @param {string} method HTTP Request Method {'GET'|'OPTIONS'}.
 */
function ping(method) { //jshint ignore:line
	'use strict';

	var apiURI = '/api/ping' + ($('#healthCheck').prop('checked') ? '/status' : '');

	$.ajax({
		url: apiHost + apiURI + xdebug,
		type: method
	}).always(function(data, textStatus, jqXHR) {
		/**
		 * @param {object} data.responseJSON JSON Response in case of Success.
		 * @param {object} data.responseText JSON Response in case of Error.
		 */
		var responseType, headers, json = '[EMPTY]';
		reset();
		if (textStatus === 'success') {
			responseType = jqXHR.getResponseHeader('Content-Type');
			if (data !== '') {
				json = data;
			}
		} else if (data !== undefined) {
			responseType = data.getResponseHeader('Content-Type');
			if (data.responseJSON !== undefined && data.responseJSON !== '') {
				json = data.responseJSON;
			} else if (data.responseText !== undefined && data.responseText !== '') {
				json = data.responseText;
			}
		}
		if (responseType === null) {
			$('#responseBody').html('[ERROR]');
		} else if (responseType.indexOf('text/html') === 0 && json.indexOf('<html') > -1) {
			iframeHTML(json);
		} else {
			json = prettyPrintJSON(json);
			$('#responseBody').html(json);
		}

		try {
			headers = jqXHR.getAllResponseHeaders();
		} catch (e) {
			// In case of Error.
			headers = data.getAllResponseHeaders();
		}
		if (!headers) {
			headers = '[ERROR]';
		}
		$('#responseHeaders').html(prettyPrintHeaders(headers));
	});
}


/**
 * Login.
 *
 * @param {string} method HTTP Request Method {'POST'|'OPTIONS'}.
 */
function login(method) { //jshint ignore:line
	'use strict';

	var apiURI = '/api/login';

	var username = $('#username').val();
	var password = $('#password').val();
	var secret = $('#secret').val();
	var testKey = $('#testKey').val();
	var dateOfBirth = $('#dateOfBirth').val();
	var studentId = $('#studentId').val();
	var state = $('#state').val();

	var timestamp = new Date().getTime() + timeOffset;
	var nonce = Math.floor(Math.random() * 99999);
	var hash = apiURI + Math.round(timestamp / 1000) + nonce;
	var message = $.sha256hmac(secret, hash);
	var authentication = window.btoa(timestamp + ':' + nonce + ':' + window.btoa(message));
	var data = {api: null};

	if (!username || !password || !secret || !testKey || !state) {
		window.alert('Please enter all Login details!');
	}

	console.log('Timestamp: ' + timestamp);
	console.log('Time Offset: ' + timeOffset);
	console.log('Nonce: ' + nonce);
	console.log('Hash: ' + hash);
	console.log('Message: ' + message);
	console.log('Authentication: ' + authentication);

	if (method !== 'GET' && method !== 'HEAD' && method !== 'OPTIONS') {
		data = JSON.stringify({
			testKey: testKey,
			dateOfBirth: dateOfBirth,
			studentId: studentId,
			state: state
		});
	}

	$.ajax({
		url: apiHost + apiURI + xdebug,
		type: method,
		headers: {
			'Content-Type': 'application/json; charset=UTF-8',
			//'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			'Authorization': 'Basic ' + window.btoa(username + ':' + password),
			'Authentication': authentication,
			'X-Requested-With': window.btoa(testKey)
		},
		data: data
	}).always(function(data, textStatus, jqXHR) {
		/**
		 * @param {object} data.responseJSON JSON Response in case of Success.
		 * @param {object} data.responseText JSON Response in case of Error.
		 */
		var json = '[EMPTY]';
		var responseType, headers;
		reset();
		if (textStatus === 'success') {
			responseType = jqXHR.getResponseHeader('Content-Type');
			if (data !== '') {
				json = data;
			}
			try {
				$('#token').val(data.result.api.token);
				$('#testId').val(data.result.test.id);
				updateServerTime(data.result.api.time);
			} catch (e) {
				// In case of Error.
				try {
					$('#token').val(data.api.token);
					$('#testId').val(data.test.id);
					updateServerTime(data.api.time);
				} catch (ev) {
					// In case of Error.
					$('#token').val('[ERROR]');
					$('#testId').val('[ERROR]');
				}
			}
		} else if (data !== undefined) {
			responseType = data.getResponseHeader('Content-Type');
			if (data.responseJSON !== undefined && data.responseJSON !== '') {
				json = data.responseJSON;
			} else if (data.responseText !== undefined && data.responseText !== '') {
				json = data.responseText;
			}
			try {
				updateServerTime(json.result.time);
			} catch (e) {
				// In case of Error.
				try {
					updateServerTime(json.time);
				} catch (ex) {
					// Do nothing.
					console.log('Unable to update Server Time.');
				}
			}
		}
		if (responseType === null) {
			$('#responseBody').html('[ERROR]');
		} else if (responseType.indexOf('text/html') === 0 && json.indexOf('<html') > -1) {
			iframeHTML(json);
		} else {
			json = prettyPrintJSON(json);
			$('#responseBody').html(json);
		}

		try {
			headers = jqXHR.getAllResponseHeaders();
		} catch (e) {
			// In case of Error.
			headers = data.getAllResponseHeaders();
		}
		if (!headers) {
			headers = '[ERROR]';
		}
		$('#responseHeaders').html(prettyPrintHeaders(headers));
	});
}


/**
 * Content.
 *
 * @param {string} method HTTP Request Method {'GET'|'OPTIONS'}.
 */
function content(method) { //jshint ignore:line
	'use strict';

	var token = $('#token').val();
	var testId = parseInt($('#testId').val(), 10);

	var contentFile = $('#contentFile').val();

	var apiURI = '/api/content/' + token + '/' + testId + (contentFile === '' ? '' : ('/' + contentFile));

	if (!token || !testId) {
		window.alert('Please Login first!');
	}

	$.ajax({
		url: apiHost + apiURI + xdebug,
		type: method,
		crossDomain: true
	}).always(function(data, textStatus, jqXHR) {
		/**
		 * @param {object} data.responseJSON JSON Response in case of Success.
		 * @param {object} data.responseText JSON Response in case of Error.
		 */
		var media, responseType, headers, json;
		reset();
		if (textStatus === 'success') {
			if (isFileType(this.url, '.jpg') || isFileType(this.url, '.jpeg') || isFileType(this.url, '.jpe') ||
			    isFileType(this.url, '.png'))
			{
				media = true;
				$('#image').attr('src', this.url).show();
			} else if (isFileType(this.url, '.mp3') || isFileType(this.url, '.ogg')) {
				media = true;
				$('#audio').attr('src', this.url).show();
			} else if (isFileType(this.url, '.mp4') || isFileType(this.url, '.webm')) {
				media = true;
				$('#video').attr('src', this.url).show();
			}
		}

		if (media) {
			$('#responseBody').html('');
		} else {
			json = '[EMPTY]';
			if (textStatus === 'success') {
				responseType = jqXHR.getResponseHeader('Content-Type');
				if (data !== '') {
					json = data;
				}
			} else if (data !== undefined) {
				responseType = data.getResponseHeader('Content-Type');
				if (data.responseJSON !== undefined && data.responseJSON !== '') {
					json = data.responseJSON;
				} else if (data.responseText !== undefined && data.responseText !== '') {
					json = data.responseText;
				}
			}
			if (responseType === null) {
				$('#responseBody').html('[ERROR]');
			} else if (responseType.indexOf('text/html') === 0 && json.indexOf('<html') > -1) {
				iframeHTML(json);
			} else {
				json = prettyPrintJSON(json);
				$('#responseBody').html(json);
			}
		}

		try {
			headers = jqXHR.getAllResponseHeaders();
		} catch (e) {
			// In case of Error.
			headers = data.getAllResponseHeaders();
		}
		if (!headers) {
			headers = '[ERROR]';
		}
		$('#responseHeaders').html(prettyPrintHeaders(headers));
	});
}


/**
 * Results.
 *
 * @param {string} method HTTP Request Method {'GET'|'POST'|'OPTIONS'}.
 */
function results(method) { //jshint ignore:line
	'use strict';

	var username = $('#username').val();
	var password = $('#password').val();
	var secret = $('#secret').val();
	var testKey = $('#testKey').val();
	var token = $('#token').val();
	var testId = parseInt($('#testId').val(), 10);

	var testStatus = $('#testStatus').val();
	var resultsFile = $('#resultsFile').val();

	var apiURI = '/api/results/' + token + '/' + testId +
	             (method !== 'GET' && testStatus !== '' ? ('/' + testStatus) : '');

	var timestamp = new Date().getTime() + timeOffset;
	var nonce = Math.floor(Math.random() * 99999);
	var hash = apiURI + Math.round(timestamp / 1000) + nonce;
	var message = $.sha256hmac(secret, hash);
	var authentication = window.btoa(timestamp + ':' + nonce + ':' + window.btoa(message));
	var data, resultsData;

	if (!token || !testId) {
		window.alert('Please Login first!');
	}

	if (method === 'POST') {
		try {
			resultsData = JSON.parse(resultsFile);
			// Handle non-exception cases:
			// Neither JSON.parse(false) nor JSON.parse(1234) throw exception, hence the type-checking, but...
			// JSON.parse(null) returns 'NULL', and NULL === "object", so we must check for that one too.
			if (!resultsData || resultsData !== 'object' || resultsData === null) {
				window.alert('Please enter Results as valid JSON!');
			}
		}
		catch (e) {
			// In case of Error.
			window.alert('Please enter Results as valid JSON!');
		}
	}

	console.log('Timestamp: ' + timestamp);
	console.log('Time Offset: ' + timeOffset);
	console.log('Nonce: ' + nonce);
	console.log('Hash: ' + hash);
	console.log('Message: ' + message);
	console.log('Authentication: ' + authentication);

	if (method !== 'GET' && method !== 'HEAD' && method !== 'OPTIONS') {
		data = resultsFile;
	}

	$.ajax({
		url: apiHost + apiURI + xdebug,
		type: method,
		headers: {
			'Content-Type': 'application/json; charset=UTF-8',
			//'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			'Authorization': 'Basic ' + window.btoa(username + ':' + password),
			'Authentication': authentication,
			'X-Requested-With': window.btoa(testKey)
		},
		data: data
	}).always(function(data, textStatus, jqXHR) {
		/**
		 * @param {object} data.responseJSON JSON Response in case of Success.
		 * @param {object} data.responseText JSON Response in case of Error.
		 */
		var json = '[EMPTY]';
		var responseType, headers;
		reset();
		if (textStatus === 'success') {
			responseType = jqXHR.getResponseHeader('Content-Type');
			if (data !== '') {
				json = data;
			}
			try {
				updateServerTime(data.result.api.time);
			} catch (e) {
				// In case of Error.
				try {
					updateServerTime(data.api.time);
				} catch (ex) {
					// Do nothing.
					console.log('Unable to update Server Time.');
				}
			}
		} else if (data !== undefined) {
			responseType = data.getResponseHeader('Content-Type');
			if (data.responseJSON !== undefined && data.responseJSON !== '') {
				json = data.responseJSON;
			} else if (data.responseText !== undefined && data.responseText !== '') {
				json = data.responseText;
			}
			try {
				updateServerTime(json.result.time);
			} catch (e) {
				// In case of Error.
				try {
					updateServerTime(json.time);
				} catch (ex) {
					// Do nothing.
					console.log('Unable to update Server Time.');
				}
			}
		}
		if (responseType === null) {
			$('#responseBody').html('[ERROR]');
		} else if (responseType.indexOf('text/html') === 0 && json.indexOf('<html') > -1) {
			iframeHTML(json);
		} else {
			json = prettyPrintJSON(json);
			$('#responseBody').html(json);
		}

		try {
			headers = jqXHR.getAllResponseHeaders();
		} catch (e) {
			// In case of Error.
			headers = data.getAllResponseHeaders();
		}
		if (!headers) {
			headers = '[ERROR]';
		}
		$('#responseHeaders').html(prettyPrintHeaders(headers));
	});
}


/**
 * Assessment.
 *
 * @param {string} method HTTP Request Method {'GET'|'POST'|'OPTIONS'}.
 */
function assessment(method) { //jshint ignore:line
	'use strict';

	var apiURI = '/assessment';

	var testKey = $('#assessmentTestKey').val();
	var name = $('#assessmentName').val();
	var data;

	if (!testKey) {
		window.alert('Please enter Test Key!');
	}

	if (method !== 'GET' && method !== 'HEAD' && method !== 'OPTIONS') {
		data = 'testKey=' + encodeURIComponent(testKey) + '&name=' + encodeURIComponent(name);
	}

	$.ajax({
		url: apiHost + apiURI + xdebug,
		type: method,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		},
		data: data
	}).always(function(data, textStatus, jqXHR) {
		/**
		 * @param {object} data.responseJSON JSON Response in case of Success.
		 * @param {object} data.responseText JSON Response in case of Error.
		 */
		var json = '[EMPTY]';
		var responseType, headers;
		reset();
		if (textStatus === 'success') {
			responseType = jqXHR.getResponseHeader('Content-Type');
			if (data !== '') {
				json = data;
			}
		} else if (data !== undefined) {
			responseType = data.getResponseHeader('Content-Type');
			if (data.responseJSON !== undefined && data.responseJSON !== '') {
				json = data.responseJSON;
			} else if (data.responseText !== undefined && data.responseText !== '') {
				json = data.responseText;
			}
		}
		if (responseType === null) {
			$('#responseBody').html('[ERROR]');
		} else if (responseType.indexOf('text/html') === 0 && json.indexOf('<html') > -1) {
			iframeHTML(json);
		} else {
			json = prettyPrintJSON(json);
			$('#responseBody').html(json);
		}

		try {
			headers = jqXHR.getAllResponseHeaders();
		} catch (e) {
			// In case of Error.
			headers = data.getAllResponseHeaders();
		}
		if (!headers) {
			headers = '[ERROR]';
		}
		$('#responseHeaders').html(prettyPrintHeaders(headers));
	});
}


/**
 * Test.
 *
 * @param {string} method HTTP Request Method {'POST'|'PUT'|'PATCH'|'DELETE'|'OPTIONS'}.
 */
function test(method) { //jshint ignore:line
	'use strict';

	var username = $('#testUsername').val();
	var password = $('#testPassword').val();
	var secret = $('#testSecret').val();
	var testFormRevisionId = $('#testFormRevisionId').val();
	var publishedBy = $('#publishedBy').val();
	var adpTestFormRevisionId = parseInt($('#adpTestFormRevisionId').val(), 10);
	var active = parseBool($('#active').val());

	var apiURI = '/api/test' +
	             (method === 'PUT' || method === 'PATCH' || method === 'DELETE' ? ('/' + adpTestFormRevisionId) : '');

	var timestamp = new Date().getTime() + timeOffset;
	var nonce = Math.floor(Math.random() * 99999);
	var hash = apiURI + Math.round(timestamp / 1000) + nonce;
	var message = $.sha256hmac(secret, hash);
	var authentication = window.btoa(timestamp + ':' + nonce + ':' + window.btoa(message));
	var data;

	if (!username || !password || !secret || !testFormRevisionId || !publishedBy ||
		(!adpTestFormRevisionId && (method === 'PUT' || method === 'DELETE')) ||
		(!adpTestFormRevisionId && !active && method === 'PATCH'))
	{
		window.alert('Please enter all Test details!');
	}

	console.log('Timestamp: ' + timestamp);
	console.log('Time Offset: ' + timeOffset);
	console.log('Nonce: ' + nonce);
	console.log('Hash: ' + hash);
	console.log('Message: ' + message);
	console.log('Authentication: ' + authentication);

	if (method === 'PATCH') {
		data = JSON.stringify({
			testFormRevisionId: testFormRevisionId,
			publishedBy: publishedBy,
			active: active
		});
	} else if (method !== 'GET' && method !== 'HEAD' && method !== 'OPTIONS') {
		data = JSON.stringify({
			testFormRevisionId: testFormRevisionId,
			publishedBy: publishedBy
		});
	}

	$.ajax({
		url: apiHost + apiURI + xdebug,
		type: method,
		headers: {
			'Content-Type': 'application/json; charset=UTF-8',
			//'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			'Authorization': 'Basic ' + window.btoa(username + ':' + password),
			'Authentication': authentication
		},
		data: data
	}).always(function(data, textStatus, jqXHR) {
		/**
		 * @param {object} data.responseJSON JSON Response in case of Success.
		 * @param {object} data.responseText JSON Response in case of Error.
		 */
		var json = '[EMPTY]';
		var responseType, headers;
		reset();
		if (textStatus === 'success') {
			responseType = jqXHR.getResponseHeader('Content-Type');
			if (data !== '') {
				json = data;
			}
		} else if (data !== undefined) {
			responseType = data.getResponseHeader('Content-Type');
			if (data.responseJSON !== undefined && data.responseJSON !== '') {
				json = data.responseJSON;
			} else if (data.responseText !== undefined && data.responseText !== '') {
				json = data.responseText;
			}
		}
		if (responseType === null) {
			$('#responseBody').html('[ERROR]');
		} else if (responseType.indexOf('text/html') === 0 && json.indexOf('<html') > -1) {
			iframeHTML(json);
		} else {
			json = prettyPrintJSON(json);
			$('#responseBody').html(json);
		}

		try {
			headers = jqXHR.getAllResponseHeaders();
		} catch (e) {
			// In case of Error.
			headers = data.getAllResponseHeaders();
		}
		if (!headers) {
			headers = '[ERROR]';
		}
		$('#responseHeaders').html(prettyPrintHeaders(headers));
	});
}


/**
 * Test Properties.
 *
 * @author mehdi.karamosly@breaktech.com (Mehdi Karamosly)
 *
 * @param {string} method HTTP Request Method {'PATCH'|'OPTIONS'}.
 */
function testProperties(method) { //jshint ignore:line
	'use strict';

	var username = $('#testUsername').val();
	var password = $('#testPassword').val();
	var secret = $('#testSecret').val();
	var adpTestId = $('#adpTestId').val();
	var batteryId = $('#batteryId').val();
	var name = $('#name').val();
	var program = $('#program').val();
	var scoreReport = $('#scoreReport').val();
	var subject = $('#subject').val();
	var grade = $('#grade').val();
	var itemSelectionAlgorithm = $('#itemSelectionAlgorithm').val();
	var security = $('#security').val();
	var multimedia = $('#multimedia').val();
	var scoring = $('#scoring').val();
	var permissions = $('#permissions').val();
	var privacy = $('#privacy').val();
	var description = $('#description').val();
	var isActive = parseBool($('#isActive').val());

	var apiURI = '/api/test/properties/' + adpTestId;

	var timestamp = new Date().getTime() + timeOffset;
	var nonce = Math.floor(Math.random() * 99999);
	var hash = apiURI + Math.round(timestamp / 1000) + nonce;
	var message = $.sha256hmac(secret, hash);
	var authentication = window.btoa(timestamp + ':' + nonce + ':' + window.btoa(message));

	var data = {};

	if (!username || !password || !secret || !adpTestId || !batteryId ||
	    (!name && !program && !scoreReport && !subject && !grade && !itemSelectionAlgorithm && !security &&
	     !multimedia && !scoring && !permissions && !privacy && !description && !isActive))
	{
		window.alert('Please enter all Test Properties details!');
	}

	if (batteryId) {
		data.batteryId = batteryId;
	}
	if (name) {
		data.name = name;
	}
	if (program) {
		data.program = program;
	}
	if (scoreReport) {
		data.scoreReport = scoreReport;
	}
	if (subject) {
		data.subject = subject;
	}
	if (grade) {
		data.grade = grade;
	}
	if (itemSelectionAlgorithm) {
		data.itemSelectionAlgorithm = itemSelectionAlgorithm;
	}
	if (security) {
		data.security = security;
	}
	if (multimedia) {
		data.multimedia = multimedia;
	}
	if (scoring) {
		data.scoring = scoring;
	}
	if (permissions) {
		data.permissions = permissions;
	}
	if (privacy) {
		data.privacy = privacy;
	}
	if (description) {
		data.description = description;
	}
	if (isActive !== null) {
		data.isActive = isActive;
	}

	console.log('Timestamp: ' + timestamp);
	console.log('Time Offset: ' + timeOffset);
	console.log('Nonce: ' + nonce);
	console.log('Hash: ' + hash);
	console.log('Message: ' + message);
	console.log('Authentication: ' + authentication);

	if (method === 'PATCH') {
		data = JSON.stringify(data);
	} else if (method !== 'GET' && method !== 'HEAD' && method !== 'OPTIONS') {
		data = JSON.stringify({
			batteryId: batteryId
		});
	}

	$.ajax({
		url: apiHost + apiURI + xdebug,
		type: method,
		headers: {
			'Content-Type': 'application/json; charset=UTF-8',
			//'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			'Authorization': 'Basic ' + window.btoa(username + ':' + password),
			'Authentication': authentication
		},
		data: data
	}).always(function(data, textStatus, jqXHR) {
		/**
		 * @param {object} data.responseJSON JSON Response in case of Success.
		 * @param {object} data.responseText JSON Response in case of Error.
		 */
		var json = '[EMPTY]';
		var responseType, headers;
		reset();
		if (textStatus === 'success') {
			responseType = jqXHR.getResponseHeader('Content-Type');
			if (data !== '') {
				json = data;
			}
		} else if (data !== undefined) {
			responseType = data.getResponseHeader('Content-Type');
			if (data.responseJSON !== undefined && data.responseJSON !== '') {
				json = data.responseJSON;
			} else if (data.responseText !== undefined && data.responseText !== '') {
				json = data.responseText;
			}
		}
		if (responseType === null) {
			$('#responseBody').html('[ERROR]');
		} else if (responseType.indexOf('text/html') === 0 && json.indexOf('<html') > -1) {
			iframeHTML(json);
		} else {
			json = prettyPrintJSON(json);
			$('#responseBody').html(json);
		}

		try {
			headers = jqXHR.getAllResponseHeaders();
		} catch (e) {
			// In case of Error.
			headers = data.getAllResponseHeaders();
		}
		if (!headers) {
			headers = '[ERROR]';
		}
		$('#responseHeaders').html(prettyPrintHeaders(headers));
	});
}


/**
 * Assignment.
 *
 * @param {string} method HTTP Request Method {'POST'|'PATCH'|'DELETE'|'OPTIONS'}.
 */
function assignment(method) { //jshint ignore:line
	'use strict';

	var username = $('#assignmentUsername').val();
	var password = $('#assignmentPassword').val();
	var secret = $('#assignmentSecret').val();

	var testAssignmentId = parseInt($('#assignmentTestAssignmentId').val(), 10);
	var adpTestAssignmentId = parseInt($('#assignmentAdpTestAssignmentId').val(), 10);
	var testId = parseInt($('#assignmentTestId').val(), 10);
	var testFormId = parseInt($('#assignmentTestFormId').val(), 10);
	var testKey = $('#assignmentTestKey').val();
	var testStatus = $('#assignmentTestStatus').val();
	var enableLineReader = parseBool($('#assignmentEnableLineReader').val());
	var enableTextToSpeech = parseBool($('#assignmentEnableTextToSpeech').val());
	var studentId = parseInt($('#assignmentStudentId').val(), 10);
	var personalId = $('#assignmentPersonalId').val();
	var firstName = $('#assignmentFirstName').val();
	var lastName = $('#assignmentLastName').val();
	var dateOfBirth = $('#assignmentDateOfBirth').val();
	var state = $('#assignmentState').val();
	var schoolName = $('#assignmentSchoolName').val();
	var grade = $('#assignmentGrade').val();

	var apiURI = '/api/assignment' + (method === 'PATCH' || method === 'DELETE' ? ('/' + adpTestAssignmentId) : '');

	var timestamp = new Date().getTime() + timeOffset;
	var nonce = Math.floor(Math.random() * 99999);
	var hash = apiURI + Math.round(timestamp / 1000) + nonce;
	var message = $.sha256hmac(secret, hash);
	var authentication = window.btoa(timestamp + ':' + nonce + ':' + window.btoa(message));
	var data, testDetails, studentDetails, status;

	if (method === 'POST' && (!username || !password || !secret || !testAssignmentId || !testId || !testFormId ||
		!testKey || !studentId))
	{
		window.alert('Please enter all Test Assignment details!');
	} else if (method === 'PATCH' && (!username || !password || !secret || !testAssignmentId || !adpTestAssignmentId ||
		!studentId || (!testStatus && !testId && !testFormId && !testKey && !enableLineReader && !enableTextToSpeech &&
		!personalId && !firstName && !lastName && !dateOfBirth && !state && !schoolName && !grade)))
	{
		window.alert('Please enter all Test Assignment details!');
	} else if (method === 'DELETE' && (!username || !password || !secret || !testAssignmentId || !adpTestAssignmentId ||
		!studentId))
	{
		window.alert('Please enter all Test Assignment details!');
	}

	console.log('Timestamp: ' + timestamp);
	console.log('Time Offset: ' + timeOffset);
	console.log('Nonce: ' + nonce);
	console.log('Hash: ' + hash);
	console.log('Message: ' + message);
	console.log('Authentication: ' + authentication);

	if (method === 'POST' || method === 'PATCH') {
		studentDetails = {
			studentId: studentId
		};
		if (personalId) {
			studentDetails.personalId = personalId;
		}
		if (firstName) {
			studentDetails.firstName = firstName;
		}
		if (lastName) {
			studentDetails.lastName = lastName;
		}
		if (dateOfBirth) {
			studentDetails.dateOfBirth = dateOfBirth;
		}
		if (state) {
			studentDetails.state = state;
		}
		if (schoolName) {
			studentDetails.schoolName = schoolName;
		}
		if (grade) {
			studentDetails.grade = grade;
		}
	}
	if (method === 'POST') {
		testDetails = {
			testId: testId,
			testFormId: testFormId,
			testKey: testKey
		};
		if (enableLineReader !== null) {
			testDetails.enableLineReader = enableLineReader;
		}
		if (enableTextToSpeech !== null) {
			testDetails.enableTextToSpeech = enableTextToSpeech;
		}
		data = JSON.stringify({
			testAssignmentId: testAssignmentId,
			test: testDetails,
			student: studentDetails
		});
	} else if (method === 'PATCH') {
		if (testStatus) {
			status = testStatus;
		}
		if (testId || testFormId || testKey || enableLineReader || enableTextToSpeech) {
			testDetails = {};
			if (testId) {
				testDetails.testId = testId;
			}
			if (testFormId) {
				testDetails.testFormId = testFormId;
			}
			if (testKey) {
				testDetails.testKey = testKey;
			}
			if (enableLineReader !== null) {
				testDetails.enableLineReader = enableLineReader;
			}
			if (enableTextToSpeech !== null) {
				testDetails.enableTextToSpeech = enableTextToSpeech;
			}
		}
		data = JSON.stringify({
			testAssignmentId: testAssignmentId,
			testStatus: status,
			test: testDetails,
			student: studentDetails
		});
	} else if (method === 'DELETE') {
		data = JSON.stringify({
			testAssignmentId: testAssignmentId,
			test: {
				testId: testId,
				testFormId: testFormId,
				testKey: testKey
			},
			student: {
				studentId: studentId
			}
		});
	}

	$.ajax({
		url: apiHost + apiURI + xdebug,
		type: method,
		headers: {
			'Content-Type': 'application/json; charset=UTF-8',
			//'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			'Authorization': 'Basic ' + window.btoa(username + ':' + password),
			'Authentication': authentication
		},
		data: data
	}).always(function(data, textStatus, jqXHR) {
		/**
		 * @param {object} data.responseJSON JSON Response in case of Success.
		 * @param {object} data.responseText JSON Response in case of Error.
		 */
		var json = '[EMPTY]';
		var responseType, headers;
		reset();
		if (textStatus === 'success') {
			responseType = jqXHR.getResponseHeader('Content-Type');
			if (data !== '') {
				json = data;
			}
		} else if (data !== undefined) {
			responseType = data.getResponseHeader('Content-Type');
			if (data.responseJSON !== undefined && data.responseJSON !== '') {
				json = data.responseJSON;
			} else if (data.responseText !== undefined && data.responseText !== '') {
				json = data.responseText;
			}
		}
		if (responseType === null) {
			$('#responseBody').html('[ERROR]');
		} else if (responseType.indexOf('text/html') === 0 && json.indexOf('<html') > -1) {
			iframeHTML(json);
		} else {
			json = prettyPrintJSON(json);
			$('#responseBody').html(json);
		}

		try {
			headers = jqXHR.getAllResponseHeaders();
		} catch (e) {
			// In case of Error.
			headers = data.getAllResponseHeaders();
		}
		if (!headers) {
			headers = '[ERROR]';
		}
		$('#responseHeaders').html(prettyPrintHeaders(headers));
	});
}


/**
 * Results Details.
 *
 * @param {string} method HTTP Request Method {'GET'|'OPTIONS'}.
 */
function resultsDetails(method) { //jshint ignore:line
	'use strict';

	var username = $('#resultsUsername').val();
	var password = $('#resultsPassword').val();
	var secret = $('#resultsSecret').val();
	var resultsAdpTestAssignmentId = parseInt($('#resultsAdpTestAssignmentId').val(), 10);

	var apiURI = '/api/results/' + resultsAdpTestAssignmentId;

	var timestamp = new Date().getTime() + timeOffset;
	var nonce = Math.floor(Math.random() * 99999);
	var hash = apiURI + Math.round(timestamp / 1000) + nonce;
	var message = $.sha256hmac(secret, hash);
	var authentication = window.btoa(timestamp + ':' + nonce + ':' + window.btoa(message));

	if (!username || !password || !secret || !resultsAdpTestAssignmentId) {
		window.alert('Please enter all Test Assignment details!');
	}

	console.log('Timestamp: ' + timestamp);
	console.log('Time Offset: ' + timeOffset);
	console.log('Nonce: ' + nonce);
	console.log('Hash: ' + hash);
	console.log('Message: ' + message);
	console.log('Authentication: ' + authentication);

	$.ajax({
		url: apiHost + apiURI + xdebug,
		type: method,
		headers: {
			'Content-Type': 'application/json; charset=UTF-8',
			//'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			'Authorization': 'Basic ' + window.btoa(username + ':' + password),
			'Authentication': authentication
		}
	}).always(function(data, textStatus, jqXHR) {
		/**
		 * @param {object} data.responseJSON JSON Response in case of Success.
		 * @param {object} data.responseText JSON Response in case of Error.
		 */
		var json = '[EMPTY]';
		var responseType, headers;
		reset();
		if (textStatus === 'success') {
			responseType = jqXHR.getResponseHeader('Content-Type');
			if (data !== '') {
				json = data;
			}
		} else if (data !== undefined) {
			responseType = data.getResponseHeader('Content-Type');
			if (data.responseJSON !== undefined && data.responseJSON !== '') {
				json = data.responseJSON;
			} else if (data.responseText !== undefined && data.responseText !== '') {
				json = data.responseText;
			}
		}
		if (responseType === null) {
			$('#responseBody').html('[ERROR]');
		} else if (responseType.indexOf('text/html') === 0 && json.indexOf('<html') > -1) {
			iframeHTML(json);
		} else {
			json = prettyPrintJSON(json);
			$('#responseBody').html(json);
		}

		try {
			headers = jqXHR.getAllResponseHeaders();
		} catch (e) {
			// In case of Error.
			headers = data.getAllResponseHeaders();
		}
		if (!headers) {
			headers = '[ERROR]';
		}
		$('#responseHeaders').html(prettyPrintHeaders(headers));
	});
}
