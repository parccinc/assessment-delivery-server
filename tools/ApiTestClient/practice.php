<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * API Test Client is used for testing web services and should be disabled in Production!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */


?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="author" content="Bojan Vulevic; Breakthrough Technologies, LLC © 2015"/>
<title>ADP Practice Test Launch</title>
<link rel="icon" type="image/x-icon" href="test/favicon.ico"/>
<style type="text/css"><!--
body {font-family:Helvetica,Arial,sans-serif;font-size:13px}
fieldset {margin-bottom:5px;padding:0;border:1px solid #ccc;border-radius:5px}
legend {margin:5px;padding:0 3px;color:#c00;font-weight:700}
div {margin:0 10px}
label, b {width:170px;float:left;position:relative;top:2px;font-size:13px}
b {top:5px}
label, input, select {margin:0 10px 10px 0}
input, button, select {font-size:12px;line-height:12px;padding:3px;border:1px solid #ccc;border-radius:5px}
input:focus, select:focus, textarea:focus {padding:2px;background-color:#f2f8fb;border:2px solid #8bd;outline:0}
input {width:230px}
button {margin:0 0 10px 10px;padding:5px;color:#fff;background-color:#c00;outline:0;font-weight:700;cursor:pointer}
button:focus, button:hover {background-color:#900;outline:0}
label.required:after {color:#c00;content:'*'}
//--></style>
<script type="text/javascript"><!--
	function launch() {
		'use strict';

		var environment = document.getElementById('environment'), form = document.getElementById('form');
		// Switch Environment.
		form.action = environment.options[environment.selectedIndex].value;
		// Submit Form.
		form.submit();
	}

	function reset() {
		'use strict';

		// Reset Form Fields.
		document.getElementById('environment').selectedIndex = 0;
		document.getElementById('testKey').value = 'PRACTXXXXX';
		document.getElementById('name').value = '';
	}


	/**
	 * @param {object} event onKeyPress Event.
	 */
	function enter(event) {
		'use strict';

		// Check if Enter Key was pressed.
		if (event.which === 10 || event.which === 13) {
			// Stop further Event Propagation.
			event.preventDefault();
			// Launch Test.
			launch();
		}
	}
//--></script>
</head>
<body>
<fieldset>
	<legend>Assessment:</legend>
	<div>
		<form id="form" method="post" action="" onsubmit="return false">
			<label for="testKey" class="required">Test Key:</label>
			<input id="testKey" name="testKey" value="PRACTXXXXX" required onkeypress="enter(event)"/><br/>
			<label for="name" class="required">Name:</label>
			<input id="name" name="name" onkeypress="enter(event)"/><br/>
			<label for="environment">Test Status:</label>
			<select id="environment" onkeypress="enter(event)">
				<option value="/">Release</option>
				<option value="/assessment/release">Release Debug</option>
				<option value="/assessment/debug">Debug</option>
			</select><br/>
			<b>Assessment:</b>
			<button type="button" onclick="launch()">Launch Test</button>
			<button type="button" onclick="reset()">Clear</button>
		<form>
	</div>
</fieldset>
</body>
</html>
