<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Data Wipe Settings for particular environment.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 */
return [
	// Tables which should not be processed.
	'doNotTouchTables' => [
		'system',
		'user'
	],
	// IDs above which all Records in a corresponding Table should be deleted.
	'recordsToKeep' => [
		'student' => 0,
		'test' => 0,
		'test_content' => 0,
		'test_form' => 0,
		'test_form_revision' => 0,
		'test_results' => 0,
		'test_session' => 0
	],
	// Indicates if Irregular Files (files that don't comply to expected naming pattern) should be deleted from AWS S3.
	'keepIrregularFiles' => false
];
