<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Data Wipe is used to delete ALL Data from Database and AWS S3.
 * Based on its Configuration certain Records can be skipped and all related Objects within AWS S3.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 * @author: Kendall Parks <kendall.parks@breaktech.com>
 */


/**
 * Database Transaction Rollback.
 *
 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 *
 * @param  PDO $pdo
 */
function rollback($pdo)
{
	$error = $pdo->errorInfo();
	echo "Error Details: {$error[2]}" . PHP_EOL;
	echo "Rolling back transaction..." . PHP_EOL;

	if ($pdo->rollBack()) {
		echo "Transaction successfully rolled back." . PHP_EOL;
	} else {
		echo "Failed rolling back transaction!" . PHP_EOL;
	}

	exit();
}


/**
 * Calculates Memory Consumption.
 *
 * @author Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @param  bool   $realUsage Indicates if real size of memory allocated from the system should be return.
 * @return string
 */
function memoryPeakUsage($realUsage = false)
{
	$units = ['B', 'KB', 'MB', 'GB', 'TB'];
	$peakMemoryUsage = memory_get_peak_usage($realUsage);

	return '[Peak Memory Usage: ' .
	       round($peakMemoryUsage / pow(1024, ($i = floor(log($peakMemoryUsage, 1024)))), 2) . ' ' .
	       $units[(int) $i] . ']' . PHP_EOL;
}
