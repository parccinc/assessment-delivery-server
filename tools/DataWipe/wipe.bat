@echo off
cls
title PARCC-ADP Data Wipe
echo Deleting Data from Database and AWS S3...
echo.
php dataWipe.php
echo.
echo.
pause
