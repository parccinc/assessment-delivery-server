<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

// Report on ALL Errors!
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


use Phalcon\Config;
use Phalcon\Db\Adapter\Pdo\Mysql as MysqlConnection;
use Phalcon\Di\FactoryDefault as DefaultDI;

/**
 * Test Driver Configuration is used to change and import configuration for Test Driver.
 * It includes a list of Test Driver Configuration parameters which include:
 * @var array $review        Array of Test Review Configuration.
 * @var array $trackEvents   Array of Events which Test Driver should track and store their details within Test Results.
 * @var array $uploadResults Array of Events which trigger when Test Results should be uploaded to the server.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */

// Try to load Test Driver Configuration files.
if (file_exists('testDriverConfiguration.json')) {
	$testDriverConfiguration = file_get_contents('testDriverConfiguration.json');
} else {
	// In case Test Driver Configuration file is missing.
	exit("Test Driver Configuration ERROR:\nFile 'testDriverConfiguration.json' is missing!");
}
if (file_exists('testDriverConfigurationPractice.json')) {
	$testDriverConfigurationPractice = file_get_contents('testDriverConfigurationPractice.json');
} else {
	// In case Test Driver Configuration for Practice Test file is missing.
	exit("Test Driver Configuration ERROR:\nFile 'testDriverConfigurationPractice.json' is missing!");
}


// Check if Test Driver Configuration files were successfully open.
if ($testDriverConfiguration === false) {
	// In case Test Driver Configuration file failed to be opened.
	exit("Test Driver Configuration ERROR:\nFile 'testDriverConfiguration.json' cannot be opened!");
} elseif ($testDriverConfigurationPractice === false) {
	// In case Test Driver Configuration for Practice Test file failed to be opened.
	exit("Test Driver Configuration ERROR:\nFile 'testDriverConfigurationPractice.json' cannot be opened!");
}

// Try to parse loaded Test Driver Configuration files.
$testDriverConfiguration = json_decode($testDriverConfiguration, true);
$testDriverConfigurationPractice = json_decode($testDriverConfigurationPractice, true);

// Check if Test Driver Configuration files were successfully parsed.
if (!is_array($testDriverConfiguration)) {
	// In case Test Driver Configuration file was not successfully parsed.
	exit("Test Driver Configuration ERROR:\nFile 'testDriverConfiguration.json' is invalid!");
} elseif (!is_array($testDriverConfigurationPractice)) {
	// In case Test Driver Configuration for Practice Test file was not successfully parsed.
	exit("Test Driver Configuration ERROR:\nFile 'testDriverConfigurationPractice.json' is invalid!");
}


/**
 * @var DefaultDI $di Default Dependency Injector.
 */
$di = new DefaultDI();

/**
 * Returns Application Configuration.
 *
 * @return Config Application Configuration.
 */
$di->setShared('config', function () {
	// Retrieve Application configuration.
	return require_once dirname(dirname(__DIR__)) . '/app/config/config.php';
});


// Request User to enter Target Tenant ID.
echo "Please enter the Tenant ID you want Test Driver Configuration to import for:\n";
// Retrieve User Input.
$stdin = fopen("php://stdin", 'r');
// Trim EOL character from User's input.
$tenantId = trim(fgets($stdin), "\n\r");
fclose($stdin);
// Check if entered Tenant ID is valid.
if (!preg_match('/^\d+$/', $tenantId)) {
	// In case entered Tenant ID is not a valid integer.
	exit("You have entered an invalid Tenant ID: '{$tenantId}'");
}


// Try to import Test Driver Configuration into Database.
try {
	// Connect to Database.
	$db = new MysqlConnection((array) $di['config']->database);

	// Import Test Driver Configuration.
	$success = $db->execute(
		'REPLACE INTO system VALUES(?, ?, ?, UTC_TIMESTAMP(), UTC_TIMESTAMP())',
		[
			'testDriverConfiguration',
			(int) $tenantId,
			json_encode($testDriverConfiguration)
		]
	);

	// Check if import of Test Driver Configuration was successful.
	if ($success === true && $db->affectedRows() > 0) {
		// Import Test Driver Configuration for Practice Tests.
		$success = $db->execute(
			'REPLACE INTO system VALUES(?, ?, ?, UTC_TIMESTAMP(), UTC_TIMESTAMP())',
			[
				'testDriverConfigurationPractice',
				(int) $tenantId,
				json_encode($testDriverConfigurationPractice)
			]
		);
	}

	// Check if import of Test Driver Configuration was successful.
	if ($success === true && $db->affectedRows() > 0) {
		echo 'Test Driver Configuration was imported successfully.';
	} else {
		echo 'There was an ERROR while importing Test Driver Configuration!';
	}

	// Close Database Connection.
	$db->close();
	unset($db);

} catch (PDOException $exception) {
	// Check if PDOException is triggered from the client side (i.e. failed to establish Database Connection), OR it was
	// caused by an invalid SQL query.
	if ($exception->getCode() >= 2000 && $exception->getCode() <= 2061) {
		echo 'Failed to import Test Driver Configuration due to Database Connection ERROR!';
	} else {
		echo 'Failed to import Test Driver Configuration due to Database SQL ERROR!';
	}
}
