--
-- This file is part of ADP.
--
-- ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
--
-- ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with ADP. If not, see
-- <http://www.gnu.org/licenses/>.
--
-- Copyright � 2015 Breakthrough Technologies, LLC
--

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `student`
--
LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
UPDATE `student` SET `created`=IF(`created` IS NULL, '2015-11-11 23:00:00', `created`),`updated`=IF(`updated` IS NULL, NOW(), `updated`) WHERE `sid`=0;
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system`
--
LOCK TABLES `system` WRITE;
/*!40000 ALTER TABLE `system` DISABLE KEYS */;
REPLACE INTO `system` VALUES
('testDriverConfigurationPractice',1,'{\"debug\":false,\"trackEvents\":{\"navto\":true,\"init\":false,\"setstate\":false,\"render\":false,\"ready\":false,\"state\":false,\"response\":false,\"navfrom\":true},\"uploadResults\":{\"timeInterval\":0,\"navto\":false,\"navfrom\":false,\"state\":false,\"response\":false},\"enablePause\":false,\"enableSubmit\":false}',NULL,NULL),
('testDriverConfigurationPractice',2,'{\"debug\":false,\"trackEvents\":{\"navto\":true,\"init\":false,\"setstate\":false,\"render\":false,\"ready\":false,\"state\":false,\"response\":false,\"navfrom\":true},\"uploadResults\":{\"timeInterval\":0,\"navto\":false,\"navfrom\":false,\"state\":false,\"response\":false},\"enablePause\":false,\"enableSubmit\":false}',NULL,NULL),
('versionApi',0,'1.26.2',NULL,NULL),
('versionDb',0,'1.26.2',NULL,NULL);
UPDATE `system` SET `created`=IF(`created` IS NULL, NOW(), `created`),`updated`=IF(`updated` IS NULL, NOW(), `updated`) WHERE `fk_tenant_id`=0;
UPDATE `system` SET `created`=IF(`created` IS NULL, '2015-11-11 23:00:00', `created`),`updated`=IF(`updated` IS NULL, NOW(), `updated`) WHERE `fk_tenant_id`=1;
UPDATE `system` SET `created`=IF(`created` IS NULL, '2016-02-25 23:00:00', `created`),`updated`=IF(`updated` IS NULL, NOW(), `updated`) WHERE `fk_tenant_id`=2;
/*!40000 ALTER TABLE `system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tenant`
--
LOCK TABLES `tenant` WRITE;
/*!40000 ALTER TABLE `tenant` DISABLE KEYS */;
UPDATE `tenant` SET `created`=IF(`created` IS NULL, '2016-02-25 23:00:00', `created`),`updated`=IF(`updated` IS NULL, NOW(), `updated`) WHERE `tid`=0;
UPDATE `tenant` SET `created`=IF(`created` IS NULL, '2016-02-25 23:00:00', `created`),`updated`=IF(`updated` IS NULL, NOW(), `updated`) WHERE `tid`=1;
UPDATE `tenant` SET `created`=IF(`created` IS NULL, '2016-02-25 23:00:00', `created`),`updated`=IF(`updated` IS NULL, NOW(), `updated`) WHERE `tid`=2;
/*!40000 ALTER TABLE `tenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_session`
--
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
ALTER TABLE `test_session` DROP INDEX `test_key`, ADD CONSTRAINT `test_key` UNIQUE (`test_key`,`fk_tenant_id`);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--
LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
UPDATE `user` SET `created`=IF(`created` IS NULL, '2015-11-11 23:00:00', `created`),`updated`=IF(`updated` IS NULL, NOW(), `updated`) WHERE `fk_tenant_id`=0;
UPDATE `user` SET `created`=IF(`created` IS NULL, '2015-11-11 23:00:00', `created`),`updated`=IF(`updated` IS NULL, NOW(), `updated`) WHERE `fk_tenant_id`=1;
UPDATE `user` SET `created`=IF(`created` IS NULL, '2016-02-25 23:00:00', `created`),`updated`=IF(`updated` IS NULL, NOW(), `updated`) WHERE `fk_tenant_id`=2;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
